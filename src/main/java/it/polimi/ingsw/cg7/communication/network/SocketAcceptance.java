package it.polimi.ingsw.cg7.communication.network;

import it.polimi.ingsw.cg7.communication.handlers.SocketRequestHandler;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

//Questa classe serve per accettare i socket e rimane sempre in ascolto

/**
 * The Class SocketAcceptance this class needed to accept sockets and stay always up..
 */
public class SocketAcceptance implements Runnable {

    /** The server socket. */
    private ServerSocket serverSocket;

    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
        startSocketServer();
    }

    /**
     * Start socket server.
     */
    public void startSocketServer() {
        try {

            new Thread(new PubSocketAcceptance()).start();
            serverSocket = new ServerSocket(60000);
            Socket socket;
            System.out.println("Socket listener Ready");
            SocketRequestHandler sk;
            while (!Thread.interrupted()) {
                socket = serverSocket.accept();
                sk = new SocketRequestHandler(socket);
                System.out.println("Un client socket si è connesso");
                Host.executor.execute(sk);

                // new ClientHandler(new SocketCommunicator(socket)).start();
            }
        } catch (IOException e) {
            System.err.println("Cannot connect!");
        }
    }
}
