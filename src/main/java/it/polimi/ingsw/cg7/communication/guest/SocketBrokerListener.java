package it.polimi.ingsw.cg7.communication.guest;

import it.polimi.ingsw.cg7.Client.controller.SubInterface;
import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;

import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * The listener interface for receiving socketBroker events. The class that is
 * interested in processing a socketBroker event implements this interface, and
 * the object created with that class is registered with a component using the
 * component's <code>addSocketBrokerListener<code> method. When
 * the socketBroker event occurs, that object's appropriate
 * method is invoked.
 *
 * @see SocketBrokerEvent
 */
public class SocketBrokerListener implements Runnable {

    /** The in. */
    private ObjectInputStream in;

    private SubInterface subinterface;

    /**
     * Instantiates a new socket broker listener.
     *
     * @param in
     *            the in
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public SocketBrokerListener(ObjectInputStream in, SubInterface sub)
            throws IOException {
        this.in = in;
        this.subinterface = sub;
    }

    /**
     * run method
     */
    @Override
    public void run() {
        while (!Thread.interrupted()) {
            try {
                NetBrokerMessage msg = (NetBrokerMessage) in.readObject();
                processResponse(msg);
                // perform something like printing the map... or someone else
            } catch (ClassNotFoundException | IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    /**
     * Process response.
     *
     * @param msg
     *            the msg
     */
    public void processResponse(NetBrokerMessage msg) {
        this.subinterface.processResponse(msg);
    }
}
