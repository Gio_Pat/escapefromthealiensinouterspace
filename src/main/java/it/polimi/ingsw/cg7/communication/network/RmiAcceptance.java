package it.polimi.ingsw.cg7.communication.network;

import it.polimi.ingsw.cg7.communication.guest.IRmiSubscriber;
import it.polimi.ingsw.cg7.communication.handlers.RmiRequestHandler;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;


//questa classe è in grado di tenere in ascolto e di accettare una richieta RMI verrà avviata da un server.!!!

/**
 * The Class RmiAcceptance this class is able to accept a request RMI will be started by a server.
 */
public class RmiAcceptance implements Runnable, IRmiServer {
    
    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
        try {
            startRmiServer();
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Start rmi server.
     *
     * @throws RemoteException the remote exception
     */
    public void startRmiServer() throws RemoteException {

        Registry reg = LocateRegistry.createRegistry(9090);
        IRmiServer stub = (IRmiServer) UnicastRemoteObject
                .exportObject(this, 9090);
        reg.rebind("Server", stub);
        System.out.println("Rmi listener Ready");
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.communication.network.IRmiServer#connect(it.polimi.ingsw.cg7.communication.guest.IRmiSubscriber)
     */
    public IRmiRequestHandler connect(IRmiSubscriber stubClient)
            throws RemoteException {
        RmiRequestHandler rmiReqHandler = new RmiRequestHandler(stubClient);
        System.out.println("Un client Rmi si è connesso");
        return (IRmiRequestHandler) UnicastRemoteObject.exportObject(
                rmiReqHandler, 0);
    }
}
