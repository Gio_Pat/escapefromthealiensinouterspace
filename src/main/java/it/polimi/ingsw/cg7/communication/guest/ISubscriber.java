package it.polimi.ingsw.cg7.communication.guest;

import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * The Interface ISubscriber.
 */
public interface ISubscriber extends Remote {
    
    /**
     * Send.
     *
     * @param msg the msg
     * @throws RemoteException the remote exception
     */
    public void send(NetBrokerMessage msg) throws RemoteException;
}
