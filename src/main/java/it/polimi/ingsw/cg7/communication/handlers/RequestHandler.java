package it.polimi.ingsw.cg7.communication.handlers;

import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;
import it.polimi.ingsw.cg7.Client.model.NetRequestMessage;
import it.polimi.ingsw.cg7.Client.model.NetResponseMessage;
import it.polimi.ingsw.cg7.Client.model.NetTypeBroker;
import it.polimi.ingsw.cg7.Client.model.NetTypeRequest;
import it.polimi.ingsw.cg7.Client.model.ViewCard;
import it.polimi.ingsw.cg7.Client.model.ViewMap;
import it.polimi.ingsw.cg7.Client.model.ViewPlayer;
import it.polimi.ingsw.cg7.Client.model.ViewPosition;
import it.polimi.ingsw.cg7.controller.LobbyManager;
import it.polimi.ingsw.cg7.controller.MoveValidation;
import it.polimi.ingsw.cg7.model.Adrenaline;
import it.polimi.ingsw.cg7.model.Attack;
import it.polimi.ingsw.cg7.model.Card;
import it.polimi.ingsw.cg7.model.CharType;
import it.polimi.ingsw.cg7.model.Coordinate;
import it.polimi.ingsw.cg7.model.Deck;
import it.polimi.ingsw.cg7.model.Defense;
import it.polimi.ingsw.cg7.model.Game;
import it.polimi.ingsw.cg7.model.Item;
import it.polimi.ingsw.cg7.model.Message;
import it.polimi.ingsw.cg7.model.Player;
import it.polimi.ingsw.cg7.model.PlayingState;
import it.polimi.ingsw.cg7.model.Sector;
import it.polimi.ingsw.cg7.model.Sedatives;
import it.polimi.ingsw.cg7.model.Spotlight;
import it.polimi.ingsw.cg7.model.Teleport;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

/**
 * The Class RequestHandler this class in very high level handles the requests.
 *
 * @author Giovanni Patruno
 */
public abstract class RequestHandler {


    /**
     * Gets the subscriber.
     *
     * @return the subscriber
     */
    public abstract SubHandler getSubscriber();

    /**
     * 
     */

    public NetResponseMessage processRequest(NetRequestMessage netMessage) {
        Player requestingPlayer = LobbyManager.getPlayer(netMessage
                .getPlayerToken());
        Game playingGame = LobbyManager.getPlayerGame(requestingPlayer);
        MoveValidation move = new MoveValidation(requestingPlayer, playingGame);
        if (netMessage.getType() == NetTypeRequest.FINISHGAME)
            if (playingGame.isFinished()) {
                NetResponseMessage r = new NetResponseMessage(
                        NetTypeRequest.FINISHGAME);
                List<ViewPlayer> players = new ArrayList<ViewPlayer>();
                ViewPlayer player;
                for (Player p : playingGame.getPlayers()) {
                    player = new ViewPlayer(p.getUsername());
                    player.setKillNumber(p.getKillNumber());
                    player.setType(p.getType());
                    player.setPlayingState(p.getState());
                    players.add(player);
                }
                r.responseInfoFinishGame(players);
                return r;
            }
        if (netMessage.getType() == NetTypeRequest.SUBSCRIBING)
            return LobbyManager.subscribe(this, netMessage);

        if (netMessage.getType() == NetTypeRequest.MESSAGE) {
            NetResponseMessage msg = new NetResponseMessage(
                    NetTypeRequest.MESSAGE);
            playingGame.getChat().setMessage(
                    playingGame,
                    new Message(requestingPlayer, netMessage.getMessage()
                            .getMessage(), new Date()));
            msg.sendMessageRequest("Messaggio inivato", true);
            return msg;
        }

        if (netMessage.getType() == NetTypeRequest.MAPS) {
            List<ViewMap> maps = new ArrayList<ViewMap>();
            maps.add(new ViewMap("GALILEI"));
            maps.add(new ViewMap("FERMI"));
            maps.add(new ViewMap("GALVANI"));
            NetResponseMessage res = new NetResponseMessage(NetTypeRequest.MAPS);
            res.requestMaps(maps, "List of possible Map", true);
            return res;
        }

        if (netMessage.getType() == NetTypeRequest.CLIENT) {
            NetResponseMessage r = new NetResponseMessage(NetTypeRequest.CLIENT);
            ViewPlayer player = new ViewPlayer(requestingPlayer.getUsername());
            player.setCards(requestingPlayer.getDeck().getCount());
            player.setKillNumber(requestingPlayer.getKillNumber());
            player.setPlayingState(requestingPlayer.getState());
            player.setType(requestingPlayer.getType());
            player.setDoneMovement(playingGame.getCurrentTurn()
                    .isFirstMovement());
            player.setDoneAttackCard(playingGame.getCurrentTurn().isAttack());
            r.requestClient(player);
            return r;
        }

        if (requestingPlayer.getState() == PlayingState.PLAYING) {

            switch (netMessage.getType()) {
            case DECK: {
                NetResponseMessage r = new NetResponseMessage(
                        NetTypeRequest.DECK);
                List<ViewCard> cards = new ArrayList<ViewCard>();
                Deck deck = requestingPlayer.getDeck();
                for (Card c : deck.getCards())
                    cards.add(new ViewCard(c.getDescription()));
                r.responseDeck(cards);
                return r;
            }
            case PLAYERS: {
                NetResponseMessage r = new NetResponseMessage(
                        NetTypeRequest.PLAYERS);
                List<ViewPlayer> players = new ArrayList<ViewPlayer>();
                ViewPlayer temp;
                for (Player p : playingGame.getPlayers()) {
                    temp = new ViewPlayer(p.getUsername());
                    temp.setKillNumber(p.getKillNumber());
                    temp.setCards(p.getDeck().getCount());
                    temp.setPlayingState(p.getState());
                    players.add(temp);
                }
                r.requestPlayers(players);
                return r;
            }

            case POSITION: {
                NetResponseMessage r = new NetResponseMessage(
                        NetTypeRequest.POSITION);
                Coordinate c = requestingPlayer.getPosition().getCoordinates();
                r.requestPosition(new ViewPosition(c.getxLett(), c.getY()));
                return r;
            }
            case NEARSECTOR: {

                Sector sector = playingGame.getMap().getSector(
                        requestingPlayer.getPosition().getCoordinates());
                if (sector != null) {
                    HashSet<Sector> movementSector = new HashSet<Sector>();
                    int iterator = 1;
                    if (requestingPlayer.ifBuffed())
                        iterator++;
                    if (requestingPlayer.getType() == CharType.ALIEN)
                        iterator++;
                    searchNearSectors(iterator, sector, movementSector,
                            requestingPlayer);
                    if (movementSector.contains(requestingPlayer.getPosition()))
                        movementSector.remove(requestingPlayer.getPosition());
                    List<ViewPosition> positions = new ArrayList<ViewPosition>();
                    for (Sector s : movementSector)
                        positions.add(new ViewPosition(s.getCoordinates()
                                .getxLett(), s.getCoordinates().getY()));
                    NetResponseMessage r = new NetResponseMessage(
                            NetTypeRequest.NEARSECTOR);
                    r.requestNearSector(positions);
                    return r;

                }
                return null;
            }
            default:
                break;
            }
            if (requestingPlayer.getUsername().equals(
                    playingGame.getCurrentTurn().getCurrentPlayer()
                            .getUsername())) {
                switch (netMessage.getType()) {
                case MOVEMENT: {
                    return move.movementRequest(new Coordinate(netMessage
                            .getPosition().getX(), netMessage.getPosition()
                            .getY()));
                }
                case DRAW: {
                    NetResponseMessage r = move.drawCardSector();
                    NetBrokerMessage msg = new NetBrokerMessage(
                            NetTypeBroker.PLAYERDRAW);
                    msg.playerDraw(new ViewPlayer(requestingPlayer
                            .getUsername()));
                    LobbyManager.notifyLobby(playingGame, msg);
                    if (r.isDoneMove()) {
                        playingGame.getCurrentTurn().setAttack();
                    }
                    return r;
                }
                case USEITEM: {
                    Coordinate coord;
                    if (netMessage.getPosition() == null)
                        coord = null;
                    else
                        coord = new Coordinate(netMessage.getPosition().getX(),
                                netMessage.getPosition().getY());
                    Item card = createItem(netMessage.getItem());
                    return move.useItemRequest(coord, card);
                }
                case ATTACK: {
                    return move.attackRequest();
                }
                case FINISHTURN: {
                    return move.finishTurn();
                }
                case SENDFALSERUMORS: {

                    return move.rumors(searchSectorinGame(new Coordinate(
                            netMessage.getPosition().getX(), netMessage
                                    .getPosition().getY()), playingGame));
                }
                case DISCARD: {

                    Item card = createItem(netMessage.getItem());
                    return move.discard(card);
                }
                default:
                    break;

                }
            }
            NetResponseMessage msg = new NetResponseMessage(NetTypeRequest.TURN);
            msg.responseTurn("it's not your turn", false);
            return msg;
        } else {
            NetResponseMessage msg = new NetResponseMessage(NetTypeRequest.TURN);
            msg.responseTurn("You're not playing anymore", false);
            return msg;
        }

    }

    private void searchNearSectors(int iterator, Sector sector,
            HashSet<Sector> movementSector, Player player) {
        movementSector.addAll(sector.getNearSector());
        iterator--;
        HashSet<Sector> impossibleSectors = new HashSet<Sector>();
        for (Sector s : movementSector)
            if (!s.validationMove(player))
                impossibleSectors.add(s);
        for (Sector s : impossibleSectors)
            if (movementSector.contains(s))
                movementSector.remove(s);
        if (iterator != 0)
            for (Sector sectorSearch : sector.getNearSector())
                if (sectorSearch.validationMove(player))
                    searchNearSectors(iterator, sectorSearch, movementSector,
                            player);
    }

    private Item createItem(ViewCard item) {

        Item card;
        switch (item.getName()) {
        case "Adrenaline":
            card = new Adrenaline();
            break;
        case "Spotlight":
            card = new Spotlight();
            break;
        case "Teleport":
            card = new Teleport();
            break;
        case "Sedatives":
            card = new Sedatives();
            break;
        case "Defense":
            card = new Defense();
            break;
        case "Attack":
            card = new Attack();
            break;
        default:
            card = null;
        }
        return card;
    }

    private Sector searchSectorinGame(Coordinate coord, Game game) {
        return game.getMap().getSector(coord);
    }

}
