package it.polimi.ingsw.cg7.communication.handlers;

import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;
import it.polimi.ingsw.cg7.model.Game;

import java.io.IOException;
import java.net.SocketException;
import java.rmi.RemoteException;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The Class Subscriber. This publisher will be only the LobbyManager
 * 
 * @author Giovanni Patruno
 */
public abstract class SubHandler {


    /** The sub token. */
    @SuppressWarnings("unused")
    private UUID subToken;

    /** The subscribed topic topic. */
    @SuppressWarnings("unused")
    private Game topic;
    
    private boolean loggedIn;
    /**
     * Send.
     *
     * @param msg
     *            the msg
     * @throws RemoteException
     * @throws SocketException 
     * @throws IOException 
     */
    public abstract void send(NetBrokerMessage message) throws IOException;

    /** The users. this static map is here because of clean */
    public static Map<UUID, SubHandler> users = new ConcurrentHashMap<UUID, SubHandler>();

    /**
     * Instantiates a new subscriber.
     *
     * @param token
     *            the token
     */
    protected SubHandler(UUID token) {
        this.subToken = token;
        this.loggedIn = true;
    }
    /**
     * Disconnect the subscriber
     */
    public void logOut()
    {
        this.loggedIn = false;
    }
    /**
     * Return true if the player is connected yet
     */
    public boolean isLoggedIn()
    {
        return this.loggedIn;
    }
    // IL DISPATCH SARà IMPLEMENTATO NEL BROKER CON QUALCHE CRITERIO
}
