package it.polimi.ingsw.cg7.communication.handlers;

import it.polimi.ingsw.cg7.communication.guest.IRmiSubscriber;
import it.polimi.ingsw.cg7.communication.network.IRmiRequestHandler;

/**
 * The Class RmiRequestHandler.
 *
 * @author Giovanni Patruno
 */
public class RmiRequestHandler extends RequestHandler implements
        IRmiRequestHandler {

    /** The stub client. */
    private IRmiSubscriber stubClient;

    public IRmiSubscriber getStubClient() {
        return stubClient;
    }

    public void setStubClient(IRmiSubscriber stubClient) {
        this.stubClient = stubClient;
    }

    /**
     * Instantiates a new rmi request handler.
     *
     * @param stubClient
     *            the stub client
     */
    public RmiRequestHandler(IRmiSubscriber stubClient) {
        this.stubClient = stubClient;
    }

    /*
     * Retrieve the right subscriber
     */
    @Override
    public SubHandler getSubscriber() {
        return new RmiSubHandler(stubClient);
    }

}
