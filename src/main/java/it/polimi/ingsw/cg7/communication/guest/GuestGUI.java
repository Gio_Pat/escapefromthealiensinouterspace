package it.polimi.ingsw.cg7.communication.guest;

import it.polimi.ingsw.cg7.Client.controller.InterfaceControl;
import it.polimi.ingsw.cg7.Client.controller.SubInterface;
import it.polimi.ingsw.cg7.Client.view.gui.ChoosingMapMenu;
import it.polimi.ingsw.cg7.Client.view.gui.ChoosingNickMenu;
import it.polimi.ingsw.cg7.Client.view.gui.GameContainerGui;
import it.polimi.ingsw.cg7.Client.view.gui.LoadingGui;
import it.polimi.ingsw.cg7.Client.view.gui.MainMenu;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.util.UUID;

import javax.swing.SwingUtilities;

/**
 * The Class GuestGUI.
 */
public class GuestGUI {

    /** The connection. */
    private MainMenu connection;
    
    /** The game view. */
    private GameContainerGui gameView;
    
    /** The map. */
    private ChoosingMapMenu map;
    
    /** The nick. */
    private ChoosingNickMenu nick;
    
    /** The loading. */
    private LoadingGui loading;
    
    /** The interface control. */
    private InterfaceControl interfaceControl;

    /**
     * Instantiates a new guest gui.
     *
     * @param interfaceControl the interface control
     * @param subinterface the subinterface
     * @param token the token
     * @throws InterruptedException the interrupted exception
     * @throws InvocationTargetException the invocation target exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public GuestGUI(InterfaceControl interfaceControl,
            SubInterface subinterface, UUID token) throws InterruptedException,
            InvocationTargetException, IOException {
        connection = new MainMenu();
        this.interfaceControl = interfaceControl;
        SwingUtilities.invokeAndWait(new Runnable() {

            @Override
            public void run() {

                connection.chooseConnection(GuestGUI.this.interfaceControl);
            }
        });

        synchronized (connection) {
            connection.wait();
        }

        map = new ChoosingMapMenu();

        SwingUtilities.invokeAndWait(new Runnable() {

            @Override
            public void run() {
                map.chooseMap(GuestGUI.this.interfaceControl);
            }
        });

        synchronized (map) {
            map.wait();
        }

        nick = new ChoosingNickMenu();
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                nick.chooseUsername(GuestGUI.this.interfaceControl);
            }
        });

        synchronized (nick) {
            nick.wait();
        }

        loading = new LoadingGui();

        gameView = new GameContainerGui(interfaceControl);
        gameView.setSubInterface(subinterface);

        token = interfaceControl.requestSubscribing();

        if (token != null)
            try {
                FileOutputStream tokenFile = new FileOutputStream(
                        interfaceControl.getUsername() + "myTokenEftAioS.tpe");
                PrintStream write = new PrintStream(tokenFile);
                write.println(token.toString());
                write.close();
            } catch (IOException e) {
                System.out.println("Errore: " + e);
                System.exit(1);
            }

        subinterface.setGameView(gameView);
        interfaceControl.setSubInterface(subinterface);

        while (!subinterface.isStarted())
            Thread.sleep(50);
        ;

        while (!subinterface.charTypeisSetting()) {
            Thread.sleep(50);
        }

        loading.close();

        gameView.update();
        gameView.setType();

    }
}
