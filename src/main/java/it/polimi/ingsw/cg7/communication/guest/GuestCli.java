package it.polimi.ingsw.cg7.communication.guest;

import it.polimi.ingsw.cg7.Client.controller.InterfaceControl;
import it.polimi.ingsw.cg7.Client.controller.SubInterface;
import it.polimi.ingsw.cg7.Client.view.cli.ChooseConnectionViewCli;
import it.polimi.ingsw.cg7.Client.view.cli.ChooseMapViewCli;
import it.polimi.ingsw.cg7.Client.view.cli.ChooseNickViewCli;
import it.polimi.ingsw.cg7.Client.view.cli.GameContainerCli;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The Class GuestCli.
 */
public class GuestCli {

    /** The game view. */
    private GameContainerCli gameView;

    /**
     * Instantiates a new guest cli.
     *
     * @param interfaceControl the interface control
     * @param subinterface the subinterface
     * @param token the token
     * @throws InterruptedException the interrupted exception
     */
    public GuestCli(InterfaceControl interfaceControl,
            SubInterface subinterface, UUID token) throws InterruptedException {
        ChooseConnectionViewCli connection = new ChooseConnectionViewCli();
        connection.chooseConnection(interfaceControl);
        ChooseMapViewCli map = new ChooseMapViewCli();
        map.chooseMap(interfaceControl);
        ChooseNickViewCli nick = new ChooseNickViewCli();
        nick.chooseUsername(interfaceControl);
        gameView = new GameContainerCli();
        gameView.setInterfaceControl(interfaceControl);
        gameView.setSubInterface(subinterface);

        token = interfaceControl.requestSubscribing();

        if (token != null)
            try {
                FileOutputStream tokenFile = new FileOutputStream(
                        interfaceControl.getUsername() + "myTokenEftAioS.tpe");
                PrintStream write = new PrintStream(tokenFile);
                write.println(token.toString());
                write.close();
            } catch (IOException e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                System.out.println("Errore: " + e);
                System.exit(1);
            }

        subinterface.setGameView(gameView);
        interfaceControl.setSubInterface(subinterface);

        gameView.print("Good game! Waiting..");
        printLogoEftaios();

        while (!subinterface.isStarted())
            Thread.sleep(50);
        ;

        while (!subinterface.charTypeisSetting()) {
            Thread.sleep(50);
        }
        while (!subinterface.isFinishGame()) {

            Thread.sleep(50);

            while (subinterface.isYourTurn() && !subinterface.isFinishGame()) {

                gameView.updateDeck(interfaceControl.requestDeck());
                gameView.updateplayers(interfaceControl.requestPlayers());
                gameView.updateClient(interfaceControl.requestClient());
                gameView.updatePosition(interfaceControl.requestPosition());
                gameView.updatePositions(interfaceControl
                        .requestPossiblePosition());
                gameView.showCurrentSituation();
                if (!subinterface.isFinishGame())
                    gameView.showMenu();

            }
        }

    }

    /**
     * Prints the logo eftaios.
     */
    private void printLogoEftaios() {
        System.out
                .println("███████╗███████╗████████╗ █████╗ ██╗ ██████╗ ███████╗");
        System.out
                .println("██╔════╝██╔════╝╚══██╔══╝██╔══██╗██║██╔═══██╗██╔════╝");
        System.out
                .println("█████╗  █████╗     ██║   ███████║██║██║   ██║███████╗");
        System.out
                .println("██╔══╝  ██╔══╝     ██║   ██╔══██║██║██║   ██║╚════██║");
        System.out
                .println("███████╗██║        ██║   ██║  ██║██║╚██████╔╝███████║");
        System.out
                .println("╚══════╝╚═╝        ╚═╝   ╚═╝  ╚═╝╚═╝ ╚═════╝ ╚══════╝");
        System.out.println("");

    }
}
