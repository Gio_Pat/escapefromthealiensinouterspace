package it.polimi.ingsw.cg7.communication.network;

import it.polimi.ingsw.cg7.communication.guest.IRmiSubscriber;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * The Interface IRmiServer.
 */
public interface IRmiServer extends Remote {
    
    /**
     * Connect to the RMI susbscriber.
     *
     * @param stubClient the stub client
     * @return the i rmi request handler
     * @throws RemoteException the remote exception
     */
    public IRmiRequestHandler connect(IRmiSubscriber stubClient)
            throws RemoteException;
}
