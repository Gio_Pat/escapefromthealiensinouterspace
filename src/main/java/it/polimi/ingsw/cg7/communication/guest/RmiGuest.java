package it.polimi.ingsw.cg7.communication.guest;

import it.polimi.ingsw.cg7.Client.controller.SubInterface;
import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;
import it.polimi.ingsw.cg7.Client.model.NetRequestMessage;
import it.polimi.ingsw.cg7.Client.model.NetResponseMessage;
import it.polimi.ingsw.cg7.communication.network.IRmiRequestHandler;
import it.polimi.ingsw.cg7.communication.network.IRmiServer;

import java.io.Serializable;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * this class represent the entry point of a Client to the Application.
 *
 * @author Giovanni Patruno
 */
public class RmiGuest implements IRmiSubscriber, IClient, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /** The rh. */
    private IRmiRequestHandler rh;
    private SubInterface subinterface;

    /**
     * Gets the rh.
     *
     * @return the rh
     */
    public IRmiRequestHandler getRh() {
        return rh;
    }

    /**
     * Instantiates a new rmi client.
     */

    public RmiGuest(SubInterface subinterface) {
        try {
            Registry reg = LocateRegistry.getRegistry("127.0.0.1", 9090);
            this.subinterface = subinterface;
            IRmiServer stubServer = (IRmiServer) reg.lookup("Server");
            IRmiSubscriber r = (IRmiSubscriber) UnicastRemoteObject
                    .exportObject(this, 0);
            rh = stubServer.connect(r);
        } catch (RemoteException | NotBoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Metodo che processa la risposta
     */
    @Override
    public void processResponse(NetBrokerMessage msg) throws RemoteException {
        this.subinterface.processResponse(msg);
    }

    @Override
    public NetResponseMessage sendRequest(NetRequestMessage request)
            throws RemoteException {
        return rh.processRequest(request);
    }

}
