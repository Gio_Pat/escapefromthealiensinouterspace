package it.polimi.ingsw.cg7.communication.network;

import it.polimi.ingsw.cg7.Client.model.NetRequestMessage;
import it.polimi.ingsw.cg7.Client.model.NetResponseMessage;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * The Interface IRmiRequestHandler.
 */
public interface IRmiRequestHandler extends Remote {

    /**
     * Process request.
     *
     * @param request the request
     * @return the net response message
     * @throws RemoteException the remote exception
     */
    public NetResponseMessage processRequest(NetRequestMessage request)
            throws RemoteException;

}
