package it.polimi.ingsw.cg7.communication.guest;

import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * The Interface IRmiSubscriber to invoke methods on the subscriber.
 */
public interface IRmiSubscriber extends Remote {
    
    /**
     * Process response.
     *
     * @param msg the msg
     * @throws RemoteException the remote exception
     */
    public void processResponse(NetBrokerMessage msg) throws RemoteException;
}
