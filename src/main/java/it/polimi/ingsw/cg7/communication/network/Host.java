package it.polimi.ingsw.cg7.communication.network;

import java.rmi.RemoteException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * this class represent the start point of the server that runs the two Rmi and
 * Socket Acceptance
 * 
 * @author Giovanni Patruno
 *
 */
public class Host {
    public static ExecutorService executor = Executors.newCachedThreadPool();
    /**
     * Main method
     * @param args
     * @throws RemoteException
     */
    public static void main(String[] args) throws RemoteException {
        System.out.println("Host machine has started");
        Host.startHosts();
    }
    /**
     * Method that start two requests listeners
     */

    public static void startHosts() {
        RmiAcceptance rmiHost = new RmiAcceptance();
        SocketAcceptance socketHost = new SocketAcceptance();
        rmiHost.run();
        Host.executor.execute(socketHost);
    }
}
