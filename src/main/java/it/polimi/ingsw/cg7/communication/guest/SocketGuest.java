package it.polimi.ingsw.cg7.communication.guest;

import it.polimi.ingsw.cg7.Client.controller.SubInterface;
import it.polimi.ingsw.cg7.Client.model.NetRequestMessage;
import it.polimi.ingsw.cg7.Client.model.NetResponseMessage;
import it.polimi.ingsw.cg7.Client.model.NetTypeRequest;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The Class SocketClient. the entry point of an socket Client
 */
public class SocketGuest implements IClient, Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The Constant ip. */
    private final static String ip = "127.0.0.1";

    /** The Constant port. */
    private final static int port = 60000;

    /** The subinterface. */
    private SubInterface subinterface;


    /**
     * Instantiates socket client.
     *
     * @param subinterface the subinterface
     */

    public SocketGuest(SubInterface subinterface) {
        this.subinterface = subinterface;
    }
    
    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.communication.guest.IClient#sendRequest(it.polimi.ingsw.cg7.Client.model.NetRequestMessage)
     */
    @Override
    public NetResponseMessage sendRequest(NetRequestMessage req)
            throws IOException {
        ObjectInputStream tempIn;
        ObjectOutputStream tempOut;
        Socket tempSocket;
        if (req.getType() == NetTypeRequest.SUBSCRIBING) {
            SocketSubscriber ss = new SocketSubscriber(req);
            new Thread(new SocketBrokerListener(ss.getIn(), subinterface))
                    .start();
            return ss.getResTwo();
        } else {
            tempSocket = new Socket(ip, port);
            tempOut = new ObjectOutputStream(tempSocket.getOutputStream());
            tempIn = new ObjectInputStream(tempSocket.getInputStream());

            tempOut.writeObject(req);
            // tempOut.flush();
        }
        try {
            NetResponseMessage net = (NetResponseMessage) tempIn.readObject();
            return net;
        } catch (ClassNotFoundException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        } finally {
            if (req.getType() != NetTypeRequest.SUBSCRIBING)
                tempSocket.close();
        }
        return null;
    }
}
