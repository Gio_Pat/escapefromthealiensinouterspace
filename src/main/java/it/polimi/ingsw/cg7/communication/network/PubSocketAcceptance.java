package it.polimi.ingsw.cg7.communication.network;

import it.polimi.ingsw.cg7.communication.handlers.SocketRequestHandler;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * The Class PubSocketAcceptance.
 *
 * @author Giovanni Patruno
 */

public class PubSocketAcceptance implements Runnable {
    
    /** The publisher socket. */
    private ServerSocket publisherSocket;
    
    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
        startPubSocketAcceptance();

    }
    
    /**
     * Start pub socket acceptance.
     */
    private void startPubSocketAcceptance() {
        try {
            publisherSocket = new ServerSocket(60002);
            Socket subSocket;
            SocketRequestHandler sk2;
            while (!Thread.interrupted()) {
                subSocket = publisherSocket.accept();
                sk2 = new SocketRequestHandler(subSocket);
                Host.executor.execute(sk2);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }

}
