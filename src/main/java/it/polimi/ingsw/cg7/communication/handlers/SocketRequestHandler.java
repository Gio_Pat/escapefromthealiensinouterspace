package it.polimi.ingsw.cg7.communication.handlers;

import it.polimi.ingsw.cg7.Client.model.NetRequestMessage;
import it.polimi.ingsw.cg7.Client.model.NetResponseMessage;
import it.polimi.ingsw.cg7.Client.model.NetTypeRequest;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The Class SocketRequestHandler, handles the requent and then
 * <strong>terminate</strong>.
 *
 * @author Giovanni Patruno
 */
public class SocketRequestHandler extends RequestHandler implements Runnable {

    /** The socket. */
    private final Socket socket;

    /** The input stream. */
    private ObjectInputStream in;

    public ObjectInputStream getIn() {
        return in;
    }

    /** The output stream */
    private ObjectOutputStream out;

    /**
     * Instantiates a new socket request handler.
     *
     * @param socket
     *            the socket
     * @throws IOException
     */
    public SocketRequestHandler(Socket socket) throws IOException {
        this.socket = socket;
    }

    /**
     * Recieve.
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void recieve() throws IOException {
        NetRequestMessage req = null;
        try {
            out = new ObjectOutputStream(socket.getOutputStream());
            in = new ObjectInputStream(socket.getInputStream());
            req = (NetRequestMessage) in.readObject();
            NetResponseMessage resp = this.processRequest(req);
            out.writeObject(resp);
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        } finally {
            if (req == null || req.getType() != NetTypeRequest.SUBSCRIBING)
                this.socket.close();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * it.polimi.ingsw.cg7.communication.handlers.RequestHandler#getSubscriber()
     */
    @Override
    public SubHandler getSubscriber() {
        // return new SocketSubscriber(out);
        return new SocketSubHandler(out, in);
    }

    @Override
    public void run() {
        try {
            this.recieve();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}
