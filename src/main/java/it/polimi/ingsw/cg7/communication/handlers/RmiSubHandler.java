package it.polimi.ingsw.cg7.communication.handlers;

import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;
import it.polimi.ingsw.cg7.communication.guest.IRmiSubscriber;
import it.polimi.ingsw.cg7.communication.guest.ISubscriber;

import java.rmi.RemoteException;
import java.util.UUID;

/**
 * The Class RmiSubscriber.
 *
 * @author Giovanni Patruno
 */
public class RmiSubHandler extends SubHandler implements ISubscriber {

    /** The stub client. */
    private IRmiSubscriber stubSubscriber;

    /**
     * Instantiates a new rmi subscriber.
     *
     * @param stubClient
     *            the stub client
     */
    public RmiSubHandler(IRmiSubscriber stubClient) {
        super(UUID.randomUUID());
        this.stubSubscriber = stubClient;
    }

    /**
     * Instantiates a new rmi subscriber.
     *
     * @param stubClient
     *            the stub client
     * @param token
     *            the token
     */
    public RmiSubHandler(IRmiSubscriber stubClient, UUID token) {
        super(token);
        this.stubSubscriber = stubClient;
    }

    /*
     * Generic sending message
     * 
     * @see
     * it.polimi.ingsw.cg7.communication.guest.Subscriber#send(java.lang.String)
     */
    @Override
    public void send(NetBrokerMessage msg) throws RemoteException{
                this.stubSubscriber.processResponse(msg);
    }
}
