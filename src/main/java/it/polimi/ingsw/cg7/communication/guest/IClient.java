package it.polimi.ingsw.cg7.communication.guest;

import it.polimi.ingsw.cg7.Client.model.NetRequestMessage;
import it.polimi.ingsw.cg7.Client.model.NetResponseMessage;

import java.io.IOException;

/**
 * This interface will be used by the guest thread that will perform requests
 * 
 * @author Giovanni Patruno
 *
 */
public interface IClient {
    /**
     * 
     * @param req
     * @return
     * @throws IOException
     */

    public NetResponseMessage sendRequest(NetRequestMessage request)
            throws IOException;

}
