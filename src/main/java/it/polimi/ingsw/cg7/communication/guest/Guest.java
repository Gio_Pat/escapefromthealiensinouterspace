package it.polimi.ingsw.cg7.communication.guest;

import it.polimi.ingsw.cg7.Client.controller.InterfaceControl;
import it.polimi.ingsw.cg7.Client.controller.SubInterface;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Scanner;
import java.util.UUID;

/**
 * The Class Guest.
 * Authors : Giovanni Patruno & Debora Rebai
 * Concept By Debora Rebai, Functionality By Giovanni Patruno
 */
public class Guest {

    /** The choose. */
    private static String choose;
    
    /** The input. */
    private static Scanner input = new Scanner(System.in);
    
    /** The token. */
    private static UUID token;
    
    /** The interface control. */
    private static InterfaceControl interfaceControl;
    
    /** The subinterface. */
    private static SubInterface subinterface;

    /**
     * The main method.
     *
     * @param args the arguments
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws InterruptedException the interrupted exception
     * @throws InvocationTargetException the invocation target exception
     */
    public static void main(String[] args) throws IOException,
            InterruptedException, InvocationTargetException {

        do {
            System.out.println("Choose CLI or GUI:");
            System.out.println("1: CLI");
            System.out.println("2: GUI");
            choose = input.next();
        } while (!(choose.equals("1")) && (!(choose.equals("2"))));

        token = null;

        File f = new File("myTokenEftAioS.tpe");
        if (f.isFile() && f.canRead()) {
            BufferedReader in = new BufferedReader(new FileReader(
                    "myTokenEftAioS.tpe"));
            token = UUID.fromString(in.readLine());
            System.out.println(token.toString());
            in.close();
        }

        subinterface = new SubInterface();
        interfaceControl = new InterfaceControl();
        interfaceControl.setSubInterface(subinterface);
        interfaceControl.setToken(token);

        if (choose.equals("1")) {

            new GuestCli(interfaceControl, subinterface, token);

        } else {

            new GuestGUI(interfaceControl, subinterface, token);
        }

    }
}
