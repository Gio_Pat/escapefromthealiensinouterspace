package it.polimi.ingsw.cg7.communication.handlers;

import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.UUID;

/**
 * Questa classe rappresenta il Subscriber gestito dal publisher. E contiene
 * tutti i metodi che permettono al Publisher di agire su di esso, come ad
 * essempio Send This class represent the subscriber handled by the publisher
 * and contains the whole methods that allows the publisher to manage him, like
 * sending a message
 * 
 * @author Giovanni
 *
 */
public class SocketSubHandler extends SubHandler {

    /** The out. */
    private ObjectOutputStream out;

    private ObjectInputStream in;

    public ObjectInputStream getIn() {
        return in;
    }

    /**
     * Instantiates a new socket subscriber.
     *
     * @param out
     *            the out
     */
    public SocketSubHandler(ObjectOutputStream out) {
        this(out, UUID.randomUUID());
    }

    public SocketSubHandler(ObjectOutputStream out, ObjectInputStream in) {
        this(out);
        this.in = in;
    }

    /**
     * Instantiates a new socket subscriber.
     *
     * @param out
     *            the out
     * @param token
     *            the token
     */
    public SocketSubHandler(ObjectOutputStream out, UUID token) {
        super(token);
        this.out = out;

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * it.polimi.ingsw.cg7.communication.guest.Subscriber#send(java.lang.String)
     */
    @Override
    public void send(NetBrokerMessage msg)  throws IOException{
                out.writeObject(msg);
            //out.flush();
    }

}
