package it.polimi.ingsw.cg7.communication.guest;

import it.polimi.ingsw.cg7.Client.model.NetRequestMessage;
import it.polimi.ingsw.cg7.Client.model.NetResponseMessage;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.UUID;

/**
 * Questa classe rappresenta il Subscriber gestito dal publisher. E contiene
 * tutti i metodi che permettono al Publisher di agire su di esso, come ad
 * essempio Send This class represent the subscriber handled by the publisher
 * and contains the whole methods that allows the publisher to manage him, like
 * sending a message
 * 
 * @author Giovanni
 *
 */
public class SocketSubscriber {

    /** The out. */
    private ObjectOutputStream out;

    private ObjectInputStream in;

    private Socket socket;

    private NetResponseMessage resTwo;

    public NetResponseMessage getResTwo() {
        return resTwo;
    }

    public ObjectInputStream getIn() {
        return in;
    }

    /**
     * Instantiates a new socket subscriber.
     *
     * @param out
     *            the out
     */
    public SocketSubscriber(ObjectOutputStream out) {
        this(out, UUID.randomUUID());
    }

    public SocketSubscriber(ObjectOutputStream out, ObjectInputStream in) {
        this(out);
        this.in = in;
    }

    public SocketSubscriber(NetRequestMessage req) {
        try {
            this.socket = new Socket("127.0.0.1", 60002);
            this.in = new ObjectInputStream(socket.getInputStream());
            this.out = new ObjectOutputStream(socket.getOutputStream());
            out.writeObject(req);
            out.flush();
            this.resTwo = (NetResponseMessage) in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    /**
     * Instantiates a new socket subscriber.
     *
     * @param out
     *            the out
     * @param token
     *            the token
     */
    public SocketSubscriber(ObjectOutputStream out, UUID token) {
        // super(token);
        this.out = out;

    }

}