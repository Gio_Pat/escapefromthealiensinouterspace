package it.polimi.ingsw.cg7.helpers;


public class LetterNumbConverter {
    /**
     * this method is used to easly convert an Integer to a String coordinate X
     * @param coordX
     * @return
     * @author Giovanni
     */
    public static String intToString(int coordX)
    {  
        char coord = (char)(coordX + 65);
        return Character.toString(coord);
    }
    /**
     * This method is used to easly convert a String coordinate into a real coordinate X
     */
    public static int stringToInt(String coordX)
    {
        String temp = coordX.toUpperCase();
        temp = temp.trim();
        char coord = temp.charAt(0);
        return (coord - 65);
    }    
}
