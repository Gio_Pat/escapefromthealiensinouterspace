package it.polimi.ingsw.cg7.Client.model;

import java.io.Serializable;
/**
 * Broker message Types
 * @author Debora Rebai & Giovanni Patruno
 *
 */
public enum NetTypeBroker implements Serializable {

    ESCAPE, SILENCE, MESSAGE, TYPESECTOR, ATTACK, RECIVECARD, PLAYERSDEAD, PLAYERRUMORS, PLAYERESCAPE, PLAYERTURN, PLAYERSLIGHT, FINISHGAME, INITGAME, ROLEPLAYER, TURN, PLAYEROUT, PLAYERDRAW;

}
