package it.polimi.ingsw.cg7.Client.model;
/**
 *  View Sectors type (For displaying side)
 * @author Debora Rebai, Giovanni Patruno
 *
 */
public enum ViewSectorType {
    DANGEROUS, SAFE, HATCH, HUMANSTARTPOINT, ALIENSTARTPOINT, UNDERLINEDSECTOR, CLICKED, MYPOSALIEN, MYPOSHUMAN
}
