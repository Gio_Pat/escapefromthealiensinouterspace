package it.polimi.ingsw.cg7.Client.model;

import java.io.Serializable;

/**
 * The Class ViewCard.
 * Authors: Debora Rebai
 */
public class ViewCard implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
    
    /** The name. */
    private String name;

    /**
     * Instantiates a new view card.
     *
     * @param name the name
     */
    public ViewCard(String name) {
        this.name = name;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

}
