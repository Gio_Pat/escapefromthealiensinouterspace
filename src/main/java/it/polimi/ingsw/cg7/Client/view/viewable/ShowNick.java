package it.polimi.ingsw.cg7.Client.view.viewable;

import it.polimi.ingsw.cg7.Client.controller.InterfaceControl;
/**
 * The Interface ShowNick.
 */
public interface ShowNick {

    /**
     * Choose username.
     *
     * @param interfaceControl the interface control
     */
    public void chooseUsername(InterfaceControl interfaceControl);

}
