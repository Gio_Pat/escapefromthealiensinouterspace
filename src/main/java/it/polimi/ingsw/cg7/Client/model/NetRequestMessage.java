package it.polimi.ingsw.cg7.Client.model;

import java.io.Serializable;
import java.util.UUID;

/**
 * The Class NetRequestMessage.
 */
public class NetRequestMessage implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The type. */
    private NetTypeRequest type;

    /** The player token. */
    private UUID playerToken;

    /** The valid send message. */
    private boolean validSendMessage;

    /** The item. */
    private ViewCard item;

    /** The position. */
    private ViewPosition position;

    /** The set attack. */
    private boolean setAttack;

    /** The message. */
    private ViewMessage message;

    /** The nick name. */
    private String nickName;

    /** The selected maps. */
    private ViewMap selectedMaps;

    /**
     * Instantiates a new net request message.
     *
     * @param type
     *            the type
     * @param playerToken
     *            the player token
     */
    public NetRequestMessage(NetTypeRequest type, UUID playerToken) {
        this.type = type;
        this.playerToken = playerToken;
        this.validSendMessage = false;
    }

    /**
     * Request turn.
     */
    public void requestTurn() {
        if (this.type == NetTypeRequest.TURN)
            this.validSendMessage = true;
    }

    /**
     * Request discard.
     *
     * @param card
     *            the card
     */
    public void requestDiscard(ViewCard card) {
        if (this.type == NetTypeRequest.DISCARD) {
            this.item = card;
            this.validSendMessage = true;
        }
    }

    /**
     * Movement request.
     *
     * @param position
     *            the position
     */
    public void movementRequest(ViewPosition position) {
        if (this.type == NetTypeRequest.MOVEMENT) {
            this.position = position;
            this.validSendMessage = true;
        }

    }

    /**
     * Request last message.
     */
    public void requestLastMessage() {
        if (this.type == NetTypeRequest.CHAT)
            this.validSendMessage = true;
    }

    /**
     * Attack request.
     */
    public void attackRequest() {
        if (this.type == NetTypeRequest.ATTACK) {
            this.setAttack = true;
            this.validSendMessage = true;
        }
    }

    /**
     * Request maps.
     */
    public void requestMaps() {
        if (this.type == NetTypeRequest.MAPS)
            this.validSendMessage = true;
    }

    /**
     * Use item request.
     *
     * @param item
     *            the item
     * @param position
     *            the position
     */
    public void useItemRequest(ViewCard item, ViewPosition position) {
        if (this.type == NetTypeRequest.USEITEM) {
            this.item = item;
            this.position = position;
            this.validSendMessage = true;
        }
    }

    /**
     * Send message request.
     *
     * @param message
     *            the message
     */
    public void sendMessageRequest(ViewMessage message) {
        if (this.type == NetTypeRequest.MESSAGE) {
            this.message = message;
            this.validSendMessage = true;
        }
    }

    /**
     * Finish turn request.
     */
    public void finishTurnRequest() {
        if (this.type == NetTypeRequest.FINISHTURN) {
            this.validSendMessage = true;
        }
    }

    /**
     * Request subscribing.
     *
     * @param nickName
     *            the nick name
     * @param selectedMaps
     *            the selected maps
     */
    public void requestSubscribing(String nickName, ViewMap selectedMaps) {
        if (this.type == NetTypeRequest.SUBSCRIBING) {
            this.nickName = nickName;
            this.selectedMaps = selectedMaps;
            this.validSendMessage = true;
        }
    }

    /**
     * Send rumor.
     *
     * @param position
     *            the position
     */
    public void sendRumor(ViewPosition position) {
        if (this.type == NetTypeRequest.SENDFALSERUMORS) {
            this.position = position;
            this.validSendMessage = true;
        }
    }

    /**
     * Gets the serialversionuid.
     *
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public NetTypeRequest getType() {
        return type;
    }

    /**
     * Gets the player token.
     *
     * @return the player token
     */
    public UUID getPlayerToken() {
        return playerToken;
    }

    /**
     * Checks if is valid send message.
     *
     * @return true, if is valid send message
     */
    public boolean isValidSendMessage() {
        return validSendMessage;
    }

    /**
     * Gets the item.
     *
     * @return the item
     */
    public ViewCard getItem() {
        return item;
    }

    /**
     * Gets the position.
     *
     * @return the position
     */
    public ViewPosition getPosition() {
        return position;
    }

    /**
     * Checks if is sets the attack.
     *
     * @return true, if is sets the attack
     */
    public boolean isSetAttack() {
        return setAttack;
    }

    /**
     * Gets the message.
     *
     * @return the message
     */
    public ViewMessage getMessage() {
        return message;
    }

    /**
     * Gets the nick name.
     *
     * @return the nick name
     */
    public String getNickName() {
        return nickName;
    }

    /**
     * Gets the selected maps.
     *
     * @return the selected maps
     */
    public ViewMap getSelectedMaps() {
        return selectedMaps;
    }

    /**
     * Request deck.
     */
    public void requestDeck() {
        if (this.type == NetTypeRequest.DECK) {
            this.validSendMessage = true;
        }

    }

    public void requestPlayers() {
        if (this.type == NetTypeRequest.PLAYERS) {
            this.validSendMessage = true;
        }

    }

    public void requestClient() {
        if (this.type == NetTypeRequest.CLIENT) {
            this.validSendMessage = true;
        }
    }

    public void requestPosition() {
        if (this.type == NetTypeRequest.POSITION) {
            this.validSendMessage = true;
        }

    }

    public void requestNearSector() {
        if (this.type == NetTypeRequest.NEARSECTOR) {
            this.validSendMessage = true;
        }
    }

    public void requestDraw() {
        if (this.type == NetTypeRequest.DRAW) {
            this.validSendMessage = true;
        }

    }

    public void requestInfoFinishGame() {
        if (this.type == NetTypeRequest.FINISHGAME) {
            this.validSendMessage = true;
        }

    }

}
