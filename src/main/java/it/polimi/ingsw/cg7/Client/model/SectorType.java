package it.polimi.ingsw.cg7.Client.model;
/**
 * Sectors View Types
 * @author Debora Rebai
 *
 */
public enum SectorType {

    SAFE, PLAYING, ESCAPE, STARTINGHUMAN, STARTINGALIEN;

}
