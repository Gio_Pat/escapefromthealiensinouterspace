package it.polimi.ingsw.cg7.Client.view.gui;

import it.polimi.ingsw.cg7.Client.model.ViewPlayer;
import it.polimi.ingsw.cg7.Client.view.viewable.ShowPlayers;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class PlayersGui extends JPanel implements ShowPlayers {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private static BufferedImage backgroundPlayers, imagePlayer, imageAlien,
            imageHuman, image;
    private TexturePaint background;
    private List<JLabel> players;
    private List<ViewPlayer> viewPlayers;
    private static JLabel namePlayer;
    private static JLabel numCard;
    private static JLabel numKill;
    private static double width;
    private static double height;
    private MouseListener ms;
    private ViewPlayer currentPlayer;
    private ViewPlayer viewingPlayer;

    public PlayersGui() {

        Toolkit t = Toolkit.getDefaultToolkit();
        Dimension screenSize = t.getScreenSize();
        width = screenSize.getWidth();
        height = screenSize.getHeight();

        setLayout(null);
        
        viewPlayers = new ArrayList<ViewPlayer>();
        players = new ArrayList<JLabel>();
        viewingPlayer = new ViewPlayer("");
        viewingPlayer.setKillNumber(0);
        viewingPlayer.setCards(0);
        currentPlayer = viewingPlayer;

        loadComponents();
        loadContents();
        ms = new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent arg0) {

                String s = new String();
                s = arg0.getComponent().getName();
                s = s.substring(6, 7);
                int number = Integer.parseInt(s);
                s = new String();
                s = arg0.getComponent().getName();
                s = s.substring(7);
                String newName = new String();
                image = imagePlayer;
                if (s.equals("Unknown")) {
                    image = imageAlien;
                    newName = new String("ALIEN");
                }
                if (s.equals("ALIEN")) {
                    image = imageHuman;
                    newName = new String("HUMAN");
                }
                if (s.equals("HUMAN")) {

                    newName = new String("Unknown");
                }
                Image resizeimg = image.getScaledInstance((int) (height / 10),
                        (int) (height / 10), Image.SCALE_DEFAULT);
                players.get(number).setIcon(new ImageIcon(resizeimg));
                players.get(number).setName("Player" + number + newName);

            }

            @Override
            public void mouseEntered(MouseEvent arg0) {

                String s = new String();
                s = arg0.getComponent().getName();
                s = s.substring(6, 7);
                int number = Integer.parseInt(s);
                viewingPlayer = viewPlayers.get(number);
                SwingUtilities.invokeLater(new Runnable() {

                    @Override
                    public void run() {
                        showStats();
                    }
                });
            }

            @Override
            public void mouseExited(MouseEvent arg0) {
                viewingPlayer = currentPlayer;
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        refreshInfo();
                    }
                });

            }

            @Override
            public void mousePressed(MouseEvent arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseReleased(MouseEvent arg0) {

            }

        };

        showCurrentSituation();

    }

    private void loadContents() {
        try {
            imageAlien = ImageIO.read(new File(
                    ".//Game Media//Characters//ALIENLogo.png"));
            imageHuman = image = ImageIO.read(new File(
                    ".//Game Media//Characters//HUMANLogo.png"));
            backgroundPlayers = ImageIO
                    .read(new File(
                            ".//Game Media//Interface Media//GameContainer//PlayersFrame.png"));
            imagePlayer = ImageIO.read(new File(
                    ".//Game Media//Characters//UnknownLogo.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void loadComponents() {
        namePlayer = new JLabel("Turn of " + viewingPlayer.getUsername());
        namePlayer.setSize((int) (width / 7), (int) (height / 40));
        namePlayer.setOpaque(false);
        namePlayer.setFont(new Font("Century Gothic", 0,
                (int) (0.0208 * height)));
        namePlayer.setForeground(Color.CYAN);
        namePlayer.setLocation((int) (0.06510 * height),
                (int) (0.4723 * height));
        //namePlayer.setEditable(false);
        this.add(namePlayer);
        numCard = new JLabel("0");
        numCard.setSize((int) (width / 7), (int) (height / 40));
        numCard.setOpaque(false);
        numCard.setFont(new Font("Century Gothic", 0, (int) (0.0208 * height)));
        numCard.setForeground(Color.CYAN);
        numCard.setLocation((int) (0.06510 * height), (int) (0.5023 * height));
        //namePlayer.setEditable(false);
        this.add(numCard);
        numKill = new JLabel("0");
        numKill.setSize((int) (width / 7), (int) (height / 40));
        numKill.setOpaque(false);
        numKill.setFont(new Font("Century Gothic", 0, (int) (0.0208 * height)));
        numKill.setForeground(Color.CYAN);
        numKill.setLocation((int) (0.06510 * height), (int) (0.5323 * height));
        //numKill.setEditable(false);
        this.add(numKill);

    }

    protected void paintComponent(Graphics g) {

        Graphics2D g2d = (Graphics2D) g.create();
        // RenderingHints rh = new RenderingHints(
        // RenderingHints.KEY_TEXT_ANTIALIASING,
        // RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        // g2d.setRenderingHints(rh);

        super.paintComponent(g);
        super.setSize((int) width / 4, (int) (height * 0.65));
        
        g2d.drawRect(0, 0, (int) width / 4, (int) (height * 0.65));
        background = new TexturePaint(backgroundPlayers, new Rectangle(0, 0,
                (int) width / 4, (int) (height * 0.65)));
        g2d.setPaint(background);
        g2d.fill(new Rectangle(0, 0, (int) width / 4, (int) (height * 0.65)));
    }

    @Override
    public void updatePlayers(List<ViewPlayer> requestPlayers) {
        this.viewPlayers = requestPlayers;
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                showCurrentSituation();
            }
        });

    }

    @Override
    public void showCurrentSituation() {
        int size = 0;
        if (viewPlayers != null)
            size = viewPlayers.size();
        for (int x = 0; x < size; x++) {
            Image resizePlayer = imagePlayer.getScaledInstance(
                    (int) (height / 10), (int) (height / 10),
                    Image.SCALE_DEFAULT);
            JLabel currentShowingPlayer = new JLabel(
                    new ImageIcon(resizePlayer));
            currentShowingPlayer.setName("Player" + x + "Unknown");
            players.add(currentShowingPlayer);
        }

        int coordX[] = { (int) (0.06510 * height), (int) (0.2213 * height),
                (int) (0.1432 * height) };
        int coordY[] = { 0, 0, (int) (0.07812 * height),
                (int) (0.1562 * height), (int) (0.1562 * height),
                (int) (0.2343 * height), (int) (0.3123 * height),
                (int) (0.3123 * height) };

        for (int x = 0; x < size; x++) {

            players.get(x).setSize((int) (width / 10), (int) (width / 10));
            players.get(x).setLocation(coordX[x % 3], coordY[x % 8]);
            add(players.get(x));
            players.get(x).addMouseListener(ms);
            setComponentZOrder(players.get(x), 0);
        }

        refreshInfo();

    }

    /**
     * Shows the statistics of a specific player when the mouse enter the player
     * in the right-box
     */
    private void showStats() {
        displayNewKills(numKill, "kill: " + viewingPlayer.getKillNumber());
        displayNewCards(numCard, "cards: " + viewingPlayer.getCards());
        displayNewPlayer(namePlayer, viewingPlayer.getUsername());
//        namePlayer.setText(viewingPlayer.getUsername());
//
//        numCard.setText("cards: " + viewingPlayer.getCards());
//
//        numKill.setText("kill: " + viewingPlayer.getKillNumber());
    }
    private void displayNewKills(JLabel oldLabel, String msg) {
        remove(numKill);
        oldLabel.setVisible(false);
        numKill = new JLabel(msg);
        numKill.setSize(oldLabel.getSize());
        numKill.setOpaque(false);
        numKill.setFont(oldLabel.getFont());
        numKill.setForeground(oldLabel.getForeground());
        numKill.setLocation(oldLabel.getLocation());
        this.add(numKill);
        
        setComponentZOrder(numKill, 0);
        this.validate();

        //namePlayer.setEditable(false);

        this.repaint();
    }
    private void displayNewCards(JLabel oldLabel, String msg) {
        remove(numCard);
        oldLabel.setVisible(false);
        numCard = new JLabel(msg);
        numCard.setSize(oldLabel.getSize());
        numCard.setOpaque(false);
        numCard.setFont(oldLabel.getFont());
        numCard.setForeground(oldLabel.getForeground());
        numCard.setLocation(oldLabel.getLocation());
        this.add(numCard);   
        setComponentZOrder(numCard, 0);
        this.validate();
        //namePlayer.setEditable(false);

        this.repaint();
    }

    private void displayNewPlayer(JLabel oldLabel, String msg) {
        remove(namePlayer);
        oldLabel.setVisible(false);
        namePlayer = new JLabel(msg);
        namePlayer.setSize(oldLabel.getSize());
        namePlayer.setOpaque(false);
        namePlayer.setFont(oldLabel.getFont());
        namePlayer.setForeground(oldLabel.getForeground());
        namePlayer.setLocation(oldLabel.getLocation());
        this.add(namePlayer);   
        this.validate();
        //namePlayer.setEditable(false);
 
        this.repaint();
    }

    private void refreshInfo() {
        
//        namePlayer.setText("Turn of " + viewingPlayer.getUsername());
//        numCard.setText("cards: " + viewingPlayer.getCards());
//        numKill.setText("kill: " + viewingPlayer.getKillNumber());
          displayNewKills(numKill, "kill: " + viewingPlayer. getKillNumber());
          displayNewCards(numCard, "cards: " + viewingPlayer.getCards());

          displayNewPlayer(namePlayer, "Turn of " + viewingPlayer.getUsername());
    }

    public void setTurn(ViewPlayer player) {
        this.currentPlayer = player;
        this.viewingPlayer = player;
        refreshInfo();
    }

}
