package it.polimi.ingsw.cg7.Client.view.gui;

import it.polimi.ingsw.cg7.Client.controller.InterfaceControl;
import it.polimi.ingsw.cg7.Client.view.viewable.ShowNick;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class ChoosingNickMenu implements ShowNick {

    private JFrame frame;
    private InterfaceControl interfaceControl;
    private MouseListener ms;
    private JTextField nick;
    private double width, height;

    public ChoosingNickMenu() {

        Toolkit t = Toolkit.getDefaultToolkit();
        Dimension screenSize = t.getScreenSize();

        width = screenSize.getWidth();
        height = screenSize.getHeight();

        frame = new JFrame("EftaioS - GUI");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setPreferredSize(new Dimension((int) (width / 2),
                (int) (height * (0.8))));
        // frame.getContentPane().setLayout(null);
        frame.getContentPane().setSize((int) (width / 2),
                (int) (height * (0.8)));
        frame.setLocation((int) (width / 4), (int) (height / 9));

        frame.pack();
        ms = new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent arg0) {
                if (nick.getText() != null && nick.getText() != "") {
                    interfaceControl.setNick(nick.getText());
                    nextWindow();
                }

            }

            @Override
            public void mouseEntered(MouseEvent arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseExited(MouseEvent arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mousePressed(MouseEvent arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseReleased(MouseEvent arg0) {
                // TODO Auto-generated method stub

            }

        };
        loadContents(frame);
    }

    private void nextWindow() {

        synchronized (this) {
            notify();
        }
        frame.dispose();

    }

    public void loadContents(JFrame frame) {
        try {
            BufferedImage background = ImageIO.read(new File(
                    ".//Game Media//Interface Media//Menu//Background.jpg"));

            nick = new JTextField();
            nick.setText("");
            nick.setSize((int) (0.42 * height), (int) (0.046 * height));
            nick.setOpaque(false);
            nick.setFont(new Font("Century Gothic", 0, (int) (0.0258 * height)));
            nick.setForeground(Color.WHITE);
            nick.setLocation((int) (width / 4 - 0.42 * 0.5 * height),
                    (int) (height * 0.59));

            JButton sckBtn = new JButton();

            sckBtn.addMouseListener(ms);
            sckBtn.setName("SIGN IN");
            sckBtn.setText("SIGN IN");
            sckBtn.setSize((int) (0.15 * height), (int) (0.056 * height));
            sckBtn.setFont(new Font("Century Gothic", 0,
                    (int) (0.0238 * height)));
            sckBtn.setLocation((int) (width / 4 - 0.15 * 0.5 * height),
                    (int) (height * 0.65));

            frame.getContentPane().add(sckBtn);
            frame.getContentPane().add(nick);

            Image backgroundResize = background.getScaledInstance(
                    (int) (width / 2), (int) (height * (0.8)),
                    Image.SCALE_DEFAULT);

            JLabel backgroundLbl = new JLabel(new ImageIcon(backgroundResize));

            frame.getContentPane().add(backgroundLbl);

        } catch (IOException e) {

            Logger.getLogger("Mouse clicked").log(Level.SEVERE, null, e);
        }
    }

    @Override
    public void chooseUsername(InterfaceControl interfaceControl) {
        this.interfaceControl = interfaceControl;
        frame.setVisible(true);

    }

}
