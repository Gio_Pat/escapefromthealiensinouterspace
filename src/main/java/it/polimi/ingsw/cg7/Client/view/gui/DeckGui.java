package it.polimi.ingsw.cg7.Client.view.gui;

import it.polimi.ingsw.cg7.Client.controller.InterfaceControl;
import it.polimi.ingsw.cg7.Client.controller.MusicController;
import it.polimi.ingsw.cg7.Client.model.NetResponseMessage;
import it.polimi.ingsw.cg7.Client.model.ViewCard;
import it.polimi.ingsw.cg7.Client.model.ViewPlayer;
import it.polimi.ingsw.cg7.Client.view.viewable.ShowDeck;
import it.polimi.ingsw.cg7.Client.view.viewable.ShowPlayer;
import it.polimi.ingsw.cg7.model.CharType;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

/**
 * The Class DeckGui.
 */
public class DeckGui extends JPanel implements ShowDeck, ShowPlayer {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
    
    /** The my deck. */
    private List<ViewCard> myDeck;
    
    /** The selection card. */
    private ViewCard selectionCard;
    
    /** The player. */
    private ViewPlayer player;
    
    /** The forward button. */
    @SuppressWarnings("unused")
    private static BufferedImage backgroundDeck, imageEmpty, imageCharHuman,
            imageCharAlien, imageCharNull, forwardButton;
    
    /** The buffered images. */
    private static Map<String, BufferedImage> bufferedImages;
    
    /** The background. */
    private TexturePaint background;
    
    /** The cards. */
    private List<JLabel> cards;
    
    /** The interface control. */
    private InterfaceControl interfaceControl;
    
    /** The width. */
    private double width;
    
    /** The height. */
    private double height;
    
    /** The name player. */
    private JTextArea namePlayer;
    
    /** The num kill. */
    private JTextArea numKill;
    
    /** The num turn. */
    private JTextArea numTurn;
    
    /** The forward butt. */
    private JLabel typeChar;
    
    /** The ms. */
    private MouseListener ms;
    
    /** The resize butt. */
    private Image resizeCard;
    
    /** The game container gui. */
    private GameContainerGui gameContainerGui;
    
    /** The use card. */
    private boolean useCard;
    
    /** The attack or use. */
    private JButton attackOrUse;
    
    /** The timer action performer. */
    private ActionListener timerActionPerformer;
    
    /** The timer trigger. */
    private Timer timerTrigger;
    
    /** The forward. */
    private JButton forward;
    
    /** The discard. */
    private boolean discard;
    
    /** The timer. */
    private static JLabel setting, timer;
    
    /** The setting button. */
    private BufferedImage settingButton;
    
    /** The count. */
    private static int count = 90;

    /**
     * Instantiates a new deck gui.
     *
     * @param gameContainerGui the game container gui
     */
    public DeckGui(GameContainerGui gameContainerGui) {
        Toolkit t = Toolkit.getDefaultToolkit();
        Dimension screenSize = t.getScreenSize();

        this.gameContainerGui = gameContainerGui;

        width = screenSize.getWidth();
        height = screenSize.getHeight();
        setLayout(null);

        loadContents();

        MusicController.startMusic();

        myDeck = new ArrayList<ViewCard>();
        player = new ViewPlayer("");
        player.setKillNumber(0);
        cards = new ArrayList<JLabel>();
        selectionCard = null;

        attackOrUse = new JButton();
        attackOrUse.setText("Attack/Use Card");
        attackOrUse.setFont(new Font("Century Gothic", 0, 14));
        attackOrUse
                .setLocation((int) (0.9672 * height), (int) (0.039 * height));
        attackOrUse.setSize((int) (height / 7), (int) (height / 20));
        attackOrUse.addMouseListener(new MouseListener() {

            @Override
            public void mouseReleased(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mousePressed(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseExited(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseClicked(MouseEvent e) {

                if (interfaceControl.requestClient().getType() == CharType.ALIEN)
                    interfaceControl.attack();
                else
                    DeckGui.this.useCard = true;

            }
        });
        initTimerActionPerformer();

        timerTrigger = new Timer(1000, timerActionPerformer);
        timerTrigger.start();
        forward = new JButton();
        forward.setText("Finish Turn");
        forward.setFont(new Font("Century Gothic", 0, 14));
        forward.setSize((int) (height / 7), (int) (height / 20));
        forward.setLocation((int) (0.9672 * height), (int) (0.11 * height));
        forward.addMouseListener(new MouseListener() {

            @Override
            public void mouseReleased(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mousePressed(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseExited(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseClicked(MouseEvent e) {

                NetResponseMessage msg = interfaceControl.finishTurn();
                if (!msg.isDoneMove()) {

                    WarningGui warning = null;
                    try {
                        warning = new WarningGui();
                    } catch (IOException e2) {
                        Logger.getLogger("mouse clicked").log(Level.SEVERE, null, e2);
                    }
                    warning.print(msg.getDescription());

                }
                DeckGui.this.gameContainerGui.update();

            }
        });

        add(forward);
        add(attackOrUse);

        numKill = new JTextArea("KILL NUMBER: "
                + String.valueOf(player.getKillNumber()));
        numKill.setSize((int) (0.2604 * height), (int) (0.039 * height));
        numKill.setOpaque(false);
        numKill.setFont(new Font("Century Gothic", 0, (int) (0.0308 * height)));
        numKill.setForeground(Color.CYAN);
        numKill.setEditable(false);
        add(numKill);

        numKill.setLocation((int) (1.158 * height), (int) (0.065 * height));
        namePlayer = new JTextArea(player.getUsername());
        namePlayer.setSize((int) (0.3604 * height), (int) (0.039 * height));
        namePlayer.setOpaque(false);
        namePlayer.setFont(new Font("Century Gothic", 0,
                (int) (0.0308 * height)));
        namePlayer.setForeground(Color.CYAN);
        namePlayer.setLocation((int) (1.158 * height), (int) (0.026 * height));
        namePlayer.setEditable(false);
        add(namePlayer);

        numTurn = new JTextArea("NUM TURN : 0");
        numTurn.setSize((int) (0.2604 * height), (int) (0.039 * height));
        numTurn.setOpaque(false);
        numTurn.setFont(new Font("Century Gothic", 0, (int) (0.0308 * height)));
        numTurn.setForeground(Color.CYAN);
        numTurn.setLocation((int) (1.158 * height), (int) (0.1041 * height));
        numTurn.setEditable(false);
        add(numTurn);

        resizeCard = settingButton.getScaledInstance((int) (height / 10),
                (int) (height / 10), Image.SCALE_DEFAULT);

        setting = new JLabel(new ImageIcon(resizeCard));
        setting.setSize((int) (0.19 * height), (int) (0.19 * height));
        setting.setOpaque(false);
        setting.setLocation((int) (1.358 * height), (int) (0.016 * height));
        setting.addMouseListener(new MouseListener() {

            @Override
            public void mouseReleased(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mousePressed(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseExited(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseClicked(MouseEvent e) {

                MusicController.toggleMusic();

            }
        });
        add(setting);

        timer = new JLabel("Timer : " + String.valueOf(count));
        timer.setSize((int) (0.2604 * height), (int) (0.039 * height));
        timer.setOpaque(false);
        timer.setFont(new Font("Century Gothic", 0, (int) (0.0308 * height)));
        timer.setForeground(Color.CYAN);
        timer.setLocation((int) (1.358 * height), (int) (0.026 * height));
        add(timer);

        resizeCard = imageCharNull.getScaledInstance((int) (height / 8),
                (int) (height / 8), Image.SCALE_DEFAULT);

        typeChar = new JLabel(new ImageIcon(resizeCard));
        typeChar.setSize((int) (height / 8), (int) (height / 8));
        add(typeChar);
        typeChar.setLocation((int) (0.8072 * height), (int) (0.039 * height));

        int x;
        cards = new ArrayList<JLabel>();

        for (x = 0; x < 4; x++) {
            BufferedImage imageNOCard = bufferedImages.get("NO");
            resizeCard = imageNOCard.getScaledInstance((int) (height / 10),
                    (int) (height / 10), Image.SCALE_DEFAULT);
            JLabel currentShowingCard = new JLabel(new ImageIcon(resizeCard));
            currentShowingCard.setName("Card" + x);
            cards.add(currentShowingCard);
        }

        for (x = 0; x < 4; x++) {
            cards.get(x).setSize((int) (height / 10), (int) (height / 10));
            cards.get(x).setLocation(
                    (int) ((height * 0.3034) + (x * (0.1041 * height))),
                    (int) ((0.053 * height) + (x % 2) * (0.039 * height)));
            add(cards.get(x));
        }

        ms = new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent arg0) {

                if (DeckGui.this.useCard) {
                    String s = new String();
                    s = arg0.getComponent().getName();
                    s = s.substring(4, 5);
                    int number = Integer.parseInt(s);
                    selectionCard = myDeck.get(number);
                    useCard(selectionCard);
                    useCard = false;
                    DeckGui.this.gameContainerGui.update();
                }

                if (DeckGui.this.discard) {
                    String s = new String();
                    s = arg0.getComponent().getName();
                    s = s.substring(4, 5);
                    int number = Integer.parseInt(s);
                    selectionCard = myDeck.get(number);
                    interfaceControl.discard(selectionCard);
                    discard = false;
                    DeckGui.this.gameContainerGui.update();
                }
            }

            @Override
            public void mouseEntered(MouseEvent arg0) {

            }

            @Override
            public void mouseExited(MouseEvent arg0) {

            }

            @Override
            public void mousePressed(MouseEvent arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseReleased(MouseEvent arg0) {

            }

        };

    }

    /**
     * Inits the timer action performer.
     */
    private void initTimerActionPerformer() {
        timerActionPerformer = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e) {
                count--;
                displayNewTimer(timer, "Timer : " + count);
                
            }
            
        };
        
    }

    /**
     * Load contents.
     */
    private void loadContents() {
        try {
            backgroundDeck = ImageIO
                    .read(new File(
                            ".//Game Media//Interface Media//GameContainer//DeckFrame.png"));
            imageEmpty = ImageIO.read(new File(
                    ".//Game Media//Card//CardNO.png"));
            bufferedImages = new HashMap<String, BufferedImage>();
            bufferedImages.put("Attack", ImageIO.read(new File(
                    ".//Game Media//Card//CardAttack.png")));
            bufferedImages.put("Adrenaline", ImageIO.read(new File(
                    ".//Game Media//Card//CardAdrenaline.png")));
            bufferedImages.put("Defense", ImageIO.read(new File(
                    ".//Game Media//Card//CardDefense.png")));
            bufferedImages.put("NO",
                    ImageIO.read(new File(".//Game Media//Card//CardNO.png")));
            bufferedImages.put("Sedatives", ImageIO.read(new File(
                    ".//Game Media//Card//CardSedatives.png")));
            bufferedImages.put("Spotlight", ImageIO.read(new File(
                    ".//Game Media//Card//CardSpotlight.png")));
            bufferedImages.put("Teleport", ImageIO.read(new File(
                    ".//Game Media//Card//CardTeleport.png")));
            imageCharNull = ImageIO.read(new File(
                    ".//Game Media//Characters//UnknownLogo.png"));

            imageCharHuman = ImageIO.read(new File(
                    ".//Game Media//Characters//HUMANLogo.png"));
            imageCharAlien = ImageIO.read(new File(
                    ".//Game Media//Characters//ALIENLogo.png"));
            forwardButton = ImageIO
                    .read(new File(
                            ".//Game Media//Interface Media//GameContainer//forward.png"));
            settingButton = ImageIO
                    .read(new File(
                            ".//Game Media//Interface Media//GameContainer//setting.png"));

        } catch (IOException e) {
            Logger.getLogger(this.getName()).log(Level.SEVERE, null, e);
        }
    }

    /* (non-Javadoc)
     * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
     */
    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g.create();
        // RenderingHints rh = new RenderingHints(
        // RenderingHints.KEY_TEXT_ANTIALIASING,
        // RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        // g2d.setRenderingHints(rh);

        super.paintComponent(g);
        super.setSize((int) width, (int) height / 4 - 5);
        g2d.drawRect(0, 0, (int) width, (int) height / 4);
        background = new TexturePaint(backgroundDeck, new Rectangle(0, 0,
                (int) width, (int) height / 4 - 5));
        g2d.setPaint(background);

        g2d.fill(new Rectangle(0, 0, (int) width, (int) height / 4));

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowDeck#getCard()
     */
    @Override
    public void getCard() {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowDeck#useCard(it.polimi.ingsw.cg7.Client.model.ViewCard)
     */
    @Override
    public void useCard(ViewCard card) {
        if (!card.getName().equals("Spotlight")) {
            NetResponseMessage msg = interfaceControl.useCard(card, null);
            WarningGui warning = null;
            try {
                warning = new WarningGui();
            } catch (IOException e) {
                Logger.getLogger(this.getName()).log(Level.SEVERE, null, e);
            }
            warning.print(msg.getDescription());
            updateDeck(interfaceControl.requestDeck());
        } else
            gameContainerGui.clickPositionSpotlight();

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowDeck#updateDeck(java.util.List)
     */
    @Override
    public void updateDeck(List<ViewCard> requestDeck) {

        myDeck = requestDeck;

        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                showCurrentSituation();
            }
        });

        if (deckFull()) {
            WarningGui warning = new WarningGui(gameContainerGui);
            warning.setPlayer(player);
            warning.chooseCard();
        }

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowDeck#showCurrentSituation()
     */
    @Override
    public synchronized void showCurrentSituation() {

        numKill.setText("KILL: " + String.valueOf(this.player.getKillNumber()));
        namePlayer.setText(this.player.getUsername());
        if (this.player.getType() == CharType.ALIEN)
            attackOrUse.setText("Attack");
        else
            attackOrUse.setText("Use Card");

        int x;
        int size;
        if (myDeck == null)
            size = 0;
        else
            size = myDeck.size();

        for (x = 0; x < size; x++) {
            BufferedImage imageCard = bufferedImages.get(myDeck.get(x)
                    .getName());
            Image resizeCard = imageCard.getScaledInstance((int) (height / 10),
                    (int) (height / 10), Image.SCALE_DEFAULT);

            cards.get(x).setIcon(new ImageIcon(resizeCard));
            cards.get(x).addMouseListener(ms);
            cards.get(x).setToolTipText(myDeck.get(x).getName());
        }

        for (; x < 4; x++) {
            BufferedImage imageCard = bufferedImages.get("NO");
            Image resizeCard = imageCard.getScaledInstance((int) (height / 10),
                    (int) (height / 10), Image.SCALE_DEFAULT);
            cards.get(x).setIcon(new ImageIcon(resizeCard));
        }

        // this.setComponentZOrder(namePlayer, 1);
        // this.setComponentZOrder(numKill, 2);

        // fare una mappa e il caricamento lo faccio sopra
        Image resizeCard = null;
        if (player != null) {
            if (player.getType() == CharType.ALIEN)
                resizeCard = imageCharAlien.getScaledInstance(
                        (int) (height / 8), (int) (height / 8),
                        Image.SCALE_DEFAULT);
            else if (player.getType() == CharType.HUMAN)
                resizeCard = imageCharHuman.getScaledInstance(
                        (int) (height / 8), (int) (height / 8),
                        Image.SCALE_DEFAULT);
            else
                resizeCard = imageCharNull.getScaledInstance(
                        (int) (height / 8), (int) (height / 8),
                        Image.SCALE_DEFAULT);
        } else
            resizeCard = imageCharNull.getScaledInstance((int) (height / 8),
                    (int) (height / 8), Image.SCALE_DEFAULT);

        typeChar.setIcon(new ImageIcon(resizeCard));
        repaint();
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowDeck#addCard(it.polimi.ingsw.cg7.Client.model.ViewCard)
     */
    @Override
    public void addCard(ViewCard card) {
        myDeck.add(card);
        gameContainerGui.update();

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowDeck#setInterfaceControl(it.polimi.ingsw.cg7.Client.controller.InterfaceControl)
     */
    @Override
    public void setInterfaceControl(InterfaceControl interfaceControl) {
        this.interfaceControl = interfaceControl;

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowDeck#deckFull()
     */
    @Override
    public boolean deckFull() {
        if (myDeck.size() == 4)
            return true;
        return false;
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowDeck#discard()
     */
    @Override
    public void discard() {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowPlayer#getPlayer()
     */
    @Override
    public ViewPlayer getPlayer() {
        return player;
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowPlayer#updateClient(it.polimi.ingsw.cg7.Client.model.ViewPlayer)
     */
    @Override
    public void updateClient(ViewPlayer requestClient) {
        this.player = requestClient;
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                showCurrentSituation();
            }
        });

    }

    /**
     * Sets the use card.
     *
     * @param b the new use card
     */
    public void setUseCard(boolean b) {
        this.useCard = b;

    }

    /**
     * Sets the discard.
     *
     * @param b the new discard
     */
    public void setdiscard(boolean b) {
        this.discard = b;

    }

    /**
     * Adds the turn.
     */
    public void addTurn() {
        count = 90;
        if (numTurn != null)
            this.numTurn
                    .setText("NUM TURN : "
                            + (Integer.valueOf(this.numTurn.getText()
                                    .substring(11, 12)) + 1));

    }

    /**
     * Display new timer.
     *
     * @param oldLabel the old label
     * @param msg the msg
     */
    private void displayNewTimer(JLabel oldLabel, String msg) {
        remove(timer);
        oldLabel.setVisible(false);
        timer = new JLabel(msg);
        timer.setSize(oldLabel.getSize());
        timer.setOpaque(false);
        timer.setFont(oldLabel.getFont());
        timer.setForeground(oldLabel.getForeground());
        timer.setLocation(oldLabel.getLocation());
        this.add(timer);
        this.validate();
        this.repaint();
    }

    /**
     * Sets the timer.
     */
    public void setTimer() {

        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                count = 90;
            }
        });

    }
}
