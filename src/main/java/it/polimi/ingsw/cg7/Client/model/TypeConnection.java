package it.polimi.ingsw.cg7.Client.model;
/**
 * Connection Type
 * @author Debora Rebai & Giovanni Patruno
 *
 */
public enum TypeConnection {

    RMI, SOCKET;

}
