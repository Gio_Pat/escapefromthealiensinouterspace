package it.polimi.ingsw.cg7.Client.model;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

/**
 * The Class NetResponseMessage.
 * Authors: Giovanni Patruno & Debora Rebai
 */
public class NetResponseMessage implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
    
    /** The near sector. */
    private List<ViewPosition> nearSector;
    
    /** The player. */
    private ViewPlayer player;
    
    /** The type. */
    private NetTypeRequest type;
    
    /** The valid recive message. */
    private boolean validReciveMessage;
    
    /** The description. */
    private String description;
    
    /** The done move. */
    private boolean doneMove;
    
    /** The chat. */
    private List<ViewMessage> chat;
    
    /** The maps. */
    private List<ViewMap> maps;
    
    /** The given token. */
    private UUID givenToken;
    
    /** The flag. */
    private flagResponseType flag;
    
    /** The players. */
    private List<ViewPlayer> players;
    
    /** The position. */
    private ViewPosition position;
    
    /** The deck. */
    private List<ViewCard> deck;

    /**
     * Instantiates a new net response message.
     *
     * @param type the type
     */
    public NetResponseMessage(NetTypeRequest type) {
        this.type = type;
        this.validReciveMessage = false;
        this.description = new String("");
    }

    /**
     * Movement response.
     *
     * @param description the description
     * @param doneMove the done move
     * @param flag the flag
     */
    public void movementResponse(String description, boolean doneMove,
            flagResponseType flag) {
        if (this.type == NetTypeRequest.MOVEMENT) {
            this.description = description;
            this.validReciveMessage = true;
            this.doneMove = doneMove;
            this.flag = flag;
        }

    }

    /**
     * Response turn.
     *
     * @param description the description
     * @param doneMove the done move
     */
    public void responseTurn(String description, boolean doneMove) {
        if (this.type == NetTypeRequest.TURN) {
            this.description = description;
            this.validReciveMessage = true;
            this.doneMove = doneMove;
        }
    }

    /**
     * Send false rumor.
     *
     * @param description the description
     * @param doneMove the done move
     */
    public void sendFalseRumor(String description, boolean doneMove) {
        if (this.type == NetTypeRequest.SENDFALSERUMORS) {
            this.description = description;
            this.validReciveMessage = true;
            this.doneMove = doneMove;
        }
    }

    /**
     * Response last message.
     *
     * @param chat the chat
     * @param description the description
     * @param doneMove the done move
     */
    public void responseLastMessage(List<ViewMessage> chat, String description,
            boolean doneMove) {
        if (this.type == NetTypeRequest.CHAT) {
            this.validReciveMessage = true;
            this.chat = chat;
            this.description = description;
            this.doneMove = doneMove;
        }
    }

    /**
     * Attack request.
     *
     * @param description the description
     * @param doneMove the done move
     */
    public void attackRequest(String description, boolean doneMove) {
        if (this.type == NetTypeRequest.ATTACK) {
            this.validReciveMessage = true;
            this.description = description;
            this.doneMove = doneMove;
        }
    }

    /**
     * Request maps.
     *
     * @param maps the maps
     * @param description the description
     * @param doneMove the done move
     */
    public void requestMaps(List<ViewMap> maps, String description,
            boolean doneMove) {
        if (this.type == NetTypeRequest.MAPS) {
            this.validReciveMessage = true;
            this.description = description;
            this.doneMove = doneMove;
            this.maps = maps;
        }
    }

    /**
     * Use item request.
     *
     * @param description the description
     * @param doneMove the done move
     */
    public void useItemRequest(String description, boolean doneMove) {
        if (this.type == NetTypeRequest.USEITEM) {
            this.description = description;
            this.validReciveMessage = true;
            this.doneMove = doneMove;
        }
    }

    /**
     * Send message request.
     *
     * @param description the description
     * @param doneMove the done move
     */
    public void sendMessageRequest(String description, boolean doneMove) {
        if (this.type == NetTypeRequest.MESSAGE) {
            this.description = description;
            this.doneMove = doneMove;
            this.validReciveMessage = true;
        }
    }

    /**
     * Finish turn request.
     *
     * @param description the description
     * @param doneMove the done move
     */
    public void finishTurnRequest(String description, boolean doneMove) {
        if (this.type == NetTypeRequest.FINISHTURN) {
            this.validReciveMessage = true;
            this.description = description;
            this.doneMove = doneMove;
        }
    }

    /**
     * Response subscribing.
     *
     * @param description the description
     * @param givenToken the given token
     */
    public void responseSubscribing(String description, UUID givenToken) {
        if (this.type == NetTypeRequest.SUBSCRIBING) {
            this.description = description;
            this.validReciveMessage = true;
            this.givenToken = givenToken;

        }
    }

    /**
     * Response discard request.
     *
     * @param description the description
     * @param doneMove the done move
     */
    public void responseDiscardRequest(String description, boolean doneMove) {
        if (this.type == NetTypeRequest.DISCARD) {
            this.description = description;
            this.doneMove = doneMove;
            this.validReciveMessage = true;
        }
    }

    /**
     * Send card.
     *
     * @param flag the flag
     * @param doneMove the done move
     */
    public void sendCard(flagResponseType flag, boolean doneMove) {
        if (this.type == NetTypeRequest.DRAW) {
            this.flag = flag;
            this.doneMove = doneMove;
            this.validReciveMessage = true;
        }
    }

    /**
     * Response deck.
     *
     * @param deck the deck
     */
    public void responseDeck(List<ViewCard> deck) {
        if (this.type == NetTypeRequest.DECK) {
            this.deck = deck;
            this.validReciveMessage = true;
        }
    }

    /**
     * Request players.
     *
     * @param players the players
     */
    public void requestPlayers(List<ViewPlayer> players) {
        if (this.type == NetTypeRequest.PLAYERS) {
            this.players = players;
            this.validReciveMessage = true;
        }
    }

    /**
     * Request client.
     *
     * @param player the player
     */
    public void requestClient(ViewPlayer player) {
        if (this.type == NetTypeRequest.CLIENT) {
            this.player = player;
            this.validReciveMessage = true;
        }
    }

    /**
     * Request position.
     *
     * @param position the position
     */
    public void requestPosition(ViewPosition position) {
        if (this.type == NetTypeRequest.POSITION) {
            this.position = position;
            this.validReciveMessage = true;
        }
    }

    /**
     * Request near sector.
     *
     * @param nearSector the near sector
     */
    public void requestNearSector(List<ViewPosition> nearSector) {
        if (this.type == NetTypeRequest.NEARSECTOR) {
            this.nearSector = nearSector;
            this.validReciveMessage = true;
        }
    }

    /**
     * Gets the player.
     *
     * @return the player
     */
    public ViewPlayer getPlayer() {
        return player;
    }

    /**
     * Gets the deck.
     *
     * @return the deck
     */
    public List<ViewCard> getDeck() {
        return deck;
    }

    /**
     * Gets the serialversionuid.
     *
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public NetTypeRequest getType() {
        return type;
    }

    /**
     * Checks if is valid recive message.
     *
     * @return true, if is valid recive message
     */
    public boolean isValidReciveMessage() {
        return validReciveMessage;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Checks if is done move.
     *
     * @return true, if is done move
     */
    public boolean isDoneMove() {
        return doneMove;
    }

    /**
     * Gets the chat.
     *
     * @return the chat
     */
    public List<ViewMessage> getChat() {
        return chat;
    }

    /**
     * Gets the maps.
     *
     * @return the maps
     */
    public List<ViewMap> getMaps() {
        return maps;
    }

    /**
     * Gets the given token.
     *
     * @return the given token
     */
    public UUID getGivenToken() {
        return givenToken;
    }

    /**
     * Gets the flag.
     *
     * @return the flag
     */
    public flagResponseType getFlag() {
        return flag;
    }

    /**
     * Gets the players.
     *
     * @return the players
     */
    public List<ViewPlayer> getPlayers() {
        return players;
    }

    /**
     * Gets the player client.
     *
     * @return the player client
     */
    public ViewPlayer getPlayerClient() {
        return player;
    }

    /**
     * Gets the position.
     *
     * @return the position
     */
    public ViewPosition getPosition() {
        return position;
    }

    /**
     * Gets the near sector.
     *
     * @return the near sector
     */
    public List<ViewPosition> getNearSector() {
        return nearSector;
    }

    /**
     * Response info finish game.
     *
     * @param players the players
     */
    public void responseInfoFinishGame(List<ViewPlayer> players) {
        if (this.type == NetTypeRequest.FINISHGAME) {
            this.validReciveMessage = true;
            this.players = players;
        }

    }

}
