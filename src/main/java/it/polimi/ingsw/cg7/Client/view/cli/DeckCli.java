package it.polimi.ingsw.cg7.Client.view.cli;

import it.polimi.ingsw.cg7.Client.controller.InterfaceControl;
import it.polimi.ingsw.cg7.Client.model.NetResponseMessage;
import it.polimi.ingsw.cg7.Client.model.ViewCard;
import it.polimi.ingsw.cg7.Client.view.viewable.GameContainer;
import it.polimi.ingsw.cg7.Client.view.viewable.ShowDeck;

import java.util.List;
import java.util.Scanner;

/**
 * The Class DeckCli.
 */
public class DeckCli implements ShowDeck {

    /** The my deck. */
    private List<ViewCard> myDeck;
    
    /** The input. */
    private static Scanner input = new Scanner(System.in);
    
    /** The interface control. */
    private static InterfaceControl interfaceControl;
    
    /** The game view. */
    private static GameContainer gameView;

    /**
     * Instantiates a new  view deck cli.
     *
     * @param gameView the game view
     */
    @SuppressWarnings("static-access")
    public DeckCli(GameContainer gameView) {

        this.gameView = gameView;
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowDeck#getCard()
     */
    public void getCard() {
        String choose;
        boolean validationFlag;
        ViewCard card;
        if (myDeck.size() == 0)
            System.out.println("You don have cards");
        else {
            do {
                validationFlag = false;
                System.out.println("");
                System.out.println("Your Cards:");
                for (ViewCard c : myDeck)
                    System.out.println(c.getName());
                System.out.println("BACK <-");
                System.out.println("Choose Card or Back");
                choose = input.next();
                String firstLetter = choose.substring(0, 1);
                firstLetter = firstLetter.toUpperCase();
                String OtherLetter = choose.substring(1);
                OtherLetter = OtherLetter.toLowerCase();
                choose = firstLetter + OtherLetter;
                card = new ViewCard(choose);
                for (ViewCard c : myDeck)
                    if (c.getName().equals(card.getName()))
                        validationFlag = true;
                if (choose.equals("Back"))
                    validationFlag = true;
            } while (!validationFlag);
            if ((new ViewCard(choose)).getName().equals("Spotlight"))
                gameView.clickPositionSpotlight();
            else if (!choose.equals("Back")) {
                NetResponseMessage r = interfaceControl.useCard(card, null);
                if (r.isDoneMove())
                    useCard(card);
                else
                    gameView.print(r.getDescription());
            }
        }
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowDeck#useCard(it.polimi.ingsw.cg7.Client.model.ViewCard)
     */
    public void useCard(ViewCard card) {

        System.out.println("");
        System.out.println("You use " + card.getName() + " card");

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowDeck#updateDeck(java.util.List)
     */
    public void updateDeck(List<ViewCard> requestDeck) {
        this.myDeck = requestDeck;

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowDeck#showCurrentSituation()
     */
    public void showCurrentSituation() {
        if (myDeck != null) {
            System.out.println("");
            System.out.println("You have this cards:");
            for (ViewCard card : myDeck)
                System.out.println(card.getName());
        }
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowDeck#addCard(it.polimi.ingsw.cg7.Client.model.ViewCard)
     */
    public void addCard(ViewCard card) {

        myDeck.add(card);

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowDeck#setInterfaceControl(it.polimi.ingsw.cg7.Client.controller.InterfaceControl)
     */
    @SuppressWarnings("static-access")
    public void setInterfaceControl(InterfaceControl interfaceControl) {
        this.interfaceControl = interfaceControl;

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowDeck#deckFull()
     */
    public boolean deckFull() {
        if (myDeck.size() > 3)
            return true;
        return false;
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowDeck#discard()
     */
    public void discard() {

        String choose;
        boolean validationFlag;
        boolean discard;
        ViewCard card;
        do {
            discard = true;
            do {
                validationFlag = false;
                System.out.println("");
                System.out.println("Your Cards:");
                for (ViewCard c : myDeck)
                    System.out.println(c.getName());
                System.out.println("Choose Card");
                choose = input.next();
                String firstLetter = choose.substring(0, 1);
                firstLetter = firstLetter.toUpperCase();
                String OtherLetter = choose.substring(1);
                OtherLetter = OtherLetter.toLowerCase();
                choose = firstLetter + OtherLetter;
                card = new ViewCard(choose);
                for (ViewCard c : myDeck)
                    if (c.getName().equals(card.getName()))
                        validationFlag = true;
            } while (!validationFlag);
            NetResponseMessage r = interfaceControl.discard(card);
            gameView.print(r.getDescription());
            if (!r.isDoneMove())
                discard = false;
        } while (!discard);
    }
}
