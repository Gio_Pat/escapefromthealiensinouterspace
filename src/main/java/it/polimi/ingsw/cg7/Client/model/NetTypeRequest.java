package it.polimi.ingsw.cg7.Client.model;

import java.io.Serializable;
/**
 * Requests Types
 * @author Debora Rebai & Giovanni Patruno
 *
 */
public enum NetTypeRequest implements Serializable {

    DRAW, SUBSCRIBING, MOVEMENT, USEITEM, DISCARD, ATTACK, MESSAGE, CHAT, FINISHTURN, SENDFALSERUMORS, MAPS, TURN, DECK, PLAYERS, CLIENT, POSITION, NEARSECTOR, FINISHGAME;

}
