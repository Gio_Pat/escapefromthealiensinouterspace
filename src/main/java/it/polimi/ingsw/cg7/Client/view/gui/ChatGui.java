package it.polimi.ingsw.cg7.Client.view.gui;

import it.polimi.ingsw.cg7.Client.controller.InterfaceControl;
import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;
import it.polimi.ingsw.cg7.Client.model.ViewMessage;
import it.polimi.ingsw.cg7.Client.view.viewable.ShowChat;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * The Class ChatGui.
 */
public class ChatGui extends JPanel implements ShowChat {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
    
    /** The image button chat. */
    private static BufferedImage backgroundChat, imageButtonChat;
    
    /** The Constant newLine. */
    private final static String newLine = "\n";
    
    /** The background. */
    private TexturePaint background;
    
    /** The button chat. */
    private JLabel buttonChat;
    
    /** The send message. */
    private JTextArea sendMessage;
    
    /** The your message. */
    private JTextField yourMessage;
    
    /** The chat pane. */
    private JTextArea chatPane;
    
    /** The scroll. */
    private JScrollPane scroll;
    
    /** The height. */
    private double width, height;
    
    /** The interface control. */
    private InterfaceControl interfaceControl;
    
    /** The username. */
    private String username;

    /**
     * Instantiates a new chat gui.
     *
     * @throws InvocationTargetException the invocation target exception
     * @throws InterruptedException the interrupted exception
     */
    public ChatGui() throws InvocationTargetException, InterruptedException {

        Toolkit t = Toolkit.getDefaultToolkit();
        Dimension screenSize = t.getScreenSize();
        width = screenSize.getWidth();
        height = screenSize.getHeight();
        this.username = null;
        setLayout(null);
        try {
            imageButtonChat = ImageIO
                    .read(new File(
                            ".//Game Media//Interface Media//GameContainer//BottoneChat.png"));

        } catch (IOException e) {
            Logger.getLogger("Mouse clicked").log(Level.SEVERE, null, e);
        }

        try {
            backgroundChat = ImageIO
                    .read(new File(
                            ".//Game Media//Interface Media//GameContainer//ChatFrame.png"));
        } catch (IOException e) {
            Logger.getLogger("Mouse clicked").log(Level.SEVERE, null, e);
        }

        initComponents();

    }

    /* (non-Javadoc)
     * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
     */
    protected void paintComponent(Graphics g) {

        Graphics2D g2d = (Graphics2D) g.create();

        super.paintComponent(g);
        super.setSize((int) width / 5, (int) (height * (0.55)));

        g2d.drawRect(0, 0, (int) width / 5, (int) (height * (0.55)));
        background = new TexturePaint(backgroundChat, new Rectangle(0, 0,
                (int) width / 5, (int) (height * (0.55))));
        g2d.setPaint(background);
        g2d.fill(new Rectangle(0, 0, (int) width / 5, (int) (height * (0.55))));

    }

    /**
     * Event message.
     */
    private void eventMessage() {
        if (!yourMessage.getText().equals("")) {
            ViewMessage msg = new ViewMessage(username, new Date(),
                    yourMessage.getText());
            if (interfaceControl != null) {
                interfaceControl.sendMessage(msg);
                yourMessage.setText("");
            } else
                yourMessage.setText("Interface Control null");

            repaint();
        }

    }

    /**
     * Inits the components.
     */
    private void initComponents() {
        Image resizePlayer = imageButtonChat.getScaledInstance(
                (int) (height / 10), (int) (height / 10), Image.SCALE_DEFAULT);
        buttonChat = new JLabel(new ImageIcon(resizePlayer));
        buttonChat.setSize((int) (height / 10), (int) (height / 10));
        buttonChat.setLocation((int) (0.039 * height), (int) (0.039 * height));
        buttonChat.addMouseListener(new MouseListener() {

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
      
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                eventMessage();

            }

        });

        add(buttonChat);

        sendMessage = new JTextArea("SEND");
        sendMessage.setSize((int) (0.052 * height), (int) (0.026 * height));
        sendMessage.setOpaque(false);
        sendMessage.setFont(new Font("Century Gothic", 0,
                (int) (0.0208 * height)));
        sendMessage.setForeground(Color.CYAN);
        sendMessage.setLocation((int) (0.156 * height), (int) (0.078 * height));
        sendMessage.setEditable(false);

        add(sendMessage);

        yourMessage = new JTextField("");
        yourMessage.setSize((int) (0.25 * height), (int) (0.026 * height));
        yourMessage.setOpaque(false);
        yourMessage.setForeground(Color.CYAN);
        yourMessage.setLocation((int) (0.039 * height), (int) (0.15 * height));
        yourMessage.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    eventMessage();
                }
            }

            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        });
        add(yourMessage);

        chatPane = new JTextArea("");
        chatPane.setSize((int) (0.25 * height), (int) (0.25 * height));
        chatPane.setOpaque(false);
        chatPane.setBorder(null);
        chatPane.setEditable(false);

        scroll = new JScrollPane(chatPane);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scroll.setBorder(null);

        scroll.setSize(190, (int) (0.25 * height));
        scroll.setOpaque(false);
        scroll.getViewport().setOpaque(false);
        scroll.getViewport().setBorder(null);

        scroll.setForeground(Color.CYAN);
        chatPane.setForeground(Color.CYAN);
        chatPane.setEditable(false);
        chatPane.setLineWrap(true);
        chatPane.append("");
        scroll.setLocation((int) (0.039 * height), (int) (0.208 * height));
        add(scroll);

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowChat#getMessage(java.lang.String)
     */
    @Override
    public void getMessage(String username) {
        // DO NOTHING

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowChat#printMessage(it.polimi.ingsw.cg7.Client.model.NetBrokerMessage)
     */
    @Override
    public void printMessage(NetBrokerMessage msg) {
        String chat = chatPane.getText();
        chat = chat.concat(newLine + "[" + msg.getDate() + "]" + newLine + "["
                + msg.getNick() + "]" + newLine + msg.getText() + newLine);
        chatPane.setText(chat);
        repaint();
        // scrollToBotton();
        chatPane.setCaretPosition(chatPane.getDocument().getLength());

    }

    /**
     * Scroll to botton.
     */
    @SuppressWarnings("unused")
    private void scrollToBotton() {

        JScrollBar scrollbar = scroll.getVerticalScrollBar();

        int maximum = scrollbar.getMaximum();
        int visibleAmount = scrollbar.getVisibleAmount();

        scrollbar.setValue(maximum + visibleAmount);
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowChat#setInterfaceControl(it.polimi.ingsw.cg7.Client.controller.InterfaceControl)
     */
    @Override
    public void setInterfaceControl(InterfaceControl interfaceControl) {
        this.interfaceControl = interfaceControl;

    }

    /**
     * Sets the username.
     *
     * @param username the new username
     */
    public void setUsername(String username) {
        this.username = username;

    }

}
