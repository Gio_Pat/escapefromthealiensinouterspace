package it.polimi.ingsw.cg7.Client.model;
/**
 * The flag response type
 * @author Giovanni Patruno
 *
 */
public enum flagResponseType {

    FALSE, TRUE, NOTDRAW, SILENCE, TRUERUMOR, FALSERUMOR, GREEN, RED, EMPTY;

}
