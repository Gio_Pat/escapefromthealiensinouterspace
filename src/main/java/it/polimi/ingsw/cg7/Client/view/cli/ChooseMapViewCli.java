package it.polimi.ingsw.cg7.Client.view.cli;

import it.polimi.ingsw.cg7.Client.controller.InterfaceControl;
import it.polimi.ingsw.cg7.Client.model.ViewMap;
import it.polimi.ingsw.cg7.Client.view.viewable.ShowChooseMap;

import java.util.List;
import java.util.Scanner;

/**
 * The Class ChooseMapViewCli.
 */
public class ChooseMapViewCli implements ShowChooseMap {

    /** The input. */
    private Scanner input = new Scanner(System.in);

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowChooseMap#chooseMap(it.polimi.ingsw.cg7.Client.controller.InterfaceControl)
     */
    public void chooseMap(InterfaceControl interfaceControl) {

        List<ViewMap> maps = interfaceControl.requestMap();
        String choose;
        boolean control;
        do {
            control = false;
            System.out.println("Choose your Map:");
            for (ViewMap m : maps)
                System.out.println(m.getName());
            choose = input.next();
            choose = choose.toUpperCase();
            for (ViewMap mControl : maps)
                if (mControl.getName().equals(choose))
                    control = true;
        } while (!control);
        String firstLetter = choose.substring(0, 1);
        firstLetter = firstLetter.toUpperCase();
        String OtherLetter = choose.substring(1);
        OtherLetter = OtherLetter.toLowerCase();
        choose = firstLetter + OtherLetter;
        interfaceControl.setMap(new ViewMap(choose));
    }
}
