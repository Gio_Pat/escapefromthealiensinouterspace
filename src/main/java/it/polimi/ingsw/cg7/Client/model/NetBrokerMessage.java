package it.polimi.ingsw.cg7.Client.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Class NetBrokerMessage.
 */
public class NetBrokerMessage implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The valid recive message. */
    private boolean validReciveMessage;

    /** The type. */
    private NetTypeBroker type;

    /** The message. */
    private String message;

    /** The nick. */
    private String nick;

    /** The date. */
    private Date date;

    /** The coord. */
    private ViewPosition coord;

    /** The item. */
    private ViewCard item;

    /** The players. */
    private List<ViewPlayer> players;

    /** The player. */
    private ViewPlayer player;

    /** The players light. */
    private Map<ViewPlayer, ViewPosition> playersLight;

    /**
     * Gets the players light.
     *
     * @return the players light
     */
    public Map<ViewPlayer, ViewPosition> getPlayersLight() {
        return playersLight;
    }

    /** The type sector. */
    private flagResponseType typeSector;

    private boolean escape;

    /**
     * Instantiates a new net broker message.
     *
     * @param type
     *            the type
     */
    public NetBrokerMessage(NetTypeBroker type) {
        this.type = type;
        this.validReciveMessage = false;
        this.coord = null;
        this.item = null;
        this.validReciveMessage = false;
        this.players = new ArrayList<ViewPlayer>();
        this.typeSector = flagResponseType.FALSE;
    }

    /**
     * Sets the type sector.
     *
     * @param typeSector
     *            the new type sector
     */
    public void setTypeSector(flagResponseType typeSector) {
        this.typeSector = typeSector;
        this.validReciveMessage = true;
    }

    public void sendSilence(ViewPlayer player) {
        if (this.type == NetTypeBroker.SILENCE) {
            this.validReciveMessage = true;
            this.player = player;
        }

    }

    public void hatchCard(ViewPlayer player, ViewPosition position,
            boolean green) {
        if (this.type == NetTypeBroker.ESCAPE) {
            this.validReciveMessage = true;
            this.player = player;
            this.coord = position;
            this.escape = green;

        }
    }

    /**
     * Turn.
     */
    public void turn() {
        if (this.type == NetTypeBroker.TURN)
            this.validReciveMessage = true;
    }

    public void playerDraw(ViewPlayer player) {
        if (this.type == NetTypeBroker.PLAYERDRAW) {
            this.validReciveMessage = true;
            this.player = player;
        }

    }

    /**
     * Attack.
     *
     * @param coord
     *            the coord
     * @param player
     *            the player
     */
    public void attack(ViewPosition coord, ViewPlayer player) {
        if (this.type == NetTypeBroker.ATTACK) {
            this.validReciveMessage = true;
            this.player = player;
            this.coord = coord;
        }
    }

    /**
     * Role player.
     *
     * @param player
     *            the player
     */
    public void rolePlayer(ViewPlayer player) {
        if (this.type == NetTypeBroker.ROLEPLAYER) {
            this.validReciveMessage = true;
            this.player = player;
        }
    }

    /**
     * Inits the game.
     */
    public void initGame() {
        if (this.type == NetTypeBroker.INITGAME)
            this.validReciveMessage = true;
    }

    /**
     * Recive card.
     *
     * @param item
     *            the item
     */
    public void reciveCard(ViewCard item) {
        if (this.type == NetTypeBroker.RECIVECARD) {
            this.item = item;
            this.validReciveMessage = true;

        }
    }

    /**
     * Players dead.
     *
     * @param players
     *            the players
     */
    public void playersDead(List<ViewPlayer> players) {
        if (this.type == NetTypeBroker.PLAYERSDEAD) {
            this.players = players;
            this.validReciveMessage = true;
        }
    }

    /**
     * Send message.
     *
     * @param message
     *            the message
     * @param username
     *            the username
     * @param date
     *            the date
     */
    public void sendMessage(String message, String username, Date date) {
        if (this.type == NetTypeBroker.MESSAGE) {
            this.message = message;
            this.nick = username;
            this.date = date;
            this.validReciveMessage = true;
        }
    }

    /**
     * Player rumor.
     *
     * @param coord
     *            the coord
     * @param player
     *            the player
     */
    public void playerRumor(ViewPosition coord, ViewPlayer player) {
        if (this.type == NetTypeBroker.PLAYERRUMORS) {
            this.coord = coord;
            this.player = player;
            this.validReciveMessage = true;
        }
    }

    /**
     * Player escape.
     *
     * @param player
     *            the player
     * @param coord
     *            the coord
     */
    public void playerEscape(ViewPlayer player, ViewPosition coord) {
        if (this.type == NetTypeBroker.PLAYERESCAPE) {
            this.player = player;
            this.coord = coord;
            this.validReciveMessage = true;
        }
    }

    /**
     * Finish game.
     */
    public void finishGame() {
        if (this.type == NetTypeBroker.FINISHGAME)
            this.validReciveMessage = true;
    }

    /**
     * Player turn.
     *
     * @param player
     *            the player
     */
    public void playerTurn(ViewPlayer player) {
        if (this.type == NetTypeBroker.PLAYERTURN) {
            this.player = player;
            this.validReciveMessage = true;
        }
    }

    /**
     * Players light.
     *
     * @param playersLight
     *            the players light
     */
    public void playersLight(Map<ViewPlayer, ViewPosition> playersLight,
            ViewPosition position) {
        if (this.type == NetTypeBroker.PLAYERSLIGHT) {
            this.playersLight = playersLight;
            this.coord = position;
            this.validReciveMessage = true;
        }
    }

    /**
     * Checks if is valid recive message.
     *
     * @return true, if is valid recive message
     */
    public boolean isValidReciveMessage() {
        return validReciveMessage;
    }

    /**
     * Gets the sector.
     *
     * @return the sector
     */
    public ViewPosition getSector() {
        return this.coord;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public NetTypeBroker getType() {
        return this.type;
    }

    /**
     * Gets the item.
     *
     * @return the item
     */
    public ViewCard getItem() {
        return item;
    }

    /**
     * Gets the players.
     *
     * @return the players
     */
    public List<ViewPlayer> getPlayers() {
        return players;
    }

    /**
     * Gets the player.
     *
     * @return the player
     */
    public ViewPlayer getPlayer() {
        return player;
    }

    /**
     * Gets the nick.
     *
     * @return the nick
     */
    public String getNick() {
        return this.nick;
    }

    /**
     * Gets the type sector.
     *
     * @return the type sector
     */
    public flagResponseType getTypeSector() {
        return typeSector;
    }

    /**
     * Gets the date.
     *
     * @return the date
     */
    public String getDate() {
        return date.toString();
    }

    /**
     * Gets the text.
     *
     * @return the text
     */
    public String getText() {
        return message;
    }

    /**
     * Disconnect player.
     *
     * @param player
     *            the player
     */
    public void disconnectPlayer(ViewPlayer player) {
        if (this.type == NetTypeBroker.PLAYEROUT) {
            this.player = player;
            this.validReciveMessage = true;
        }

    }

    public boolean isEscape() {
        return escape;
    }
}
