package it.polimi.ingsw.cg7.Client.view.viewable;

import it.polimi.ingsw.cg7.Client.controller.InterfaceControl;
import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;


/**
 *  Show Chat class.
 *
 * @author Debora Rebai
 */

public interface ShowChat {

    /**
     * Gets the message.
     *
     * @param username the username
     * @return the message
     */
    public void getMessage(String username);

    /**
     * Prints the message.
     *
     * @param msg the msg
     */
    public void printMessage(NetBrokerMessage msg);

    /**
     * Sets the interface control.
     *
     * @param interfaceControl the new interface control
     */
    public void setInterfaceControl(InterfaceControl interfaceControl);

}
