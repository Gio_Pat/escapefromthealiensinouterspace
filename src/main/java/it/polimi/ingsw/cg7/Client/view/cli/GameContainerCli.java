package it.polimi.ingsw.cg7.Client.view.cli;

import it.polimi.ingsw.cg7.Client.controller.InterfaceControl;
import it.polimi.ingsw.cg7.Client.controller.SubInterface;
import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;
import it.polimi.ingsw.cg7.Client.model.NetResponseMessage;
import it.polimi.ingsw.cg7.Client.model.ViewCard;
import it.polimi.ingsw.cg7.Client.model.ViewPlayer;
import it.polimi.ingsw.cg7.Client.model.ViewPosition;
import it.polimi.ingsw.cg7.Client.view.viewable.GameContainer;
import it.polimi.ingsw.cg7.model.CharType;

import java.util.List;
import java.util.Scanner;

/**
 * The Class GameContainerCli.
 */
public class GameContainerCli implements GameContainer {

    /** The map. */
    private MapCli map;

    /** The players. */
    private PlayersCli players;

    /** The deck. */
    private DeckCli deck;

    /** The chat. */
    private ChatCli chat;

    /** The player. */
    private PlayerCli player;

    /** The warning. */
    private WarningCli warning;

    /** The input. */
    private static Scanner input = new Scanner(System.in);

    /** The interface control. */
    private InterfaceControl interfaceControl;

    /** The subinterface. */
    private SubInterface subinterface;

    /** The number turn. */
    private int numberTurn;

    /**
     * Instantiates a new game container cli.
     */
    public GameContainerCli() {
        players = new PlayersCli();
        warning = new WarningCli(this);
        player = new PlayerCli();
        chat = new ChatCli();
        deck = new DeckCli(this);
        map = new MapCli(warning, player, deck);
        numberTurn = 0;

    }

    /**
     * update deck from server
     */
    @Override
    public synchronized void updateDeck(List<ViewCard> requestDeck) {
        deck.updateDeck(requestDeck);

    }

    /**
     * update players from server
     */
    @Override
    public synchronized void updateplayers(List<ViewPlayer> requestPlayers) {
        players.updatePlayers(requestPlayers);

    }

    /**
     * update players
     */
    @Override
    public synchronized void updateClient(ViewPlayer requestClient) {
        player.updateClient(requestClient);

    }

    /**
     * update player's position
     */
    @Override
    public synchronized void updatePosition(ViewPosition requestPosition) {
        map.updatePosition(requestPosition);

    }

    /**
     * create personality menu for alien or human
     */
    @Override
    public synchronized void showMenu() {

        if (deck.deckFull())
            discardOrUse();
        deck.updateDeck(interfaceControl.requestDeck());
        if (player.getPlayer().getType() == CharType.ALIEN)
            showAlienMenu();
        else
            showHumanMenu();
    }

    /**
     * Show human menu.
     */
    private synchronized void showHumanMenu() {

        String choose;
        do {
            System.out.println("");
            System.out.println("Your possibile move:");
            if (!player.getPlayer().isDoneMovement()) {
                System.out.println("MOVE : Digit MOVE");
            }
            System.out.println("USE CARD : Digit CARD");
            System.out.println("SEND MESSAGE: Digit MESSAGE");
            System.out.println("FINISH TURN : Digit FINISH");
            choose = input.next();
            choose = choose.toUpperCase();
        } while ((!choose.equals("MOVE")) && (!choose.equals("CARD"))
                && (!choose.equals("MESSAGE")) && (!choose.equals("FINISH")));

        chooseMove(choose);

    }

    /**
     * Discard or use.
     */
    private void discardOrUse() {
        String choose;
        if (player.getPlayer().getType() == CharType.HUMAN)
            do {
                System.out.println("");
                System.out.println("Your deck is full");
                System.out.println("You can:");
                System.out.println("DISCARD : Digit DISCARD");
                System.out.println("USE CARD : Digit USE");
                choose = input.next();
                choose = choose.toUpperCase();
            } while ((!choose.equals("DISCARD")) && (!choose.equals("USE")));
        else
            do {
                System.out.println("");
                System.out.println("Your deck is full");
                System.out.println("You can:");
                System.out.println("DISCARD : Digit DISCARD");
                choose = input.next();
                choose = choose.toUpperCase();
            } while (!choose.equals("DISCARD"));

        if (choose.equals("DISCARD"))
            deck.discard();
        else
            deck.getCard();

    }

    /**
     * Show alien menu.
     */
    private synchronized void showAlienMenu() {

        String choose;
        do {
            System.out.println("");
            System.out.println("Your possibile move:");
            if (!player.getPlayer().isDoneMovement()) {

                System.out.println("MOVE : Digit MOVE");
            }
            if (!player.getPlayer().isDoneAttackCard()) {

                System.out.println("ATTACK : Digit ATTACK");
            }
            System.out.println("SEND MESSAGE : Digit MESSAGE");
            System.out.println("FINISH TURN : Digit FINISH");
            choose = input.next();
            choose = choose.toUpperCase();
        } while ((!choose.equals("MOVE")) && (!choose.equals("ATTACK"))
                && (!choose.equals("MESSAGE")) && (!choose.equals("FINISH")));

        chooseMove(choose);

    }

    /**
     * Choose move.
     *
     * @param choose
     *            the choose
     */
    private void chooseMove(String choose) {
        NetResponseMessage msg;
        switch (choose) {
        case "MOVE": {
            movement();
            break;
        }
        case "ATTACK": {
            msg = interfaceControl.attack();
            if (msg.isDoneMove()) {
                effectAttack();
            }
            break;
        }
        case "CARD": {
            useCard();
            break;
        }
        case "MESSAGE": {
            readMessage();
            break;
        }
        case "FINISH": {
            msg = interfaceControl.finishTurn();
            if (msg.isDoneMove()) {
                System.out.println(msg.getDescription());
                subinterface.finishTurn();
            }
            break;
        }
        }

    }

    /**
     * request movement
     */
    @Override
    public synchronized void movement() {
        map.getPosition();
    }

    /**
     * request card from deck
     */
    @Override
    public synchronized void useCard() {
        deck.getCard();
    }

    /**
     * update chat messages
     */
    @Override
    public synchronized void readMessage() {
        chat.getMessage(player.getPlayer().getUsername());
    }

    /**
     * request attack in map
     */
    @Override
    public synchronized void effectAttack() {
        map.attack();

    }

    /**
     * see effect of card
     */
    @Override
    public synchronized void effectUseCard(ViewCard card) {
        deck.useCard(card);

    }

    /**
     * update and see all player's possible position
     */
    @Override
    public synchronized void updatePositions(
            List<ViewPosition> requestPossiblePosition) {
        map.updatePositions(requestPossiblePosition);
    }

    /**
     * update class and print current stats of the game
     */
    @Override
    public synchronized void showCurrentSituation() {

        player.showCurrentSituation();
        deck.showCurrentSituation();
        map.showCurrentSituation();
        players.showCurrentSituation();
        warning.print("Turn : " + numberTurn);

    }

    /**
     * request position for spotlight
     */
    @Override
    public synchronized void clickPositionSpotlight() {
        map.clickPositionSpotlight();
    }

    /**
     * request position for rumors
     */
    @Override
    public synchronized void effectRumors(ViewPosition position) {
        map.effectRumor(position);
    }

    /**
     * add card
     */
    @Override
    public synchronized void printCard(ViewCard card) {
        deck.addCard(card);

    }

    /**
     * print dead players
     */
    @Override
    public synchronized void playersDead(NetBrokerMessage msg) {
        warning.playersDead(msg);

    }

    /**
     * print player's rumor
     */
    @Override
    public synchronized void printRumors(NetBrokerMessage msg) {
        map.printRumors(msg);
    }

    /**
     * print escaped player
     */
    @Override
    public synchronized void printPlayerEscape(NetBrokerMessage msg) {
        warning.printEscape(msg);

    }

    /**
     * print current Player
     */
    @Override
    public synchronized void printPlayerTurn(NetBrokerMessage msg) {
        warning.printTurn(msg);

    }

    /**
     * shows the players after a spotlight
     */
    @Override
    public synchronized void printPlayersLight(NetBrokerMessage msg) {
        map.light(msg);
    }

    /**
     * show a generic message
     */
    @Override
    public synchronized void print(String string) {
        warning.print(string);

    }

    /**
     * print chat message
     */
    @Override
    public synchronized void printMessage(NetBrokerMessage msg) {
        chat.printMessage(msg);

    }

    /**
     * show shows the player that attacked and the sector
     */
    @Override
    public void printAttack(NetBrokerMessage msg) {
        map.attack(msg);

    }

    /**
     * set interface control
     */
    @Override
    public void setInterfaceControl(InterfaceControl interfaceControl) {
        this.interfaceControl = interfaceControl;
        warning.setInterfaceControl(interfaceControl);
        chat.setInterfaceControl(interfaceControl);
        deck.setInterfaceControl(interfaceControl);
        map.setInterfaceControl(interfaceControl);

    }

    /**
     * request position for movement
     */
    @Override
    public void effectMovement(ViewPosition position) {
        map.movement(position);

    }

    /**
     * request false rumor
     */
    @Override
    public void falseRumor() {
        map.getRumor();
    }

    /**
     * set subinterface
     */
    @Override
    public void setSubInterface(SubInterface subinterface) {
        this.subinterface = subinterface;

    }

    /**
     * Show final stats of the game
     */
    @Override
    public void finishGame() {
        ViewPlayer msg = interfaceControl.requestClient();
        if (msg != null)
            warning.setState(msg.getPlayingState());
        warning.finishGame();

    }

    /*
     * (non-Javadoc)
     * 
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#update()
     */
    @Override
    public void update() {
        // TODO Auto-generated method stub
    }

    /*
     * (non-Javadoc)
     * 
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#discard()
     */
    @Override
    public void discard() {
        // TODO Auto-generated method stub

    }

    /**
     * show current player
     */
    @Override
    public void updateTurn(ViewPlayer requestClient) {
        warning.print("Turn of " + requestClient.getUsername());

    }

    /*
     * (non-Javadoc)
     * 
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#setType()
     */
    @Override
    public void setType() {
        // TODO Auto-generated method stub

    }

    /**
     * add turn
     */
    @Override
    public void addTurn() {
        numberTurn++;

    }

    /*
     * (non-Javadoc)
     * 
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#start()
     */
    @Override
    public void start() {
        // TODO Auto-generated method stub

    }

}
