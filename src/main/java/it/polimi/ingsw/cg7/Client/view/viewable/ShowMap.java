package it.polimi.ingsw.cg7.Client.view.viewable;

import it.polimi.ingsw.cg7.Client.controller.InterfaceControl;
import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;
import it.polimi.ingsw.cg7.Client.model.ViewPosition;

import java.util.List;

/**
 * The Interface ShowMap.
 */
public interface ShowMap {

    /**
     * Attack.
     */
    public void attack();

    /**
     * Gets the position.
     *
     * @return the position
     */
    public void getPosition();

    /**
     * Movement.
     *
     * @param position the position
     */
    public void movement(ViewPosition position);

    /**
     * Update positions.
     *
     * @param possiblePosition the possible position
     */
    public void updatePositions(List<ViewPosition> possiblePosition);

    /**
     * Update position.
     *
     * @param requestPosition the request position
     */
    public void updatePosition(ViewPosition requestPosition);

    /**
     * Show current situation.
     */
    public void showCurrentSituation();

    /**
     * Click position spotlight.
     */
    public void clickPositionSpotlight();

    /**
     * Gets the rumor.
     *
     * @return the rumor
     */
    public void getRumor();

    /**
     * Effect rumor.
     *
     * @param position the position
     */
    public void effectRumor(ViewPosition position);

    /**
     * Prints the rumors.
     *
     * @param msg the msg
     */
    public void printRumors(NetBrokerMessage msg);

    /**
     * Light.
     *
     * @param msg the msg
     */
    public void light(NetBrokerMessage msg);

    /**
     * Attack.
     *
     * @param msg the msg
     */
    public void attack(NetBrokerMessage msg);

    /**
     * Sets the interface control.
     *
     * @param interfaceControl the new interface control
     */
    public void setInterfaceControl(InterfaceControl interfaceControl);
}
