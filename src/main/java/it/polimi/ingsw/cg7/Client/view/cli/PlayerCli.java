package it.polimi.ingsw.cg7.Client.view.cli;

import it.polimi.ingsw.cg7.Client.model.ViewPlayer;
import it.polimi.ingsw.cg7.Client.view.viewable.ShowPlayer;

import java.util.List;

/**
 * The Class PlayerCli.
 * @author Debora Rebai
 */
public class PlayerCli implements ShowPlayer {

    /** The player. */
    private ViewPlayer player;
    
    /** The players. */
    @SuppressWarnings("unused")
    private List<ViewPlayer> players;

    /**
     * Instantiates a new player cli.
     */
    public PlayerCli() {
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowPlayer#getPlayer()
     */
    public ViewPlayer getPlayer() {
        return player;
    }

    /**
     * Update players.
     *
     * @param requestPlayers the request players
     */
    public void updatePlayers(List<ViewPlayer> requestPlayers) {
        this.players = requestPlayers;

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowPlayer#updateClient(it.polimi.ingsw.cg7.Client.model.ViewPlayer)
     */
    public void updateClient(ViewPlayer requestClient) {
        this.player = requestClient;

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowPlayer#showCurrentSituation()
     */
    public void showCurrentSituation() {
        if (player != null) {
            System.out.println(player.getUsername() + " you are an "
                    + player.getType());
            System.out.println("You did " + player.getKillNumber() + " kill");
        }

    }

}
