package it.polimi.ingsw.cg7.Client.view.gui;

import it.polimi.ingsw.cg7.Client.controller.InterfaceControl;
import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;
import it.polimi.ingsw.cg7.Client.model.NetResponseMessage;
import it.polimi.ingsw.cg7.Client.model.ViewCard;
import it.polimi.ingsw.cg7.Client.model.ViewPosition;
import it.polimi.ingsw.cg7.Client.model.ViewSector;
import it.polimi.ingsw.cg7.Client.model.ViewSectorType;
import it.polimi.ingsw.cg7.Client.model.flagResponseType;
import it.polimi.ingsw.cg7.Client.view.viewable.ShowMap;
import it.polimi.ingsw.cg7.model.CharType;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JPanel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * The Class MapGui.
 */
public class MapGui extends JPanel implements ShowMap {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The size. */
    private static int size;

    /** The width. */
    private static int width;

    /** The height. */
    private static int height;


    /** The height screen. */
    private static int heightScreen;

    /** The action listener. */
    private static MouseListener actionListener;

    /** The sectors. */
    private static ArrayList<ViewSector> sectors;

    /** The underlined sectors. */
    private ArrayList<Cell> underlinedSectors;
    
    /**  Player position. */
    private Cell myPos;
    
    /**  Clicked Cell *. */
    private Cell clickedSector;

    /** The map cells. */
    private ArrayList<Cell> mapCells;

    /** The current position. */
    @SuppressWarnings("unused")
    private ViewPosition currentPosition;

    /** The new position. */
    private ViewPosition newPosition;

    /** The human start coord. */
    private static Coord alienStartCoord, humanStartCoord;

    /** The set position. */
    private boolean setPosition;

    /** The rumor. */
    private boolean rumor;

    /** The spotlight. */
    private boolean spotlight;

    /** The interface control. */
    private InterfaceControl interfaceControl;

    /** The deck. */
    private DeckGui deck;

    /** The player type. */
    @SuppressWarnings("unused")
    private CharType playerType;

    /**
     * Instantiates a new map gui.
     *
     * @param map            the map
     * @param deck the deck
     */
    public MapGui(String map, DeckGui deck) {
        // JFrame frame = new JFrame("Map");
        this.deck = deck;
        this.underlinedSectors = new ArrayList<Cell>();
        this.mapCells = new ArrayList<Cell>();
        setPosition = false;
        actionListener = new MouseListener() {
            public void mouseClicked(MouseEvent arg0) {

                Cell c = (Cell) (arg0.getComponent());
                if (clickedSector == null) {
                    createClickedSector(c);
                } else {
                    reposClickedSector(c);
                }
                if (!setPosition) {
                    setPosition = true;
                    newPosition = new ViewPosition(c.getXCell(), c.getYCell());

                } else {
                    if (newPosition.getX() == c.getXCell()
                            && newPosition.getY() == c.getYCell()) {

                        if (rumor)
                            sendRumor();

                        else if (spotlight)
                            sendCardSpotlight(new ViewPosition(
                                    newPosition.getX(), newPosition.getY()));
                        else
                            requestMovement();
                        newPosition = null;
                        setPosition = false;

                    } else
                        newPosition = new ViewPosition(c.getXCell(),
                                c.getYCell());
                }

            }

            public void mouseEntered(MouseEvent arg0) {
                // TODO Auto-generated method stub

            }

            public void mouseExited(MouseEvent arg0) {
                // TODO Auto-generated method stub

            }

            public void mousePressed(MouseEvent arg0) {
                // TODO Auto-generated method stub

            }

            public void mouseReleased(MouseEvent arg0) {
                // TODO Auto-generated method stub

            }

        };
        Toolkit t = Toolkit.getDefaultToolkit();
        Dimension screenSize = t.getScreenSize();
        heightScreen = (int) screenSize.getHeight();

        size = (int) (heightScreen * (0.026041));
        width = size * 2;
        height = (int) ((Math.sqrt(3) * 0.5) * size * 2);

        this.setLayout(null);
        this.setSize(40 * size, (int) ((14.5) * height));
        sectors = new ArrayList<ViewSector>();
        parseMap(map);
        for (ViewSector viewSector : sectors) {
            Cell cell = new Cell(new Coord(size, size), viewSector.getType(),
                    viewSector.getxLett(), viewSector.getY());
            cell.setVisible(true);
            cell.addMouseListener(actionListener);
            cell.setLocation(
                    (int) (25 + (viewSector.getxLett() * 0.75 * width)),
                    (int) ((10 + height * viewSector.getY()) + (0.5 * height * (viewSector
                            .getxLett() % 2))));
            this.mapCells.add(cell);
            this.add(cell);
        }
        // frame.pack();
        //
        // frame.setVisible(true);
    }

    /**
     *  Creates the underlined clicked sector *.
     *
     * @param c the c
     */
    private void createClickedSector(Cell c) {
        clickedSector = new Cell(new Coord(size, size), ViewSectorType.CLICKED,
                c.getXCell(), c.getYCell());
        clickedSector.setVisible(true);

        clickedSector.setLocation(c.getBounds().x, c.getBounds().y);
        this.add(clickedSector);
        this.setComponentZOrder(clickedSector, 0);
        repaint();
    }

    /**
     * Moves the clicked sector.
     *
     * @param c the c
     */
    private void reposClickedSector(Cell c) {
        clickedSector.setLocation(c.getBounds().x, c.getBounds().y);
        this.setComponentZOrder(clickedSector, 0);
        repaint();
    }

    /**
     * Creates the cell that represent player.
     *
     * @param type the type
     */
    public void initPos(CharType type) {
        Cell tempCell;
        Cell currentCell;
        if (type == CharType.ALIEN) {
            currentCell = getCell(alienStartCoord);
            tempCell = new Cell(new Coord(size, size),
                    ViewSectorType.MYPOSALIEN, alienStartCoord.getX(),
                    alienStartCoord.getY());
        } else {
            currentCell = getCell(humanStartCoord);
            tempCell = new Cell(new Coord(size, size),
                    ViewSectorType.MYPOSHUMAN, humanStartCoord.getX(),
                    humanStartCoord.getY());
        }
        tempCell.setVisible(true);

        tempCell.setLocation(currentCell.getBounds().x,
                currentCell.getBounds().y);
        this.add(tempCell);
        this.setComponentZOrder(tempCell, 0);
        myPos = tempCell;
        repaint();
    }

    /**
     * Moves the player in a specific coord.
     *
     * @param pos the pos
     */
    public void movePlayer(Coord pos) {
        Cell currentCell = getCell(pos);
        myPos.setLocation(currentCell.getBounds().x, currentCell.getBounds().y);
        this.setComponentZOrder(myPos, 0);

        repaint();
    }

    /**
     * Send card spotlight.
     *
     * @param viewPosition the view position
     */
    protected void sendCardSpotlight(ViewPosition viewPosition) {
        NetResponseMessage msg = interfaceControl.useCard(new ViewCard(
                "Spotlight"), viewPosition);
        if (msg.isDoneMove())
            deck.updateDeck(interfaceControl.requestDeck());

        WarningGui warning = null;
        try {
            warning = new WarningGui();
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        warning.print(msg.getDescription());

        spotlight = false;
    }

    /**
     * Instantiates a new map gui.
     *
     * @param deck the deck
     */
    public MapGui(DeckGui deck) {
        this("Galilei", deck);
    }

    /**
     * Parses the map.
     *
     * @param name
     *            the name
     */
    private static void parseMap(String name) {
        Document docMap = getDocument(name);
        @SuppressWarnings("unused")
        Element root = docMap.getDocumentElement();
        // this.name = root.getAttribute("name");
        NodeList sectorsNode = docMap.getElementsByTagName("sector");
        for (int i = 0; i < sectorsNode.getLength(); i++) {
            Node sectorElement = sectorsNode.item(i);
            if (sectorElement.getNodeType() == Node.ELEMENT_NODE) {
                Element eSector = (Element) sectorElement;
                Coord currCoord = new Coord(Integer.parseInt(eSector
                        .getAttribute("xLett")), Integer.parseInt(eSector
                        .getAttribute("yNumb")));
                ViewSector currSector = createViewSector(
                        eSector.getAttribute("type"), currCoord);
                if (currSector != null) {
                    sectors.add(currSector);
                }

            }
        }
    }

    /**
     * Request movement.
     */
    private void requestMovement() {
        NetResponseMessage msg = interfaceControl.movement(newPosition);
        if (msg.isDoneMove()) {
            updatePositions(interfaceControl.requestPossiblePosition());
            movePlayer(new Coord(newPosition.getX(), newPosition.getY()));
            currentPosition = new ViewPosition(newPosition.getX(),
                    newPosition.getY());
            if (!(msg.getFlag() == flagResponseType.TRUE))
                actionPostMovement(msg);
            else
                request();
        }
    }

    /**
     * Request.
     */
    private void request() {
        WarningGui warning = null;
        warning = new WarningGui(this);

        if (interfaceControl.requestClient().getType() == CharType.ALIEN)

            warning.chooseAlien();
        else
            warning.chooseHuman();

    }

    /**
     * Send rumor.
     */
    private void sendRumor() {
        NetResponseMessage msg = interfaceControl.sendFalseRumor(newPosition);
        if (!msg.isDoneMove())
            message(msg.getDescription());
        else
            this.rumor = false;
    }

    /**
     * Action post movement.
     *
     * @param msg the msg
     */
    private void actionPostMovement(NetResponseMessage msg) {

        if (msg.getFlag() == flagResponseType.FALSERUMOR) {
            message(msg.getDescription());
            message("You have to say one sector");
            this.rumor = true;
        } else {
            message(msg.getDescription());
        }

    }

    /**
     * Message.
     *
     * @param description the description
     */
    private void message(String description) {
        WarningGui warning = null;
        try {
            warning = new WarningGui();
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        warning.print(description);

    }

    // Get document from the XML file name
    /**
     * Gets the document.
     *
     * @param name
     *            the name
     * @return the document
     */
    private static Document getDocument(String name) {
        File file = new File(".//XmlMaps//" + name + ".xml");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document docMap = builder.parse(file);
            docMap.getDocumentElement().normalize();
            return docMap;
        } catch (SAXException | IOException | ParserConfigurationException e) {
            Logger.getLogger("marshalling error").log(Level.SEVERE, null, e);
            return null;
        }
    }

    // Gived the type name in the xml file, create the right sector
    /**
     * Creates the sector.
     *
     * @param s
     *            the s
     * @param currCoord
     *            the curr coord
     * @return the sector
     */
    private static ViewSector createViewSector(String s, Coord currCoord) {
        switch (s) {
        case "HUMANSTARTPOINT": {
            humanStartCoord = currCoord;
            return new ViewSector(currCoord.getX(), currCoord.getY(),
                    ViewSectorType.HUMANSTARTPOINT);
        }
        case "ALIENSTARTPOINT": {
            alienStartCoord = currCoord;
            return new ViewSector(currCoord.getX(), currCoord.getY(),
                    ViewSectorType.ALIENSTARTPOINT);
        }
        case "SAFE":
            return new ViewSector(currCoord.getX(), currCoord.getY(),
                    ViewSectorType.SAFE);
        case "HATCH":
            return new ViewSector(currCoord.getX(), currCoord.getY(),
                    ViewSectorType.HATCH);
        case "DANGEROUS":
            return new ViewSector(currCoord.getX(), currCoord.getY(),
                    ViewSectorType.DANGEROUS);
        default:
            return null;
        }
    }

    /**
     * this method underline all the sectors that have these coordinates.
     *
     * @param coordinates
     *            the coords
     */
    private void underlineSectors(List<Coord> coordinates) {
        cleanAllUnderlined();
        for (Coord c : coordinates) {
            Cell currentCell = getCell(c);
            if (currentCell != null) {
                Cell tempCell;
                tempCell = new Cell(new Coord(size, size),
                        ViewSectorType.UNDERLINEDSECTOR,
                        currentCell.getXCell(), currentCell.getYCell());
                tempCell.setVisible(true);
                tempCell.addMouseListener(actionListener);
                tempCell.setLocation(currentCell.getBounds().x,
                        currentCell.getBounds().y);
                this.add(tempCell);
                this.setComponentZOrder(tempCell, 0);
                int zOrder = currentCell.getComponentCount();
                this.setComponentZOrder(currentCell, zOrder);
                this.underlinedSectors.add(tempCell);
                repaint();
            }
        }
    }

    /**
     * this method return the Cell with that coordinate.
     *
     * @param coord
     *            the coord
     * @return the view sector
     */
    private Cell getCell(Coord coord) {
        for (Cell c : mapCells)
            if (c.getXCell() == coord.getX() && c.getYCell() == coord.getY())
                return c;
        return null;
    }

    /**
     * this method all ViewSectors Underlined.
     */
    private synchronized void cleanAllUnderlined() {
        for (Cell c : underlinedSectors)
            if (c.getType() != ViewSectorType.CLICKED)
                remove(c);
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
     */
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        super.setSize(40 * size, 16 * height);
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowMap#attack()
     */
    @Override
    public void attack() {
        interfaceControl.attack();
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowMap#getPosition()
     */
    @Override
    public void getPosition() {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowMap#movement(it.polimi.ingsw.cg7.Client.model.ViewPosition)
     */
    @Override
    public void movement(ViewPosition position) {
        this.currentPosition = new ViewPosition(position.getX(),
                position.getY());
        movePlayer(new Coord(position.getX(), position.getY()));
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowMap#updatePositions(java.util.List)
     */
    @Override
    public void updatePositions(List<ViewPosition> possiblePosition) {
        List<Coord> coordinates = new ArrayList<Coord>();
        for (ViewPosition p : possiblePosition)
            coordinates.add(new Coord(p.getX(), p.getY()));
        underlineSectors(coordinates);

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowMap#updatePosition(it.polimi.ingsw.cg7.Client.model.ViewPosition)
     */
    @Override
    public void updatePosition(ViewPosition requestPosition) {
        this.currentPosition = requestPosition;

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowMap#showCurrentSituation()
     */
    @Override
    public void showCurrentSituation() {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowMap#clickPositionSpotlight()
     */
    @Override
    public void clickPositionSpotlight() {
        this.spotlight = true;

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowMap#getRumor()
     */
    @Override
    public void getRumor() {

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowMap#effectRumor(it.polimi.ingsw.cg7.Client.model.ViewPosition)
     */
    @Override
    public void effectRumor(ViewPosition position) {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowMap#printRumors(it.polimi.ingsw.cg7.Client.model.NetBrokerMessage)
     */
    @Override
    public void printRumors(NetBrokerMessage msg) {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowMap#light(it.polimi.ingsw.cg7.Client.model.NetBrokerMessage)
     */
    @Override
    public void light(NetBrokerMessage msg) {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowMap#attack(it.polimi.ingsw.cg7.Client.model.NetBrokerMessage)
     */
    @Override
    public void attack(NetBrokerMessage msg) {
        interfaceControl.attack();
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowMap#setInterfaceControl(it.polimi.ingsw.cg7.Client.controller.InterfaceControl)
     */
    @Override
    public void setInterfaceControl(InterfaceControl interfaceControl) {
        this.interfaceControl = interfaceControl;

    }

    /**
     * Draw.
     */
    public void draw() {
        NetResponseMessage msg = interfaceControl.drawCard();
        if (msg.isDoneMove()) {
            if (msg.getFlag() == flagResponseType.FALSERUMOR) {
                message(msg.getDescription());
                message("You have to say one sector");
                this.rumor = true;
                deck.updateDeck(interfaceControl.requestDeck());
            }

        } else {
            WarningGui warning = null;
            try {
                warning = new WarningGui();
            } catch (IOException e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
            warning.print(msg.getDescription());
        }

    }

    /**
     * Use.
     *
     * @param mustUseItem the must use item
     */
    public void use(boolean mustUseItem) {
        if (mustUseItem)
            deck.useCard(new ViewCard("Sedatives"));
        else {
            NetResponseMessage msg = interfaceControl.drawCard();
            message(msg.getDescription());
        }
        deck.updateDeck(interfaceControl.requestDeck());
    }
}
