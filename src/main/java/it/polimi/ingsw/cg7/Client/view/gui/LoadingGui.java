package it.polimi.ingsw.cg7.Client.view.gui;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.net.MalformedURLException;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * The Class LoadingGui.
 */
public class LoadingGui extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /** The f. */
    private JFrame f;
    
    /** The image. */
    private Image image;

    /**
     * Instantiates a new loading gui.
     *
     * @param image the image
     */
    public LoadingGui(Image image) {
        super();
        this.image = image;
    }

    /* (non-Javadoc)
     * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(this.image, 0, 0, getWidth(), getHeight(), this);
    }

    /**
     * Instantiates a new loading gui.
     *
     * @throws MalformedURLException the malformed url exception
     */
    public LoadingGui() throws MalformedURLException {

        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                f = new JFrame("Looking for a Match - EftAioS");
                f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                f.setLocationByPlatform(true);
                f.setSize(800, 600);
                f.setPreferredSize(new Dimension(800, 600));

                Image image = f.getToolkit().createImage(
                        ".//Game Media//Interface Media//Menu//loading.gif");
                LoadingGui imagePanel = new LoadingGui(image);

                f.setContentPane(imagePanel);
                f.pack();
                f.setVisible(true);
            }
        });
    }

    /**
     * Close.
     */
    public void close() {
        f.dispose();
    }
}
