package it.polimi.ingsw.cg7.Client.view.gui;

import it.polimi.ingsw.cg7.Client.controller.InterfaceControl;
import it.polimi.ingsw.cg7.Client.model.ViewMap;
import it.polimi.ingsw.cg7.Client.view.viewable.ShowChooseMap;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SpringLayout;

/**
 * The Class ChoosingMapMenu.
 * @author Debora Rebai, Giovanni Patruno
 */
public class ChoosingMapMenu implements ShowChooseMap {
    
    /** The previews maps**/
    private BufferedImage background, Galilei, Galvani, Fermi;
    
    /** The Mouse listener */
    private MouseListener ms;
    
    /** The frame. */
    private JFrame frame;
    
    /** The interface control. */
    private InterfaceControl interfaceControl;
    
    /** The height. */
    private double width, height;

    /**
     * Instantiates a new choosing map menu.
     */
    public ChoosingMapMenu() {

        Toolkit t = Toolkit.getDefaultToolkit();
        Dimension screenSize = t.getScreenSize();

        width = screenSize.getWidth();
        height = screenSize.getHeight();

        frame = new JFrame("EftAioS - GUI");

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setPreferredSize(new Dimension((int) (width / 2),
                (int) (height * (0.8))));
        // frame.getContentPane().setLayout(null);
        frame.getContentPane().setSize((int) (width / 2),
                (int) (height * (0.8)));
        frame.setLocation((int) (width / 4), (int) (height / 9));

        frame.pack();

        ms = new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                interfaceControl.setMap(new ViewMap(arg0.getComponent()
                        .getName()));
                nextWindow();
            }

            @Override
            public void mouseEntered(MouseEvent arg0) {

            }

            @Override
            public void mouseExited(MouseEvent arg0) {
            }

            @Override
            public void mousePressed(MouseEvent arg0) {

            }

            @Override
            public void mouseReleased(MouseEvent arg0) {

            }

        };
        loadContents(frame);

    }

    /**
     * Next window.
     */
    private void nextWindow() {
        synchronized (this) {
            notifyAll();
        }
        frame.dispose();
    }

    /**
     * Load contents.
     *
     * @param frame the frame
     */
    public void loadContents(JFrame frame) {
        try {
            background = ImageIO.read(new File(
                    ".//Game Media//Interface Media//Menu//Background.jpg"));
            Galilei = ImageIO.read(new File(
                    ".//Game Media//Interface Media//Menu//Galilei.jpg"));
            Galvani = ImageIO.read(new File(
                    ".//Game Media//Interface Media//Menu//Galvani.jpg"));
            Fermi = ImageIO.read(new File(
                    ".//Game Media//Interface Media//Menu//Fermi.jpg"));
            // mainLogo = ImageIO.read(new
            // File(".//Game Media//Interface Media//Menu//EFTAIOS-Portada.jpg"));
            Image imgResize = Galvani.getScaledInstance((int) (width / 14),
                    (int) (width / 14), Image.SCALE_DEFAULT);
            JLabel galvaniLbl = new JLabel(new ImageIcon(imgResize));
            imgResize = Galilei.getScaledInstance((int) (width / 14),
                    (int) (width / 14), Image.SCALE_DEFAULT);
            JLabel galileiLbl = new JLabel(new ImageIcon(imgResize));
            imgResize = Fermi.getScaledInstance((int) (width / 14),
                    (int) (width / 14), Image.SCALE_DEFAULT);
            JLabel fermiLbl = new JLabel(new ImageIcon(imgResize));

            SpringLayout springLayout = new SpringLayout();
            frame.getContentPane().setLayout(springLayout);

            galileiLbl.setSize((int) (width / 14), (int) (width / 14));
            galileiLbl.addMouseListener(ms);
            galileiLbl.setToolTipText("Galilei");
            galileiLbl.setName("Galilei");

            galvaniLbl.setSize((int) (width / 14), (int) (width / 14));
            galvaniLbl.addMouseListener(ms);
            galvaniLbl.setToolTipText("Galvani");
            galvaniLbl.setName("Galvani");

            fermiLbl.setSize((int) (width / 14), (int) (width / 14));
            fermiLbl.addMouseListener(ms);
            fermiLbl.setToolTipText("Fermi");
            fermiLbl.setName("Fermi");

            springLayout.putConstraint(SpringLayout.SOUTH, fermiLbl,
                    -((int) (height / 16)), SpringLayout.SOUTH,
                    frame.getContentPane());
            springLayout.putConstraint(SpringLayout.EAST, fermiLbl,
                    -((int) (width / 3)), SpringLayout.EAST,
                    frame.getContentPane());
            springLayout.putConstraint(SpringLayout.SOUTH, galvaniLbl,
                    -((int) (height / 16)), SpringLayout.SOUTH,
                    frame.getContentPane());
            springLayout.putConstraint(SpringLayout.EAST, galvaniLbl,
                    -((int) (width / 4) - (galvaniLbl.getWidth() / 2)),
                    SpringLayout.EAST, frame.getContentPane());
            springLayout.putConstraint(SpringLayout.SOUTH, galileiLbl,
                    -((int) (height / 16)), SpringLayout.SOUTH,
                    frame.getContentPane());
            springLayout.putConstraint(SpringLayout.WEST, galileiLbl,
                    ((int) (width / 3)), SpringLayout.WEST,
                    frame.getContentPane());

            frame.getContentPane().add(galvaniLbl);
            frame.getContentPane().add(galileiLbl);
            frame.getContentPane().add(fermiLbl);

            Image backgroundResize = background.getScaledInstance(
                    (int) (width / 2), (int) (height * (0.8)),
                    Image.SCALE_DEFAULT);

            JLabel backgroundLbl = new JLabel(new ImageIcon(backgroundResize));
            backgroundLbl.setAlignmentX(Component.CENTER_ALIGNMENT);

            springLayout.putConstraint(SpringLayout.NORTH, backgroundLbl, 0,
                    SpringLayout.NORTH, frame.getContentPane());
            springLayout.putConstraint(SpringLayout.WEST, backgroundLbl, 0,
                    SpringLayout.WEST, frame.getContentPane());
            springLayout.putConstraint(SpringLayout.SOUTH, backgroundLbl, 0,
                    SpringLayout.SOUTH, frame.getContentPane());
            springLayout.putConstraint(SpringLayout.EAST, backgroundLbl, 0,
                    SpringLayout.EAST, frame.getContentPane());

            frame.getContentPane().add(backgroundLbl);

            // frame.getContentPane().add(new JLabel(new ImageIcon(mainLogo)));
        } catch (IOException e) {

            Logger.getLogger("Mouse clicked").log(Level.SEVERE, null, e);
        }
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowChooseMap#chooseMap(it.polimi.ingsw.cg7.Client.controller.InterfaceControl)
     */
    @Override
    public void chooseMap(InterfaceControl interfaceControl) {
        this.interfaceControl = interfaceControl;
        frame.setVisible(true);

    }
}
