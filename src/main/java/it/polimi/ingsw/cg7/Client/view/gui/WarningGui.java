package it.polimi.ingsw.cg7.Client.view.gui;

import it.polimi.ingsw.cg7.Client.controller.InterfaceControl;
import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;
import it.polimi.ingsw.cg7.Client.model.NetResponseMessage;
import it.polimi.ingsw.cg7.Client.model.ViewPlayer;
import it.polimi.ingsw.cg7.Client.model.ViewPosition;
import it.polimi.ingsw.cg7.Client.view.viewable.ShowWarning;
import it.polimi.ingsw.cg7.helpers.LetterNumbConverter;
import it.polimi.ingsw.cg7.model.CharType;
import it.polimi.ingsw.cg7.model.PlayingState;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.SpringLayout;

/**
 * The Class WarningGui.
 */
public class WarningGui extends JFrame implements ShowWarning {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The game view. */
    private GameContainerGui gameView;

    /** The width. */
    private double width;

    /** The height. */
    private double height;

    /** The background. */
    private BufferedImage background, imgQuestion;

    /** The backgroundlb. */
    private JLabel backgroundlb;

    /** The l. */
    private SpringLayout l;

    /** The interface control. */
    private InterfaceControl interfaceControl;

    /** The playing state. */
    private PlayingState playingState;

    /** The view players. */
    private List<ViewPlayer> viewPlayers;

    /** The username. */
    private String username;

    /** The map gui. */
    private MapGui mapGui;

    /** The char type. */
    private CharType charType;

    /** The new line. */
    private String newLine = "\n";

    private Clip clip;

    /**
     * Instantiates a new warning gui.
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public WarningGui() throws IOException {
        // this.gameView = gameView;
        Toolkit t = Toolkit.getDefaultToolkit();
        Dimension screenSize = t.getScreenSize();

        width = screenSize.getWidth();
        height = screenSize.getHeight();

        l = new SpringLayout();
        getContentPane().setLayout(l);

        setLocation((int) (width / 4.5), (int) (height / 4));
        setAlwaysOnTop(true);

    }

    /**
     * Instantiates a new warning gui.
     *
     * @param mapGui
     *            the map gui
     */
    public WarningGui(MapGui mapGui) {
        Toolkit t = Toolkit.getDefaultToolkit();
        Dimension screenSize = t.getScreenSize();

        width = screenSize.getWidth();
        height = screenSize.getHeight();

        l = new SpringLayout();
        getContentPane().setLayout(l);

        setLocation((int) (width / 4.5), (int) (height / 4));
        this.mapGui = mapGui;
    }

    /**
     * Instantiates a new warning gui.
     *
     * @param gameContainerGui
     *            the game container gui
     */

    public WarningGui(GameContainerGui gameView) {

        this.gameView = gameView;
        Toolkit t = Toolkit.getDefaultToolkit();
        Dimension screenSize = t.getScreenSize();

        width = screenSize.getWidth();
        height = screenSize.getHeight();

        l = new SpringLayout();

        getContentPane().setLayout(l);
        setLocation((int) (width / 4.5), (int) (height / 4));
    }

    /*
     * (non-Javadoc)
     * 
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowWarning#chooseAlien()
     */
    @Override
    public void chooseAlien() {

        JButton attack = new JButton("ATTACK");
        attack.setSize((int) (0.1302 * height), (int) (0.0651 * height));
        attack.setOpaque(false);
        attack.setFont(new Font("Century Gothic", 0, (int) (0.0208 * height)));
        attack.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                mapGui.attack();
                dispose();

            }
        });

        add(attack);

        JButton draw = new JButton("DRAW");
        draw.setSize((int) (0.1302 * height), (int) (0.0651 * height));
        draw.setOpaque(false);
        draw.setFont(new Font("Century Gothic", 0, (int) (0.0208 * height)));
        draw.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                mapGui.draw();
                dispose();
            }
        });

        add(draw);

        JTextArea chooseAlien = new JTextArea("You want to draw or attack ?");
        chooseAlien.setSize((int) (0.1302 * height), (int) (0.052 * height));
        chooseAlien.setOpaque(false);
        chooseAlien.setFont(new Font("Century Gothic", 0,
                (int) (0.026 * height)));
        chooseAlien.setForeground(Color.CYAN);
        chooseAlien.setEditable(false);
        add(chooseAlien);
        Image imgResize = imgQuestion.getScaledInstance(
                (int) (0.1302 * height), (int) (0.1302 * height),
                Image.SCALE_DEFAULT);
        JLabel imgLogo = new JLabel(new ImageIcon(imgResize));
        imgLogo.setSize((int) (0.1302 * height), (int) (0.1302 * height));

        this.add(imgLogo);
        Image backgroundResize = background.getScaledInstance(
                (int) (1.041 * height), (int) (0.52 * height),
                Image.SCALE_DEFAULT);
        backgroundlb = new JLabel(new ImageIcon(backgroundResize));
        getContentPane().add(backgroundlb);
        setSize((int) (1.041 * height), (int) (0.52 * height));
        l.putConstraint(SpringLayout.WEST, imgLogo, (int) (0.78 * height),
                SpringLayout.WEST, backgroundlb);
        l.putConstraint(SpringLayout.NORTH, imgLogo, (int) (0.195 * height),
                SpringLayout.NORTH, backgroundlb);
        l.putConstraint(SpringLayout.NORTH, chooseAlien,
                (int) (0.195 * height), SpringLayout.NORTH, backgroundlb);
        l.putConstraint(SpringLayout.WEST, chooseAlien, (int) (0.195 * height),
                SpringLayout.WEST, backgroundlb);
        l.putConstraint(SpringLayout.NORTH, attack, (int) (0.26 * height),
                SpringLayout.NORTH, backgroundlb);
        l.putConstraint(SpringLayout.WEST, attack, (int) (0.195 * height),
                SpringLayout.WEST, backgroundlb);
        l.putConstraint(SpringLayout.NORTH, draw, (int) (0.26 * height),
                SpringLayout.NORTH, backgroundlb);
        l.putConstraint(SpringLayout.WEST, draw, (int) (0.455 * height),
                SpringLayout.WEST, backgroundlb);
        this.setVisible(true);

    }

    /*
     * (non-Javadoc)
     * 
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowWarning#chooseHuman()
     */
    @Override
    public void chooseHuman() {

        JButton use = new JButton("USE");
        use.setSize((int) (0.13 * height), (int) (0.065 * height));
        use.setOpaque(false);
        use.setFont(new Font("Century Gothic", 0, (int) (0.0208 * height)));
        use.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                mapGui.use(true);
                dispose();

            }
        });

        add(use);

        JButton noUse = new JButton("DON'T USE");
        noUse.setSize((int) (0.13 * height), (int) (0.065 * height));
        noUse.setOpaque(false);
        noUse.setFont(new Font("Century Gothic", 0, (int) (0.0208 * height)));
        noUse.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                mapGui.use(false);
                dispose();
            }
        });

        add(noUse);

        JTextArea chooseHuman = new JTextArea("You have a Sedatives card");
        chooseHuman.setSize((int) (0.13 * height), (int) (0.52 * height));
        chooseHuman.setOpaque(false);
        chooseHuman.setFont(new Font("Century Gothic", 0,
                (int) (0.026 * height)));
        chooseHuman.setForeground(Color.CYAN);
        chooseHuman.setEditable(false);
        add(chooseHuman);

        BufferedImage imgQuestion = null;
        try {
            imgQuestion = ImageIO.read(new File(
                    ".//Game Media//Warning//Domanda.png"));
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        Image imgResize = imgQuestion.getScaledInstance((int) (0.13 * height),
                (int) (0.13 * height), Image.SCALE_DEFAULT);
        JLabel imgLogo = new JLabel(new ImageIcon(imgResize));
        imgLogo.setSize((int) (0.13 * height), (int) (0.13 * height));

        this.add(imgLogo);

        try {
            background = ImageIO.read(new File(
                    ".//Game Media//Warning/warningDesktop.jpg"));
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        Image backgroundResize = background.getScaledInstance(
                (int) (1.041 * height), (int) (0.52 * height),
                Image.SCALE_DEFAULT);
        backgroundlb = new JLabel(new ImageIcon(backgroundResize));
        getContentPane().add(backgroundlb);
        setSize((int) (1.041 * height), (int) (0.52 * height));
        l.putConstraint(SpringLayout.WEST, imgLogo, (int) (0.78 * height),
                SpringLayout.WEST, backgroundlb);
        l.putConstraint(SpringLayout.NORTH, imgLogo, (int) (0.195 * height),
                SpringLayout.NORTH, backgroundlb);
        l.putConstraint(SpringLayout.NORTH, chooseHuman,
                (int) (0.195 * height), SpringLayout.NORTH, backgroundlb);
        l.putConstraint(SpringLayout.WEST, chooseHuman, (int) (0.195 * height),
                SpringLayout.WEST, backgroundlb);
        l.putConstraint(SpringLayout.NORTH, use, (int) (0.26 * height),
                SpringLayout.NORTH, backgroundlb);
        l.putConstraint(SpringLayout.WEST, use, (int) (0.195 * height),
                SpringLayout.WEST, backgroundlb);
        l.putConstraint(SpringLayout.NORTH, noUse, (int) (0.26 * height),
                SpringLayout.NORTH, backgroundlb);
        l.putConstraint(SpringLayout.WEST, noUse, (int) (0.39 * height),
                SpringLayout.WEST, backgroundlb);
        this.setVisible(true);

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * it.polimi.ingsw.cg7.Client.view.viewable.ShowWarning#print(java.lang.
     * String)
     */
    @Override
    public void print(String string) {

        JButton exit = new JButton("OK");
        exit.setSize((int) (0.13 * height), (int) (0.065 * height));
        exit.setOpaque(false);
        exit.setFont(new Font("Century Gothic", 0, (int) (0.0208 * height)));
        exit.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                dispose();
            }
        });

        add(exit);

        JTextArea information = new JTextArea(string);
        information.setLineWrap(true);
        information.setSize((int) (0.52 * height), (int) (0.065 * height));
        information.setOpaque(false);
        information.setFont(new Font("Century Gothic", 0,
                (int) (0.0208 * height)));
        information.setForeground(Color.CYAN);
        information.setEditable(false);
        add(information);

        BufferedImage imgQuestion = null;
        try {
            imgQuestion = ImageIO.read(new File(
                    ".//Game Media//Warning//Info.png"));
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        Image imgResize = imgQuestion.getScaledInstance((int) (0.13 * height),
                (int) (0.13 * height), Image.SCALE_DEFAULT);
        JLabel imgLogo = new JLabel(new ImageIcon(imgResize));
        imgLogo.setSize((int) (0.13 * height), (int) (0.13 * height));

        this.add(imgLogo);

        try {
            background = ImageIO.read(new File(
                    ".//Game Media//Warning/warningDesktop.jpg"));
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        Image backgroundResize = background.getScaledInstance(
                (int) (1.041 * height), (int) (0.52 * height),
                Image.SCALE_DEFAULT);
        backgroundlb = new JLabel(new ImageIcon(backgroundResize));
        getContentPane().add(backgroundlb);
        setSize((int) (1.041 * height), (int) (0.52 * height));
        l.putConstraint(SpringLayout.WEST, imgLogo, (int) (0.78 * height),
                SpringLayout.WEST, backgroundlb);
        l.putConstraint(SpringLayout.NORTH, imgLogo, (int) (0.195 * height),
                SpringLayout.NORTH, backgroundlb);
        l.putConstraint(SpringLayout.NORTH, information,
                (int) (0.169 * height), SpringLayout.NORTH, backgroundlb);
        l.putConstraint(SpringLayout.WEST, information, (int) (0.195 * height),
                SpringLayout.WEST, backgroundlb);
        l.putConstraint(SpringLayout.NORTH, information,
                (int) (0.169 * height), SpringLayout.NORTH, backgroundlb);
        l.putConstraint(SpringLayout.NORTH, exit, (int) (0.325 * height),
                SpringLayout.NORTH, backgroundlb);
        l.putConstraint(SpringLayout.WEST, exit, (int) (0.39 * height),
                SpringLayout.WEST, backgroundlb);
        this.setVisible(true);

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * it.polimi.ingsw.cg7.Client.view.viewable.ShowWarning#setInterfaceControl
     * (it.polimi.ingsw.cg7.Client.controller.InterfaceControl)
     */
    @Override
    public void setInterfaceControl(InterfaceControl interfaceControl) {
        this.interfaceControl = interfaceControl;
    }

    /*
     * (non-Javadoc)
     * 
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowWarning#finishGame()
     */
    @Override
    public void finishGame() {
        NetResponseMessage msg = interfaceControl.infofinishGame();
        for (ViewPlayer p : msg.getPlayers())
            if (p.getUsername().equals(username))
                playingState = p.getPlayingState();
        createPlayers(msg);

        JButton okButton = new JButton();
        okButton.setText("EXIT");
        okButton.setSize((int) (0.13 * height), (int) (0.065 * height));
        okButton.setFont(new Font("Century Gothic", 0, (int) (0.0208 * height)));
        okButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        l.putConstraint(SpringLayout.NORTH, okButton, (int) (height
                - (height / 8) - height / 10), SpringLayout.NORTH, this);
        l.putConstraint(SpringLayout.WEST, okButton,
                (int) (width / 4 - (0.13 * 0.5 * height)), SpringLayout.WEST,
                this);
        add(okButton);
        createBackground();
        setVisible(true);

    }

    /**
     * Creates the players.
     *
     * @param msg
     *            the msg
     */
    private void createPlayers(NetResponseMessage msg) {

        int size = msg.getPlayers().size();
        viewPlayers = msg.getPlayers();
        for (int x = 0; x < size; x++) {
            BufferedImage imgPlayer = null;
            if (viewPlayers.get(x).getType() == CharType.ALIEN)
                try {
                    imgPlayer = ImageIO.read(new File(
                            ".//Game Media//Characters//ALIENLogo.png"));
                } catch (IOException e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                }
            else
                try {
                    imgPlayer = ImageIO.read(new File(
                            ".//Game Media//Characters//HUMANLogo.png"));
                } catch (IOException e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                }
            Image resizePlayer = imgPlayer.getScaledInstance(
                    (int) (height / 10), (int) (height / 10),
                    Image.SCALE_DEFAULT);
            JLabel leb = new JLabel(new ImageIcon(resizePlayer));
            leb.setSize((int) (0.013 * height), (int) (0.013 * height));
            JTextArea info = new JTextArea();
            info.setText(viewPlayers.get(x).getUsername() + "\n"
                    + viewPlayers.get(x).getPlayingState());
            info.setSize((int) (0.013 * height), (int) (0.013 * height));
            info.setOpaque(false);
            info.setFont(new Font("Century Gothic", 0, (int) (0.026 * height)));
            info.setEditable(false);
            l.putConstraint(SpringLayout.WEST, leb, (int) (0.065 * height)
                    + ((x % 2) * (int) (0.65 * height)), SpringLayout.WEST,
                    this);
            l.putConstraint(SpringLayout.NORTH, leb, (int) (0.19 * height)
                    + (x * (int) (0.091 * height)), SpringLayout.NORTH, this);
            l.putConstraint(SpringLayout.WEST, info, (int) (0.19 * height)
                    + ((x % 2) * (int) (0.39 * height)), SpringLayout.WEST,
                    this);
            l.putConstraint(SpringLayout.NORTH, info, (int) (0.19 * height)
                    + (x * (int) (0.091 * height)), SpringLayout.NORTH, this);
            getContentPane().add(leb);
            getContentPane().add(info);
        }

    }

    /**
     * Creates the background.
     */
    private void createBackground() {
        if (playingState == PlayingState.LOSER) {
            try {
                background = ImageIO.read(new File(
                        ".//Game Media//Warning/defeat.jpg"));
            } catch (IOException e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
            victoryMusic(false);
        } else {
            try {
                background = ImageIO.read(new File(
                        ".//Game Media//Warning/victory.jpg"));
            } catch (IOException e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
            victoryMusic(true);
        }
        Image backgroundResize = background.getScaledInstance((int) width / 2,
                (int) (height - height / 10), Image.SCALE_DEFAULT);

        backgroundlb = new JLabel(new ImageIcon(backgroundResize));
        getContentPane().add(backgroundlb);
        setSize((int) width / 2, (int) (height - height / 10));
        setLocation((int) width / 3, 0);

    }

    private void victoryMusic(boolean victory) {
        if (victory) {
            AudioInputStream audioInputStream;
            try {
                audioInputStream = AudioSystem.getAudioInputStream(new File(
                        ".//Game Media//Audio//victory.wav").getAbsoluteFile());
                clip = AudioSystem.getClip();
                clip.open(audioInputStream);
                clip.start();
            } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
        } else {
            AudioInputStream audioInputStream;
            try {
                audioInputStream = AudioSystem.getAudioInputStream(new File(
                        ".//Game Media//Audio//defeat.wav").getAbsoluteFile());
                clip = AudioSystem.getClip();
                clip.open(audioInputStream);
                clip.start();
            } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
                Logger.getLogger("Audio problem").log(Level.SEVERE, null, e);
            }
        }

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * it.polimi.ingsw.cg7.Client.view.viewable.ShowWarning#playersDead(it.polimi
     * .ingsw.cg7.Client.model.NetBrokerMessage)
     */
    @Override
    public void playersDead(NetBrokerMessage msg) {

        String s = new String("");
        s.concat("Players");
        for (ViewPlayer p : msg.getPlayers())
            s.concat(" " + p.getUsername());
        s.concat(" are dead");
        print(s);

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * it.polimi.ingsw.cg7.Client.view.viewable.ShowWarning#printEscape(it.polimi
     * .ingsw.cg7.Client.model.NetBrokerMessage)
     */
    @Override
    public void printEscape(NetBrokerMessage msg) {
        print("Player " + msg.getPlayer().getUsername() + " has escaped");

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * it.polimi.ingsw.cg7.Client.view.viewable.ShowWarning#printTurn(it.polimi
     * .ingsw.cg7.Client.model.NetBrokerMessage)
     */
    @Override
    public void printTurn(NetBrokerMessage msg) {
        print("It's turn of " + msg.getPlayer().getUsername());

    }

    /**
     * Prints the rumors.
     *
     * @param msg
     *            the msg
     */
    public void printRumors(NetBrokerMessage msg) {
        print("Rumor in "
                + LetterNumbConverter.intToString(msg.getSector().getX()) + " "
                + msg.getSector().getY() + " for Player "
                + msg.getPlayer().getUsername());

    }

    /**
     * Sets the player.
     *
     * @param player
     *            the new player
     */
    public void setPlayer(ViewPlayer player) {
        this.charType = player.getType();
        this.username = player.getUsername();

    }

    /**
     * Choose card.
     */
    @SuppressWarnings("static-access")
    public void chooseCard() {

        JButton use = new JButton("USE CARD");
        use.setSize((int) (0.13 * height), (int) (0.065 * height));
        use.setFont(new Font("Century Gothic", 0, (int) (0.0208 * height)));
        use.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                gameView.useCard();
                dispose();

            }
        });

        if (charType == charType.HUMAN)
            add(use);

        JButton noUse = new JButton("DISCARD");

        noUse.setSize((int) (0.13 * height), (int) (0.065 * height));
        noUse.setFont(new Font("Century Gothic", 0, (int) (0.0208 * height)));
        noUse.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                gameView.discard();
                dispose();
            }
        });

        add(noUse);

        JTextArea chooseHuman = new JTextArea("Your deck is full! Choose:");
        chooseHuman.setSize((int) (0.13 * height), (int) (0.052 * height));
        chooseHuman.setOpaque(false);
        chooseHuman.setFont(new Font("Century Gothic", 0,
                (int) (0.026 * height)));
        chooseHuman.setForeground(Color.CYAN);
        chooseHuman.setEditable(false);
        add(chooseHuman);


        loadContents();
    
        Image imgResize = imgQuestion.getScaledInstance((int) (0.13 * height),
                (int) (0.13 * height), Image.SCALE_DEFAULT);
        JLabel imgLogo = new JLabel(new ImageIcon(imgResize));
        imgLogo.setSize((int) (0.13 * height), (int) (0.13 * height));

        this.add(imgLogo);

        try {
            background = ImageIO.read(new File(
                    ".//Game Media//Warning/warningDesktop.jpg"));
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        Image backgroundResize = background.getScaledInstance(
                (int) (1.041 * height), (int) (0.53 * height),
                Image.SCALE_DEFAULT);
        backgroundlb = new JLabel(new ImageIcon(backgroundResize));
        getContentPane().add(backgroundlb);
        setSize((int) (1.041 * height), (int) (0.53 * height));
        l.putConstraint(SpringLayout.WEST, imgLogo, (int) (0.78 * height),
                SpringLayout.WEST, backgroundlb);
        l.putConstraint(SpringLayout.NORTH, imgLogo, (int) (0.195 * height),
                SpringLayout.NORTH, backgroundlb);
        l.putConstraint(SpringLayout.NORTH, chooseHuman,
                (int) (0.195 * height), SpringLayout.NORTH, backgroundlb);
        l.putConstraint(SpringLayout.WEST, chooseHuman, (int) (0.195 * height),
                SpringLayout.WEST, backgroundlb);
        if (charType == charType.HUMAN) {
            l.putConstraint(SpringLayout.NORTH, use, (int) (0.26 * height),
                    SpringLayout.NORTH, backgroundlb);
            l.putConstraint(SpringLayout.WEST, use, (int) (0.195 * height),
                    SpringLayout.WEST, backgroundlb);
            l.putConstraint(SpringLayout.NORTH, noUse, (int) (0.26 * height),
                    SpringLayout.NORTH, backgroundlb);
            l.putConstraint(SpringLayout.WEST, noUse, (int) (0.39 * height),
                    SpringLayout.WEST, backgroundlb);
        } else {
            l.putConstraint(SpringLayout.NORTH, noUse, (int) (0.26 * height),
                    SpringLayout.NORTH, backgroundlb);
            l.putConstraint(SpringLayout.WEST, noUse, (int) (0.29 * height),
                    SpringLayout.WEST, backgroundlb);
        }
        this.setVisible(true);

    }

    private void loadContents() {
        try {
            imgQuestion = ImageIO.read(new File(
                    ".//Game Media//Warning//Domanda.png"));
            background = ImageIO.read(new File(
                    ".//Game Media//Warning/warningDesktop.jpg"));
            
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }   
    }

    /**
     * Prints the light.
     *
     * @param msg
     *            the msg
     */
    public void printLight(NetBrokerMessage msg) {
        String s = new String("");
        s = s.concat("Light in sector "
                + LetterNumbConverter.intToString(msg.getSector().getX()) + " "
                + msg.getSector().getY() + ": " + newLine);
        if ((msg.getPlayersLight().size() == 0) || (msg.getPlayers() == null))
            s = s.concat("No nearby!");
        else {
            for (Map.Entry<ViewPlayer, ViewPosition> element : msg
                    .getPlayersLight().entrySet())

                s = s.concat(element.getKey().getUsername()
                        + " in "
                        + LetterNumbConverter.intToString(element.getValue()
                                .getX()) + " " + element.getValue().getY()
                        + newLine);
            print(s);

        }

    }

    public void option() {

        SpringLayout l = new SpringLayout();
        setLayout(l);
        JButton exit = new JButton();
        exit.setText("EXIT");
        exit.setSize((int) (0.13 * height), (int) (0.065 * height));
        exit.setOpaque(false);
        exit.setFont(new Font("Century Gothic", 0, (int) (0.0208 * height)));
        exit.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        add(exit);

        try {
            background = ImageIO.read(new File(
                    ".//Game Media//Warning/warningDesktop.jpg"));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Image backgroundResize = background.getScaledInstance(
                (int) (1.041 * height), (int) (0.52 * height),
                Image.SCALE_DEFAULT);
        backgroundlb = new JLabel(new ImageIcon(backgroundResize));
        getContentPane().add(backgroundlb);
        setSize((int) (1.041 * height), (int) (0.52 * height));
        l.putConstraint(SpringLayout.NORTH, exit, (int) (0.52 * 0.5 * height),
                SpringLayout.NORTH, backgroundlb);
        l.putConstraint(SpringLayout.WEST, exit,
                (int) (1.041 * 0.5 * height - (0.13 * 0.5 * height)),
                SpringLayout.WEST, backgroundlb);
        this.setVisible(true);

    }

    public void exit() {
        JButton attack = new JButton("BACK");
        attack.setSize((int) (0.1302 * height), (int) (0.0651 * height));
        attack.setOpaque(false);
        attack.setFont(new Font("Century Gothic", 0, (int) (0.0208 * height)));
        attack.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                dispose();

            }
        });

        add(attack);

        JButton draw = new JButton("EXIT");
        draw.setSize((int) (0.1302 * height), (int) (0.0651 * height));
        draw.setOpaque(false);
        draw.setFont(new Font("Century Gothic", 0, (int) (0.0208 * height)));
        draw.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                System.exit(0);
            }
        });

        add(draw);

        JTextArea chooseAlien = new JTextArea(
                " Are you sure you want to quit ?");
        chooseAlien.setSize((int) (0.1302 * height), (int) (0.052 * height));
        chooseAlien.setOpaque(false);
        chooseAlien.setFont(new Font("Century Gothic", 0,
                (int) (0.026 * height)));
        chooseAlien.setForeground(Color.CYAN);
        chooseAlien.setEditable(false);
        add(chooseAlien);

        BufferedImage imgQuestion = null;
        try {
            imgQuestion = ImageIO.read(new File(
                    ".//Game Media//Warning//Domanda.png"));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Image imgResize = imgQuestion.getScaledInstance(
                (int) (0.1302 * height), (int) (0.1302 * height),
                Image.SCALE_DEFAULT);
        JLabel imgLogo = new JLabel(new ImageIcon(imgResize));
        imgLogo.setSize((int) (0.1302 * height), (int) (0.1302 * height));

        this.add(imgLogo);

        try {
            background = ImageIO.read(new File(
                    ".//Game Media//Warning/warningDesktop.jpg"));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Image backgroundResize = background.getScaledInstance(
                (int) (1.041 * height), (int) (0.52 * height),
                Image.SCALE_DEFAULT);
        backgroundlb = new JLabel(new ImageIcon(backgroundResize));
        getContentPane().add(backgroundlb);
        setSize((int) (1.041 * height), (int) (0.52 * height));
        l.putConstraint(SpringLayout.WEST, imgLogo, (int) (0.78 * height),
                SpringLayout.WEST, backgroundlb);
        l.putConstraint(SpringLayout.NORTH, imgLogo, (int) (0.195 * height),
                SpringLayout.NORTH, backgroundlb);
        l.putConstraint(SpringLayout.NORTH, chooseAlien,
                (int) (0.195 * height), SpringLayout.NORTH, backgroundlb);
        l.putConstraint(SpringLayout.WEST, chooseAlien, (int) (0.195 * height),
                SpringLayout.WEST, backgroundlb);
        l.putConstraint(SpringLayout.NORTH, attack, (int) (0.26 * height),
                SpringLayout.NORTH, backgroundlb);
        l.putConstraint(SpringLayout.WEST, attack, (int) (0.195 * height),
                SpringLayout.WEST, backgroundlb);
        l.putConstraint(SpringLayout.NORTH, draw, (int) (0.26 * height),
                SpringLayout.NORTH, backgroundlb);
        l.putConstraint(SpringLayout.WEST, draw, (int) (0.455 * height),
                SpringLayout.WEST, backgroundlb);
        this.setVisible(true);

    }

}
