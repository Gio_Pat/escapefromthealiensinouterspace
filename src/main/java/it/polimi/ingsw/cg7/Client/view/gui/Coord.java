package it.polimi.ingsw.cg7.Client.view.gui;

/**
 * The Class Coord.
 */
public class Coord {
	
	/** The x. */
	private int x;
	
	/** The y. */
	private int y;
	
//	@Override
//	public boolean equals(Object coord)
//	{
//	    if(coord!=null && ((Coord)(coord)).getX() == this.x && ((Coord)(coord)).getY() == this.y)
//	        return true;
//	    else
//	        return false;
//	}
	
	/**
	 * Instantiates a new coord.
	 *
	 * @param x the x
	 * @param y the y
	 */
	public Coord(int x, int y)
	{
		this.x=x;
		this.y=y;
	}
	
	/**
	 * Gets the x.
	 *
	 * @return the x
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * Sets the x.
	 *
	 * @param x the new x
	 */
	public void setX(int x) {
		this.x = x;
	}
	
	/**
	 * Gets the y.
	 *
	 * @return the y
	 */
	public int getY() {
		return y;
	}
	
	/**
	 * Sets the y.
	 *
	 * @param y the new y
	 */
	public void setY(int y) {
		this.y = y;
	}
}
