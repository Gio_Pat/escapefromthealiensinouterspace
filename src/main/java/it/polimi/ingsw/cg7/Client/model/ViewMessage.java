package it.polimi.ingsw.cg7.Client.model;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class ViewMessage.
 * Authors: Debora Rebai
 */
public class ViewMessage implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
    
    /** The username. */
    private String username;
    
    /** The date. */
    private Date date;
    
    /** The message. */
    private String message;

    /**
     * Instantiates a new view message.
     *
     * @param username the username
     * @param date the date
     * @param message the message
     */
    public ViewMessage(String username, Date date, String message) {
        this.username = username;
        this.date = date;
        this.message = message;
    }

    /**
     * Gets the username.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the username.
     *
     * @param username the new username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets the date.
     *
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * Sets the date.
     *
     * @param date the new date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Gets the message.
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the message.
     *
     * @param message the new message
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
