package it.polimi.ingsw.cg7.Client.model;

/**
 * The Class ViewSector.
 * Authors: Debora Rebai & Giovanni Patruno
 */
public class ViewSector {
	
	/** The x lett. */
	private int xLett;
	
	/** The y. */
	private int y;
	
	/** The type. */
	private ViewSectorType type;
	
	/**
	 * Instantiates a new view sector.
	 *
	 * @param xLett the x lett
	 * @param y the y
	 * @param type the type
	 */
	public ViewSector(int xLett, int y, ViewSectorType type)
	{
		this.xLett = xLett;
		this.y = y;
		this.type = type;
	}
	
	/**
	 * Gets the x lett.
	 *
	 * @return the x lett
	 */
	public int getxLett() {
		return xLett;
	}
	
	/**
	 * Sets the x lett.
	 *
	 * @param xLett the new x lett
	 */
	public void setxLett(int xLett) {
		this.xLett = xLett;
	}
	
	/**
	 * Gets the y.
	 *
	 * @return the y
	 */
	public int getY() {
		return y;
	}
	
	/**
	 * Sets the y.
	 *
	 * @param y the new y
	 */
	public void setY(int y) {
		this.y = y;
	}
	
	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public ViewSectorType getType() {
		return type;
	}
	
	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(ViewSectorType type) {
		this.type = type;
	}
}
