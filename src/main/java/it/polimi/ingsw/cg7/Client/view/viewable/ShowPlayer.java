package it.polimi.ingsw.cg7.Client.view.viewable;

import it.polimi.ingsw.cg7.Client.model.ViewPlayer;

/**
 * The Interface ShowPlayer.
 */
public interface ShowPlayer {

    /**
     * Gets the player.
     *
     * @return the player
     */
    public ViewPlayer getPlayer();

    /**
     * Update client.
     *
     * @param requestClient the request client
     */
    public void updateClient(ViewPlayer requestClient);

    /**
     * Show current situation.
     */
    public void showCurrentSituation();

}
