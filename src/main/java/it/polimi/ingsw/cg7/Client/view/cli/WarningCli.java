package it.polimi.ingsw.cg7.Client.view.cli;

import it.polimi.ingsw.cg7.Client.controller.InterfaceControl;
import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;
import it.polimi.ingsw.cg7.Client.model.NetResponseMessage;
import it.polimi.ingsw.cg7.Client.model.ViewCard;
import it.polimi.ingsw.cg7.Client.model.ViewPlayer;
import it.polimi.ingsw.cg7.Client.model.flagResponseType;
import it.polimi.ingsw.cg7.Client.view.viewable.GameContainer;
import it.polimi.ingsw.cg7.helpers.LetterNumbConverter;
import it.polimi.ingsw.cg7.model.PlayingState;

import java.util.Scanner;

/**
 * The Class WarningCli.
 * 
 * @author Debora Rebai
 */
public class WarningCli {

    /** The input. */
    private static Scanner input = new Scanner(System.in);

    /** The interface control. */
    private static InterfaceControl interfaceControl;

    /** The game view. */
    private static GameContainer gameView;

    /** The state. */
    private PlayingState state;

    /**
     * Instantiates a new warning cli.
     *
     * @param gameView
     *            the game view
     */
    @SuppressWarnings("static-access")
    public WarningCli(GameContainer gameView) {
        this.gameView = gameView;
    }

    /**
     * Choose human move if human has a sedatives card
     */
    public void chooseHuman() {

        String choose;
        System.out.println("");
        do {
            System.out.println("You have a Sedatives card, you can:");
            System.out.println("USE : Digit Y");
            System.out.println("DON'T USE : Digit N");
            System.out.println("");
            choose = input.next();
            choose = choose.toUpperCase();
        } while ((!choose.equals("Y")) && (!choose.equals("N")));
        NetResponseMessage msg;
        switch (choose) {
        case "N": {
            msg = interfaceControl.drawCard();
            if (msg.isDoneMove())
                if (msg.getFlag() == flagResponseType.FALSERUMOR)
                    gameView.falseRumor();
            break;
        }
        case "Y": {
            msg = interfaceControl.useCard(new ViewCard("Sedatives"), null);
            if (msg.isDoneMove()) {
            }
            break;
        }

        }
    }

    /**
     * Choose alien move after movement
     */
    public void chooseAlien() {
        String choose;
        System.out.println("");
        do {
            System.out.println("You want to draw or attack ?");
            System.out.println("DRAW : Digit DRAW");
            System.out.println("ATTACK : Digit ATTACK");
            System.out.println("");
            choose = input.next();
            choose = choose.toUpperCase();
        } while ((!choose.equals("DRAW")) && (!choose.equals("ATTACK")));

        NetResponseMessage msg;
        switch (choose) {
        case "DRAW": {
            msg = interfaceControl.drawCard();
            if (msg.isDoneMove()) {
                if (msg.getFlag() == flagResponseType.FALSERUMOR)
                    gameView.falseRumor();
                if (msg.getFlag() == flagResponseType.EMPTY)
                    gameView.print("Deck is empty");
            }
            break;

        }

        case "ATTACK": {
            msg = interfaceControl.attack();
            if (msg.isDoneMove()) {
                gameView.effectAttack();
            }
            break;
        }

        }
    }

    /**
     * Prints the.
     *
     * @param string
     *            the string
     */
    public void print(String string) {
        System.out.println("");
        System.out.println(string);
        System.out.println("");
    }

    /**
     * Sets the interface control.
     *
     * @param interfaceControl
     *            the new interface control
     */
    @SuppressWarnings("static-access")
    public void setInterfaceControl(InterfaceControl interfaceControl) {
        this.interfaceControl = interfaceControl;

    }

    /**
     * Print summary on the players and the final states
     */
    public void finishGame() {
        NetResponseMessage r = interfaceControl.infofinishGame();
        System.out.println("Game is finished!");
        System.out.println("");
        if (state == PlayingState.LOSER)
            defeatAsciiArt();
        if (state == PlayingState.WINNER)
            victoryAsciiArt();
        System.out.println("");

        for (ViewPlayer p : r.getPlayers()) {
            System.out.println("Player " + p.getUsername());
            System.out.println("Type: " + p.getType());
            System.out.println("State: " + p.getPlayingState());
            System.out.println("");
        }

    }

    /**
     * Victory ascii art.
     */
    private void victoryAsciiArt() {
        System.out
                .println("██╗   ██╗██╗ ██████╗████████╗ ██████╗ ██████╗ ██╗   ██╗");
        System.out
                .println("██║   ██║██║██╔════╝╚══██╔══╝██╔═══██╗██╔══██╗╚██╗ ██╔╝");
        System.out
                .println("██║   ██║██║██║        ██║   ██║   ██║██████╔╝ ╚████╔╝ ");
        System.out
                .println("╚██╗ ██╔╝██║██║        ██║   ██║   ██║██╔══██╗  ╚██╔╝  ");
        System.out
                .println(" ╚████╔╝ ██║╚██████╗   ██║   ╚██████╔╝██║  ██║   ██║   ");
        System.out
                .println("  ╚═══╝  ╚═╝ ╚═════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ");

    }

    /**
     * Defeat ascii art.
     */
    private void defeatAsciiArt() {
        System.out.println("▓█████▄ ▓█████   █████▒▓█████ ▄▄▄     ▄▄▄█████▓");
        System.out.println("▒██▀ ██▌▓█   ▀ ▓██   ▒ ▓█   ▀▒████▄   ▓  ██▒ ▓▒");
        System.out.println("░██   █▌▒███   ▒████ ░ ▒███  ▒██  ▀█▄ ▒ ▓██░ ▒░");
        System.out.println("░▓█▄   ▌▒▓█  ▄ ░▓█▒  ░ ▒▓█  ▄░██▄▄▄▄██░ ▓██▓ ░ ");
        System.out.println("░▒████▓ ░▒████▒░▒█░    ░▒████▒▓█   ▓██▒ ▒██▒ ░ ");
        System.out.println(" ▒▒▓  ▒ ░░ ▒░ ░ ▒ ░    ░░ ▒░ ░▒▒   ▓▒█░ ▒ ░░   ");
        System.out.println(" ░ ▒  ▒  ░ ░  ░ ░       ░ ░  ░ ▒   ▒▒ ░   ░    ");
        System.out.println(" ░ ░  ░    ░    ░ ░       ░    ░   ▒    ░      ");
        System.out.println("   ░       ░  ░           ░  ░     ░  ░        ");
        System.out.println(" ░                                             ");

    }

    /**
     * Prints players dead.
     *
     * @param msg
     *            from server
     */
    public void playersDead(NetBrokerMessage msg) {

        System.out.println("");
        System.out.print("[");
        for (ViewPlayer p : msg.getPlayers())
            System.out.print("Player " + p.getUsername() + " is dead");
        System.out.println("]");

    }

    /**
     * Prints escaped players
     *
     * @param msg
     *            from server
     */
    public void printEscape(NetBrokerMessage msg) {
        System.out.println("");
        System.out.print("[");
        System.out.print("Player " + msg.getPlayer().getUsername()
                + " escape in "
                + LetterNumbConverter.intToString(msg.getSector().getX()) + " "
                + msg.getSector().getY());
        System.out.println("]");

    }

    /**
     * Prints current Player
     * 
     * @param msg
     *            from server
     */
    public void printTurn(NetBrokerMessage msg) {
        System.out.println("");
        System.out.print("[");
        System.out.print("Turn of " + msg.getPlayer().getUsername());
        System.out.println("]");
        System.out.println("");

    }

    /**
     * Sets the state of player
     *
     * @param state
     *            the new state
     */
    public void setState(PlayingState state) {
        this.state = state;
    }

}
