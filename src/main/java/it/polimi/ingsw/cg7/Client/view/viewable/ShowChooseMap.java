package it.polimi.ingsw.cg7.Client.view.viewable;

import it.polimi.ingsw.cg7.Client.controller.InterfaceControl;

/**
 * The Interface ShowChooseMap.
 */
public interface ShowChooseMap {

    /**
     * Choose map.
     *
     * @param interfaceControl the interface control
     */
    public void chooseMap(InterfaceControl interfaceControl);

}
