package it.polimi.ingsw.cg7.Client.view.cli;

import it.polimi.ingsw.cg7.Client.controller.InterfaceControl;
import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;
import it.polimi.ingsw.cg7.Client.model.NetResponseMessage;
import it.polimi.ingsw.cg7.Client.model.ViewCard;
import it.polimi.ingsw.cg7.Client.model.ViewPlayer;
import it.polimi.ingsw.cg7.Client.model.ViewPosition;
import it.polimi.ingsw.cg7.Client.model.flagResponseType;
import it.polimi.ingsw.cg7.Client.view.viewable.ShowMap;
import it.polimi.ingsw.cg7.helpers.LetterNumbConverter;
import it.polimi.ingsw.cg7.model.CharType;

import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * The Class MapCli.
 */
public class MapCli implements ShowMap {

    /** The current position. */
    private ViewPosition position;

    /** The current possible position. */
    private List<ViewPosition> possiblePosition;

    private static Scanner input = new Scanner(System.in);

    /** The interface control. */
    private static InterfaceControl interfaceControl;

    /** The warning view. */
    private static WarningCli warning;

    /** The player view. */
    private static PlayerCli player;

    /** The deck view. */
    private static DeckCli deck;

    /**
     * Instantiates a new map cli.
     *
     * @param warning
     *            the warning
     * @param player
     *            the player
     * @param deck
     *            the deck
     */
    @SuppressWarnings("static-access")
    public MapCli(WarningCli warning, PlayerCli player, DeckCli deck) {
        this.warning = warning;
        this.player = player;
        this.deck = deck;
    }

    /**
     * show attack message
     */
    public void attack() {
        System.out.println("");
        System.out.println("You have attacked!");
    }

    /**
     * request position for movement and send request. If the motion is made you
     * are required to do or not to make a noise
     */
    public void getPosition() {
        int x, y;
        String xCoord;
        System.out.println("");
        if (possiblePosition != null) {
            System.out.println("You can movem in these positions:");
            for (ViewPosition p : possiblePosition)
                System.out.print("["
                        + LetterNumbConverter.intToString(p.getX()) + " "
                        + p.getY() + "]  ");
        }
        System.out.println("");
        System.out.println("Choose one sector:");
        System.out.println("X Coordinate:");
        xCoord = input.next();
        x = LetterNumbConverter.stringToInt(xCoord);
        System.out.println("Y Coordinate:");
        y = input.nextInt();
        ViewPosition position = new ViewPosition(x, y);
        NetResponseMessage msg = interfaceControl.movement(position);
        if (msg.isDoneMove()) {
            movement(position);
            if (msg.getFlag() == flagResponseType.FALSERUMOR)
                getRumor();
            else if (msg.getFlag() == flagResponseType.TRUE)
                chooseMove();
        }

    }

    /**
     * show movement effect
     */
    public void movement(ViewPosition position) {

        System.out.println("");
        System.out.println("You are moving in "
                + LetterNumbConverter.intToString(position.getX()) + " "
                + position.getY());

    }

    /**
     * update current possible position
     */
    public void updatePositions(List<ViewPosition> possiblePosition) {
        this.possiblePosition = possiblePosition;
    }

    /**
     * update current position
     */
    public void updatePosition(ViewPosition requestPosition) {
        this.position = requestPosition;

    }

    /**
     * show current position
     */
    public void showCurrentSituation() {
        System.out.println("");
        if (position != null)
            System.out.println("You are in sector "
                    + LetterNumbConverter.intToString(position.getX()) + " "
                    + position.getY());

    }

    /**
     * request a coordinates for spotlight card
     */
    public void clickPositionSpotlight() {
        int x, y;
        String xCoord;
        System.out.println("");
        System.out.println("Choose position:");
        System.out.println("X Coordinate:");
        xCoord = input.next();
        x = LetterNumbConverter.stringToInt(xCoord);
        System.out.println("Y Coordinate:");
        y = input.nextInt();
        ViewPosition position = new ViewPosition(x, y);
        ViewCard card = new ViewCard("Spotlight");
        NetResponseMessage msg = interfaceControl.useCard(card, position);
        if ((msg).isDoneMove()) {
            deck.useCard(card);
        }
    }

    /**
     * request coordinates for rumor
     */
    public void getRumor() {
        int x, y;
        String xCoord;
        boolean var;
        NetResponseMessage r;
        do {
            System.out.println("");
            System.out.println("You have to make a rumor!");
            System.out.println("Choose the sector in wich make the rumor:");
            System.out.println("X Coordinate:");
            xCoord = input.next();
            x = LetterNumbConverter.stringToInt(xCoord);
            System.out.println("Y Coordinate:");
            y = input.nextInt();
            ViewPosition position = new ViewPosition(x, y);
            r = interfaceControl.sendFalseRumor(position);
            var = r.isDoneMove();
            warning.print(r.getDescription());
            if (var) {
                effectRumor(position);
            }
        } while (!var);
    }

    /**
     * show your rumor effect
     */
    public void effectRumor(ViewPosition position) {
        System.out.println("");
        System.out.print("You made noise in ");
        System.out.println(LetterNumbConverter.intToString(position.getX())
                + " " + position.getY());
        System.out.println("");
    }

    /**
     * show rumor effect
     */
    public void printRumors(NetBrokerMessage msg) {
        System.out.print("[");
        System.out.print("Rumors in "
                + LetterNumbConverter.intToString(msg.getSector().getX()) + " "
                + msg.getSector().getY() + " for the player "
                + msg.getPlayer().getUsername());
        System.out.println("]");
        System.out.println("");

    }

    /**
     * show spotlight effect
     */
    public void light(NetBrokerMessage msg) {
        System.out.println("");
        System.out.println("Light in "
                + LetterNumbConverter.intToString(msg.getSector().getX()) + " "
                + msg.getSector().getY() + "");
        if ((msg.getPlayersLight().size() == 0) || (msg.getPlayers() == null))
            System.out.println("No nearby!");
        else
            for (Map.Entry<ViewPlayer, ViewPosition> element : msg
                    .getPlayersLight().entrySet())
                System.out.println(element.getKey().getUsername()
                        + " in "
                        + LetterNumbConverter.intToString(element.getValue()
                                .getX()) + " " + element.getValue().getY());
        System.out.println("");

    }

    /**
     * show attack effect
     */
    public void attack(NetBrokerMessage msg) {
        System.out.print("[");
        System.out.println("Player " + msg.getPlayer().getUsername()
                + " is attacking in "
                + LetterNumbConverter.intToString(msg.getSector().getX()) + " "
                + msg.getSector().getY() + "]");
        System.out.println("");

    }

    /**
     * Choose move for alien/human
     */
    public synchronized void chooseMove() {
        if (player.getPlayer().getType() == CharType.ALIEN)
            chooseAlien();
        else
            chooseHuman();

    }

    /**
     * Choose human move
     */
    private synchronized void chooseHuman() {
        warning.chooseHuman();
    }

    /**
     * Choose alien move
     */
    private synchronized void chooseAlien() {
        warning.chooseAlien();
    }

    /**
     * set interface control
     */
    @SuppressWarnings("static-access")
    public void setInterfaceControl(InterfaceControl interfaceControl) {
        this.interfaceControl = interfaceControl;

    }

}
