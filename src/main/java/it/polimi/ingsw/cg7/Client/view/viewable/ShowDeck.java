package it.polimi.ingsw.cg7.Client.view.viewable;

import it.polimi.ingsw.cg7.Client.controller.InterfaceControl;
import it.polimi.ingsw.cg7.Client.model.ViewCard;

import java.util.List;

/**
 * The Interface ShowDeck.
 */
public interface ShowDeck {

    /**
     * Gets the card.
     *
     * @return the card
     */
    public void getCard();

    /**
     * Use card.
     *
     * @param card the card
     */
    public void useCard(ViewCard card);

    /**
     * Update deck.
     *
     * @param requestDeck the request deck
     */
    public void updateDeck(List<ViewCard> requestDeck);

    /**
     * Show current situation.
     */
    public void showCurrentSituation();

    /**
     * Adds the card.
     *
     * @param card the card
     */
    public void addCard(ViewCard card);

    /**
     * Sets the interface control.
     *
     * @param interfaceControl the new interface control
     */
    public void setInterfaceControl(InterfaceControl interfaceControl);

    /**
     * Deck full.
     *
     * @return true, if successful
     */
    public boolean deckFull();

    /**
     * Discard a card.
     */
    public void discard();

}
