package it.polimi.ingsw.cg7.Client.controller;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * The Class that handles game music MusicController.
 */
public class MusicController {
    
    /** The clip. */
    private static Clip clip;
    
    /**
     * Start music.
     */
    public static void startMusic() {
        AudioInputStream audioInputStream;
            try {
                audioInputStream = AudioSystem.getAudioInputStream(new File(
                        ".//Game Media//Audio//Heroes.wav").getAbsoluteFile());
                clip = AudioSystem.getClip();
                clip.open(audioInputStream);
                clip.start();
                clip.loop(Clip.LOOP_CONTINUOUSLY);
            } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {

                Logger.getLogger("Audio critical problem").log(Level.SEVERE, null, e);
            }
    }
    
    /**
     * Toggle music.
     */
    public static void toggleMusic() {
        if(clip.isRunning())
            clip.stop();
        else
            clip.start();  
    }
    
    /**
     * Stop music.
     */
    public static void stopMusic()
    {
        clip.stop();
    }
}
