package it.polimi.ingsw.cg7.Client.view.gui;

import it.polimi.ingsw.cg7.Client.controller.InterfaceControl;
import it.polimi.ingsw.cg7.Client.controller.MusicController;
import it.polimi.ingsw.cg7.Client.controller.SubInterface;
import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;
import it.polimi.ingsw.cg7.Client.model.ViewCard;
import it.polimi.ingsw.cg7.Client.model.ViewPlayer;
import it.polimi.ingsw.cg7.Client.model.ViewPosition;
import it.polimi.ingsw.cg7.Client.view.viewable.GameContainer;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SpringLayout;
import javax.swing.SwingUtilities;

/**
 * The Class GameContainerGui.
 */
public class GameContainerGui implements GameContainer {

    /** The exit. */
    private BufferedImage background, exit;

    /** The exit resize. */
    private Image backgroundResize, exitResize;
    
    /** The frame. */
    private JFrame frame;
    
    /** The new_w. */
    public int new_w;
    
    /** The new_h. */
    public int new_h;
    
    /** The width. */
    private double width;
    
    /** The height. */
    private double height;
    
    /** The deck. */
    private DeckGui deck;
    
    /** The chat. */
    private ChatGui chat;
    
    /** The players. */
    private PlayersGui players;
    
    /** The map. */
    private MapGui map;
    
    /** The subinterface always needed to perform server requests. */
    @SuppressWarnings("unused")
    private SubInterface subinterface;
    
    /** The button exit. */
    private JLabel buttonExit;

    /** The interface control. */
    private InterfaceControl interfaceControl;

    /**
     * Creates the and show gui.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws InvocationTargetException the invocation target exception
     * @throws InterruptedException the interrupted exception
     */
    @SuppressWarnings("static-access")
    public void createAndShowGUI() throws IOException,
            InvocationTargetException, InterruptedException {

        background = ImageIO
                .read(new File(
                        ".//Game Media//Interface Media//GameContainer//background.jpg"));
        backgroundResize = background.getScaledInstance((int) width,
                (int) (height), Image.SCALE_DEFAULT);

        exit = ImageIO
                .read(new File(
                        ".//Game Media//Interface Media//GameContainer//button_cancel.png"));
        exitResize = exit.getScaledInstance((int) (0.039 * height),
                (int) (0.039 * height), Image.SCALE_DEFAULT);

        buttonExit = new JLabel(new ImageIcon(exitResize));
        buttonExit.setOpaque(false);
        buttonExit.setSize((int) (0.039 * height), (int) (0.039 * height));

        buttonExit.addMouseListener(new MouseListener() {

            @Override
            public void mouseReleased(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mousePressed(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseExited(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseClicked(MouseEvent e) {
                WarningGui warning = null;
                try {
                    warning = new WarningGui();
                } catch (IOException e2) {
                    Logger.getLogger("Mouse clicked").log(Level.SEVERE, null, e2);
                }
                warning.exit();

            }
        });

        frame = new JFrame("EftAioS - GUI");
        frame.setUndecorated(true);
        // frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // frame.setPreferredSize(new Dimension(1100, 825));
        frame.setExtendedState(frame.MAXIMIZED_BOTH);
        // frame.getContentPane().setLayout(null);
        frame.getContentPane().setSize(frame.MAXIMIZED_HORIZ,
                frame.MAXIMIZED_VERT);

        frame.pack();
        frame.setResizable(false);

        loadContents();

        // frame.setVisible(true);

    }

    /**
     * Instantiates a new game container gui.
     *
     * @param interfaceControl the interface control
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws InvocationTargetException the invocation target exception
     * @throws InterruptedException the interrupted exception
     */
    public GameContainerGui(InterfaceControl interfaceControl)
            throws IOException, InvocationTargetException, InterruptedException {

        this.interfaceControl = interfaceControl;

        Toolkit t = Toolkit.getDefaultToolkit();
        Dimension screenSize = t.getScreenSize();

        width = screenSize.getWidth();
        height = screenSize.getHeight();

        createClass();

        setInterfaceControl(interfaceControl);

        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                try {
                    createAndShowGUI();
                } catch (InvocationTargetException | IOException
                        | InterruptedException e) {
                    Logger.getLogger("Run invoke later").log(Level.SEVERE, null, e);
                }
            }
        });

    }

    /**
     * Creates the class.
     */
    private void createClass() {
        deck = new DeckGui(this);
        try {
            chat = new ChatGui();
        } catch (InvocationTargetException | InterruptedException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        players = new PlayersGui();
        map = new MapGui(interfaceControl.getMapName(), deck);

    }

    /**
     * Load contents.
     */
    public void loadContents() {

        players.setOpaque(false);
        chat.setSize((int) ((width - map.getWidth()) / 2),
                (int) (height * (0.6)));
        players.setSize((int) ((width - map.getWidth()) / 2),
                (int) (height * (0.6)));
        map.setOpaque(false);
        deck.setOpaque(false);
        chat.setOpaque(false);

        SpringLayout l = new SpringLayout();

        frame.getContentPane().setLayout(l);

        frame.getContentPane().add(buttonExit);
        l.putConstraint(SpringLayout.NORTH, buttonExit, (int) (0.013 * height),
                SpringLayout.NORTH, frame);
        l.putConstraint(SpringLayout.WEST, buttonExit, (int) (0.013 * height),
                SpringLayout.WEST, frame);

        JLabel backgroundlb = new JLabel(new ImageIcon(backgroundResize));
        frame.getContentPane().add(deck, SpringLayout.SOUTH);
        l.putConstraint(SpringLayout.SOUTH, deck, -(int) (height * (0.23)),
                SpringLayout.SOUTH, backgroundlb);
        frame.getContentPane().add(chat, SpringLayout.WEST);
        l.putConstraint(SpringLayout.WEST, chat, (int) (width * 0.01098),
                SpringLayout.WEST, backgroundlb);
        l.putConstraint(SpringLayout.SOUTH, chat, (int) height / 6,
                SpringLayout.NORTH, backgroundlb);
        frame.getContentPane().add(players, SpringLayout.EAST);
        l.putConstraint(SpringLayout.WEST, players,
                (int) (map.getWidth() - (0.052 * height)), SpringLayout.WEST,
                map);
        l.putConstraint(SpringLayout.NORTH, players, (int) (height * 0.056),
                SpringLayout.NORTH, backgroundlb);
        frame.getContentPane().add(map, SpringLayout.NORTH);
        l.putConstraint(SpringLayout.NORTH, map, 0, SpringLayout.NORTH,
                backgroundlb);
        l.putConstraint(SpringLayout.WEST, map,
                (int) ((width - map.getWidth()) / 2), SpringLayout.WEST, frame);

        frame.getContentPane().add(backgroundlb);

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#updateDeck(java.util.List)
     */
    @Override
    public void updateDeck(List<ViewCard> requestDeck) {
        deck.updateDeck(requestDeck);

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#updateplayers(java.util.List)
     */
    @Override
    public void updateplayers(List<ViewPlayer> requestPlayers) {
        players.updatePlayers(requestPlayers);

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#updateTurn(it.polimi.ingsw.cg7.Client.model.ViewPlayer)
     */
    @Override
    public void updateTurn(ViewPlayer requestClient) {
        players.setTurn(requestClient);
        deck.setTimer();
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#updateClient(it.polimi.ingsw.cg7.Client.model.ViewPlayer)
     */
    @Override
    public void updateClient(ViewPlayer requestClient) {
        deck.updateClient(requestClient);
        chat.setUsername(requestClient.getUsername());
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#updatePosition(it.polimi.ingsw.cg7.Client.model.ViewPosition)
     */
    @Override
    public void updatePosition(ViewPosition requestPosition) {
        map.updatePosition(requestPosition);

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#showMenu()
     */
    @Override
    public void showMenu() {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#movement()
     */
    @Override
    public void movement() {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#useCard()
     */
    @Override
    public synchronized void useCard() {
        deck.setUseCard(true);

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#readMessage()
     */
    @Override
    public void readMessage() {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#effectMovement(it.polimi.ingsw.cg7.Client.model.ViewPosition)
     */
    @Override
    public void effectMovement(ViewPosition position) {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#effectAttack()
     */
    @Override
    public void effectAttack() {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#effectUseCard(it.polimi.ingsw.cg7.Client.model.ViewCard)
     */
    @Override
    public void effectUseCard(ViewCard card) {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#updatePositions(java.util.List)
     */
    @Override
    public void updatePositions(List<ViewPosition> requestPossiblePosition) {
        map.updatePositions(requestPossiblePosition);
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#showCurrentSituation()
     */
    @Override
    public void showCurrentSituation() {

        deck.showCurrentSituation();
        map.showCurrentSituation();
        players.showCurrentSituation();

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#effectRumors(it.polimi.ingsw.cg7.Client.model.ViewPosition)
     */
    @Override
    public void effectRumors(ViewPosition position) {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#printCard(it.polimi.ingsw.cg7.Client.model.ViewCard)
     */
    @Override
    public void printCard(ViewCard card) {
        deck.addCard(card);

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#playersDead(it.polimi.ingsw.cg7.Client.model.NetBrokerMessage)
     */
    @Override
    public void playersDead(NetBrokerMessage msg) {
        WarningGui warning = null;
        try {
            warning = new WarningGui();
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        warning.playersDead(msg);
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#printRumors(it.polimi.ingsw.cg7.Client.model.NetBrokerMessage)
     */
    @Override
    public void printRumors(NetBrokerMessage msg) {
        WarningGui warning = null;
        try {
            warning = new WarningGui();
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        warning.printRumors(msg);

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#printPlayerEscape(it.polimi.ingsw.cg7.Client.model.NetBrokerMessage)
     */
    @Override
    public void printPlayerEscape(NetBrokerMessage msg) {
        WarningGui warning = null;
        try {
            warning = new WarningGui();
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        warning.printEscape(msg);

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#printPlayerTurn(it.polimi.ingsw.cg7.Client.model.NetBrokerMessage)
     */
    @Override
    public void printPlayerTurn(NetBrokerMessage msg) {
        WarningGui warning = null;
        try {
            warning = new WarningGui();
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        warning.printTurn(msg);
        players.updatePlayers(interfaceControl.requestPlayers());
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#printPlayersLight(it.polimi.ingsw.cg7.Client.model.NetBrokerMessage)
     */
    @Override
    public void printPlayersLight(NetBrokerMessage msg) {
        WarningGui warning = null;
        try {
            warning = new WarningGui();
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        warning.printLight(msg);
        players.updatePlayers(interfaceControl.requestPlayers());

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#print(java.lang.String)
     */
    @Override
    public void print(String string) {
        WarningGui warning = null;
        try {
            warning = new WarningGui();
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        warning.print(string);

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#printMessage(it.polimi.ingsw.cg7.Client.model.NetBrokerMessage)
     */
    @Override
    public void printMessage(NetBrokerMessage msg) {
        chat.printMessage(msg);

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#printAttack(it.polimi.ingsw.cg7.Client.model.NetBrokerMessage)
     */
    @Override
    public void printAttack(NetBrokerMessage msg) {
        WarningGui warning = null;
        try {
            warning = new WarningGui();
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        warning.print("Player " + msg.getPlayer().getUsername()
                + " attack in sector " + msg.getSector().getX() + " "
                + msg.getSector().getY());

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#setInterfaceControl(it.polimi.ingsw.cg7.Client.controller.InterfaceControl)
     */
    @Override
    public void setInterfaceControl(InterfaceControl interfaceControl) {
        this.interfaceControl = interfaceControl;
        deck.setInterfaceControl(interfaceControl);
        chat.setInterfaceControl(interfaceControl);
        map.setInterfaceControl(interfaceControl);

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#clickPositionSpotlight()
     */
    @Override
    public void clickPositionSpotlight() {
        map.clickPositionSpotlight();

        WarningGui warning = null;
        try {
            warning = new WarningGui();
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        warning.print("SPOTLIGHT COORD");

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#falseRumor()
     */
    @Override
    public void falseRumor() {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#setSubInterface(it.polimi.ingsw.cg7.Client.controller.SubInterface)
     */
    @Override
    public void setSubInterface(SubInterface subinterface) {
        this.subinterface = subinterface;

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#finishGame()
     */
    @Override
    public void finishGame() {

        MusicController.stopMusic();

        WarningGui warning = null;
        try {
            warning = new WarningGui();
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        warning.setPlayer(deck.getPlayer());
        warning.setInterfaceControl(interfaceControl);
        warning.finishGame();
        frame.dispose();

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#update()
     */
    @Override
    public void update() {
        updateDeck(interfaceControl.requestDeck());
        updateplayers(interfaceControl.requestPlayers());
        updateClient(interfaceControl.requestClient());
        updatePosition(interfaceControl.requestPosition());
        updatePositions(interfaceControl.requestPossiblePosition());

    }

    /**
     * Use spotlight.
     */
    public void useSpotlight() {
        WarningGui warning = null;
        try {
            warning = new WarningGui();
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        warning.print("Choose one sector for use card");
        map.clickPositionSpotlight();

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#discard()
     */
    @Override
    public synchronized void discard() {
        deck.setdiscard(true);
        deck.setUseCard(false);

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#setType()
     */
    @Override
    public void setType() {
        map.initPos(interfaceControl.requestClient().getType());

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#addTurn()
     */
    @Override
    public void addTurn() {
        deck.addTurn();

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.GameContainer#start()
     */
    @Override
    public void start() {
        frame.setVisible(true);
    }
}
