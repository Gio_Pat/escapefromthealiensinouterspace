package it.polimi.ingsw.cg7.Client.view.viewable;

import it.polimi.ingsw.cg7.Client.model.ViewPlayer;

import java.util.List;

/**
 * The Interface ShowPlayers.
 */
public interface ShowPlayers {

    /**
     * Update players.
     *
     * @param requestPlayers the request players
     */
    public void updatePlayers(List<ViewPlayer> requestPlayers);

    /**
     * Show current situation.
     */
    public void showCurrentSituation();

}
