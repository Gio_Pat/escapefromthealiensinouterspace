package it.polimi.ingsw.cg7.Client.view.viewable;

import it.polimi.ingsw.cg7.Client.controller.InterfaceControl;
import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;

/**
 * The Interface ShowWarning handling the broker messages.
 */
public interface ShowWarning {

    /**
     * Choose human.
     */
    public void chooseHuman();

    /**
     * Choose alien.
     */
    public void chooseAlien();

    /**
     * Prints the.
     *
     * @param string the string
     */
    public void print(String string);

    /**
     * Sets the interface control.
     *
     * @param interfaceControl the new interface control
     */
    public void setInterfaceControl(InterfaceControl interfaceControl);

    /**
     * Finish game.
     */
    public void finishGame();

    /**
     * Players dead.
     *
     * @param msg the msg
     */
    public void playersDead(NetBrokerMessage msg);

    /**
     * Prints the escape.
     *
     * @param msg the msg
     */
    public void printEscape(NetBrokerMessage msg);

    /**
     * Prints the turn.
     *
     * @param msg the msg
     */
    public void printTurn(NetBrokerMessage msg);

}
