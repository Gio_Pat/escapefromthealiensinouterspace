package it.polimi.ingsw.cg7.Client.view.viewable;

import it.polimi.ingsw.cg7.Client.controller.InterfaceControl;
import it.polimi.ingsw.cg7.Client.controller.SubInterface;
import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;
import it.polimi.ingsw.cg7.Client.model.ViewCard;
import it.polimi.ingsw.cg7.Client.model.ViewPlayer;
import it.polimi.ingsw.cg7.Client.model.ViewPosition;

import java.util.List;

/**
 * The Interface GameContainer.
 */
public interface GameContainer {

    /**
     * Updates the deck.
     *
     * @param requestDeck the request deck
     */
    public void updateDeck(List<ViewCard> requestDeck);

    /**
     * Update all players.
     *
     * @param requestPlayers the request players
     */
    public void updateplayers(List<ViewPlayer> requestPlayers);

    /**
     * Update the client infos.
     *
     * @param requestClient the request client
     */
    public void updateClient(ViewPlayer requestClient);

    /**
     * Update position.
     *
     * @param requestPosition the request position
     */
    public void updatePosition(ViewPosition requestPosition);

    /**
     * Show menu.
     */
    public void showMenu();

    /**
     * Movement.
     */
    public void movement();

    /**
     * Use card.
     */
    public void useCard();

    /**
     * Read message.
     */
    public void readMessage();

    /**
     * Effect movement.
     *
     * @param position the position
     */
    public void effectMovement(ViewPosition position);

    /**
     * Effect attack.
     */
    public void effectAttack();

    /**
     * Effect use card.
     *
     * @param card the card
     */
    public void effectUseCard(ViewCard card);

    /**
     * Update positions.
     *
     * @param requestPossiblePosition the request possible position
     */
    public void updatePositions(List<ViewPosition> requestPossiblePosition);

    /**
     * Show current situation.
     */
    public void showCurrentSituation();

    /**
     * Effect rumors.
     *
     * @param position the position
     */
    public void effectRumors(ViewPosition position);

    /**
     * Prints the card.
     *
     * @param card the card
     */
    public void printCard(ViewCard card);

    /**
     * Players dead.
     *
     * @param msg the msg
     */
    public void playersDead(NetBrokerMessage msg);

    /**
     * Prints the rumors.
     *
     * @param msg the msg
     */
    public void printRumors(NetBrokerMessage msg);

    /**
     * Prints the player escape.
     *
     * @param msg the msg
     */
    public void printPlayerEscape(NetBrokerMessage msg);

    /**
     * Prints the player turn.
     *
     * @param msg the msg
     */
    public void printPlayerTurn(NetBrokerMessage msg);

    /**
     * Prints the players light.
     *
     * @param msg the msg
     */
    public void printPlayersLight(NetBrokerMessage msg);

    /**
     * Prints the.
     *
     * @param string the string
     */
    public void print(String string);

    /**
     * Prints the message.
     *
     * @param msg the msg
     */
    public void printMessage(NetBrokerMessage msg);

    /**
     * Prints the attack.
     *
     * @param msg the msg
     */
    public void printAttack(NetBrokerMessage msg);

    /**
     * Sets the interface control.
     *
     * @param interfaceControl the new interface control
     */
    public void setInterfaceControl(InterfaceControl interfaceControl);

    /**
     * Click position spotlight.
     */
    public void clickPositionSpotlight();

    /**
     * False rumor.
     */
    public void falseRumor();

    /**
     * Sets the sub interface.
     *
     * @param subinterface the new sub interface
     */
    public void setSubInterface(SubInterface subinterface);

    /**
     * Finish game.
     */
    public void finishGame();

    /**
     * Update.
     */
    public void update();

    /**
     * Discard.
     */
    public void discard();

    /**
     * Update turn.
     *
     * @param requestClient the request client
     */
    public void updateTurn(ViewPlayer requestClient);

    /**
     * Sets the type.
     */
    public void setType();

    /**
     * Adds the turn.
     */
    public void addTurn();

    /**
     * Start.
     */
    public void start();

}
