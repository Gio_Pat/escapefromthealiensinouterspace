package it.polimi.ingsw.cg7.Client.view.cli;

import it.polimi.ingsw.cg7.Client.controller.InterfaceControl;
import it.polimi.ingsw.cg7.Client.model.TypeConnection;
import it.polimi.ingsw.cg7.Client.view.viewable.ShowChooseConnection;

import java.util.Scanner;

public class ChooseConnectionViewCli implements ShowChooseConnection {

    private Scanner input = new Scanner(System.in);

    public void chooseConnection(InterfaceControl interfaceControl) {

        String choose;
        do {
            System.out.println("Choose your Connection:");
            System.out.println("");
            System.out.println("1: RMI");
            System.out.println("2: Socket");
            System.out.println("");
            choose = input.next();
        } while (!(choose.equals("1")) && (!(choose.equals("2"))));

        if (choose.equals("1"))
            interfaceControl.setConnection(TypeConnection.RMI);
        else
            interfaceControl.setConnection(TypeConnection.SOCKET);
    }

}
