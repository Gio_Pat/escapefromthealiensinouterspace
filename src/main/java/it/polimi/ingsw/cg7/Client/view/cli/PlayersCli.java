package it.polimi.ingsw.cg7.Client.view.cli;

import it.polimi.ingsw.cg7.Client.model.ViewPlayer;
import it.polimi.ingsw.cg7.Client.view.viewable.ShowPlayers;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class PlayersCli.
 */
public class PlayersCli implements ShowPlayers {

    /** The players. */
    List<ViewPlayer> players;

    /**
     * Instantiates a new players cli.
     */
    public PlayersCli() {
        players = new ArrayList<ViewPlayer>();
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowPlayers#showCurrentSituation()
     */
    public void showCurrentSituation() {
        System.out.println("");
        if (players != null)
            for (ViewPlayer p : players)
                System.out.println("Player : " + p.getUsername()
                        + "  |  State: " + p.getPlayingState()
                        + "  |  N° Kill : " + p.getKillNumber()
                        + "  |  N°Cards : " + p.getCards());

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.Client.view.viewable.ShowPlayers#updatePlayers(java.util.List)
     */
    public void updatePlayers(List<ViewPlayer> requestPlayers) {
        this.players = requestPlayers;

    }

}
