package it.polimi.ingsw.cg7.Client.model;

import it.polimi.ingsw.cg7.model.Map;
import it.polimi.ingsw.cg7.model.Player;

import java.util.UUID;

/**
 * this class handles the request from a player to join a game
 * 
 * @author Giovanni Patruno
 *
 */
public class JoinRequest {
    /**
     * represent the object created by the client side
     */
    private Player requestingPlayer;

    public ViewMap getSelectedMaps() {
        return selectedMaps;
    }

    /**
     * represent the token of the user
     */
    private UUID requestingPlayerToken;
    /**
     * Specifies the request Playing Map
     */
    private Map requestingPlayingMap;

    private ViewMap selectedMaps;

    public Player getRequestingPlayer() {
        return requestingPlayer;
    }

    public UUID getRequestingPlayerToken() {
        return requestingPlayerToken;
    }

    public Map getRequestingPlayingMap() {
        return requestingPlayingMap;
    }

    /**
     * This instance a new Request with parameters
     * 
     * @param requestingPlayer
     * @param requestingPlayerToken
     * @param requestingPlayingMap
     */
    public JoinRequest(Player requestingPlayer, UUID requestingPlayerToken,
            Map requestingPlayingMap) {
        super();
        this.requestingPlayer = requestingPlayer;
        this.requestingPlayerToken = requestingPlayerToken;
        this.requestingPlayingMap = requestingPlayingMap;
    }

    public JoinRequest(Player requestingPlayer, UUID givenToken,
            ViewMap selectedMaps) {
        super();
        this.requestingPlayer = requestingPlayer;
        this.requestingPlayerToken = givenToken;
        this.selectedMaps = selectedMaps;
    }
}
