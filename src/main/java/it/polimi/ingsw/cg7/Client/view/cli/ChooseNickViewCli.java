package it.polimi.ingsw.cg7.Client.view.cli;

import it.polimi.ingsw.cg7.Client.controller.InterfaceControl;

import java.util.Scanner;

/**
 * The Class ChooseNickViewCli.
 * @author: Debora Rebai
 */
public class ChooseNickViewCli {

    /** The input. */
    private Scanner input = new Scanner(System.in);

    /**
     * Choose username.
     *
     * @param interfaceControl the interface control
     */
    public void chooseUsername(InterfaceControl interfaceControl) {

        String username;
        System.out.println("Choose your Name:");
        username = input.next();
        interfaceControl.setNick(username);
    }

}
