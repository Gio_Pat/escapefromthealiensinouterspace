package it.polimi.ingsw.cg7.Client.model;
/**
 * Viewing engine type
 * @author Debora Rebai & Giovanni Patruno
 *
 */
public enum TypeView {

    CLI, GUI;
}
