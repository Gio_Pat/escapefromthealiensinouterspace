package it.polimi.ingsw.cg7.Client.controller;

import it.polimi.ingsw.cg7.Client.model.NetRequestMessage;
import it.polimi.ingsw.cg7.Client.model.NetResponseMessage;
import it.polimi.ingsw.cg7.Client.model.NetTypeRequest;
import it.polimi.ingsw.cg7.Client.model.TypeConnection;
import it.polimi.ingsw.cg7.Client.model.ViewCard;
import it.polimi.ingsw.cg7.Client.model.ViewMap;
import it.polimi.ingsw.cg7.Client.model.ViewMessage;
import it.polimi.ingsw.cg7.Client.model.ViewPlayer;
import it.polimi.ingsw.cg7.Client.model.ViewPosition;
import it.polimi.ingsw.cg7.communication.guest.IClient;
import it.polimi.ingsw.cg7.communication.guest.RmiGuest;
import it.polimi.ingsw.cg7.communication.guest.SocketGuest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The Class InterfaceControl handles the client requests.
 */
public class InterfaceControl {

    /** The my commands. */
    private static IClient myCommands;
    
    /** The token. */
    private static UUID token;
    
    /** The connection. */
    private static TypeConnection connection;
    
    /** The subinterface. */
    @SuppressWarnings("unused")
    private static SubInterface subinterface;
    
    /** The username. */
    private static String username;
    
    /** The map. */
    private static ViewMap map;
    //assumes the current class is called logger
    /** The Constant LOGGER. */
    private final static Logger LOGGER = Logger.getLogger(InterfaceControl.class.getName()); 
    
    /**
     * Instantiates a new interface control.
     *
     * @param connection the connection
     */
    @SuppressWarnings("static-access")
    public InterfaceControl(TypeConnection connection) {
        this.connection = connection;
    }

    /**
     * Instantiates a new interface control.
     */
    public InterfaceControl() {

    }

    /**
     * Sets the sub interface.
     *
     * @param subinterface the new sub interface
     */
    @SuppressWarnings("static-access")
    public void setSubInterface(SubInterface subinterface) {
        this.subinterface = subinterface;
        if (connection == TypeConnection.RMI) {
            myCommands = new RmiGuest(subinterface);
        } else {
            myCommands = new SocketGuest(subinterface);
        }
    }

    /**
     * Request map.
     *
     * @return the list
     */
    public List<ViewMap> requestMap() {

        NetResponseMessage res = null;
        NetRequestMessage request = new NetRequestMessage(NetTypeRequest.MAPS,
                null);
        request.requestMaps();
        try {
            res = myCommands.sendRequest(request);
        } catch (IOException e) {
            LOGGER.setLevel(Level.SEVERE);
            LOGGER.info("Connection error on map requests");
            LOGGER.info(e.getMessage());
            
        }
        return res.getMaps();
    }

    /**
     * Request subscribing.
     *
     * @param map the map
     * @param username the username
     * @param token the token
     * @return the uuid
     */
    public UUID requestSubscribing(ViewMap map, String username, UUID token) {
        NetRequestMessage request;
        request = new NetRequestMessage(NetTypeRequest.SUBSCRIBING, token);
        request.requestSubscribing(username, map);
        NetResponseMessage res = null;
        try {
            res = myCommands.sendRequest(request);
        } catch (IOException e) {
            LOGGER.setLevel(Level.SEVERE);
            LOGGER.info("Connection error on subscribing request");
            LOGGER.info(e.getMessage());

            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        if (res != null) {
            token = res.getGivenToken();
            return res.getGivenToken();
        }
        return null;

    }

    /**
     * Request deck.
     *
     * @return the list
     */
    public List<ViewCard> requestDeck() {
        NetRequestMessage request;
        request = new NetRequestMessage(NetTypeRequest.DECK, token);
        request.requestDeck();
        NetResponseMessage res = null;
        try {
            res = myCommands.sendRequest(request);
        } catch (IOException e) {
            LOGGER.setLevel(Level.SEVERE);
            LOGGER.info("Connection error on a deck request");
            LOGGER.info(e.getMessage());

            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        if (res != null) {
            return res.getDeck();
        }
        return new ArrayList<ViewCard>();
    }

    /**
     * Request players.
     *
     * @return the list
     */
    public List<ViewPlayer> requestPlayers() {
        NetRequestMessage request;
        request = new NetRequestMessage(NetTypeRequest.PLAYERS, token);
        // if (connection == TypeConnection.RMI) {
        // myCommands = new RmiGuest(subinterface);
        // } else {
        // myCommands = new SocketGuest(subinterface);
        // }
        request.requestPlayers();
        NetResponseMessage res = null;
        try {
            res = myCommands.sendRequest(request);
        } catch (IOException e) {
            LOGGER.setLevel(Level.SEVERE);
            LOGGER.info("Connection error on a player state request");
            LOGGER.info(e.getMessage());

            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        if (res != null) {
            return res.getPlayers();
        }
        return new ArrayList<ViewPlayer>();
    }

    /**
     * Request client.
     *
     * @return the view player
     */
    public ViewPlayer requestClient() {
        NetRequestMessage request;
        request = new NetRequestMessage(NetTypeRequest.CLIENT, token);
        // if (connection == TypeConnection.RMI) {
        request.requestClient();
        NetResponseMessage res = null;
        try {
            res = myCommands.sendRequest(request);
        } catch (IOException e) {
            LOGGER.setLevel(Level.SEVERE);
            LOGGER.info("Connection error on a request");
            LOGGER.log(Level.SEVERE, e.getMessage());

            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        if (res != null) {
            return res.getPlayerClient();
        }
        return null;
    }

    /**
     * Request position.
     *
     * @return the view position
     */
    public ViewPosition requestPosition() {
        NetRequestMessage request;
        request = new NetRequestMessage(NetTypeRequest.POSITION, token);
        // if (connection == TypeConnection.RMI) {
        // myCommands = new RmiGuest(subinterface);
        // } else {
        // myCommands = new SocketGuest(subinterface);
        // }
        request.requestPosition();
        NetResponseMessage res = null;
        try {
            res = myCommands.sendRequest(request);
        } catch (IOException e) {
            LOGGER.setLevel(Level.SEVERE);
            LOGGER.info("Connection error on a request position");
            LOGGER.log(Level.SEVERE, e.getMessage());
        }
        if (res != null) {
            return res.getPosition();
        }
        return null;
    }

    /**
     * Finish turn.
     *
     * @return the net response message
     */
    public NetResponseMessage finishTurn() {
        NetRequestMessage request;
        request = new NetRequestMessage(NetTypeRequest.FINISHTURN, token);
        // if (connection == TypeConnection.RMI) {
        // myCommands = new RmiGuest(subinterface);
        // } else {
        // myCommands = new SocketGuest(subinterface);
        // }
        request.finishTurnRequest();
        NetResponseMessage res = null;
        try {
            res = myCommands.sendRequest(request);
        } catch (IOException e) {
            LOGGER.setLevel(Level.SEVERE);
            LOGGER.info("Connection error on a finish turn request");
            LOGGER.log(Level.SEVERE, e.getMessage());

            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return res;
    }

    /**
     * Attack.
     *
     * @return the net response message
     */
    public NetResponseMessage attack() {
        NetRequestMessage request;
        request = new NetRequestMessage(NetTypeRequest.ATTACK, token);
        // if (connection == TypeConnection.RMI) {
        // myCommands = new RmiGuest(subinterface);
        // } else {
        // myCommands = new SocketGuest(subinterface);
        // }
        request.attackRequest();
        NetResponseMessage res = null;
        try {
            res = myCommands.sendRequest(request);
        } catch (IOException e) {
            LOGGER.setLevel(Level.SEVERE);
            LOGGER.info("Connection error on an attack request");
            LOGGER.log(Level.SEVERE, e.getMessage());

            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        if (res != null) {
            return res;
        }
        return null;
    }

    /**
     * Movement.
     *
     * @param position the position
     * @return the net response message
     */
    public NetResponseMessage movement(ViewPosition position) {
        NetRequestMessage request;
        request = new NetRequestMessage(NetTypeRequest.MOVEMENT, token);
        // if (connection == TypeConnection.RMI) {
        // myCommands = new RmiGuest(subinterface);
        // } else {
        // myCommands = new SocketGuest(subinterface);
        // }
        request.movementRequest(position);
        NetResponseMessage res = null;
        try {
            res = myCommands.sendRequest(request);
        } catch (IOException e) {
            LOGGER.setLevel(Level.SEVERE);
            LOGGER.info("Connection error on a movement request");
            LOGGER.log(Level.SEVERE, e.getMessage());

            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        if (res != null) {
            return res;
        }
        return null;

    }

    /**
     * Use card.
     *
     * @param card the card
     * @param position the position
     * @return the net response message
     */
    public NetResponseMessage useCard(ViewCard card, ViewPosition position) {
        NetRequestMessage request;
        request = new NetRequestMessage(NetTypeRequest.USEITEM, token);
        // if (connection == TypeConnection.RMI) {
        // myCommands = new RmiGuest(subinterface);
        // } else {
        // myCommands = new SocketGuest(subinterface);
        // }
        request.useItemRequest(card, position);
        NetResponseMessage res = null;
        try {
            res = myCommands.sendRequest(request);
        } catch (IOException e) {
            LOGGER.setLevel(Level.SEVERE);
            LOGGER.info("Connection error on a usecard request");
            LOGGER.log(Level.SEVERE, e.getMessage());

            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        if (res != null) {
            return res;
        }
        return null;
    }

    /**
     * Discard.
     *
     * @param card the card
     * @return the net response message
     */
    public NetResponseMessage discard(ViewCard card) {
        NetRequestMessage request;
        request = new NetRequestMessage(NetTypeRequest.DISCARD, token);
        // if (connection == TypeConnection.RMI) {
        // myCommands = new RmiGuest(subinterface);
        // } else {
        // myCommands = new SocketGuest(subinterface);
        // }
        request.requestDiscard(card);
        NetResponseMessage res = null;
        try {
            res = myCommands.sendRequest(request);
        } catch (IOException e) {
            LOGGER.setLevel(Level.SEVERE);
            LOGGER.info("Connection error on a discard request");
            LOGGER.log(Level.SEVERE, e.getMessage());

            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        if (res != null) {
            return res;
        }
        return null;
    }

    /**
     * Send message.
     *
     * @param message the message
     */
    public void sendMessage(ViewMessage message) {

        NetRequestMessage request;
        request = new NetRequestMessage(NetTypeRequest.MESSAGE, token);
        request.sendMessageRequest(message);
        @SuppressWarnings("unused")
        NetResponseMessage res = null;
        try {
            res = myCommands.sendRequest(request);
        } catch (IOException e) {
            LOGGER.setLevel(Level.SEVERE);
            LOGGER.info("Connection error on a sending message request");
            LOGGER.log(Level.SEVERE, e.getMessage());

            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }

    }

    /**
     * Request possible position.
     *
     * @return the list
     */
    public List<ViewPosition> requestPossiblePosition() {
        NetRequestMessage request;
        request = new NetRequestMessage(NetTypeRequest.NEARSECTOR, token);
        // if (connection == TypeConnection.RMI) {
        // myCommands = new RmiGuest(subinterface);
        // } else {
        // myCommands = new SocketGuest(subinterface);
        // }
        request.requestNearSector();
        NetResponseMessage res = null;
        try {
            res = myCommands.sendRequest(request);
        } catch (IOException e) {
            LOGGER.setLevel(Level.SEVERE);
            LOGGER.info("Connection error on a possible positions request");
            LOGGER.log(Level.SEVERE, e.getMessage());

            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        if (res != null) {
            return res.getNearSector();
        }
        return null;
    }

    /**
     * Send false rumor.
     *
     * @param position the position
     * @return the net response message
     */
    public NetResponseMessage sendFalseRumor(ViewPosition position) {
        NetRequestMessage request;
        request = new NetRequestMessage(NetTypeRequest.SENDFALSERUMORS, token);
        // if (connection == TypeConnection.RMI) {
        // myCommands = new RmiGuest(subinterface);
        // } else {
        // myCommands = new SocketGuest(subinterface);
        // }
        request.sendRumor(position);
        NetResponseMessage res = null;
        try {
            res = myCommands.sendRequest(request);
        } catch (IOException e) {
            LOGGER.setLevel(Level.SEVERE);
            LOGGER.info("Connection error on a false rumor request");
            LOGGER.log(Level.SEVERE, e.getMessage());

            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        if (res != null) {
            return res;
        }
        return null;

    }

    /**
     * Draw card.
     *
     * @return the net response message
     */
    public NetResponseMessage drawCard() {
        NetRequestMessage request;
        request = new NetRequestMessage(NetTypeRequest.DRAW, token);
        // if (connection == TypeConnection.RMI) {
        // myCommands = new RmiGuest(subinterface);
        // } else {
        // myCommands = new SocketGuest(subinterface);
        // }
        request.requestDraw();
        NetResponseMessage res = null;
        try {
            res = myCommands.sendRequest(request);
        } catch (IOException e) {
            LOGGER.setLevel(Level.SEVERE);
            LOGGER.info("Connection error on a draw request");
            LOGGER.log(Level.SEVERE, e.getMessage());

            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        if (res != null) {
            return res;
        }
        return null;
    }

    /**s
     * Sets the token.
     *
     * @param token the new token
     */
    @SuppressWarnings("static-access")
    public void setToken(UUID token) {
        this.token = token;

    }

    /**
     * Request subscribing.
     *
     * @return the uuid
     */
    public UUID requestSubscribing() {
        NetRequestMessage request;
        request = new NetRequestMessage(NetTypeRequest.SUBSCRIBING, token);
        request.requestSubscribing(username, map);
        NetResponseMessage res = null;
        try {
            res = myCommands.sendRequest(request);
        } catch (IOException e) {
            LOGGER.setLevel(Level.SEVERE);
            LOGGER.info("Connection error on a request");
            LOGGER.log(Level.SEVERE, e.getMessage());

            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        if (res != null) {
            token = res.getGivenToken();
            return res.getGivenToken();
        }
        return null;

    }

    /**
     * Sets the connection.
     *
     * @param connection the new connection
     */
    @SuppressWarnings("static-access")
    public void setConnection(TypeConnection connection) {
        this.connection = connection;

    }

    /**
     * Sets the map.
     *
     * @param map the new map
     */
    @SuppressWarnings("static-access")
    public void setMap(ViewMap map) {
        this.map = map;

    }

    /**
     * Sets the nick.
     *
     * @param username the new nick
     */
    @SuppressWarnings("static-access")
    public void setNick(String username) {
        this.username = username;

    }

    /**
     * Gets the username.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Infofinish game.
     *
     * @return the net response message
     */
    public NetResponseMessage infofinishGame() {
        NetRequestMessage request;
        request = new NetRequestMessage(NetTypeRequest.FINISHGAME, token);
        request.requestInfoFinishGame();
        NetResponseMessage res = null;
        try {
            res = myCommands.sendRequest(request);
        } catch (IOException e) {

            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        if (res != null) {
            return res;
        }
        return null;
    }

    /**
     * Gets the map name.
     *
     * @return the map name
     */
    public String getMapName() {
        return map.getName();
    }

}
