package it.polimi.ingsw.cg7.Client.controller;

import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;
import it.polimi.ingsw.cg7.Client.model.ViewCard;
import it.polimi.ingsw.cg7.Client.model.flagResponseType;
import it.polimi.ingsw.cg7.Client.view.viewable.GameContainer;
import it.polimi.ingsw.cg7.helpers.LetterNumbConverter;
import it.polimi.ingsw.cg7.model.CharType;

import java.io.Serializable;

/**
 * The Class SubInterface.
 */
public class SubInterface implements Serializable {

    /** The Constant serialVersionUID. */

    private static final long serialVersionUID = 1L;

    /** The is started. */
    private boolean isStarted;

    /** The typeset. */
    private boolean typeset;

    /** The type. */
    private CharType type;

    /** The finish game. */
    private boolean finishGame;

    /** The card. */
    private ViewCard card;

    /** The new card recive. */
    private boolean newCardRecive;

    /** The your turn. */
    private boolean yourTurn;

    /** The type sector. */
    private flagResponseType typeSector;

    /** The game view. */
    private GameContainer gameView;

    /**
     * Instantiates a new sub interface.
     */
    public SubInterface() {
        this.isStarted = false;
        this.typeset = false;
        this.finishGame = false;
        this.newCardRecive = false;
        this.yourTurn = false;
    }

    /**
     * Process response.
     *
     * @param msg
     *            the msg
     */
    public void processResponse(NetBrokerMessage msg) {

        switch (msg.getType()) {
        case INITGAME: {
            this.isStarted = true;
            gameView.start();
            break;
        }
        case SILENCE: {
            gameView.print("Player " + msg.getPlayer().getUsername()
                    + " say <Silence>");
            break;
        }
        case ATTACK: {
            printAttack(msg);
            break;
        }

        case RECIVECARD: {
            card = msg.getItem();
            if (card != null)
                gameView.printCard(card);
            else
                gameView.print("Deck is empty");
            this.newCardRecive = true;
            break;
        }
        case PLAYERSDEAD: {
            if (msg.getPlayers().size() != 0)
                gameView.playersDead(msg);
            break;
        }
        case PLAYERRUMORS: {
            gameView.printRumors(msg);
            break;
        }
        case PLAYERESCAPE: {
            gameView.printPlayerEscape(msg);
            break;
        }
        case PLAYERTURN: {
            gameView.updateTurn(msg.getPlayer());
            break;
        }
        case PLAYERSLIGHT: {
            gameView.printPlayersLight(msg);
            break;
        }
        case FINISHGAME: {

            this.finishGame = true;
            gameView.finishGame();
            break;
        }
        case ROLEPLAYER: {
            this.typeset = true;
            this.type = msg.getPlayer().getType();
            break;
        }
        case TURN: {
            this.yourTurn = true;
            gameView.print("It's your turn");
            gameView.addTurn();
            break;

        }
        case TYPESECTOR: {
            this.typeSector = msg.getTypeSector();
            break;
        }

        case MESSAGE: {
            gameView.printMessage(msg);
            break;
        }

        case PLAYEROUT: {
            gameView.print("Player " + msg.getPlayer().getUsername()
                    + " is offline");
            break;
        }
        case PLAYERDRAW: {
            gameView.print("Player " + msg.getPlayer().getUsername()
                    + " recive a card");
            break;
        }
        case ESCAPE: {
            String s = new String("Player " + msg.getPlayer().getUsername()
                    + " has been in hatch sector "
                    + LetterNumbConverter.intToString(msg.getSector().getX())
                    + " " + msg.getSector().getY() + " and has drawn ");
            if (msg.isEscape())
                s = s.concat("GREEN card");
            else
                s = s.concat("RED card");
            gameView.print(s);
            break;
        }
        }
    }

    private void printAttack(NetBrokerMessage msg) {
        gameView.printAttack(msg);

    }

    /**
     * Checks if is typeset.
     *
     * @return true, if is typeset
     */
    public boolean isTypeset() {
        return typeset;
    }

    /**
     * Checks if is finish game.
     *
     * @return true, if is finish game
     */
    public boolean isFinishGame() {
        return finishGame;
    }

    /**
     * Gets the card.
     *
     * @return the card
     */
    public ViewCard getCard() {
        return card;
    }

    /**
     * Checks if is new card recive.
     *
     * @return true, if is new card recive
     */
    public boolean isNewCardRecive() {
        return newCardRecive;
    }

    /**
     * Checks if is started.
     *
     * @return true, if is started
     */
    public boolean isStarted() {
        return isStarted;
    }

    /**
     * Char typeis setting.
     *
     * @return true, if successful
     */
    public boolean charTypeisSetting() {
        return typeset;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public CharType getType() {
        return type;
    }

    /**
     * Gets the type sector.
     *
     * @return the type sector
     */
    public flagResponseType getTypeSector() {
        return typeSector;
    }

    /**
     * Checks if is your turn.
     *
     * @return true, if is your turn
     */
    public boolean isYourTurn() {
        return yourTurn;
    }

    /**
     * Finish turn.
     */
    public void finishTurn() {
        this.yourTurn = false;
    }

    /**
     * Reset new card recive.
     */
    public void resetNewCardRecive() {
        this.newCardRecive = false;
    }

    /**
     * Sets the game view.
     *
     * @param gameView
     *            the new game view
     */
    public void setGameView(GameContainer gameView) {
        this.gameView = gameView;

    }
}
