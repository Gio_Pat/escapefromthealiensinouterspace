package it.polimi.ingsw.cg7.Client.model;

import java.io.Serializable;

/**
 * The Class ViewPosition.
 * Authors: Debora Rebai & Giovanni Patruno
 */
public class ViewPosition implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
    
    /** The x. */
    private int x;
    
    /** The y. */
    private int y;
    
    /** The type. */
    private SectorType type;
    
    /** The blocked. */
    private boolean blocked;

    /**
     * Instantiates a new view position.
     *
     * @param x the x
     * @param y the y
     * @param type the type
     */
    public ViewPosition(int x, int y, SectorType type) {
        this.x = x;
        this.y = y;
        this.type = type;
        this.blocked = false;
    }

    /**
     * Instantiates a new view position.
     *
     * @param x the x
     * @param y the y
     */
    public ViewPosition(int x, int y) {
        this.x = x;
        this.y = y;
        this.blocked = false;
    }

    /**
     * Gets the x.
     *
     * @return the x
     */
    public int getX() {
        return x;
    }

    /**
     * Sets the x.
     *
     * @param x the new x
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * Gets the y.
     *
     * @return the y
     */
    public int getY() {
        return y;
    }

    /**
     * Sets the y.
     *
     * @param y the new y
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public SectorType getType() {
        return type;
    }

    /**
     * Sets the type.
     *
     * @param type the new type
     */
    public void setType(SectorType type) {
        this.type = type;
    }

    /**
     * Checks if is blocked.
     *
     * @return true, if is blocked
     */
    public boolean isBlocked() {
        return blocked;
    }

    /**
     * Sets the blocked.
     *
     * @param blocked the new blocked
     */
    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

}
