package it.polimi.ingsw.cg7.Client.view.cli;

import it.polimi.ingsw.cg7.Client.controller.InterfaceControl;
import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;
import it.polimi.ingsw.cg7.Client.model.ViewMessage;
import it.polimi.ingsw.cg7.Client.view.viewable.ShowChat;

import java.util.Date;
import java.util.Scanner;

public class ChatCli implements ShowChat {

    private static Scanner input = new Scanner(System.in);
    private static InterfaceControl interfaceControl;

    public ChatCli() {
    }

    public void getMessage(String username) {

        String message;
        System.out.println("Insert your message:");
        message = input.nextLine();
        ViewMessage msg = new ViewMessage(username, new Date(), message);
        interfaceControl.sendMessage(msg);
    }

    public void printMessage(NetBrokerMessage msg) {

        System.out.println("");
        System.out.println("[" + msg.getDate() + "] [" + msg.getNick() + "]");
        System.out.println(msg.getText());
        System.out.println("");

    }

    @SuppressWarnings("static-access")
    public void setInterfaceControl(InterfaceControl interfaceControl) {
        this.interfaceControl = interfaceControl;
    }

}
