package it.polimi.ingsw.cg7.Client.view.gui;

import it.polimi.ingsw.cg7.Client.controller.InterfaceControl;
import it.polimi.ingsw.cg7.Client.model.TypeConnection;
import it.polimi.ingsw.cg7.Client.view.viewable.ShowChooseConnection;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.SpringLayout;

public class MainMenu implements ShowChooseConnection {
    private BufferedImage background;
    private MouseListener ms;
    private JFrame frame;
    private InterfaceControl interfaceControl;
    private double width, height;

    public MainMenu() {
        initialize();
    }

    private void initialize() {

        Toolkit t = Toolkit.getDefaultToolkit();
        Dimension screenSize = t.getScreenSize();

        width = screenSize.getWidth();
        height = screenSize.getHeight();

        frame = new JFrame("EftaioS - GUI");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setPreferredSize(new Dimension((int) (width / 2),
                (int) (height * (0.8))));
        // frame.getContentPane().setLayout(null);
        frame.getContentPane().setSize((int) (width / 2),
                (int) (height * (0.8)));
        frame.setLocation((int) (width / 4), (int) (height / 9));

        frame.pack();
        ms = new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent arg0) {
                TypeConnection t;
                if (arg0.getComponent().getName().equals("RMI"))
                    t = TypeConnection.RMI;
                else
                    t = TypeConnection.SOCKET;
                interfaceControl.setConnection(t);
                nextWindow();

            }

            @Override
            public void mouseEntered(MouseEvent arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseExited(MouseEvent arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mousePressed(MouseEvent arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseReleased(MouseEvent arg0) {
                // TODO Auto-generated method stub

            }

        };
        loadContents(frame);

    }

    private void nextWindow() {

        synchronized (this) {
            notifyAll();
        }
        frame.dispose();

    }

    public void loadContents(JFrame frame) {
        try {
            background = ImageIO.read(new File(
                    ".//Game Media//Interface Media//Menu//Background.jpg"));

            JTextArea chooseConnection = new JTextArea();
            chooseConnection.setText("Choose connection type");
            chooseConnection.setOpaque(false);
            chooseConnection.setEditable(false);
            chooseConnection.setFont(new Font("Century Gothic", 0,
                    (int) (0.0308 * height)));
            chooseConnection.setForeground(Color.WHITE);

            JButton rmiBtn = new JButton();
            JButton sckBtn = new JButton();

            rmiBtn.addMouseListener(ms);
            rmiBtn.setName("RMI");
            rmiBtn.setText("RMI");
            rmiBtn.setFont(new Font("Century Gothic", 0,
                    (int) (0.0258 * height)));
            sckBtn.addMouseListener(ms);
            sckBtn.setName("SOCKET");
            sckBtn.setText("SOCKET");
            sckBtn.setFont(new Font("Century Gothic", 0,
                    (int) (0.0258 * height)));

            SpringLayout springLayout = new SpringLayout();
            frame.getContentPane().setLayout(springLayout);

            springLayout.putConstraint(SpringLayout.SOUTH, rmiBtn,
                    -((int) (width / 16)), SpringLayout.SOUTH,
                    frame.getContentPane());
            springLayout.putConstraint(SpringLayout.WEST, rmiBtn,
                    (int) (height / 6), SpringLayout.WEST,
                    frame.getContentPane());

            springLayout.putConstraint(SpringLayout.SOUTH, sckBtn,
                    -((int) (width / 16)), SpringLayout.SOUTH,
                    frame.getContentPane());
            springLayout.putConstraint(SpringLayout.EAST, sckBtn,
                    -((int) (height / 6)), SpringLayout.EAST,
                    frame.getContentPane());

            springLayout.putConstraint(SpringLayout.SOUTH, chooseConnection,
                    -((int) (width / 10)), SpringLayout.SOUTH,
                    frame.getContentPane());
            springLayout.putConstraint(SpringLayout.EAST, chooseConnection,
                    -((int) (height / 4)), SpringLayout.EAST,
                    frame.getContentPane());

            frame.getContentPane().add(rmiBtn);
            frame.getContentPane().add(sckBtn);
            frame.getContentPane().add(chooseConnection);

            Image backgroundResize = background.getScaledInstance(
                    (int) (width / 2), (int) (height * (0.8)),
                    Image.SCALE_DEFAULT);

            JLabel backgroundLbl = new JLabel(new ImageIcon(backgroundResize));
            backgroundLbl.setAlignmentX(Component.CENTER_ALIGNMENT);

            springLayout.putConstraint(SpringLayout.NORTH, backgroundLbl, 0,
                    SpringLayout.NORTH, frame.getContentPane());
            springLayout.putConstraint(SpringLayout.WEST, backgroundLbl, 0,
                    SpringLayout.WEST, frame.getContentPane());
            springLayout.putConstraint(SpringLayout.SOUTH, backgroundLbl, 0,
                    SpringLayout.SOUTH, frame.getContentPane());
            springLayout.putConstraint(SpringLayout.EAST, backgroundLbl, 0,
                    SpringLayout.EAST, frame.getContentPane());

            frame.getContentPane().add(backgroundLbl);

        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    public void chooseConnection(InterfaceControl interfaceControl) {
        this.interfaceControl = interfaceControl;
        frame.setVisible(true);

    }
}
