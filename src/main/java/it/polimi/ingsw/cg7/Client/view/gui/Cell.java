package it.polimi.ingsw.cg7.Client.view.gui;

import it.polimi.ingsw.cg7.Client.model.ViewSectorType;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.TexturePaint;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.JLabel;

/**
 * The Class represents the generic Cell.
 */
public class Cell extends JLabel {

    /** The size. */
    private static int size;

    /** The width. */
    private static int width;

    /** The height. */
    private static int height;


    /** The height screen. */
    private static int heightScreen;
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The type. */
    private final ViewSectorType type;

    /** The x lett. */
    private int xLett;

    /** The y. */
    private int y;

    /** The Coordinate. */
    private String Coordinate;

    /** The center. */
    private Coord center;

    /** The sectorp. */
    private TexturePaint sectorp;

    /** The hatch sector. */
    private BufferedImage dangerousSector, safeSector, alienStartSector,
            humanStartSector, hatchSector, underlinedSector, alien, human;

    /**
     * Instantiates a new cell.
     *
     * @param center
     *            the center
     * @param type
     *            the type
     * @param xLett
     *            the x lett
     * @param yCoordinate
     *            the y coordinate
     */
    public Cell(Coord center, ViewSectorType type, int xLett, int yCoordinate) {

        Toolkit t = Toolkit.getDefaultToolkit();
        Dimension screenSize = t.getScreenSize();

        heightScreen = (int) screenSize.getHeight();

        size = (int) (heightScreen * (0.026041));
        width = size * 2;
        height = (int) ((Math.sqrt(3) * 0.5) * size * 2);

        this.type = type;
        this.setSize((int) (width), (int) (height + 10));
        int x = ((int) (center.getX() + width * 0.5));
        int y = ((int) (center.getY() + height * 0.5));
        this.center = new Coord(x, y);
        this.xLett = xLett;
        this.y = yCoordinate;

        loadImage();

        char xCoord = (char) (xLett + 65);
        if (yCoordinate >= 10)
            this.Coordinate = String.valueOf(xCoord)
                    + Integer.toString(yCoordinate);
        else
            this.Coordinate = String.valueOf(xCoord) + "0"
                    + Integer.toString(yCoordinate);
        setCoordinate();

        this.setLocation(x, y);
        repaint();
    }

    public int getXCell() {
        return this.xLett;
    }

    public int getYCell() {
        return this.y;
    }
    public ViewSectorType getType() {
        return type;
    }

    /**
     * Sets the coordinate.
     */
    private void setCoordinate() {
        if (this.type.equals(ViewSectorType.DANGEROUS)
                || this.type.equals(ViewSectorType.SAFE)) {
            JLabel coord = new JLabel(this.Coordinate);
            coord.setFont(new Font("Tahoma", Font.PLAIN, (int) (size * 0.5)));
            coord.setForeground(Color.DARK_GRAY);
            coord.setBounds(this.getBounds());
            coord.setLocation(this.getBounds().x + (int) (width * 0.3),
                    this.getBounds().y);
            this.add(coord);
            repaint();
        }
    }

    /**
     * Loading all contents [images].
     */
    private void loadImage() {
        try {
            dangerousSector = ImageIO.read(new File(
                    ".//Game Media//Map//Dangerous_Sector[HighRes].png"));
            safeSector = ImageIO.read(new File(
                    ".//Game Media//Map//Secure_Sector.png"));
            alienStartSector = ImageIO.read(new File(
                    ".//Game Media//Map//StartingPointAliens_Sector.png"));
            humanStartSector = ImageIO.read(new File(
                    ".//Game Media//Map//StartingPointHuman_Sector.png"));
            hatchSector = ImageIO.read(new File(
                    ".//Game Media//Map//EscapeHatch_Sector.png"));
            underlinedSector = ImageIO.read(new File(
                    ".//Game Media//Map//UnderlinedSector.png"));
            alien = ImageIO.read(new File(".//Game Media//Map//ALIEN.png"));
            
            human = ImageIO.read(new File(".//Game Media//Map//HUMAN.png"));
        } catch (IOException ex) {
            Logger.getLogger(this.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Tell who you are [Coordinate].
     *
     * @param e
     *            the e
     * @return the string
     */
    public String tellWhoYouAre(MouseEvent e) {
        if (getPolygon().contains(e.getPoint()))
            return (this.Coordinate);
        return null;
    }

    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g.create();
        RenderingHints rh = new RenderingHints(
                RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g2d.setRenderingHints(rh);

        g2d.drawPolygon(getPolygon());
        switch (this.type) {
        case DANGEROUS:
            sectorp = new TexturePaint(dangerousSector, getPolygon()
                    .getBounds2D());
            break;
        case SAFE:
            sectorp = new TexturePaint(safeSector, getPolygon().getBounds2D());
            break;
        case HATCH:
            sectorp = new TexturePaint(hatchSector, getPolygon().getBounds2D());
            break;
        case ALIENSTARTPOINT:
            sectorp = new TexturePaint(alienStartSector, getPolygon()
                    .getBounds2D());
            break;
        case HUMANSTARTPOINT:
            sectorp = new TexturePaint(humanStartSector, getPolygon()
                    .getBounds2D());
            break;
        case UNDERLINEDSECTOR:
            sectorp = new TexturePaint(underlinedSector, getPolygon()
                    .getBounds2D());
            break;
        case CLICKED:
            g2d.setColor(new Color((float) 1.0, (float) 0.0, (float) 0.0,
                    (float) 0.4));
        case MYPOSALIEN:
            sectorp = new TexturePaint(alien, getPolygon().getBounds2D());
            break;
        case MYPOSHUMAN:
            sectorp = new TexturePaint(human, getPolygon().getBounds2D());
        default:
            break;
        }
        if (this.type != ViewSectorType.CLICKED)
            g2d.setPaint(sectorp);
        g2d.fillPolygon(getPolygon());
        // super.paintComponent(g);
    }

    /**
     * Gets the hexagon.
     *
     * @return the hexagon
     */
    private Polygon getPolygon() {
        int[] xValues = new int[6];
        int[] yValues = new int[6];
        for (int i = 0; i < 6; i++) {
            double angle_deg = 60 * i;
            double angle_rad = (Math.PI / 180) * angle_deg;
            xValues[i] = this.center.getX()
                    - (int) (size * Math.cos(angle_rad))
                    - (int) (width * 0.5);
            yValues[i] = this.center.getY()
                    - (int) (size * Math.sin(angle_rad))
                    - (int) (height * 0.5);
        }
        return new Polygon(xValues, yValues, 6);
    }
}
