package it.polimi.ingsw.cg7.Client.model;

import it.polimi.ingsw.cg7.model.CharType;
import it.polimi.ingsw.cg7.model.PlayingState;

import java.io.Serializable;

/**
 * The Class ViewPlayer.
 * Authors: Debora Rebai
 */
public class ViewPlayer implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /** The username. */
    private String username;

    /** The kill number. */
    private int killNumber;

    /** The cards. */
    private int cards;

    /** The state. */
    private PlayingState state;

    /** The type. */
    private CharType type;

    /**
     * Sets the state.
     *
     * @param state
     *            the new state
     */
    public PlayingState getPlayingState(PlayingState state) {
        return state;
    }

    /**
     * Checks if is done movement.
     *
     * @return true, if is done movement
     */
    public boolean isDoneMovement() {
        return doneMovement;
    }

    /**
     * Sets the done movement.
     *
     * @param doneMovement
     *            the new done movement
     */
    public void setDoneMovement(boolean doneMovement) {
        this.doneMovement = doneMovement;
    }

    /**
     * Checks if is done attack card.
     *
     * @return true, if is done attack card
     */
    public boolean isDoneAttackCard() {
        return doneAttackCard;
    }

    /**
     * Sets the done attack card.
     *
     * @param doneAttackCard
     *            the new done attack card
     */
    public void setDoneAttackCard(boolean doneAttackCard) {
        this.doneAttackCard = doneAttackCard;
    }

    /** The done movement. */
    private boolean doneMovement;

    /** The done attack card. */
    private boolean doneAttackCard;

    /**
     * Instantiates a new view player.
     *
     * @param username
     *            the username
     */
    public ViewPlayer(String username) {
        this.username = username;
    }

    public ViewPlayer(String username, CharType type) {
        this.username = username;
        this.type = type;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public CharType getType() {
        return type;
    }

    /**
     * Sets the type.
     *
     * @param type
     *            the new type
     */
    public void setType(CharType type) {
        this.type = type;
    }

    /**
     * Gets the username.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the username.
     *
     * @param username
     *            the new username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets the kill number.
     *
     * @return the kill number
     */
    public int getKillNumber() {
        return killNumber;
    }

    /**
     * Sets the kill number.
     *
     * @param killNumber
     *            the new kill number
     */
    public void setKillNumber(int killNumber) {
        this.killNumber = killNumber;
    }

    /**
     * Gets the cards.
     *
     * @return the cards
     */
    public int getCards() {
        return cards;
    }

    /**
     * Sets the cards.
     *
     * @param cards
     *            the new cards
     */
    public void setCards(int cards) {
        this.cards = cards;
    }

    /**
     * Sets the playing state.
     *
     * @param state
     *            the new playing state
     */
    public void setPlayingState(PlayingState state) {
        this.state = state;
    }

    /**
     * Gets the playing state.
     *
     * @return the playing state
     */
    public PlayingState getPlayingState() {
        return state;
    }

}
