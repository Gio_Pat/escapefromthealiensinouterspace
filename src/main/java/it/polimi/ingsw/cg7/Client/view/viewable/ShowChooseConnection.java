package it.polimi.ingsw.cg7.Client.view.viewable;

import it.polimi.ingsw.cg7.Client.controller.InterfaceControl;

/**
 * The Interface ShowChooseConnection.
 */
public interface ShowChooseConnection {

    /**
     * Choose connection.
     *
     * @param interfaceControl the interface control
     */
    public void chooseConnection(InterfaceControl interfaceControl);

}
