package it.polimi.ingsw.cg7.controller;

import it.polimi.ingsw.cg7.model.Attack;
import it.polimi.ingsw.cg7.model.CharType;
import it.polimi.ingsw.cg7.model.Player;

public class AttackItemAction extends ItemAction {

    private PlayerStateHandler playerStateHandler;

    public AttackItemAction() {

    }

    @Override
    public boolean actionValidation(Player player,
            PlayerStateHandler playerStateHandler) {

        this.playerStateHandler = playerStateHandler;
        if ((player.getType() == CharType.HUMAN)
                && (player.getPosition().validationAttack())) {
            if (execute(player))
                return true;
            return false;
        }
        return false;

    }

    private boolean execute(Player player) {

        player.addAttack();
        AttackAction attack = new AttackAction();
        if (attack.actionValidation(player, playerStateHandler)) {
            player.getDeck().removeCard(new Attack());
            return true;
        }
        return false;
    }

}
