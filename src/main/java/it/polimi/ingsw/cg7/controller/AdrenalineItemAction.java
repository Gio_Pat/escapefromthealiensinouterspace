package it.polimi.ingsw.cg7.controller;

import it.polimi.ingsw.cg7.model.Adrenaline;
import it.polimi.ingsw.cg7.model.CharType;
import it.polimi.ingsw.cg7.model.Player;

// TODO: Auto-generated Javadoc
/**
 * The Class AdrenalineItemAction.
 */
public class AdrenalineItemAction extends ItemAction {

    /**
     * Instantiates a new adrenaline item action.
     */
    public AdrenalineItemAction() {

    }

    @Override
    public boolean actionValidation(Player player,
            PlayerStateHandler playerStateHandler) {

        if (player.getType() == CharType.HUMAN) {
            execute(player);
            return true;
        }
        return false;

    }

    /**
     * Execute the action.
     *
     * @param player
     *            the player
     */
    private void execute(Player player) {
        player.addBuff();
        player.getDeck().removeCard(new Adrenaline());
    }

}
