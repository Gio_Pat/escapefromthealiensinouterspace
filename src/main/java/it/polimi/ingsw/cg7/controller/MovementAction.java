/**
 * 
 */
package it.polimi.ingsw.cg7.controller;

import it.polimi.ingsw.cg7.Client.model.flagResponseType;
import it.polimi.ingsw.cg7.model.CharType;
import it.polimi.ingsw.cg7.model.Coordinate;
import it.polimi.ingsw.cg7.model.Game;
import it.polimi.ingsw.cg7.model.Player;
import it.polimi.ingsw.cg7.model.PlayingState;
import it.polimi.ingsw.cg7.model.Sector;
import it.polimi.ingsw.cg7.model.Sedatives;

import java.util.HashSet;

/**
 * The Class MovementAction.
 *
 * @author Deb
 */

public class MovementAction {

    /** The movement sector. */
    private HashSet<Sector> movementSector;
    
    /** The game. */
    private Game game;
    
    /** The player state handler. */
    private PlayerStateHandler playerStateHandler;

    /**
     * Instantiates a new movement action.
     *
     * @param playerStateHandler the player state handler
     */
    public MovementAction(PlayerStateHandler playerStateHandler) {
        this.movementSector = new HashSet<Sector>();
        this.game = playerStateHandler.getGame();
        this.playerStateHandler = playerStateHandler;
    }

    /**
     * Validation for movement action.
     *
     * @param player the player
     * @param coord the coord
     * @return the flag response type
     */

    public flagResponseType actionValidation(Player player, Coordinate coord) {

        Sector sector = game.getMap().getSector(coord);
        if (sector != null)
            if (sector.validationMove(player)) {
                int iterator = 1;
                if (player.ifBuffed())
                    iterator++;
                if (player.getType() == CharType.ALIEN)
                    iterator++;
                searchNearSectors(iterator, player.getPosition(), player);
                if (movementSector.contains(player.getPosition()))
                    movementSector.remove(player.getPosition());
                if (movementSector.contains(sector)) {
                    player.removeSedative();
                    return (execute(player, sector));

                }
            }
        return flagResponseType.FALSE;

    }

    /**
     * Moves the player in a sector.
     *
     * @param player the player
     * @param sector the sector
     * @return the flag response type
     */

    private flagResponseType execute(Player player, Sector sector) {
        Sector removeSector = player.getPosition();
        removeSector.removePlayer(player);
        player.setPosition(sector);
        player.removeBuff();
        if (!sector.canDraw())
            return sector.controlDrawCard(game, playerStateHandler, player);
        if (player.getType() == CharType.HUMAN) {
            if (sector.canEscape()) {
                flagResponseType flag = sector.controlDrawCard(game,
                        playerStateHandler, player);
                if (flag == flagResponseType.GREEN) {
                    player.setState(PlayingState.WINNER);
                }
                return flag;
            }
            if (!(player.getDeck().containsCard(new Sedatives()))) {
                flagResponseType flag = sector.controlDrawCard(game,
                        playerStateHandler, player);
                return flag;

            }
        }

        return flagResponseType.TRUE;
    }

    /**
     * Recursive function that search near sector;.
     *
     * @param iterator the iterator
     * @param sector the sector
     * @param player the player
     */

    private void searchNearSectors(int iterator, Sector sector, Player player) {

        movementSector.addAll(sector.getNearSector());
        iterator--;
        HashSet<Sector> impossibleSectors = new HashSet<Sector>();
        for (Sector s : movementSector)
            if (!s.validationMove(player))
                impossibleSectors.add(s);
        for (Sector s : impossibleSectors)
            if (movementSector.contains(s))
                movementSector.remove(s);
        if (iterator != 0)
            for (Sector sectorSearch : sector.getNearSector()) {
                if (sectorSearch.validationMove(player))
                    searchNearSectors(iterator, sectorSearch, player);
            }
    }

}
