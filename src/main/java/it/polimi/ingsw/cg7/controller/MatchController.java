package it.polimi.ingsw.cg7.controller;

import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;
import it.polimi.ingsw.cg7.Client.model.NetTypeBroker;
import it.polimi.ingsw.cg7.Client.model.ViewPlayer;
import it.polimi.ingsw.cg7.model.Card;
import it.polimi.ingsw.cg7.model.CharType;
import it.polimi.ingsw.cg7.model.Player;
import it.polimi.ingsw.cg7.model.PlayingState;

import java.util.ArrayList;
import java.util.List;

/**
 * @version 1.0
 * @author Deb
 *
 */

public class MatchController {

    private PlayerStateHandler playerStateHandler;
    private List<Player> players;

    public MatchController(PlayerStateHandler playerStateHandler) {
        this.playerStateHandler = playerStateHandler;
    }

    /**
     * This function kill every players that stay in sector of param Player
     * exception param Player. If one player have an Defense item he's save and
     * his card is remove of deck's player.
     * 
     * @param player
     */

    public void kill(Player player) {
        players = player.getPosition().getPlayers();
        players.remove(player);
        List<Player> playersRemove = new ArrayList<Player>();
        for (Player p : players)
            if (!defenseCheck(p))
                if (p.getState() == PlayingState.PLAYING) {
                    p.setState(PlayingState.LOSER);
                    playersRemove.add(p);
                    player.addKill();
                    if (player.getType() == CharType.ALIEN)
                        player.addBuff();
                }

        if (playersRemove.size() != 0) {
            NetBrokerMessage message = new NetBrokerMessage(
                    NetTypeBroker.PLAYERSDEAD);
            List<ViewPlayer> players = new ArrayList<ViewPlayer>();
            for (Player p : playersRemove)
                players.add(new ViewPlayer(p.getUsername()));
            message.playersDead(players);
            LobbyManager.notifyLobby(playerStateHandler.getGame(), message);
            playerStateHandler.updateState();
        }
    }

    /**
     * This function control if a player have a defense card. If the player have
     * a defense card, a card will be discarded
     * 
     * @param player
     * @return true if the player have a defense card, false otherwise
     */

    private boolean defenseCheck(Player player) {
        List<Card> cards = new ArrayList<Card>(player.getDeck().getCards());
        for (Card card : cards)
            if (card.getDescription().equals("Defense")) {
                player.getDeck().removeCard(card);
                return true;
            }
        return false;
    }

}
