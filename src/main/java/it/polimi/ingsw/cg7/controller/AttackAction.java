package it.polimi.ingsw.cg7.controller;

import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;
import it.polimi.ingsw.cg7.Client.model.NetTypeBroker;
import it.polimi.ingsw.cg7.Client.model.ViewPlayer;
import it.polimi.ingsw.cg7.Client.model.ViewPosition;
import it.polimi.ingsw.cg7.model.CharType;
import it.polimi.ingsw.cg7.model.Player;
import it.polimi.ingsw.cg7.model.Sector;

import java.rmi.RemoteException;

// TODO: Auto-generated Javadoc
/**
 * The Class AttackAction.
 */
public class AttackAction extends Action {

    /** The match controller. */
    MatchController matchController;

    /**
     * Instantiates a new attack action.
     */
    public AttackAction() {
        this.matchController = null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * it.polimi.ingsw.cg7.controller.Action#actionValidation(it.polimi.ingsw
     * .cg7.model.Player, it.polimi.ingsw.cg7.controller.PlayerStateHandler)
     */
    @Override
    public boolean actionValidation(Player player,
            PlayerStateHandler playerStateHandler) {
        Sector sector = player.getPosition();
        if ((sector.validationAttack())
                && ((player.getType() == CharType.ALIEN) || (player.canAttack()))) {
            execute(player, playerStateHandler);
            return true;
        }
        return false;
    }

    /**
     * Execute.
     *
     * @param player
     *            the player
     * @param playerStateHandler
     *            the player state handler
     * @throws RemoteException
     */
    private void execute(Player player, PlayerStateHandler playerStateHandler) {
        player.removeAttack();
        LobbyManager.notifyLobby(playerStateHandler.getGame(),
                createMsgToSend(player));
        matchController = new MatchController(playerStateHandler);
        matchController.kill(player);
    }

    private NetBrokerMessage createMsgToSend(Player player) {
        NetBrokerMessage msg = new NetBrokerMessage(NetTypeBroker.ATTACK);
        msg.attack(new ViewPosition(player.getPosition().getCoordinates()
                .getxLett(), player.getPosition().getCoordinates().getY()),
                new ViewPlayer(player.getUsername()));
        return msg;
    }
}
