package it.polimi.ingsw.cg7.controller;

import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;
import it.polimi.ingsw.cg7.Client.model.NetTypeBroker;
import it.polimi.ingsw.cg7.Client.model.ViewPlayer;
import it.polimi.ingsw.cg7.model.CharType;
import it.polimi.ingsw.cg7.model.Game;
import it.polimi.ingsw.cg7.model.Map;
import it.polimi.ingsw.cg7.model.Player;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.Timer;

/**
 * @version 1.0
 * @author Deb
 *
 */

public class GameController {

    private Game game;
    private PlayerStateHandler playerStateHandler;
    /** Timer for the movement */
    private Timer moveTimer;
    /** Action listener **/
    private ActionListener movementTimeExpired;

    public GameController(Player player, Map map) {
        this.game = new Game(player, map);
        this.playerStateHandler = new PlayerStateHandler(game);
        this.movementTimeExpired = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LobbyManager.kickPlayer(game, game.getCurrentTurn()
                        .getCurrentPlayer());
                moveTimer.restart();
                moveTimer.stop();
            }
        };
    }

    /**
     * This function add a player in the game
     * 
     * @param player
     * @return true if the function has added a player, false otherwise
     */

    public boolean addPlayer(Player player) {
        if (this.game.isFinished())
            return false;
        this.game.addPlayer(player);
        return true;
    }

    /**
     * Gets the game
     * 
     * @return game
     */

    public Game getGame() {
        return this.game;
    }

    public PlayerStateHandler getPlayerStateHandler() {
        return this.playerStateHandler;
    }

    public void changeTurn() {
        moveTimer.restart();
        game.changeTurn();
        sendTurn(game.getCurrentTurn().getCurrentPlayer());
    }

    /**
     * Started the game
     * 
     * @return true if the game has started, false if the game has started yet
     *         or has finished
     */

    public boolean startGame() {
        if ((this.game.isFinished()) || (this.game.isStarted()))
            return false;
        Date date = new Date();
        moveTimer = new Timer(90000, movementTimeExpired);
        this.game.start(date);
        moveTimer.start();
        NetBrokerMessage message = new NetBrokerMessage(NetTypeBroker.INITGAME);
        message.initGame();
        LobbyManager.notifyLobby(game, message);
        sendRole();
        sendTurn(game.getCurrentTurn().getCurrentPlayer());
        return true;
    }

    private void sendTurn(Player currentPlayer) {
        NetBrokerMessage msg = new NetBrokerMessage(NetTypeBroker.TURN);
        msg.turn();
        List<Player> player = new ArrayList<Player>();
        player.add(searchPlayer(currentPlayer));
        LobbyManager.notifyLobbyPlayers(game, msg, player);
        msg = new NetBrokerMessage(NetTypeBroker.PLAYERTURN);
        ViewPlayer playerTurn = new ViewPlayer(searchPlayer(currentPlayer)
                .getUsername());
        playerTurn.setKillNumber(searchPlayer(currentPlayer).getKillNumber());
        playerTurn.setCards(searchPlayer(currentPlayer).getDeck().getCards()
                .size());
        msg.playerTurn(playerTurn);
        LobbyManager.notifyLobby(game, msg);
    }

    private Player searchPlayer(Player currentPlayer) {

        List<Player> players = game.getPlayers();
        for (Player p : players)
            if (p.getUsername().equals(currentPlayer.getUsername()))
                return p;
        return null;
    }

    private void sendRole() {
        List<Player> Aliens = new ArrayList<Player>();
        List<Player> Human = new ArrayList<Player>();
        for (Player p : game.getPlayers())
            if (p.getType() == CharType.ALIEN)
                Aliens.add(p);
            else
                Human.add(p);
        NetBrokerMessage msg = new NetBrokerMessage(NetTypeBroker.ROLEPLAYER);
        msg.rolePlayer(new ViewPlayer("Type", CharType.ALIEN));
        LobbyManager.notifyLobbyPlayers(game, msg, Aliens);
        msg.rolePlayer(new ViewPlayer("Type", CharType.HUMAN));
        LobbyManager.notifyLobbyPlayers(game, msg, Human);

    }

    public void finishGame() {
        this.game.finishGame();
        NetBrokerMessage message = new NetBrokerMessage(
                NetTypeBroker.FINISHGAME);
        message.finishGame();
        moveTimer.stop();
        LobbyManager.notifyLobby(game, message);
    }

}
