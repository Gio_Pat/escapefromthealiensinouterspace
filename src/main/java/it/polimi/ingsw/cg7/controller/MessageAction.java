package it.polimi.ingsw.cg7.controller;

import it.polimi.ingsw.cg7.model.Player;

/**
 * The Class MessageAction.
 */
public class MessageAction extends Action {

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.controller.Action#actionValidation(it.polimi.ingsw.cg7.model.Player, it.polimi.ingsw.cg7.controller.PlayerStateHandler)
     */
    @Override
    public boolean actionValidation(Player player,
            PlayerStateHandler playerStateHandler) {

        return true;

    }

}
