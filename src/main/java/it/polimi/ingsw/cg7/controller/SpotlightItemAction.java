package it.polimi.ingsw.cg7.controller;

import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;
import it.polimi.ingsw.cg7.Client.model.NetTypeBroker;
import it.polimi.ingsw.cg7.Client.model.ViewPlayer;
import it.polimi.ingsw.cg7.Client.model.ViewPosition;
import it.polimi.ingsw.cg7.model.CharType;
import it.polimi.ingsw.cg7.model.Player;
import it.polimi.ingsw.cg7.model.Sector;
import it.polimi.ingsw.cg7.model.Spotlight;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Class SpotlightItemAction.
 * 
 * @author Debora
 */
public class SpotlightItemAction {

    /** The player state handler. */
    private PlayerStateHandler playerStateHandler;

    /**
     * Instantiates a new spotlight item action.
     */
    public SpotlightItemAction() {
    }

    /**
     * Action validation.
     *
     * @param sector the sector
     * @param player the player
     * @param playerStateHandler the player state handler
     * @return true, if successful
     */
    public boolean actionValidation(Sector sector, Player player,
            PlayerStateHandler playerStateHandler) {

        this.playerStateHandler = playerStateHandler;
        if (player.getType() == CharType.HUMAN) {
            execute(sector, player);
            return true;
        }
        return false;

    }

    /**
     * Execute.
     *
     * @param sector the sector
     * @param player the player
     */
    private void execute(Sector sector, Player player) {
        player.getDeck().removeCard(new Spotlight());
        List<Player> players = new ArrayList<Player>();
        Map<ViewPlayer, ViewPosition> msgPlayers = new HashMap<ViewPlayer, ViewPosition>();
        List<Sector> sectors = new ArrayList<Sector>();
        NetBrokerMessage message = new NetBrokerMessage(
                NetTypeBroker.PLAYERSLIGHT);
        sectors.add(sector);
        sectors.addAll(sector.getNearSector());
        for (Sector s : sectors)
            players.addAll(s.getPlayers());
        for (Player p : players)
            msgPlayers.put(new ViewPlayer(p.getUsername()), new ViewPosition(p
                    .getPosition().getCoordinates().getxLett(), p.getPosition()
                    .getCoordinates().getY()));
        message.playersLight(msgPlayers, new ViewPosition(sector
                .getCoordinates().getxLett(), sector.getCoordinates().getY()));
        LobbyManager.notifyLobby(playerStateHandler.getGame(), message);
    }

}
