package it.polimi.ingsw.cg7.controller;

import it.polimi.ingsw.cg7.model.Player;

/**
 * The Class Action.
 */
public abstract class Action {

    /**
     * Action validation.
     *
     * @param player the player
     * @param playerStateHandler the player state handler
     * @return true, if successful
     */
    public abstract boolean actionValidation(Player player,
            PlayerStateHandler playerStateHandler);

}
