package it.polimi.ingsw.cg7.controller;

import it.polimi.ingsw.cg7.model.Player;

public abstract class ItemAction extends Action {

    @Override
    public abstract boolean actionValidation(Player player,
            PlayerStateHandler playerStateHandler);

}
