package it.polimi.ingsw.cg7.controller;

import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;
import it.polimi.ingsw.cg7.Client.model.NetResponseMessage;
import it.polimi.ingsw.cg7.Client.model.NetTypeBroker;
import it.polimi.ingsw.cg7.Client.model.NetTypeRequest;
import it.polimi.ingsw.cg7.Client.model.ViewPlayer;
import it.polimi.ingsw.cg7.Client.model.ViewPosition;
import it.polimi.ingsw.cg7.Client.model.flagResponseType;
import it.polimi.ingsw.cg7.model.Card;
import it.polimi.ingsw.cg7.model.CharType;
import it.polimi.ingsw.cg7.model.Coordinate;
import it.polimi.ingsw.cg7.model.Game;
import it.polimi.ingsw.cg7.model.Item;
import it.polimi.ingsw.cg7.model.Player;
import it.polimi.ingsw.cg7.model.Sector;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class MoveValidation.
 */
public class MoveValidation {

    /** The player. */
    private Player player;

    /** The game. */
    private Game game;

    /** The player state handler. */
    protected PlayerStateHandler playerStateHandler;

    /** The move. */
    protected Action move;

    /** The movement. */
    protected MovementAction movement;

    /**
     * Instantiates a new move validation.
     *
     * @param player
     *            the player
     * @param game
     *            the game
     */
    public MoveValidation(Player player, Game game) {
        this.player = player;
        this.game = game;
        this.playerStateHandler = new PlayerStateHandler(game);
    }

    /**
     * Attack request.
     *
     * @return the net response message
     */
    public NetResponseMessage attackRequest() {

        NetResponseMessage msg = new NetResponseMessage(NetTypeRequest.ATTACK);
        if (player.getDeck().getCards().size() == 4) {
            msg.attackRequest("You must discard an item card", false);
            return msg;
        }
        if (!game.getCurrentTurn().isFirstMovement()) {
            msg.attackRequest("Movement not executed yet", false);
            return msg;
        }
        if (game.getCurrentTurn().isAttack()) {
            msg.attackRequest("Attack already executed", false);
            return msg;
        }
        if (game.getCurrentTurn().getFlag() == flagResponseType.FALSERUMOR) {

            msg.attackRequest("You have to say one sector", false);
            return msg;
        }
        move = new AttackAction();
        if (move.actionValidation(player, playerStateHandler)) {
            this.game.getCurrentTurn().setAttack();
            msg.attackRequest("Attack executed", true);
            this.playerStateHandler.updateState();
            return msg;
        }
        msg.attackRequest("You can't perform the attack", false);
        return msg;
    }

    /**
     * Movement request.
     *
     * @param coord
     *            the coord
     * @return the net response message
     */
    public NetResponseMessage movementRequest(Coordinate coord) {
        NetResponseMessage msg = new NetResponseMessage(NetTypeRequest.MOVEMENT);
        if (game.getCurrentTurn().isFirstMovement()) {
            msg.movementResponse("Movement already executed", false,
                    flagResponseType.FALSE);
            return msg;
        }
        if (player.getDeck().getCards().size() == 4) {
            msg.movementResponse("You must discard an item card", false,
                    flagResponseType.FALSE);
            return msg;
        }
        if (game.getCurrentTurn().getFlag() == flagResponseType.FALSERUMOR) {
            msg.movementResponse("You have to say one sector", false,
                    flagResponseType.FALSERUMOR);
            return msg;
        }

        movement = new MovementAction(playerStateHandler);
        game.getCurrentTurn().changeFlag(
                movement.actionValidation(player, coord));
        if (game.getCurrentTurn().getFlag() != flagResponseType.FALSE) {
            this.game.getCurrentTurn().setFirstMovement();
            this.playerStateHandler.updateState();
            msg.movementResponse("Movement executed", true, game
                    .getCurrentTurn().getFlag());
            if (game.getCurrentTurn().getFlag() == flagResponseType.TRUE) {
                notifyFlag(game.getCurrentTurn().getFlag());
                return msg;
            }
            notifyAll(game.getCurrentTurn().getFlag());
            return msg;
        }
        msg.movementResponse("You can't perform the movement", false,
                flagResponseType.FALSE);
        return msg;
    }

    /**
     * Notify all.
     *
     * @param flag
     *            the flag
     */
    private void notifyAll(flagResponseType flag) {
        if (flag == flagResponseType.TRUERUMOR) {
            NetBrokerMessage msg = new NetBrokerMessage(
                    NetTypeBroker.PLAYERRUMORS);
            msg.playerRumor(new ViewPosition(player.getPosition()
                    .getCoordinates().getxLett(), player.getPosition()
                    .getCoordinates().getY()),
                    new ViewPlayer(player.getUsername()));
            LobbyManager.notifyLobby(game, msg);
        }
        if (flag == flagResponseType.SILENCE) {
            NetBrokerMessage msg = new NetBrokerMessage(NetTypeBroker.SILENCE);
            msg.sendSilence(new ViewPlayer(player.getUsername()));
            LobbyManager.notifyLobby(game, msg);
        }
        if (flag == flagResponseType.GREEN || flag == flagResponseType.RED) {

            boolean escape;
            if (flag == flagResponseType.GREEN)
                escape = true;
            else
                escape = false;
            NetBrokerMessage msg = new NetBrokerMessage(NetTypeBroker.ESCAPE);
            msg.hatchCard(new ViewPlayer(player.getUsername()),
                    new ViewPosition(player.getPosition().getCoordinates()
                            .getxLett(), player.getPosition().getCoordinates()
                            .getY()), escape);
            LobbyManager.notifyLobby(game, msg);
        }

    }

    /**
     * Notify flag.
     *
     * @param flag
     *            the flag
     */
    private void notifyFlag(flagResponseType flag) {

        NetBrokerMessage msg = new NetBrokerMessage(NetTypeBroker.TYPESECTOR);
        msg.setTypeSector(flag);
        List<Player> players = new ArrayList<Player>();
        Player currentPlayer = game.getCurrentTurn().getCurrentPlayer();
        for (Player p : game.getPlayers())
            if (p.getUsername().equals(currentPlayer.getUsername()))
                players.add(p);
        LobbyManager.notifyLobbyPlayers(game, msg, players);
    }

    /**
     * Use item request.
     *
     * @param coord
     *            the coord
     * @param card
     *            the card
     * @return the net response message
     */
    public NetResponseMessage useItemRequest(Coordinate coord, Card card) {

        Sector sector = game.getMap().getSector(coord);
        NetResponseMessage msg = new NetResponseMessage(NetTypeRequest.USEITEM);
        if (sector == null && card.getDescription().equals("Spotlight")) {
            msg.useItemRequest("This sector don't exist", false);
            return msg;
        }

        if (player.getType() != CharType.HUMAN) {
            msg.useItemRequest("You are an Alien so you can't use items", false);
            return msg;
        }
        if (game.getCurrentTurn().getFlag() == flagResponseType.FALSERUMOR) {
            msg.useItemRequest("You have to say one sector", false);
            return msg;
        }
        if (player.getDeck().containsCard(card))
            if (((Item) card).use(sector, player, playerStateHandler)) {
                msg.useItemRequest("Card " + card.getDescription() + " used",
                        true);
                return msg;
            }
        msg.useItemRequest("You can't use this card now", false);
        return msg;

    }

    /**
     * Finish turn.
     *
     * @return the net response message
     */
    public NetResponseMessage finishTurn() {
        NetResponseMessage msg = new NetResponseMessage(
                NetTypeRequest.FINISHTURN);
        if (player.getDeck().getCards().size() == 4) {
            msg.finishTurnRequest("You must discard an item card", false);
            return msg;
        }
        if (!game.getCurrentTurn().isFirstMovement()) {
            msg.finishTurnRequest("Movement not executed yet", false);
            return msg;
        }
        if (game.getCurrentTurn().getFlag() == flagResponseType.FALSERUMOR) {
            msg.finishTurnRequest("You have to say one sector", false);
            return msg;
        }
        LobbyManager.getLobby(game).getLobbyGameController().changeTurn();
        msg.finishTurnRequest("Turn change, you have finished your turn", true);
        return msg;
    }

    /**
     * Rumors.
     *
     * @param sector
     *            the sector
     * @return the net response message
     */
    public NetResponseMessage rumors(Sector sector) {
        NetResponseMessage msg = new NetResponseMessage(
                NetTypeRequest.SENDFALSERUMORS);
        if (sector == null) {
            msg.sendFalseRumor("This sector not exists", false);
            return msg;
        }
        if (game.getCurrentTurn().getFlag() == flagResponseType.FALSERUMOR) {
            NetBrokerMessage message = new NetBrokerMessage(
                    NetTypeBroker.PLAYERRUMORS);
            message.playerRumor(new ViewPosition(sector.getCoordinates()
                    .getxLett(), sector.getCoordinates().getY()),
                    new ViewPlayer(player.getUsername()));
            LobbyManager.notifyLobby(game, message);
            game.getCurrentTurn().changeFlag(flagResponseType.FALSE);
            msg.sendFalseRumor("False rumor made", true);
            return msg;
        }
        msg.sendFalseRumor("You can't make any rumor", false);
        return msg;
    }

    /**
     * Discard.
     *
     * @param item
     *            the item
     * @return the net response message
     */
    public NetResponseMessage discard(Card item) {
        NetResponseMessage msg = new NetResponseMessage(NetTypeRequest.DISCARD);
        if (player.getDeck().getCount() == 4)
            if (player.getDeck().containsCard(item)) {
                player.getDeck().removeCard(item);
                msg.responseDiscardRequest("Discard card", true);
            } else
                msg.responseDiscardRequest("You don't have this card", false);
        else
            msg.responseDiscardRequest("You don't have 4 cards", false);
        return msg;

    }

    public NetResponseMessage drawCardSector() {
        NetResponseMessage msg = new NetResponseMessage(NetTypeRequest.DRAW);
        game.getCurrentTurn().changeFlag(
                player.getPosition().controlDrawCard(game, playerStateHandler,
                        player));
        if (game.getCurrentTurn().getFlag() == flagResponseType.EMPTY) {
            msg.sendCard(game.getCurrentTurn().getFlag(), false);
        }
        if (game.getCurrentTurn().getFlag() == flagResponseType.TRUERUMOR) {
            NetBrokerMessage message = new NetBrokerMessage(
                    NetTypeBroker.PLAYERRUMORS);
            message.playerRumor(new ViewPosition(player.getPosition()
                    .getCoordinates().getxLett(), player.getPosition()
                    .getCoordinates().getY()),
                    new ViewPlayer(player.getUsername()));
            LobbyManager.notifyLobby(game, message);
            player.addAttack();
            msg.sendCard(game.getCurrentTurn().getFlag(), true);
            return msg;
        }
        if (game.getCurrentTurn().getFlag() == flagResponseType.FALSERUMOR
                || game.getCurrentTurn().getFlag() == flagResponseType.SILENCE)
            msg.sendCard(game.getCurrentTurn().getFlag(), true);
        else
            msg.sendCard(null, false);
        return msg;

    }
}
