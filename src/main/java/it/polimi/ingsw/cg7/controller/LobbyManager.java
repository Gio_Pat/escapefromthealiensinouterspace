package it.polimi.ingsw.cg7.controller;

import it.polimi.ingsw.cg7.Client.model.JoinRequest;
import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;
import it.polimi.ingsw.cg7.Client.model.NetRequestMessage;
import it.polimi.ingsw.cg7.Client.model.NetResponseMessage;
import it.polimi.ingsw.cg7.Client.model.NetTypeBroker;
import it.polimi.ingsw.cg7.Client.model.NetTypeRequest;
import it.polimi.ingsw.cg7.Client.model.ViewPlayer;
import it.polimi.ingsw.cg7.communication.handlers.RequestHandler;
import it.polimi.ingsw.cg7.communication.handlers.SubHandler;
import it.polimi.ingsw.cg7.model.Game;
import it.polimi.ingsw.cg7.model.Lobby;
import it.polimi.ingsw.cg7.model.Player;
import it.polimi.ingsw.cg7.model.PlayingState;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Lobby Manager Handles the connection groups, this can represent the Publisher
 * or the broker.
 *
 * @author Giovanni
 */
public class LobbyManager {

    /** The Constant Lobbies. */
    public final static HashSet<Lobby> Lobbies = new HashSet<Lobby>();

    /**
     * this method verifies if the request to join/Create a game of the
     * requesting player; All info in requestToJoin[ViewModel] $Token,Player,Map
     * if the game is up to date, it adds the player to the game else create new
     * Lobby.
     *
     * @param rh
     *            the rh
     * @param r
     *            the r
     * @return the net response message
     */

    /**
     * Join lobby.
     *
     * @param requestToJoin
     *            the request to join
     */

    // public synchronized static void joinLobby(JoinRequest requestToJoin) {
    // // Verify if there is a Lobby with a game that is played in the
    // // requested map
    // HashSet<Lobby> mylobbies = Lobbies;
    // Player requestingPlayer = requestToJoin.getRequestingPlayer();
    // Game g = null;
    // boolean findLobby = false;
    // for (Lobby l : mylobbies)
    // // It's the same game when you play it in the same map. For us
    // if ((l.getLobbyGame().getMap().getName().equals(requestToJoin
    // .getRequestingPlayingMap().getName()))
    // && (!l.getLobbyGame().isStarted())) {
    // // LobbyManager.Lobbies.put(l.getLobbyGame(), l);
    // l.addLobbyElement(requestToJoin.getRequestingPlayerToken(),
    // requestingPlayer);
    // g = l.getLobbyGame();
    // if (!g.getPlayers().contains(requestingPlayer))
    // g.addPlayer(requestingPlayer);
    // findLobby = true;
    // }
    // if (!findLobby) {
    // GameController gameController;
    // gameController = new GameController(requestingPlayer,
    // requestToJoin.getRequestingPlayingMap());
    // LobbyManager.Lobbies.add(new Lobby(requestToJoin
    // .getRequestingPlayerToken(), requestingPlayer,
    // gameController));
    // }
    // }

    public synchronized static void joinLobby(JoinRequest requestToJoin) {
        // Verify if there is a Lobby with a game that is played in the
        // requested map
        HashSet<Lobby> mylobbies = Lobbies;
        Player requestingPlayer = requestToJoin.getRequestingPlayer();
        Game g = null;
        boolean findLobby = false;
        for (Lobby l : mylobbies)
            // It's the same game when you play it in the same map. For us
            if ((l.getLobbyGame().getMap().getName().equals(requestToJoin
                    .getSelectedMaps().getName()))
                    && (!l.getLobbyGame().isStarted())) {
                // LobbyManager.Lobbies.put(l.getLobbyGame(), l);
                l.addLobbyElement(requestToJoin.getRequestingPlayerToken(),
                        requestingPlayer);
                g = l.getLobbyGame();
                if (!g.getPlayers().contains(requestingPlayer))
                    g.addPlayer(requestingPlayer);
                findLobby = true;
            }
        if (!findLobby) {
            GameController gameController;
            gameController = new GameController(requestingPlayer,
                    new it.polimi.ingsw.cg7.model.Map(requestToJoin
                            .getSelectedMaps().getName()));
            LobbyManager.Lobbies.add(new Lobby(requestToJoin
                    .getRequestingPlayerToken(), requestingPlayer,
                    gameController));
        }
    }

    /**
     * Clear lobby set.
     */
    public static void clearLobbySet() {
        LobbyManager.Lobbies.clear();
    }

    /**
     * Gets the player game.
     *
     * @param player
     *            the player
     * @return the player game
     */
    public static Game getPlayerGame(Player player) {
        HashSet<Lobby> myLobbies = Lobbies;
        for (Lobby l : myLobbies)
            for (Player p : l.getLobbyPlayers())
                if (p.equals(player))
                    return l.getLobbyGame();
        return null;
    }

    /**
     * Gets the lobby.
     *
     * @param game
     *            the game
     * @return the lobby
     */
    public static Lobby getLobby(Game game) {
        HashSet<Lobby> myLobbies = Lobbies;
        for (Lobby l : myLobbies)
            if (l.getLobbyGame().equals(game))
                return l;
        return null;
    }

    /**
     * Gets the player.
     *
     * @param uuid
     *            the uuid
     * @return the player
     */
    public static Player getPlayer(UUID uuid) {
        HashSet<Lobby> myLobbies = Lobbies;
        for (Lobby l : myLobbies)
            if (l.getPlayerFromUUID(uuid) != null)
                return l.getPlayerFromUUID(uuid);
        return null;
    }

    /**
     * This method will be able to notify a game Handles the exception for a
     * Client disconnection.
     *
     * @param gameToNotify
     *            the game to notify
     * @param msgToSend
     *            the msg to send
     */

    public static void notifyLobby(Game gameToNotify, NetBrokerMessage msgToSend) {
        Lobby l = LobbyManager.getLobby(gameToNotify);
        if (l != null) {
            Map<UUID, Player> elements = l.getLobbyElements();
            if (elements.size() != 0)
                for (Map.Entry<UUID, Player> element : elements.entrySet()) {
                    if (SubHandler.users.get(element.getKey()) != null) {
                        try {
                            if(SubHandler.users.get(element.getKey()).isLoggedIn())
                                SubHandler.users.get(element.getKey()).send(
                                        msgToSend);
                        } catch (IOException e) {
                            kickPlayer(gameToNotify,element.getValue(),true);
                        }
                    }

                }
        }
    }   

    /**
     * this method handles the kicking Player function
     * @param game
     * @param player
     */
    public static void kickPlayer(Game game, Player player, boolean playerLoggedOut)
    {
        UUID token = null;
        NetBrokerMessage msgToSend = new NetBrokerMessage(NetTypeBroker.PLAYEROUT);
        msgToSend.disconnectPlayer(new ViewPlayer(player.getUsername()));
        Lobby l = LobbyManager.getLobby(game);
        Map<UUID, Player> elements = l.getLobbyElements();
        for (Map.Entry<UUID,Player> element : elements.entrySet())
            if(element.getValue().getUsername().equals(player.getUsername()))
                token = element.getKey();
        if(token!=null)
        {
            LobbyManager.getPlayer(token).setState(PlayingState.LOSER);
            if(playerLoggedOut)
                SubHandler.users.get(token).logOut();
        }

        LobbyManager.notifyLobby(game, msgToSend);
        l.getLobbyGameController().getPlayerStateHandler().updateState();    
        if(!game.isFinished())
            l.getLobbyGameController().changeTurn();
        
        
    }
    public static void kickPlayer(Game game, Player player)
    {
        kickPlayer(game,player,false);
    }
    /**
     * Notify lobby player list.
     *
     * @param gameToNotify
     *            the game to notify
     * @param msgToSend
     *            the msg to send
     * @param players
     *            the players
     */
    public static void notifyLobbyPlayers(Game gameToNotify,
            NetBrokerMessage msgToSend, List<Player> players) {
        Lobby l = LobbyManager.getLobby(gameToNotify);
        if (l != null) {
            Map<UUID, Player> elements = l.getLobbyElements();
            if (elements.size() != 0)
                for (Map.Entry<UUID, Player> element : elements.entrySet()) {
                    if (players.contains(element.getValue())) {
                        try {
                            if(SubHandler.users.get(element.getKey()).isLoggedIn())
                                SubHandler.users.get(element.getKey()).send(msgToSend);
                        } catch ( IOException e) {
                            kickPlayer(gameToNotify,element.getValue(), true);
                        }
                    }
                        
                }
        }
    }

    public static NetResponseMessage subscribe(RequestHandler rh,
            NetRequestMessage r) {
        NetResponseMessage res = new NetResponseMessage(
                NetTypeRequest.SUBSCRIBING);
        SubHandler s = rh.getSubscriber();

        if (r.getPlayerToken() == null) {
            UUID tempToken = UUID.randomUUID();
            res.responseSubscribing("Sottoscrizione effettuata  con successo",
                    tempToken);

        } else {

            res.responseSubscribing("Sottoscrizione effettuata  con successo",
                    r.getPlayerToken());
        }
        JoinRequest joinReq = new JoinRequest(new Player(r.getNickName()),
                res.getGivenToken(), r.getSelectedMaps());

        joinLobby(joinReq);
        SubHandler.users.put(res.getGivenToken(), s);
        return res;
    }

}
