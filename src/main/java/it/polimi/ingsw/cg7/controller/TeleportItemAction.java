package it.polimi.ingsw.cg7.controller;

import it.polimi.ingsw.cg7.model.Player;
import it.polimi.ingsw.cg7.model.Sector;
import it.polimi.ingsw.cg7.model.Teleport;

/**
 * The Class TeleportItemAction.
 */
public class TeleportItemAction extends ItemAction {

    /**
     * Instantiates a new teleport item action.
     */
    public TeleportItemAction() {

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.controller.ItemAction#actionValidation(it.polimi.ingsw.cg7.model.Player, it.polimi.ingsw.cg7.controller.PlayerStateHandler)
     */
    @Override
    public boolean actionValidation(Player player,
            PlayerStateHandler playerStateHandler) {

        execute(player, playerStateHandler);
        return true;

    }

    /**
     * Execute.
     *
     * @param player the player
     * @param playerStateHandler the player state handler
     */
    private void execute(Player player, PlayerStateHandler playerStateHandler) {
        Sector sector = player.getPosition();
        sector.removePlayer(player);
        player.setPosition(playerStateHandler.getMap().getHumanSector());
        player.getDeck().removeCard(new Teleport());
    }

}
