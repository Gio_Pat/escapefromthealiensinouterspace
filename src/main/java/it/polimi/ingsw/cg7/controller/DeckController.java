package it.polimi.ingsw.cg7.controller;

import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;
import it.polimi.ingsw.cg7.Client.model.NetTypeBroker;
import it.polimi.ingsw.cg7.Client.model.ViewCard;
import it.polimi.ingsw.cg7.Client.model.flagResponseType;
import it.polimi.ingsw.cg7.exception.AbsentCardException;
import it.polimi.ingsw.cg7.model.Card;
import it.polimi.ingsw.cg7.model.DangerousSector;
import it.polimi.ingsw.cg7.model.Deck;
import it.polimi.ingsw.cg7.model.EscapeHatch;
import it.polimi.ingsw.cg7.model.EscapeType;
import it.polimi.ingsw.cg7.model.Game;
import it.polimi.ingsw.cg7.model.Hatch;
import it.polimi.ingsw.cg7.model.Noise;
import it.polimi.ingsw.cg7.model.Player;
import it.polimi.ingsw.cg7.model.PlayingState;

import java.util.ArrayList;
import java.util.List;

/**
 * @version 1.0
 * @author Deb
 *
 */
public class DeckController {

    private Deck itemDeck;
    private Deck escapeHatchDeck;
    private Deck sectorDeck;
    private Game game;
    private flagResponseType flag;
    private PlayerStateHandler playerStateHandler;

    public DeckController(Game game, PlayerStateHandler playerStateHandler) {
        this.itemDeck = game.getItemDeck();
        this.escapeHatchDeck = game.getEscapeDeck();
        this.sectorDeck = game.getSectorDeck();
        this.game = game;
        this.playerStateHandler = playerStateHandler;
    }

    /**
     * Return if the item Deck is empty
     * 
     * @return true if it's empty, false otherwise
     */

    public boolean isItemDeckEmpty() {
        return itemDeck.isEmpty();
    }

    /**
     * Return if the escape Deck is empty
     * 
     * @return true if it's empty, false otherwise
     */

    public boolean isEscapeDeckEmpty() {
        return escapeHatchDeck.isEmpty();
    }

    /**
     * Return if the sector Deck is empty
     * 
     * @return true if it's empty, false otherwise
     */

    public boolean isSectorDeckEmpty() {
        return sectorDeck.isEmpty();
    }

    /**
     * If one deck is empty recreate a deck.
     * 
     * @return number of change deks
     */

    public int refreshDecks() {
        int count = 0;
        if (isItemDeckEmpty()) {
            game.createItemDeck();
            count++;
        }
        if (isSectorDeckEmpty()) {
            game.createSectorDeck();
            count++;
        }
        if (isEscapeDeckEmpty()) {
            game.createEscapeDeck();
            count++;
        }
        return count;
    }

    /**
     * Draw an item card
     * 
     * @param player
     */

    public void drawItemCard(Player player) {
        refreshDecks();
        Card card = itemDeck.draw();
        List<Player> notifyPlayer = new ArrayList<Player>();
        notifyPlayer.add(player);
        NetBrokerMessage message = new NetBrokerMessage(
                NetTypeBroker.RECIVECARD);
        if (card != null) {
            player.getDeck().insert(card);

            message.reciveCard(new ViewCard(card.getDescription()));

        } else {
            message.reciveCard(null);
        }
        LobbyManager.notifyLobbyPlayers(playerStateHandler.getGame(), message,
                notifyPlayer);
    }

    /**
     * Draw a sector card
     * 
     * @param player
     */

    public flagResponseType drawSectorCard(Player player) {
        refreshDecks();
        if (sectorDeck.isEmpty()) {
            return flagResponseType.EMPTY;
        }
        if (!player.isSedative()) {
            Card card = sectorDeck.draw();
            if (((DangerousSector) card).getType() == Noise.ANYSECTOR)
                flag = flagResponseType.FALSERUMOR;
            if (((DangerousSector) card).getType() == Noise.ONESECTOR)
                flag = flagResponseType.TRUERUMOR;
            if (((DangerousSector) card).getType() == Noise.SILENCE)
                flag = flagResponseType.SILENCE;
            if (((DangerousSector) card).itemFounded()) {
                drawItemCard(player);
            }
            return flag;
        } else
            return flagResponseType.FALSE;
    }

    /**
     * Draw an escape card
     * 
     * @param player
     */

    public flagResponseType drawEscapeCard(Player player) {
        refreshDecks();
        Card card = escapeHatchDeck.draw();
        if (((EscapeHatch) card).getType() == EscapeType.GREEN) {
            player.setState(PlayingState.WINNER);
            return flagResponseType.GREEN;

        } else {
            ((Hatch) (player.getPosition())).lock();
            return flagResponseType.RED;
        }

    }

    /**
     * Remove card from player's item deck
     * 
     * @param card
     * @param player
     * @throws AbsentCardException
     *             if the card is not in item deck
     */

    public void removeCard(Card card, Player player) throws AbsentCardException {
        if (!player.getDeck().removeCard(card))
            throw new AbsentCardException();
        // ritorna l'avviso che ha tolto una carta
    }

}
