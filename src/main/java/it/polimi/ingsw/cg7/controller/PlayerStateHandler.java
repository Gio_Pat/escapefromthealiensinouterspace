package it.polimi.ingsw.cg7.controller;

import it.polimi.ingsw.cg7.model.CharType;
import it.polimi.ingsw.cg7.model.Game;
import it.polimi.ingsw.cg7.model.Map;
import it.polimi.ingsw.cg7.model.Player;
import it.polimi.ingsw.cg7.model.PlayingState;

import java.util.ArrayList;
import java.util.List;

/**
 * @version 1.0
 * @author Deb
 *
 */

public class PlayerStateHandler {

    private Game game;

    public PlayerStateHandler(Game game) {
        this.game = game;
    }

    public Game getGame() {
        return this.game;
    }

    public Map getMap() {
        return this.game.getMap();
    }

    /**
     * Update lists of player
     * 
     * @return list of loser and winner player
     */

    public List<Player> updateState() {
        List<Player> notPlayingInGame = new ArrayList<Player>();
        notPlayingInGame.addAll(game.getNotPlayingPlayer());
        List<Player> removePlayers = new ArrayList<Player>();
        for (Player player : game.getPlayers()) {
            if (player.getState() != PlayingState.PLAYING) {
                removePlayers.add(player);
                if (!notPlayingInGame.contains(player))
                    game.getNotPlayingPlayer().add(player);
            }
        }
        finishGameControl();
        return removePlayers;
    }

    /**
     * Control if the game is finish The game finish if all players are loser or
     * winner or if number of round is 40
     */

    public void finishGameControl() {
        if (!game.isFinished())
            if ((alienWinner()) || (game.numberRound() > 39) || (humanWinner())
                    || (onlyHumansRemain())) {
                if (game.numberRound() > 39)
                    for (Player p : game.getPlayers())
                        if (p.getState() == PlayingState.PLAYING)
                            p.setState(PlayingState.LOSER);
                LobbyManager.getLobby(game).getLobbyGameController()
                        .finishGame();
            }
    }

    protected boolean alienWinner() {
        boolean varControlPlaying = true;
        boolean varControlLoser = false;
        for (Player player : game.getPlayers())
            if (player.getType() == CharType.HUMAN) {
                if (player.getState() == PlayingState.PLAYING)
                    varControlPlaying = false;
                if (player.getState() == PlayingState.LOSER)
                    varControlLoser = true;
            }
        if (varControlPlaying && varControlLoser)
            for (Player p : game.getPlayers())
                if (p.getState() == PlayingState.PLAYING)
                    p.setState(PlayingState.WINNER);
        return varControlPlaying && varControlLoser;
    }

    public boolean onlyHumansRemain() {
        boolean result = true;
        for (Player player : game.getPlayers())
            if (player.getType() == CharType.ALIEN
                    && player.getState() == PlayingState.PLAYING)
                result = false;
        if (result)
            for (Player p : game.getPlayers())
                if (p.getState() == PlayingState.PLAYING)
                    p.setState(PlayingState.WINNER);
        return result;
    }

    protected boolean humanWinner() {
        boolean varControl = true;
        for (Player player : game.getPlayers())
            if (player.getType() == CharType.HUMAN)
                if (!(player.getState() == PlayingState.WINNER))
                    varControl = false;
        if (varControl)
            for (Player p : game.getPlayers())
                if (p.getState() == PlayingState.PLAYING)
                    p.setState(PlayingState.LOSER);
        return varControl;
    }

    public List<Player> getPlayers()

    {
        return this.game.getPlayers();
    }

    public List<Player> getNotPlayingPlayers()

    {
        return this.game.getNotPlayingPlayer();
    }

}
