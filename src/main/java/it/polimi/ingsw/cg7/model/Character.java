package it.polimi.ingsw.cg7.model;

/**
 * The Class Character.
 */
public class Character extends Card {

    /** The type. */
    private CharType type;

    /**
     * Instantiates a new character.
     */
    public Character() {
        super("Character");
        type = CharType.HUMAN;
    }

    /**
     * Instantiates a new character.
     *
     * @param type the type
     */
    public Character(CharType type) {
        super("Character");
        this.type = type;
    }

    /**
     * Instantiates a new character.
     *
     * @param type the type
     * @param description the description
     */
    public Character(CharType type, String description) {
        super(description);
        this.type = type;
    }

    /**
     * Gets type.
     *
     * @return CharType Object
     */

    public CharType getType() {
        return type;
    }

}
