package it.polimi.ingsw.cg7.model;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * The Class Map.
 * @author Giovanni Patruno
 */
public class Map implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /** The name. */
    private String name;

    /** The sectors. */
    private List<Sector> sectors;

    /** The human sector. */
    private Sector humanSector;

    /** The alien sector. */
    private Sector alienSector;

    /**
     * Instantiates a new map.
     */
    public Map() {
        this.sectors = new ArrayList<Sector>();
        parseMap();
        initNearSectors();
    }

    /**
     * Instantiates a new map.
     *
     * @param name
     *            the name
     */
    public Map(String name) {
        this.sectors = new ArrayList<Sector>();
        this.name = name;
        parseMap(name);
        initNearSectors();
    }

    /**
     * Parses the map.
     *
     * @param name
     *            the name
     */
    private void parseMap(String name) {
        Document docMap = getDocument(name);
        Element root = docMap.getDocumentElement();
        this.name = root.getAttribute("name");
        NodeList sectorsNode = docMap.getElementsByTagName("sector");
        for (int i = 0; i < sectorsNode.getLength(); i++) {
            Node sectorElement = sectorsNode.item(i);
            if (sectorElement.getNodeType() == Node.ELEMENT_NODE) {
                Element eSector = (Element) sectorElement;
                Coordinate currCoord = new Coordinate(Integer.parseInt(eSector
                        .getAttribute("xLett")), Integer.parseInt(eSector
                        .getAttribute("yNumb")));
                Sector currSector = createSector(eSector.getAttribute("type"));
                if (currSector != null) {
                    currSector.setCoord(currCoord);
                    this.sectors.add(currSector);
                }

            }
        }
    }

    /**
     * Parses the map.
     */
    private void parseMap() {
        // Default map
        parseMap("Galilei");
    }

    // Get document from the XML file name
    /**
     * Gets the document.
     *
     * @param name
     *            the name
     * @return the document
     */
    private Document getDocument(String name) {
        File file = new File(".//XmlMaps//" + name + ".xml");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document docMap = builder.parse(file);
            docMap.getDocumentElement().normalize();
            return docMap;
        } catch (SAXException | IOException | ParserConfigurationException e) {
            e.printStackTrace();
            System.err.println("Marshalling error");
            return null;
        }
    }

    // Gived the type name in the xml file, create the right sector
    /**
     * Creates the sector.
     *
     * @param s
     *            the s
     * @return the sector
     */
    private Sector createSector(String s) {
        Sector sector;
        switch (s) {
        case "HUMANSTARTPOINT": {
            sector = new PlayingSector(StartedSectorType.HUMANSTARTPOINT);
            addHumanSector(sector);
            return sector;
        }
        case "ALIENSTARTPOINT": {
            sector = new PlayingSector(StartedSectorType.ALIENSTARTPOINT);
            addAlienSector(sector);
            return sector;
        }
        case "SAFE":
            return new PlayingSector(StartedSectorType.SAFE);
        case "HATCH":
            return new Hatch(false);
        case "DANGEROUS":
            return new Dangerous();
        default:
            return null;
        }
    }

    /**
     * Inits the near sectors.
     */
    private void initNearSectors() {
        List<Coordinate> coordList = new ArrayList<Coordinate>();
        Sector currSector;
        List<Sector> nearSectors;

        for (Sector s : this.sectors) {

            nearSectors = new ArrayList<Sector>();
            coordList = this.getNearCoordinates(s);
            for (Coordinate coord : coordList) {
                currSector = this.getSector(coord);
                if ((currSector != null) && (!currSector.equals(s)))
                    nearSectors.add(currSector);
            }
            s.setNearSectors(nearSectors);

        }
    }

    /**
     * Gets the near coordinates.
     *
     * @param s
     *            the s
     * @return the near coordinates
     */
    private List<Coordinate> getNearCoordinates(Sector s) {
        List<Coordinate> coordList = new ArrayList<Coordinate>();
        int xCoord = s.getCoordinates().getxLett();
        int yCoord = s.getCoordinates().getY();
        coordList.add(new Coordinate(xCoord + 1, yCoord));
        coordList.add(new Coordinate(xCoord - 1, yCoord));
        coordList.add(new Coordinate(xCoord, yCoord - 1));
        coordList.add(new Coordinate(xCoord, yCoord + 1));
        coordList.add(new Coordinate(xCoord + 1, yCoord + 1 + 1
                * (-2 * ((xCoord + 1) % 2))));
        coordList.add(new Coordinate(xCoord - 1, yCoord + 1 + 1
                * (-2 * ((xCoord + 1) % 2))));
        return coordList;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name
     *            the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the sectors.
     *
     * @return the sectors
     */
    public List<Sector> getSectors() {
        return sectors;
    }

    /**
     * Sets the sectors.
     *
     * @param sectors
     *            the new sectors
     */
    public void setSectors(List<Sector> sectors) {
        this.sectors = sectors;
    }

    /**
     * Gets the sector coords.
     *
     * @param sector
     *            the sector
     * @return the sector coords
     */
    public Coordinate getSectorCoords(Sector sector) {
        if (this.sectors.contains(sector)) {
            for (Sector s : this.sectors)
                if (s.equals(sector))
                    return s.getCoordinates();
        }
        return null;
    }

    /**
     * Gets the sector.
     *
     * @param coord
     *            the coord
     * @return the sector
     */
    public Sector getSector(Coordinate coord) {
        for (Sector s : this.sectors) {
            if (s.getCoordinates().equals(coord))
                return s;
        }
        return null;
    }

    /**
     * Adds the human sector.
     *
     * @param sector
     *            the sector
     */
    private void addHumanSector(Sector sector) {
        this.humanSector = sector;
    }

    /**
     * Adds the alien sector.
     *
     * @param sector
     *            the sector
     */
    private void addAlienSector(Sector sector) {
        this.alienSector = sector;
    }

    /**
     * Gets the human sector.
     *
     * @return the human sector
     */
    public Sector getHumanSector() {
        return this.humanSector;
    }

    /**
     * Gets the alien sector.
     *
     * @return the alien sector
     */
    public Sector getAlienSector() {
        return this.alienSector;
    }
}