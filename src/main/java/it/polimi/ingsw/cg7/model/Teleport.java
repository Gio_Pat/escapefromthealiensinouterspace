package it.polimi.ingsw.cg7.model;

import it.polimi.ingsw.cg7.controller.PlayerStateHandler;
import it.polimi.ingsw.cg7.controller.TeleportItemAction;

/**
 * The Class Teleport.
 */
public class Teleport extends Item {

    /**
     * Instantiates a new teleport.
     */
    public Teleport() {
        super("Teleport");
    }

    /**
     * Instantiates a new teleport.
     *
     * @param description the description
     */
    public Teleport(String description) {
        super(description);
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.model.Item#use(it.polimi.ingsw.cg7.model.Sector, it.polimi.ingsw.cg7.model.Player, it.polimi.ingsw.cg7.controller.PlayerStateHandler)
     */
    @Override
    public boolean use(Sector sector, Player player,
            PlayerStateHandler playerStateHandler) {
        TeleportItemAction teleport = new TeleportItemAction();
        return teleport.actionValidation(player, playerStateHandler);
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.model.Item#couldNotDraw(it.polimi.ingsw.cg7.model.Player)
     */
    @Override
    public boolean couldNotDraw(Player player) {
        return false;
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.model.Item#isCardBeforeMovement()
     */
    @Override
    public boolean isCardBeforeMovement() {
        return true;
    }

}
