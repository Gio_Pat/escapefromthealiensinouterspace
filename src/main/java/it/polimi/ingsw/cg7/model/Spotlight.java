package it.polimi.ingsw.cg7.model;

import it.polimi.ingsw.cg7.controller.PlayerStateHandler;
import it.polimi.ingsw.cg7.controller.SpotlightItemAction;

/**
 * The Class Spotlight.
 */
public class Spotlight extends Item {

    /**
     * Instantiates a new spotlight.
     */
    public Spotlight() {
        super("Spotlight");
    }

    /**
     * Instantiates a new spotlight.
     *
     * @param description the description
     */
    public Spotlight(String description) {
        super(description);
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.model.Item#use(it.polimi.ingsw.cg7.model.Sector, it.polimi.ingsw.cg7.model.Player, it.polimi.ingsw.cg7.controller.PlayerStateHandler)
     */
    @Override
    public boolean use(Sector sector, Player player,
            PlayerStateHandler playerStateHandler) {
        SpotlightItemAction action = new SpotlightItemAction();
        return action.actionValidation(sector, player, playerStateHandler);
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.model.Item#couldNotDraw(it.polimi.ingsw.cg7.model.Player)
     */
    @Override
    public boolean couldNotDraw(Player player) {
        return false;
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.model.Item#isCardBeforeMovement()
     */
    @Override
    public boolean isCardBeforeMovement() {
        return false;
    }

}
