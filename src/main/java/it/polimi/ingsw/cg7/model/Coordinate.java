package it.polimi.ingsw.cg7.model;


/**
 * The Class Coordinate.
 */
public class Coordinate{

    
    /** The x coordinate [letter] */
    private int xLett;
    
    /** The  y coordinate. */
    private int y;

    /**
     * Instantiates a new coordinate.
     */
    public Coordinate() {
        this.xLett = 0;
        this.y = 0;
    }

    /**
     * Instantiates a new coordinate.
     *
     * @param xLett the x lett
     * @param y the y
     */
    public Coordinate(int xLett, int y) {
        this.xLett = xLett;
        this.y = y;
    }

    /**
     * Gets x coordinate.
     *
     * @return int Object
     */

    public int getxLett() {
        return xLett;
    }

    /**
     * Gets y coordinate.
     *
     * @return int Object
     */

    public int getY() {
        return y;
    }

    /**
     * Set x coordinate.
     *
     * @param xLett the new x lett
     */

    public void setxLett(int xLett) {
        this.xLett = xLett;
    }

    /**
     * Sets y coordinate.
     *
     * @param y the new y
     */

    public void setY(int y) {
        this.y = y;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + xLett;
        result = prime * result + y;
        return result;
    }

    /**
     * Controls equality with another object.
     *
     * @param obj the obj
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Coordinate other = (Coordinate) obj;
        if (xLett != other.xLett)
            return false;
        if (y != other.y)
            return false;
        return true;
    }

}