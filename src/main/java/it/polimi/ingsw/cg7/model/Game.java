package it.polimi.ingsw.cg7.model;

import it.polimi.ingsw.cg7.exception.PlayerNotFound;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * The Class Game.
 */
public class Game {

    /** The map on wich the game is been playing. */
    private Map map;

    /** The n round. */
    private int nRound;

    /**
     * The token (of a tourn) this variable is needed to select a specific
     * player in one array.
     */
    private int token;

    /** The chat. */
    private Chat chat;

    /** The turns. */
    private List<Turn> turns;

    /** The players. */
    private List<Player> players;

    /** The not playing players. */
    private List<Player> notPlayingPlayers;

    /** The current turn. */
    private Turn currentTurn;

    /** The starting time. */
    private Date startingTime;

    /** The character deck. */
    private Deck characterDeck;

    /** The item deck. */
    private Deck itemDeck;

    /** The sector deck. */
    private Deck sectorDeck;

    /** The escape deck. */
    private Deck escapeDeck;

    /** game paused. */
    private boolean paused;

    /** game started. */
    private boolean started;

    /** game finished. */
    private boolean finish;

    /**
     * Instantiates a new game.
     *
     * @param players
     *            the players
     * @param startingTime
     *            the starting time
     * @param map
     *            the map
     */
    public Game(List<Player> players, Date startingTime, Map map) {
        this.started = true;
        this.finish = false;
        this.nRound = 0;
        this.token = 0;
        this.turns = new ArrayList<Turn>();
        this.paused = false;
        this.map = map;
        this.players = players;
        this.notPlayingPlayers = new ArrayList<Player>();
        this.startingTime = startingTime;
        this.chat = new Chat(this);
        createDecks();
        assignCharacter();
        createTurn(token);
    }

    /**
     * Instantiates a new game.
     *
     * @param player
     *            the player
     * @param map
     *            the map
     */
    public Game(Player player, Map map) {
        this.turns = new ArrayList<Turn>();
        this.started = false;
        this.finish = false;
        this.nRound = 0;
        this.token = 0;
        this.players = new ArrayList<Player>();
        this.players.add(player);
        this.notPlayingPlayers = new ArrayList<Player>();
        this.paused = false;
        this.map = map;
        this.startingTime = new Date();
        this.chat = new Chat(this);
    }

    /**
     * get the map of this Game.
     *
     * @return Map
     */
    public Map getMap() {
        return map;
    }

    /**
     * Set the map of this Game.
     *
     * @param map
     *            the new map
     */
    public void setMap(Map map) {
        this.map = map;
    }

    /**
     * Return if game is finish.
     *
     * @return true if the game is finish, false otherwise
     */

    public boolean isFinished() {
        return this.finish;
    }

    /**
     * Finish game.
     */
    public void finishGame() {
        finish = true;
    }

    /**
     * Return if the game has already started.
     *
     * @return true if the game has already started, false otherwise
     */

    public boolean isStarted() {
        return this.started;
    }

    /**
     * Add new player.
     *
     * @param player
     *            the player
     */

    public void addPlayer(Player player) {
        if (!isStarted())
            this.players.add(player);
    }

    /**
     * Starts the game.
     *
     * @param startingTime
     *            the starting time
     * @return true if the game has not started yet and start it, false
     *         otherwise
     */

    public boolean start(Date startingTime) {
        if (isStarted())
            return false;
        this.startingTime = startingTime;
        this.started = true;
        // Collections.shuffle(players);
        createDecks();
        assignCharacter();
        createTurn(token);
        return true;
    }

    /**
     * Gets the date on which the game started.
     *
     * @return startingTime
     */

    public Date getStartingTime() {
        return this.startingTime;
    }

    /**
     * Gets game chat.
     *
     * @return Chat
     */

    public Chat getChat() {
        return this.chat;
    }

    /**
     * Adds a message to the chat.
     *
     * @param m1
     *            the m1
     */

    public void addMessage(Message m1) {
        this.chat.setMessage(this, m1);
    }

    /**
     * Gets last message.
     *
     * @return Message
     */

    public Message getLastMessage() {
        return this.chat.getLastMessage();
    }

    /**
     * Gets the number of the round.
     *
     * @return int
     */

    public int numberRound() {
        return nRound;
    }

    /**
     * Gets player list.
     *
     * @return List<Player>
     */

    public List<Player> getPlayers() {
        return this.players;
    }

    /**
     * Gets the not playing player.
     *
     * @return the not playing player
     */
    public List<Player> getNotPlayingPlayer() {
        return this.notPlayingPlayers;
    }

    /**
     * Return a list of playing player.
     *
     * @param players
     *            the players
     * @return List<Player>
     * @throws PlayerNotFound
     *             the player not found
     */

    public List<Player> playingPlayers(List<Player> players)
            throws PlayerNotFound {
        List<Player> playingPlayers = new ArrayList<Player>(players);
        for (Player player : notPlayingPlayers)
            if (!(playingPlayers.contains(player)))
                throw new PlayerNotFound();
            else
                playingPlayers.remove(player);
        return playingPlayers;
    }

    /**
     * Increment the token.
     *
     * @return new token
     */

    private int getNextToken() {
        token++;
        token = token % players.size();
        if (players.get(token).getState() != PlayingState.PLAYING)
            getNextToken();
        return token;
    }

    /**
     * Changes and creates the turn , increasing the round if needed.
     */

    public void changeTurn() {
        if (!isFinished()) {
            token = getNextToken();
            if (token == 0)
                nRound++;
            createTurn(token);
        }
    }

    /**
     * Gets if the game is paused.
     *
     * @return true if it's paused, false otherwise
     */
    public boolean isPaused() {
        return paused;
    }

    /**
     * Pauses if not and vice versa.
     */

    public void changePaused() {
        this.paused = !(this.paused);
    }

    /**
     * Give each player a card character.
     */

    private void assignCharacter() {
        Collections.shuffle(players);
        for (Player player : players) {
            player.setType(((Character) characterDeck.draw()).getType());
            if (player.getType() == CharType.HUMAN)
                player.setPosition(map.getHumanSector());
            else
                player.setPosition(map.getAlienSector());
        }
    }

    /**
     * Create all decks.
     */

    private void createDecks() {
        createCharacterDeck();
        createSectorDeck();
        createItemDeck();
        createEscapeDeck();
    }

    /**
     * Creates the Sector Deck.
     */

    public void createSectorDeck() {
        sectorDeck = new Deck();
        for (int x = 0; x < 5; x++)
            sectorDeck.insert(new DangerousSector(Noise.SILENCE, true));
        for (int x = 0; x < 10; x++)
            if (x < 4)
                sectorDeck.insert(new DangerousSector(Noise.ANYSECTOR, true));
            else
                sectorDeck.insert(new DangerousSector(Noise.ANYSECTOR, false));

        for (int x = 0; x < 10; x++)
            if (x < 4)
                sectorDeck.insert(new DangerousSector(Noise.ONESECTOR, true));
            else
                sectorDeck.insert(new DangerousSector(Noise.ONESECTOR, false));
        sectorDeck.shuffle();

    }

    /**
     * Create the Item Deck.
     */

    public void createItemDeck() {
        itemDeck = new Deck();
        for (int x = 0; x < 2; x++) {
            itemDeck.insert(new Attack());
            itemDeck.insert(new Teleport());
            itemDeck.insert(new Adrenaline());
            itemDeck.insert(new Spotlight());
            itemDeck.insert(new Sedatives());
        }
        itemDeck.insert(new Spotlight());
        itemDeck.insert(new Defense());
        List<Card> cards = new ArrayList<Card>();
        for (Player p : players)
            for (Card c : p.getDeck().getCards())
                if (itemDeck.getCards().contains(c))
                    cards.add(c);
        for (Card c : cards)
            itemDeck.removeCard(c);
        itemDeck.shuffle();
    }

    /**
     * Create Escape Deck.
     */

    public void createEscapeDeck() {
        escapeDeck = new Deck();
        for (int x = 0; x < 3; x++) {
            escapeDeck.insert(new EscapeHatch(EscapeType.GREEN));
            escapeDeck.insert(new EscapeHatch(EscapeType.RED));
        }
        escapeDeck.shuffle();
    }

    /**
     * Create Character Deck.
     */
    private void createCharacterDeck() {
        int numberHuman = players.size() / 2;
        List<Card> temp = new ArrayList<Card>();
        for (int x = 0; x < numberHuman; x++)
            temp.add(new Character(CharType.HUMAN));
        for (int x = 0; x < (players.size() - numberHuman); x++)
            temp.add(new Character(CharType.ALIEN));
        characterDeck = new Deck(temp);
    }

    /**
     * Creates a new turn and add it to the list.
     *
     * @param n
     *            the n
     */
    private void createTurn(int n) {
        currentTurn = new Turn(players.get(n), itemDeck, sectorDeck, escapeDeck);
        turns.add(currentTurn);
    }

    /**
     * Gets current Turn.
     *
     * @return current Turn
     */

    public Turn getCurrentTurn() {
        return this.currentTurn;
    }

    /**
     * Gets item Deck.
     *
     * @return Deck
     */

    public Deck getItemDeck() {
        return this.itemDeck;
    }

    /**
     * Gets escape Deck.
     *
     * @return Deck
     */

    public Deck getEscapeDeck() {
        return this.escapeDeck;
    }

    /**
     * Gets sector Deck.
     *
     * @return Deck
     */

    public Deck getSectorDeck() {
        return this.sectorDeck;
    }

    /**
     * Returns last two turns of player.
     *
     * @param player
     *            the player
     * @return Turns list of player
     */

    public List<Turn> getLastPlayerTurn(Player player) {
        List<Turn> playerTurn = new ArrayList<Turn>();
        playerTurn.add(this.currentTurn);
        boolean control = false;
        boolean exception = false;
        int size = turns.size();
        while ((control) || (exception)) {
            size--;
            if (size == 0)
                exception = true;
            if (turns.get(size).getCurrentPlayer().getUsername()
                    .equals(player.getUsername()))
                control = true;
        }
        if (control)
            playerTurn.add(turns.get(size));
        return playerTurn;
    }

}
