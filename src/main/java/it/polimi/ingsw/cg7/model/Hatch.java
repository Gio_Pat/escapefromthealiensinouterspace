package it.polimi.ingsw.cg7.model;

import it.polimi.ingsw.cg7.Client.model.flagResponseType;
import it.polimi.ingsw.cg7.controller.DeckController;
import it.polimi.ingsw.cg7.controller.PlayerStateHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class Hatch.
 */
public class Hatch extends SpecialSector {

    /** The number. */
    private int number;
    
    /** The locked. */
    private boolean locked;

    /**
     * Instantiates a new hatch.
     */
    public Hatch() {
        this.coord = new Coordinate(0, 0);
        this.nearSectors = new ArrayList<Sector>();
        this.players = new ArrayList<Player>();
        this.number = 0;
        this.locked = false;
    }

    /**
     * Instantiates a new hatch.
     *
     * @param number the number
     * @param locked the locked
     */
    public Hatch(int number, boolean locked) {
        this.number = number;
        this.locked = locked;
        this.coord = new Coordinate(0, 0);
        this.nearSectors = new ArrayList<Sector>();
        this.players = new ArrayList<Player>();
    }

    /**
     * Instantiates a new hatch.
     *
     * @param number the number
     * @param locked the locked
     * @param coord the coord
     * @param nearSectors the near sectors
     * @param player the player
     */
    public Hatch(int number, boolean locked, Coordinate coord,
            List<Sector> nearSectors, List<Player> player) {
        this.number = number;
        this.locked = locked;
        this.coord = coord;
        this.nearSectors = new ArrayList<Sector>(nearSectors);
        this.players = new ArrayList<Player>(player);
    }

    /**
     * Instantiates a new hatch.
     *
     * @param locked the locked
     */
    public Hatch(boolean locked) {
        this.locked = locked;
    }

    /**
     * Gets number of the Hatch.
     *
     * @return int
     */

    public int getNumber() {
        return this.number;
    }

    /**
     * Returns if the hatch is blocked.
     *
     * @return true if it is blocked, otherwise false
     */

    public boolean isLocked() {
        return this.locked;
    }

    /**
     * lock the Hatch.
     */

    public void lock() {
        this.locked = true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + (locked ? 1231 : 1237);
        result = prime * result + number;
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        if (number != ((Hatch) obj).number)
            return false;
        return true;
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.model.Sector#validationAttack()
     */
    @Override
    public boolean validationAttack() {
        return false;
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.model.Sector#validationMove(it.polimi.ingsw.cg7.model.Player)
     */
    @Override
    public boolean validationMove(Player player) {
        if (player.getType() == CharType.HUMAN)
            return true;
        return false;
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.model.Sector#controlDrawCard(it.polimi.ingsw.cg7.model.Game, it.polimi.ingsw.cg7.controller.PlayerStateHandler, it.polimi.ingsw.cg7.model.Player)
     */
    @Override
    public flagResponseType controlDrawCard(Game game,
            PlayerStateHandler playerStateHandler, Player player) {
        if (this.locked)
            return flagResponseType.FALSE;
        DeckController deckController = new DeckController(game,
                playerStateHandler);
        this.locked = true;
        return deckController.drawEscapeCard(player);

    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.model.Sector#canDraw()
     */
    @Override
    public boolean canDraw() {
        return true;
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.model.Sector#canEscape()
     */
    @Override
    public boolean canEscape() {
        return true;
    }

}
