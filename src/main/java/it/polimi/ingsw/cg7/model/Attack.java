package it.polimi.ingsw.cg7.model;

import it.polimi.ingsw.cg7.controller.AttackItemAction;
import it.polimi.ingsw.cg7.controller.PlayerStateHandler;

// TODO: Auto-generated Javadoc
/**
 * The Class Attack.
 */
public class Attack extends Item {

    /**
     * Instantiates a new attack.
     */
    public Attack() {
        super("Attack");
    }

    /**
     * Instantiates a new attack.
     *
     * @param description the description
     */
    public Attack(String description) {
        super(description);
    }

    /**
     * See usage of a generic card
     */
    @Override
    public boolean use(Sector sector, Player player,
            PlayerStateHandler playerStateHandler) {
        AttackItemAction attack = new AttackItemAction();
        return attack.actionValidation(player, playerStateHandler);
    }

    /**
     *  See generic item doc
     */
    @Override
    public boolean couldNotDraw(Player player) {
        return false;
    }

    /**
     * See generic item doc
     */
    @Override
    public boolean isCardBeforeMovement() {
        return false;
    }

}
