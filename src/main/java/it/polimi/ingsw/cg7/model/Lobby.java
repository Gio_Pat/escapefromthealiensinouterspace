package it.polimi.ingsw.cg7.model;

import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;
import it.polimi.ingsw.cg7.Client.model.NetTypeBroker;
import it.polimi.ingsw.cg7.controller.GameController;
import it.polimi.ingsw.cg7.controller.LobbyManager;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.swing.Timer;


/**
 * The Class Lobby.
 *
 * @author Giovanni Patruno This class will be manage the connection to a
 *         specific Game in order to notify the right group for the effect of a
 *         Move
 */

public class Lobby {

    /** The Lobby elements. */
    private Map<UUID, Player> LobbyElements = new HashMap<UUID, Player>();

    /** The lobby game controller. */
    private GameController lobbyGameController; // The topic

    private ActionListener startGame;

    private Timer timer;

    /**
     * Instantiates a new lobby.
     *
     * @param newPlayerToken
     *            the new player token
     * @param creator
     *            the creator
     * @param lobbyGameController
     *            the lobby game controller
     * @return Lobby elements
     */
    public Lobby(UUID newPlayerToken, Player creator,
            GameController lobbyGameController) {
        startGame = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                startingGame();
                stopTimer();
            }
        };
        this.setLobbyGameController(lobbyGameController);
        this.LobbyElements.put(newPlayerToken, creator);
    }

    private void stopTimer() {
        this.timer.stop();
    }

    /**
     * Gets the lobby players.
     *
     * @return the lobby players
     */

    public Collection<Player> getLobbyPlayers() {
        return this.LobbyElements.values();

    }

    /**
     * Gets the lobby elements.
     *
     * @return the lobby elements
     */
    public Map<UUID, Player> getLobbyElements() {
        return LobbyElements;
    }

    /**
     * set LobbyElements.
     *
     * @param lobbyElements
     *            the lobby elements
     */
    public void setLobbyElements(Map<UUID, Player> lobbyElements) {
        LobbyElements = lobbyElements;
    }

    /**
     * add an element to the Lobby.
     *
     * @param currentToken
     *            the current token
     * @param subscribingPlayer
     *            the subscribing player
     */
    public void addLobbyElement(UUID currentToken, Player subscribingPlayer) {
        this.LobbyElements.put(currentToken, subscribingPlayer);
        if (LobbyElements.size() == 2) {
            timer = new Timer(10000, startGame);
            timer.start();
        }
        if (LobbyElements.size() == 8) {
            startingGame();
            stopTimer();
        }
    }

    private void startingGame() {
        this.lobbyGameController.startGame();
        LobbyManager.notifyLobby(getLobbyGame(), new NetBrokerMessage(
                NetTypeBroker.INITGAME));
    }

    /**
     * Gets the lobby game.
     *
     * @return the lobby game
     */
    public Game getLobbyGame() {
        return this.lobbyGameController.getGame();
    }

    /**
     * Sets the lobby game controller.
     *
     * @param lobbyGameController
     *            the new lobby game controller
     */
    public void setLobbyGameController(GameController lobbyGameController) {
        this.lobbyGameController = lobbyGameController;
    }

    /**
     * Gets the lobby game controller.
     *
     * @return the lobby game controller
     */
    public GameController getLobbyGameController() {
        return this.lobbyGameController;
    }

    /**
     * Gets the player from uuid.
     *
     * @param uuid
     *            the uuid
     * @return the player from uuid
     */
    public Player getPlayerFromUUID(UUID uuid) {
        return this.LobbyElements.get(uuid);
    }

}
