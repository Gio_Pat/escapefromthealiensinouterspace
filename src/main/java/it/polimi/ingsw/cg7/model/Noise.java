package it.polimi.ingsw.cg7.model;

/**
 *  Enum Noise.
 */
public enum Noise {

    /** specific player sector noise. */
    ONESECTOR,
    /** silence. */
    SILENCE, 
    /** false noise*/
    ANYSECTOR;

}
