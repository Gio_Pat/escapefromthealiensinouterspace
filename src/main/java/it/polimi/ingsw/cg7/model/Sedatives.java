package it.polimi.ingsw.cg7.model;

import it.polimi.ingsw.cg7.controller.PlayerStateHandler;

/**
 * The Class Sedatives card.
 */
public class Sedatives extends Item {

    /**
     * Instantiates a new sedatives.
     */
    public Sedatives() {
        super("Sedatives");
    }

    /**
     * Instantiates a new sedatives.
     *
     * @param description the description
     */
    public Sedatives(String description) {
        super(description);
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.model.Item#use(it.polimi.ingsw.cg7.model.Sector, it.polimi.ingsw.cg7.model.Player, it.polimi.ingsw.cg7.controller.PlayerStateHandler)
     */
    @Override
    public boolean use(Sector sector, Player player,
            PlayerStateHandler playerStateHandler) {
        player.addSedative();
        player.getDeck().removeCard(new Sedatives());
        return true;
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.model.Item#couldNotDraw(it.polimi.ingsw.cg7.model.Player)
     */
    @Override
    public boolean couldNotDraw(Player player) {
        return false;
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.model.Item#isCardBeforeMovement()
     */
    @Override
    public boolean isCardBeforeMovement() {
        return false;
    }
}
