package it.polimi.ingsw.cg7.model;

/**
 * The Enum PlayingState that represent the generic game-state of a player.
 */
public enum PlayingState {
    
    /** Winner state **/
    WINNER,
    /** Loser state **/
    LOSER,
    /** Playing state */
    PLAYING;
}