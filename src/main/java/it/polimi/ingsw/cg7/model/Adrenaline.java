package it.polimi.ingsw.cg7.model;

import it.polimi.ingsw.cg7.controller.AdrenalineItemAction;
import it.polimi.ingsw.cg7.controller.PlayerStateHandler;

/**
 * The Class Adrenaline.
 */
public class Adrenaline extends Item {

    /**
     * Instantiates a new adrenaline.
     */
    public Adrenaline() {
        super("Adrenaline");
    }

    /**
     * Instantiates a new adrenaline.
     *
     * @param description the description
     */
    public Adrenaline(String description) {
        super(description);
    }

    /**
     * See the interface doc
     */
    @Override
    public boolean use(Sector sector, Player player,
            PlayerStateHandler playerStateHandler) {
        AdrenalineItemAction action = new AdrenalineItemAction();
        return action.actionValidation(player, playerStateHandler);

    }

    /**
     * See the interface doc
     */
    @Override
    public boolean couldNotDraw(Player player) {
        return false;
    }
    /**
     * See the interface doc
     */
    @Override
    public boolean isCardBeforeMovement() {
        return true;
    }

}
