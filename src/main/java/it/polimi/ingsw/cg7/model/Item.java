package it.polimi.ingsw.cg7.model;

import it.polimi.ingsw.cg7.controller.PlayerStateHandler;
/**
 * Generic Item card
 * @author Debora Rebai
 *
 */
public abstract class Item extends Card {
    /**
     * Constructor
     * @param description
     */
    public Item(String description) {
        super(description);
    }
    
    /**
     * Specific using of a card
     */

    public abstract boolean use(Sector sector, Player player,
            PlayerStateHandler playerStateHandler);
    /**
     * Method that tells if the player could not draw
     */

    public abstract boolean couldNotDraw(Player player);
    /**
     * Method that tells if this is a card that can be used before a movement
     */
    public abstract boolean isCardBeforeMovement();

}
