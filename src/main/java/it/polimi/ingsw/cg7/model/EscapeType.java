package it.polimi.ingsw.cg7.model;


/**
 * The Enum EscapeType.
 */
public enum EscapeType {


    /** The working hatch. */
    GREEN, 
    /** The broken. */
    RED;


}
