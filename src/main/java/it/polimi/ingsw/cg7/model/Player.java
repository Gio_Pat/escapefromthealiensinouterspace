package it.polimi.ingsw.cg7.model;

import java.io.Serializable;

/**
 * The Class Player that represent a player [User and game player].
 */
public class Player implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
    
    /** The position. */
    private Sector position;
    
    /** The state. */
    private PlayingState state;
    
    /** The username. */
    private String username;
    /** The kill number. */
    private int killNumber;
    
    /** The item deck. */
    private Deck itemDeck;
    
    /** The type. */
    private CharType type;
    
    /** The item attack. */
    private boolean itemAttack;
    
    /** The sedative. */
    private boolean sedative;
    
    /** The buff. */
    private boolean buff;
    
    
    /**
     * Instantiates a new player.
     *
     * @param username the username
     * @param type the type
     */
    public Player(String username, CharType type) {
        this.position = new PlayingSector();
        this.state = PlayingState.PLAYING;
        this.username = username;
        this.killNumber = 0;
        this.itemDeck = new Deck();
        this.type = type;
        this.itemAttack = false;
        this.buff = false;
        this.sedative = false;
    }

    /**
     * Instantiates a new player.
     *
     * @param username the username
     */
    public Player(String username) {
        this.position = new PlayingSector();
        this.state = PlayingState.PLAYING;
        this.username = username;
        this.killNumber = 0;
        this.itemDeck = new Deck();
        this.type = CharType.HUMAN;
        this.itemAttack = false;
        this.buff = false;
        this.sedative = false;
    }

    /**
     * Instantiates a new player.
     *
     * @param position the position
     * @param username the username
     * @param type the type
     */
    public Player(Sector position, String username, CharType type) {
        setPosition(position);
        this.username = username;
        this.type = type;
        this.state = PlayingState.PLAYING;
        this.itemDeck = new Deck();
        this.killNumber = 0;
        this.itemAttack = false;
        this.buff = false;

        this.sedative = false;
    }

    /**
     * Instantiates a new player.
     *
     * @param position the position
     * @param username the username
     * @param type the type
     * @param turn the turn
     */
    public Player(Sector position, String username, CharType type, Turn turn) {
        this.position = position;
        this.username = username;
        this.type = type;
        this.state = PlayingState.PLAYING;
        this.itemDeck = new Deck();
        this.killNumber = 0;
        this.itemAttack = false;
        this.buff = false;
        this.sedative = false;

    }

    /**
     * Removes the sedative.
     */
    public void removeSedative() {
        this.sedative = false;
    }

    /**
     * Adds the sedative.
     */
    public void addSedative() {
        this.sedative = true;
    }

    /**
     * Checks if is sedative.
     *
     * @return true, if is sedative
     */
    public boolean isSedative() {
        return this.sedative;
    }

    /**
     * Adds the buff.
     */
    public void addBuff() {
        this.buff = true;
    }

    /**
     * Removes the buff.
     */
    public void removeBuff() {
        this.buff = false;
    }

    /**
     * If buffed.
     *
     * @return true, if successful
     */
    public boolean ifBuffed() {
        return this.buff;
    }

    /**
     * return if the player can attack.
     *
     * @return true if player can attack, false otherwise
     */

    public boolean canAttack() {
        return this.itemAttack;
    }

    /**
     * Sets itemAttack true: if Player have itemAttack = true, he has used
     * attack card.
     */

    public void addAttack() {
        this.itemAttack = true;
    }

    /**
     * remove possibility attack.
     */

    public void removeAttack() {
        this.itemAttack = false;
    }

    /**
     * Gets position.
     *
     * @return Sector
     */

    public Sector getPosition() {
        return position;
    }

    /**
     * Sets position.
     *
     * @param position the new position
     */

    public void setPosition(Sector position) {
        this.position = position;
        position.setPlayer(this);
    }

    /**
     * Gets state of player.
     *
     * @return state
     */

    public PlayingState getState() {
        return state;
    }

    /**
     * Set state of player.
     *
     * @param state the new state
     */

    public void setState(PlayingState state) {
        this.state = state;
    }

    /**
     * add to player killing.
     */

    public void addKill() {
        killNumber++;
    }

    /**
     * Gets username of player.
     *
     * @return String
     */

    public String getUsername() {
        return username;
    }

    /**
     * Gets number of killings.
     *
     * @return int
     */

    public int getKillNumber() {
        return killNumber;
    }

    /**
     * Gets Item Deck of Player.
     *
     * @return Deck
     */

    public Deck getDeck() {
        return itemDeck;
    }

    /**
     * Gets type of Player.
     *
     * @return CharType
     */

    public CharType getType() {
        return type;
    }

    /**
     * Set type of player.
     *
     * @param type the new type
     */

    public void setType(CharType type) {
        this.type = type;
    }

    /**
     * Set item Deck.
     *
     * @param itemDeck the new deck
     */

    public void setDeck(Deck itemDeck) {
        this.itemDeck = itemDeck;
    }

}
