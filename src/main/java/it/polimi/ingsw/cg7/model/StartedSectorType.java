package it.polimi.ingsw.cg7.model;

/**
 * The Enum StartedSectorType.
 */
public enum StartedSectorType {
    
    /** The human start point. */
    HUMANSTARTPOINT,
    /** The alien start point. */
    ALIENSTARTPOINT,
    /** The safe. */
    SAFE;

}