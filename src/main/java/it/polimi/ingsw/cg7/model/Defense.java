package it.polimi.ingsw.cg7.model;

import it.polimi.ingsw.cg7.controller.PlayerStateHandler;


/**
 * The Class Defense.
 */
public class Defense extends Item {

    /**
     * Instantiates a new defense.
     */
    public Defense() {
        super("Defense");
    }

    /**
     * Instantiates a new defense.
     *
     * @param description the description
     */
    public Defense(String description) {
        super(description);
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.model.Item#use(it.polimi.ingsw.cg7.model.Sector, it.polimi.ingsw.cg7.model.Player, it.polimi.ingsw.cg7.controller.PlayerStateHandler)
     */
    @Override
    public boolean use(Sector sector, Player player,
            PlayerStateHandler playerStateHandler) {
        return true;
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.model.Item#couldNotDraw(it.polimi.ingsw.cg7.model.Player)
     */
    @Override
    public boolean couldNotDraw(Player player) {
        return false;
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.model.Item#isCardBeforeMovement()
     */
    @Override
    public boolean isCardBeforeMovement() {
        return false;
    }

}
