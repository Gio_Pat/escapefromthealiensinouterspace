package it.polimi.ingsw.cg7.model;

import it.polimi.ingsw.cg7.Client.model.flagResponseType;
import it.polimi.ingsw.cg7.controller.PlayerStateHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class Sector.
 */
public abstract class Sector {

    
    /** The coordinate. */
    protected Coordinate coord;
    
    /** The near sectors. */
    protected List<Sector> nearSectors;
    
    /** The players. */
    protected List<Player> players;

    /**
     * Sets the near sectors.
     *
     * @param nearSectors the new near sectors
     */
    public void setNearSectors(List<Sector> nearSectors) {
        this.nearSectors = nearSectors;
        this.players = new ArrayList<Player>();
    }

    /**
     * Checks if all players have passed in the sector.
     *
     * @param p the p
     * @return true if the player is in the list, false otherwise
     */
    private boolean controlEquals(List<Player> p) {
        for (Player removingPlayer : p)
            if (!players.contains(removingPlayer))
                return false;
        return true;
    }

    /**
     * Remove from sector players passed.
     *
     * @param p the p
     */

    private void remove(List<Player> p) {
        for (Player removingPlayer : p)
            if (players.contains(removingPlayer))
                players.remove(removingPlayer);
    }

    /**
     * Remove from sector players passed.
     *
     * @param p the p
     * @return true if they contain them and removed, false otherwise
     */

    public boolean removePlayers(List<Player> p) {
        if (!controlEquals(p))
            return false;
        remove(p);
        return true;
    }

    /**
     * Remove from sector player passed.
     *
     * @param player the player
     * @return true if player has removed, false otherwise
     */

    public boolean removePlayer(Player player) {
        if (!players.contains(player))
            return false;
        players.remove(player);
        return true;
    }

    /**
     * Gets coordinate.
     *
     * @return Coordinate
     */

    public Coordinate getCoordinates() {
        return this.coord;
    }

    /**
     * Gets all near sector.
     *
     * @return List<Sector>
     */

    public List<Sector> getNearSector() {
        return this.nearSectors;
    }

    /**
     * Gets all player in the sector.
     *
     * @return List<Player>
     */

    public List<Player> getPlayers() {
        return this.players;
    }

    /**
     * Add player in sector.
     *
     * @param p the new player
     */

    public void setPlayer(Player p) {
        players.add(p);
    }

    /**
     * Set a coordinate of the sector.
     *
     * @param coord the new coord
     */

    public void setCoord(Coordinate coord) {
        this.coord = coord;
    }

    /**
     * Validation attack.
     *
     * @return true, if successful
     */
    public abstract boolean validationAttack();

    /**
     * Validation move.
     *
     * @param player the player
     * @return true, if successful
     */
    public abstract boolean validationMove(Player player);

    /**
     * Control draw card.
     *
     * @param game the game
     * @param playerStateHandler the player state handler
     * @param player the player
     * @return the flag response type
     */
    public abstract flagResponseType controlDrawCard(Game game,
            PlayerStateHandler playerStateHandler, Player player);

    /**
     * return true if the player can draw in that sector.
     *
     * @return true, if successful
     */
    public abstract boolean canDraw();

    /**
     * Can escape.
     *
     * @return true, if successful
     */
    public abstract boolean canEscape();

}
