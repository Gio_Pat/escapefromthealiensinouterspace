package it.polimi.ingsw.cg7.model;

/**
 * The Enum that represent the character type.
 */
public enum CharType {

    /** The alien. */
    ALIEN, 
    /** The human. */
    HUMAN;

}
