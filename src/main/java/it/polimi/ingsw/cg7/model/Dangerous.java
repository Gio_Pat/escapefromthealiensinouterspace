package it.polimi.ingsw.cg7.model;

import it.polimi.ingsw.cg7.Client.model.flagResponseType;
import it.polimi.ingsw.cg7.controller.DeckController;
import it.polimi.ingsw.cg7.controller.PlayerStateHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class Dangerous sector.
 */
public class Dangerous extends SpecialSector {

    /**
     * Instantiates a new dangerous.
     */
    public Dangerous() {
        this.coord = new Coordinate();
        this.nearSectors = new ArrayList<Sector>();
        this.players = new ArrayList<Player>();
    }

    /**
     * Instantiates a new dangerous.
     *
     * @param coord the coord
     * @param nearSector the near sector
     * @param player the player
     */
    public Dangerous(Coordinate coord, List<Sector> nearSector,
            List<Player> player) {
        this.coord = coord;
        this.nearSectors = nearSector;
        this.players = player;
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.model.Sector#validationAttack()
     */
    @Override
    public boolean validationAttack() {
        return true;
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.model.Sector#validationMove(it.polimi.ingsw.cg7.model.Player)
     */
    @Override
    public boolean validationMove(Player player) {
        return true;
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.model.Sector#controlDrawCard(it.polimi.ingsw.cg7.model.Game, it.polimi.ingsw.cg7.controller.PlayerStateHandler, it.polimi.ingsw.cg7.model.Player)
     */
    @Override
    public flagResponseType controlDrawCard(Game game,
            PlayerStateHandler playerStateHandler, Player player) {
        DeckController deckController = new DeckController(game,
                playerStateHandler);
        return deckController.drawSectorCard(player);
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.model.Sector#canDraw()
     */
    @Override
    public boolean canDraw() {
        return true;
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.model.Sector#canEscape()
     */
    @Override
    public boolean canEscape() {
        return false;
    }
}
