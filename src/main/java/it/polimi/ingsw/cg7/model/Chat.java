package it.polimi.ingsw.cg7.model;

import it.polimi.ingsw.cg7.Client.model.NetBrokerMessage;
import it.polimi.ingsw.cg7.Client.model.NetTypeBroker;
import it.polimi.ingsw.cg7.controller.LobbyManager;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class Chat.
 */
public class Chat {
    
    /** The messages. */
    private List<Message> messages;

    /**
     * Instantiates a new chat.
     */
    public Chat() {
        messages = new ArrayList<Message>();
    }

    /**
     * Instantiates a new chat.
     *
     * @param game the game
     */
    public Chat(Game game) {
        messages = new ArrayList<Message>();
    }

    /**
     * Sets new message.
     *
     * @param playingGame the playing game
     * @param message the message
     */

    public void setMessage(Game playingGame, Message message) {
        messages.add(message);
        NetBrokerMessage msg = new NetBrokerMessage(NetTypeBroker.MESSAGE);
        msg.sendMessage(message.getText(), message.getPlayer().getUsername(),
                message.getDate());
        LobbyManager.notifyLobby(playingGame, msg);
    }

    /**
     * Gets <code>messages</code>.
     *
     * @return List<Message> Object
     */

    public List<Message> getMessages() {
        return this.messages;
    }

    /**
     * Gets last message of the list.
     *
     * @return Message Object
     */

    public Message getLastMessage() {
        if (!messages.isEmpty())
            return this.messages.get(messages.size() - 1);
        return null;
    }

}
