package it.polimi.ingsw.cg7.model;

import it.polimi.ingsw.cg7.Client.model.flagResponseType;
import it.polimi.ingsw.cg7.controller.PlayerStateHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class PlayingSector.
 */
public class PlayingSector extends Sector {

    /** The type. */
    StartedSectorType type;

    /**
     * Instantiates a new playing sector.
     */
    public PlayingSector() {
        this.type = StartedSectorType.SAFE;
        this.coord = new Coordinate(0, 0);
        this.nearSectors = new ArrayList<Sector>();
        this.players = new ArrayList<Player>();
    }

    /**
     * Instantiates a new playing sector.
     *
     * @param type the type
     */
    public PlayingSector(StartedSectorType type) {
        this.type = type;
        this.coord = new Coordinate(0, 0);
        this.nearSectors = new ArrayList<Sector>();
        this.players = new ArrayList<Player>();
    }

    /**
     * Instantiates a new playing sector.
     *
     * @param type the type
     * @param coord the coord
     * @param nearSectors the near sectors
     * @param player the player
     */
    public PlayingSector(StartedSectorType type, Coordinate coord,
            List<Sector> nearSectors, List<Player> player) {
        this.type = type;
        this.coord = coord;
        this.nearSectors = new ArrayList<Sector>(nearSectors);
        this.players = new ArrayList<Player>(player);
    }

    /**
     * Get type of Sector.
     *
     * @return type
     */

    @Override
    public boolean validationAttack() {
        if (this.type == StartedSectorType.SAFE)
            return true;
        return false;
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.model.Sector#validationMove(it.polimi.ingsw.cg7.model.Player)
     */
    @Override
    public boolean validationMove(Player player) {
        if (this.type == StartedSectorType.SAFE)
            return true;
        return false;
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.model.Sector#controlDrawCard(it.polimi.ingsw.cg7.model.Game, it.polimi.ingsw.cg7.controller.PlayerStateHandler, it.polimi.ingsw.cg7.model.Player)
     */
    @Override
    public flagResponseType controlDrawCard(Game game,
            PlayerStateHandler playerStateHandler, Player player) {
        return flagResponseType.NOTDRAW;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public StartedSectorType getType() {
        return this.type;
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.model.Sector#canDraw()
     */
    @Override
    public boolean canDraw() {
        return false;
    }

    /* (non-Javadoc)
     * @see it.polimi.ingsw.cg7.model.Sector#canEscape()
     */
    @Override
    public boolean canEscape() {
        return false;
    }

}
