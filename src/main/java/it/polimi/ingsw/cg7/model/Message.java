package it.polimi.ingsw.cg7.model;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class chat Message.
 */
public class Message implements Serializable{
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
    
    /** The sender. */
    private Player sender;
    
    /** The text. */
    private String text;
    
    /** The date. */
    private Date date;
    
    /**
     * Instantiates a new message.
     *
     * @param sender the sender
     * @param text the text
     * @param date the date
     */
    public Message(Player sender,String text,Date date)
    {
        this.sender=sender;
        this.text=text;
        this.date=date;
    }
    
    /**
     * Gets text of message.
     *
     * @return String
     */
    
    public String getText()
    {
        return this.text;
    }
    
    /**
     * Gets player of message.
     *
     * @return Player
     */
    
    public Player getPlayer()
    {
        return this.sender;
    }
    
    /**
     * Gets date of message.
     *
     * @return Date
     */
    
    public Date getDate()
    {
        return this.date;
    }

}
