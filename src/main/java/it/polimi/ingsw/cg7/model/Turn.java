package it.polimi.ingsw.cg7.model;

import it.polimi.ingsw.cg7.Client.model.flagResponseType;

import java.io.Serializable;

/**
 * The Class Turn.
 */
public class Turn implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
    
    /** The current player. */
    private Player currentPlayer;
    
    /** The item deck. */
    private Deck itemDeck;
    
    /** The sector deck. */
    private Deck sectorDeck;
    
    /** The escape deck. */
    private Deck escapeDeck;
    
    /** The attack. */
    private boolean attack;
    
    /** The first movement. */
    private boolean firstMovement;
    
    /** The flag. */
    private flagResponseType flag;

    /**
     * Instantiates a new turn.
     *
     * @param currentPlayer the current player
     * @param itemDeck the item deck
     * @param sectorDeck the sector deck
     * @param escapeDeck the escape deck
     */
    public Turn(Player currentPlayer, Deck itemDeck, Deck sectorDeck,
            Deck escapeDeck) {
        this.currentPlayer = new Player(currentPlayer.getPosition(),
                currentPlayer.getUsername(), currentPlayer.getType(), this);
        this.currentPlayer.setDeck(itemDeck);
        incrementKill(this.currentPlayer, currentPlayer);
        this.itemDeck = new Deck(itemDeck.getCards());
        this.sectorDeck = new Deck(sectorDeck.getCards());
        this.escapeDeck = new Deck(escapeDeck.getCards());
        this.attack = false;
        this.firstMovement = false;
        this.flag = flagResponseType.FALSE;
    }

    /**
     * Gets a player of turn.
     *
     * @return Player
     */
    public flagResponseType getFlag() {
        return flag;
    }

    /**
     * Change flag.
     *
     * @param type the type
     */
    public void changeFlag(flagResponseType type) {
        this.flag = type;
    }

    /**
     * Gets the current player.
     *
     * @return the current player
     */
    public Player getCurrentPlayer() {
        return this.currentPlayer;
    }

    /**
     * Checks if is attack.
     *
     * @return true, if is attack
     */
    public boolean isAttack() {
        return this.attack;
    }

    /**
     * Checks if is first movement.
     *
     * @return true, if is first movement
     */
    public boolean isFirstMovement() {
        return this.firstMovement;
    }

    /**
     * Sets the attack.
     */
    public void setAttack() {
        this.attack = true;
    }

    /**
     * Sets the first movement.
     */
    public void setFirstMovement() {
        this.firstMovement = true;
    }

    /**
     * Return an item Deck that is possible draw.
     *
     * @return Deck item Deck
     */

    public Deck getItemDeck() {
        return this.itemDeck;
    }

    /**
     * Return an sector Deck that is possible draw.
     *
     * @return Deck sector Deck
     */

    public Deck getSectorDeck() {
        return this.sectorDeck;
    }

    /**
     * Return an escape Deck that is possible draw.
     *
     * @return Deck escape Deck
     */

    public Deck getEscapeDeck() {
        return this.escapeDeck;
    }

    /**
     * Increment a kill of turn's player.
     *
     * @param p1            Player
     * @param p2            Player
     */

    private void incrementKill(Player p1, Player p2) {
        int y = p2.getKillNumber();
        for (int x = 0; x < y; x++)
            p1.addKill();
    }

}
