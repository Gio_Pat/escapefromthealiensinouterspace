package it.polimi.ingsw.cg7.model;


public abstract class Card {
    protected String description;

    public Card() {
        description = new String();
    }

    public Card(String description) {
        this.description = description;
    }

    /**
     * Gets description of the Card
     * 
     * @return String
     */

    public String getDescription() {
        return description;
    }

}
