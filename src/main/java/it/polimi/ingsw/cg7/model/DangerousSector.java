package it.polimi.ingsw.cg7.model;

/**
 * The Class DangerousSector.
 */
public class DangerousSector extends Card {

    /** The type. */
    private Noise type;
    
    /** The item. */
    private boolean item;

    /**
     * Instantiates a new dangerous sector.
     */
    public DangerousSector() {
        super("SILENCE");
        type = Noise.SILENCE;
        item = false;
    }

    /**
     * Instantiates a new dangerous sector.
     *
     * @param type the type
     */
    public DangerousSector(Noise type) {
        super(type.toString());
        this.type = type;
        this.item = false;
    }

    /**
     * Instantiates a new dangerous sector.
     *
     * @param type the type
     * @param item the item
     */
    public DangerousSector(Noise type, boolean item) {
        super(type.toString());
        this.type = type;
        this.item = item;
    }

    /**
     * Instantiates a new dangerous sector.
     *
     * @param type the type
     * @param description the description
     */
    public DangerousSector(Noise type, String description) {
        super(description);
        this.type = type;
    }

    /**
     * Gets if the card forces the draft paper object.
     *
     * @return boolean
     */

    public boolean itemFounded() {
        return item;
    }

    /**
     * Gets <code>type</code>.
     *
     * @return Noise Object
     */

    public Noise getType() {
        return type;
    }

}
