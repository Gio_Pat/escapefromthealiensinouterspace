package it.polimi.ingsw.cg7.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * The Class Deck.
 */
public class Deck  {

    /** The cards. */
    private List<Card> cards;

    /**
     * Instantiates a new deck.
     */
    public Deck() {
        cards = new ArrayList<Card>();
    }

    /**
     * Instantiates a new deck.
     *
     * @param c the c
     */
    public Deck(List<Card> c) {
        cards = new ArrayList<Card>(c);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((cards == null) ? 0 : cards.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (getClass() != obj.getClass()))
            return false;
        Deck other = (Deck) obj;
        if (cards == null) {
            if (other.cards != null)
                return false;
        } else if (!cardEquals(other.cards))
            return false;
        return true;
    }

    /**
     * Control if the list of card passed is equals of the cards of the deck.
     *
     * @param othercards            List<Card>
     * @return true if the list if equals, false otherwise
     */

    public boolean cardEquals(List<Card> othercards) {
        for (Card c : this.cards)
            if (!othercards.contains(c))
                return false;
        return true;
    }

    /**
     * Return if the deck is empty.
     *
     * @return true is the deck is empty, false otherwise
     */

    public boolean isEmpty() {
        return this.cards.isEmpty();
    }

    /**
     * Draws a card from the " back " of the Deck.
     *
     * @return Card
     */

    public Card draw() {
        if (!cards.isEmpty())
            return cards.remove(0);
        return null;

    }

    /**
     * Inserts a card in the deck.
     *
     * @param c the c
     */

    public void insert(Card c) {
        cards.add(c);
    }

    /**
     * Shuffles the cards in the deck.
     */

    public void shuffle() {
        Collections.shuffle(cards);
    }

    /**
     * Gets number of cards in the deck.
     *
     * @return int
     */

    public int getCount() {
        return cards.size();
    }

    /**
     * Gets a list of cards.
     *
     * @return List<Card>
     */

    public List<Card> getCards() {
        return cards;
    }

    /**
     * Contains card.
     *
     * @param card the card
     * @return true, if successful
     */
    public boolean containsCard(Card card) {
        for (Card c : cards)
            if (c.getDescription().equals(card.getDescription()))
                return true;
        return false;
    }

    /**
     * Removed the card from deck.
     *
     * @param card            Card
     * @return true if the deck contains the card and remove it, false otherwise
     */

    public boolean removeCard(Card card) {
        Card temp = null;
        boolean find = false;
        for (Card c : cards)
            if (c.getDescription().equals(card.getDescription())) {
                find = true;
                temp = c;
            }
        if (find) {
            cards.remove(temp);
            return true;
        }
        return false;
    }

}