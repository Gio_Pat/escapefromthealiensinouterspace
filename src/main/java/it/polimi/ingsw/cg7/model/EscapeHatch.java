package it.polimi.ingsw.cg7.model;


/**
 * The Class EscapeHatch.
 */
public class EscapeHatch extends Card {

    /** The type. */
    private EscapeType type;

    /**
     * Instantiates a new escape hatch sector, by default, green.
     */
    public EscapeHatch()

    {
        super("EscapeHatch");
        type = EscapeType.GREEN;
    }

    /**
     * Instantiates a new escape hatch.
     *
     * @param type the type
     */
    public EscapeHatch(EscapeType type)

    {
        super("EscapeHatch");
        this.type = type;
    }

    /**
     * Instantiates a new escape hatch.
     *
     * @param type the type
     * @param description the description
     */
    public EscapeHatch(EscapeType type, String description)

    {
        super(description);
        this.type = type;
    }

    /**
     * Gets <code>type</code>.
     *
     * @return EscapeType Object
     */
    public EscapeType getType() {
        return type;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "type=" + type;
    }

}
