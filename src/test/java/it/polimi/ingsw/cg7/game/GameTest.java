package it.polimi.ingsw.cg7.game;

import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg7.Client.model.JoinRequest;
import it.polimi.ingsw.cg7.controller.LobbyManager;
import it.polimi.ingsw.cg7.controller.MoveValidation;
import it.polimi.ingsw.cg7.model.CharType;
import it.polimi.ingsw.cg7.model.Coordinate;
import it.polimi.ingsw.cg7.model.Game;
import it.polimi.ingsw.cg7.model.Map;
import it.polimi.ingsw.cg7.model.Player;

import java.util.Date;
import java.util.UUID;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class GameTest {

    Player p1, p2, p3, p4;
    Game game;
    Map map;
    MoveValidation move;

    @Before
    @Ignore
    public synchronized void init() {
        p1 = new Player("Giovanni");
        p2 = new Player("Debora");
        p3 = new Player("Paolo");
        p4 = new Player("Massimiliano");
        map = new Map();

        LobbyManager.joinLobby(new JoinRequest(p1, UUID.randomUUID(), map));
        LobbyManager.joinLobby(new JoinRequest(p2, UUID.randomUUID(), map));
        LobbyManager.joinLobby(new JoinRequest(p3, UUID.randomUUID(), map));
        LobbyManager.joinLobby(new JoinRequest(p4, UUID.randomUUID(), map));
        game = LobbyManager.getPlayerGame(p1);
        game.start(new Date());
        p1.setType(CharType.ALIEN);
        p2.setType(CharType.ALIEN);
        p3.setType(CharType.HUMAN);
        p4.setType(CharType.HUMAN);
        p1.setPosition(game.getMap().getAlienSector());
        p2.setPosition(game.getMap().getAlienSector());
        p4.setPosition(game.getMap().getHumanSector());
        p3.setPosition(game.getMap().getHumanSector());

    }

    @Test
    @Ignore
    public void alienWin() {
        move = new MoveValidation(p1, game);
        move.movementRequest(new Coordinate(9, 6));
        move.rumors(p1.getPosition());
        move.finishTurn();
        move = new MoveValidation(p2, game);
        move.movementRequest(new Coordinate(13, 6));
        move.rumors(p2.getPosition());
        move.finishTurn();
        move = new MoveValidation(p3, game);
        move.movementRequest(new Coordinate(12, 9));
        move.finishTurn();
        move = new MoveValidation(p4, game);
        move.movementRequest(new Coordinate(11, 9));
        move.finishTurn();

        move = new MoveValidation(p1, game);
        move.movementRequest(new Coordinate(7, 7));
        move.finishTurn();
        move = new MoveValidation(p2, game);
        move.movementRequest(new Coordinate(14, 8));
        move.rumors(p2.getPosition());

        move.finishTurn();
        move = new MoveValidation(p3, game);
        move.movementRequest(new Coordinate(13, 9));
        move.rumors(p3.getPosition());
        move.finishTurn();
        move = new MoveValidation(p4, game);
        move.movementRequest(new Coordinate(10, 10));
        move.rumors(p4.getPosition());
        move.finishTurn();

        move = new MoveValidation(p1, game);
        move.movementRequest(new Coordinate(7, 9));
        move.rumors(p1.getPosition());
        move.finishTurn();
        move = new MoveValidation(p2, game);
        move.movementRequest(new Coordinate(14, 9));
        move.finishTurn();
        move = new MoveValidation(p3, game);
        move.movementRequest(new Coordinate(14, 10));
        move.rumors(p3.getPosition());
        move.finishTurn();
        move = new MoveValidation(p4, game);
        int y = p4.getDeck().getCount();
        for (int i = 0; i < y; i++)
            p4.getDeck().draw();
        move.movementRequest(new Coordinate(10, 11));
        move.finishTurn();

        int z = p3.getDeck().getCount();
        for (int i = 0; i < z; i++)
            p3.getDeck().draw();
        move = new MoveValidation(p1, game);
        move.movementRequest(new Coordinate(6, 11));
        move.rumors(p1.getPosition());
        move.finishTurn();
        move = new MoveValidation(p2, game);
        move.movementRequest(new Coordinate(14, 10));
        move.rumors(p1.getPosition());
        int x = p3.getDeck().getCount();
        for (int i = 0; i < x; i++)
            p3.getDeck().draw();
        move.attackRequest();
        move.finishTurn();
        move = new MoveValidation(p4, game);
        move.movementRequest(new Coordinate(11, 11));
        move.finishTurn();

        move = new MoveValidation(p1, game);
        move.movementRequest(new Coordinate(4, 12));
        move.finishTurn();
        move = new MoveValidation(p4, game);
        move.movementRequest(new Coordinate(12, 12));
        move.rumors(p4.getPosition());
        move.finishTurn();

        move = new MoveValidation(p1, game);
        move.movementRequest(new Coordinate(6, 12));
        move.finishTurn();
        move = new MoveValidation(p4, game);
        move.movementRequest(new Coordinate(12, 13));
        move.rumors(p4.getPosition());
        move.finishTurn();
        // Cleanng the deck
        int j = p4.getDeck().getCount();
        for (int i = 0; i < j; i++)
            p4.getDeck().draw();
        move = new MoveValidation(p1, game);
        move.movementRequest(new Coordinate(8, 13));
        move.rumors(p1.getPosition());
        move.finishTurn();
        move = new MoveValidation(p4, game);
        move.movementRequest(new Coordinate(11, 13));
        move.rumors(p4.getPosition());
        move.finishTurn();
        int w = p4.getDeck().getCount();
        for (int i = 0; i < w; i++)
            p4.getDeck().draw();
        move = new MoveValidation(p1, game);
        move.movementRequest(new Coordinate(9, 13));
        move.rumors(p1.getPosition());
        move.finishTurn();
        move = new MoveValidation(p4, game);
        move.movementRequest(new Coordinate(10, 14));
        move.rumors(p4.getPosition());
        move.finishTurn();

        move = new MoveValidation(p1, game);
        move.movementRequest(new Coordinate(10, 14));
        move.rumors(p1.getPosition());
        x = p4.getDeck().getCount();
        for (int i = 0; i < x; i++)
            p4.getDeck().draw();
        move.attackRequest();
        assertTrue(game.isFinished());

    }
}
