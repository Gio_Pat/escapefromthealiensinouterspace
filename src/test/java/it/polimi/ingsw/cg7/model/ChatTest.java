package it.polimi.ingsw.cg7.model;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class ChatTest {

    Player p1, p2;
    List<Player> players;
    Sector sec1, sec2;
    String s1, s2, s3;
    Date d1, d2, d3;
    Message m1, m2, m3;
    long date;
    Game g;
    Chat c;

    @Before
    public void init() {
        p1 = new Player("player 1");
        p2 = new Player("Player 2");
        players = new ArrayList<Player>();
        sec1 = new PlayingSector(StartedSectorType.ALIENSTARTPOINT);
        sec2 = new PlayingSector(StartedSectorType.HUMANSTARTPOINT);
        s1 = new String("s1 Test");
        s2 = new String("s2 Test");
        s3 = new String("s3 Test");
        date = System.currentTimeMillis();
        d1 = new Date(date);
        d2 = new Date(date);
        d3 = new Date(date);
        m1 = new Message(p1, s1, d1);
        m2 = new Message(p2, s2, d2);
        m3 = new Message(p1, s3, d3);
    }

    @Test
    public void MessTest() {
        assertEquals(m1.getText(), "s1 Test");
        assertEquals(m1.getDate(), d1);
        assertEquals(m1.getPlayer(), p1);
    }

    @Test
    public void ChatMessTest() {
        c = new Chat();
        c.setMessage(null, m1);
        c.setMessage(null, m2);
        c.setMessage(null, m3);
        assertEquals(m3, c.getLastMessage());
    }

}
