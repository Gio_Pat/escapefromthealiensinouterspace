package it.polimi.ingsw.cg7.model;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;

public class MapTest {
    Map mapTest;
    List<Sector> testSectors;
    List<Sector> unmarshalledMapSectors;

    @Before
    public void init() {
        testSectors = new ArrayList<Sector>();
        unmarshalledMapSectors = new ArrayList<Sector>();
        Sector workingSector;
        mapTest = new Map();
        workingSector = new Dangerous();
        workingSector.setCoord(new Coordinate(17, 1));
        testSectors.add(workingSector);
        unmarshalledMapSectors.add(mapTest.getSector(new Coordinate(17, 1)));
        workingSector = new PlayingSector(StartedSectorType.ALIENSTARTPOINT);
        workingSector.setCoord(new Coordinate(22, 14));
        testSectors.add(workingSector);
        unmarshalledMapSectors.add(mapTest.getSector(new Coordinate(22, 14)));
        workingSector = new PlayingSector(StartedSectorType.HUMANSTARTPOINT);
        workingSector.setCoord(new Coordinate(4, 10));
        testSectors.add(workingSector);
        unmarshalledMapSectors.add(mapTest.getSector(new Coordinate(4, 10)));
        workingSector = new PlayingSector(StartedSectorType.SAFE);
        workingSector.setCoord(new Coordinate(2, 2));
        testSectors.add(workingSector);
        unmarshalledMapSectors.add(mapTest.getSector(new Coordinate(2, 2)));
        workingSector = new Hatch();
        workingSector.setCoord(new Coordinate(16, 1));
        testSectors.add(workingSector);
        unmarshalledMapSectors.add(mapTest.getSector(new Coordinate(16, 1)));
    }

    // @Test
    // public void unmarshalTest() {
    // // The test doesn't check if the two Hatches are equals because of the
    // // different incremental Number
    // for (int i = 0; i < 4; i++) {
    // assertTrue(testSectors.get(i).equals(unmarshalledMapSectors.get(i)));
    // }
    // // Now we will check that the Hatches are in the same position
    // assertTrue(testSectors.get(4).getCoordinates()
    // .equals(unmarshalledMapSectors.get(4).getCoordinates()));
    // // Now we will check the null pointer on a "Not-existing" Sector in
    // // position 10/7
    // assertEquals(mapTest.getSector(new Coordinate(10, 7)), null);
    // }
}
