package it.polimi.ingsw.cg7.model;

import static org.junit.Assert.*;
import it.polimi.ingsw.cg7.model.Card;
import it.polimi.ingsw.cg7.model.CharType;
import it.polimi.ingsw.cg7.model.Character;
import it.polimi.ingsw.cg7.model.DangerousSector;
import it.polimi.ingsw.cg7.model.Deck;
import it.polimi.ingsw.cg7.model.EscapeHatch;
import it.polimi.ingsw.cg7.model.EscapeType;
import it.polimi.ingsw.cg7.model.Noise;
import it.polimi.ingsw.cg7.model.Spotlight;
import it.polimi.ingsw.cg7.model.Teleport;

import java.util.*;

import org.junit.Before;
import org.junit.Test;

public class DeckTest {
    
    List<Card> Test;
    Deck DeckCard;
    Deck DeckCardVoid;
    Card Char = new Character(CharType.ALIEN,"Char");
    Card DangerousSector = new DangerousSector(Noise.ANYSECTOR,"Dangerous");
    Card Teleport = new Teleport("Teleport");
    Card EscapeHatch = new EscapeHatch(EscapeType.GREEN,"Escape");
    
    @Before
    public void init() {
        Test = new ArrayList<Card>();
        Test.add(Char);
        Test.add(DangerousSector);
        Test.add(Teleport);
        Test.add(EscapeHatch);
        DeckCard = new Deck(Test); 
        DeckCardVoid = new Deck();
    }
    
    @Test
    public void VoidTest()
    {
        assertEquals(DeckCardVoid.draw(),null);
    }
    
    @Test
    public void DrawTest()
    {
        Card c = DeckCard.draw();
        assertEquals(c,Char);
        c = DeckCard.draw();
        assertEquals(c,DangerousSector);
    }
    
    @Test
    public void shuffleTest(){
        DeckCard.shuffle();
        List<Card> c = new ArrayList<Card>();
        int count=DeckCard.getCount();
        for (int x=0;x<count;x++)
        {
            c.add(DeckCard.draw());
        }
        assertTrue(controllEquals(c,Test));
    }
    
    public boolean controllEquals(List<Card> c,List<Card> card)
    {
        if(!(c.size()==card.size()))
            return false;
        int count=0;
        for(Card removeC : c)
            for(Card currentCard : card)
                if(currentCard.equals(removeC))
                    count++;
        if(!(count==c.size()))
            return false;
        return true;
    }
    
    @Test
    public void InsertTest()
    {
        Card Test = new Spotlight("Spotlight");
        DeckCard.insert(Test);
        Card c = DeckCard.draw();
        c = DeckCard.draw();
        c = DeckCard.draw();
        c = DeckCard.draw();
        c = DeckCard.draw();
        assertEquals(c,Test);      
    }
    
    @Test
    public void countTest()
    {
        int x = DeckCard.getCount();
        assertEquals(4,x);
    }
}
