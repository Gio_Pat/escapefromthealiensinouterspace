package it.polimi.ingsw.cg7.model;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CardTest {

    Card EscapeVoid;
    Card CharacterVoid;
    Card DangerousVoid;
    Card Escape;
    Card Character;
    Card Dangerous;
    Card Description;

    @Before
    public void init() {
        EscapeVoid = new EscapeHatch();
        CharacterVoid = new Character();
        DangerousVoid = new DangerousSector();
        Escape = new EscapeHatch(EscapeType.RED);
        Character = new Character(CharType.ALIEN);
        Dangerous = new DangerousSector(Noise.ANYSECTOR);
        Description = new EscapeHatch(EscapeType.RED, "Test Description");
    }

    @Test
    public void returnDescription() {
        String description = Description.getDescription();
        assertEquals(description, "Test Description");
    }

    @Test
    public void returnTypeEscapeVoid() {
        EscapeType type = ((EscapeHatch) EscapeVoid).getType();
        assertEquals(type, EscapeType.GREEN);
    }

    @Test
    public void returnTypeEscape() {
        EscapeType type = ((EscapeHatch) Escape).getType();
        assertEquals(type, EscapeType.RED);
    }

    @Test
    public void returnTypeCharacterVoid() {
        CharType type = ((Character) CharacterVoid).getType();
        assertEquals(type, CharType.HUMAN);
    }

    @Test
    public void returnTypeCharacter() {
        CharType type = ((Character) Character).getType();
        assertEquals(type, CharType.ALIEN);
    }

    @Test
    public void returnTypeDangerousVoid() {
        Noise type = ((DangerousSector) DangerousVoid).getType();
        assertEquals(type, Noise.SILENCE);
    }

    @Test
    public void returnTypeDngerous() {
        Noise type = ((DangerousSector) Dangerous).getType();
        assertEquals(type, Noise.ANYSECTOR);
    }

}
