package it.polimi.ingsw.cg7.model;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class PlayerTest {

    Coordinate coord = new Coordinate(3, 6);
    List<Sector> listTest = new ArrayList<Sector>();
    List<Player> playerTest = new ArrayList<Player>();
    Sector secTest = new PlayingSector(StartedSectorType.SAFE, coord, listTest,
            playerTest);
    Player PlayerVoid;
    Player PlayerAll;

    @Before
    public void init() {
        PlayerVoid = new Player("pVoid");
        PlayerAll = new Player(secTest, "Test", CharType.HUMAN);
    }

    // @Test
    // public void sectorTest() {
    // Sector sec = PlayerAll.getPosition();
    // assertEquals(sec, secTest);
    // sec = PlayerVoid.getPosition();
    // assertTrue(sec.equals(new PlayingSector()));
    // Coordinate c = new Coordinate(6, 5);
    // Sector secTest2 = new Hatch(3, false, c, listTest, playerTest);
    // PlayerVoid.setPosition(secTest2);
    // assertTrue(secTest2.equals(PlayerVoid.getPosition()));
    // }

    @Test
    public void getsetTypeTest() {
        PlayingState p = PlayerAll.getState();
        assertEquals(p, PlayingState.PLAYING);
        PlayerAll.setState(PlayingState.WINNER);
        p = PlayerAll.getState();
        assertEquals(p, PlayingState.WINNER);
    }

    @Test
    public void getUserTest() {
        String sAll = PlayerAll.getUsername();
        assertEquals(sAll, "Test");
    }

    @Test
    public void addKillTest() {
        PlayerAll.addKill();
        int x = PlayerAll.getKillNumber();
        assertEquals(x, 1);
    }

}
