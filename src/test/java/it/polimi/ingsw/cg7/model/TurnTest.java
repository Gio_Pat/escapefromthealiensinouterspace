package it.polimi.ingsw.cg7.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class TurnTest {

    Turn tAll;
    Player p, p2;
    Deck item, sector, escape, escapeTest;
    Card c1, c2, c3, c4;
    List<Card> cards, cardsTest;

    @Before
    public void init() {
        c1 = new EscapeHatch(EscapeType.GREEN);
        c2 = new EscapeHatch(EscapeType.RED);
        c3 = new EscapeHatch(EscapeType.GREEN);
        c4 = new EscapeHatch(EscapeType.RED);
        cards = new ArrayList<Card>();
        cardsTest = new ArrayList<Card>();
        cards.add(c1);
        cards.add(c2);
        cards.add(c3);
        cards.add(c4);
        cardsTest.add(c1);
        cardsTest.add(c2);
        p2 = new Player("p2");
        p = new Player("p");
        item = new Deck();
        sector = new Deck();
        escape = new Deck(cards);
        escapeTest = new Deck(cardsTest);
        tAll = new Turn(p, item, sector, escape);
    }

    @Test
    public void createTurn() {
        assertEquals(tAll.getCurrentPlayer().getUsername(), p.getUsername());
        assertTrue(tAll.getEscapeDeck().equals(escape));
        assertTrue(tAll.getItemDeck().equals(item));
        assertTrue(tAll.getSectorDeck().equals(sector));
        assertFalse(tAll.getEscapeDeck().equals(escapeTest));
    }

}
