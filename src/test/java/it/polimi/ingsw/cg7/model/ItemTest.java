package it.polimi.ingsw.cg7.model;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ItemTest {

    Item ItemTest;
    Attack AttackTest;
    Adrenaline AdrenalineTest;
    Defense DefenceTest;
    Sedatives SedativesTest;
    Spotlight SpotlightTest;
    Teleport TeleportTest;
    Item ItemTestVoid;
    Attack AttackTestVoid;
    Adrenaline AdrenalineTestVoid;
    Defense DefenceTestVoid;
    Sedatives SedativesTestVoid;
    Spotlight SpotlightTestVoid;
    Teleport TeleportTestVoid;

    @Before
    public void init() {
        ItemTest = new Attack("Item Test");
        AttackTest = new Attack("Attack Test");
        AdrenalineTest = new Adrenaline("Adrenaline Test");
        DefenceTest = new Defense("Defence Test");
        SedativesTest = new Sedatives("Sedatives Test");
        SpotlightTest = new Spotlight("Spotlight Test");
        TeleportTest = new Teleport("Teleport Test");
        ItemTestVoid = new Attack();
        AttackTestVoid = new Attack();
        AdrenalineTestVoid = new Adrenaline();
        DefenceTestVoid = new Defense();
        SedativesTestVoid = new Sedatives();
        SpotlightTestVoid = new Spotlight();
        TeleportTestVoid = new Teleport();
    }

    @Test
    public void teleportTest() {
        String description = TeleportTest.getDescription();
        assertEquals(description, "Teleport Test");
        description = TeleportTestVoid.getDescription();
        assertEquals(description, "Teleport");

    }

    @Test
    public void spotlightTest() {
        String description = SpotlightTest.getDescription();
        assertEquals(description, "Spotlight Test");
        description = SpotlightTestVoid.getDescription();
        assertEquals(description, "Spotlight");
    }

    @Test
    public void sedativesTest() {
        String description = SedativesTest.getDescription();
        assertEquals(description, "Sedatives Test");
        description = SedativesTestVoid.getDescription();
        assertEquals(description, "Sedatives");

    }

    @Test
    public void defenceTest() {
        String description = DefenceTest.getDescription();
        assertEquals(description, "Defence Test");
        description = DefenceTestVoid.getDescription();
        assertEquals(description, "Defense");
    }

    @Test
    public void adrenalineTest() {
        String description = AdrenalineTest.getDescription();
        assertEquals(description, "Adrenaline Test");
        description = AdrenalineTestVoid.getDescription();
        assertEquals(description, "Adrenaline");
    }

    @Test
    public void attackTest() {
        String description = AttackTest.getDescription();
        assertEquals(description, "Attack Test");
        description = AttackTestVoid.getDescription();
        assertEquals(description, "Attack");
    }

    @Test
    public void itemTest() {
        String description = ItemTest.getDescription();
        assertEquals(description, "Item Test");
        description = ItemTestVoid.getDescription();
        assertEquals(description, "Attack");
    }

}
