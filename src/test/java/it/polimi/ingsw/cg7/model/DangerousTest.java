package it.polimi.ingsw.cg7.model;

import static org.junit.Assert.*;
import it.polimi.ingsw.cg7.model.DangerousSector;
import it.polimi.ingsw.cg7.model.Noise;

import org.junit.Before;
import org.junit.Test;

public class DangerousTest {

	DangerousSector DangerousVoid;
	DangerousSector DangerousSilence;
	DangerousSector DangerousOne;
	DangerousSector DangerousAny;
	DangerousSector DangerousAll;

	@Before
	public void init() {
		DangerousVoid = new DangerousSector();
		DangerousSilence = new DangerousSector(Noise.SILENCE);
		DangerousOne = new DangerousSector(Noise.ONESECTOR);
		DangerousAny = new DangerousSector(Noise.ANYSECTOR);
	    DangerousAll = new DangerousSector(Noise.ANYSECTOR,"AnySector");
		
	}

	@Test
	public void returnTypeVoid() {
		Noise type = DangerousVoid.getType();
		assertEquals(type, Noise.SILENCE);
	}

	@Test
	public void returnTypeSilence() {
		Noise type = DangerousSilence.getType();
		assertEquals(type, Noise.SILENCE);
	}

	@Test
	public void returnTypeOne() {
		Noise type = DangerousOne.getType();
		assertEquals(type, Noise.ONESECTOR);
	}

	@Test
	public void returnTypeAny() {
		Noise type = DangerousAny.getType();
		assertEquals(type, Noise.ANYSECTOR);
	}

}
