package it.polimi.ingsw.cg7.model;

import static org.junit.Assert.*;
import it.polimi.ingsw.cg7.model.EscapeHatch;
import it.polimi.ingsw.cg7.model.EscapeType;

import org.junit.Before;
import org.junit.Test;

public class EscapeHatchTest {

	EscapeHatch EscapeVoid;
	EscapeHatch EscapeRed;
	EscapeHatch EscapeGreen;
	EscapeHatch EscapeAll;

	@Before
	public void init() {
		EscapeVoid = new EscapeHatch();
		EscapeRed = new EscapeHatch(EscapeType.RED);
		EscapeGreen = new EscapeHatch(EscapeType.GREEN);
		EscapeAll = new EscapeHatch(EscapeType.GREEN,"Green Escape");
	}

	@Test
	public void returnTypeVoid() {
		EscapeType type = EscapeVoid.getType();
		assertEquals(EscapeType.GREEN, type);
	}

	@Test
	public void returnTypeRed() {
		EscapeType type = EscapeRed.getType();
		assertEquals(EscapeType.RED, type);
	}

	@Test
	public void returnTypeGreen() {
		EscapeType type = EscapeGreen.getType();
		assertEquals(EscapeType.GREEN, type);
	}

}
