package it.polimi.ingsw.cg7.model;

import static org.junit.Assert.*;
import it.polimi.ingsw.cg7.model.CharType;
import it.polimi.ingsw.cg7.model.Character;

import org.junit.Before;
import org.junit.Test;

public class CharacterTest {

	Character CharacterVoid;
	Character CharacterHuman;
	Character CharacterAlien;
	Character CharacterAll;

	@Before
	public void init() {
		CharacterVoid = new Character();
		CharacterAll = new Character(CharType.ALIEN,"Alien Type");
		CharacterHuman = new Character(CharType.HUMAN);
		CharacterAlien = new Character(CharType.ALIEN);
	}

	@Test
	public void returnTypeVoid() {
		CharType type = CharacterVoid.getType();
		assertEquals(CharType.HUMAN, type);
	}

	@Test
	public void returnTypeHuman() {
		CharType type = CharacterHuman.getType();
		assertEquals(CharType.HUMAN, type);
	}

	@Test
	public void returnTypeAlien() {
		CharType type = CharacterAlien.getType();
		assertEquals(CharType.ALIEN, type);
	}

}
