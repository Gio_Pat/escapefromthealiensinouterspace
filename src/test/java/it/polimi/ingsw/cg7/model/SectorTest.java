package it.polimi.ingsw.cg7.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class SectorTest {

    Coordinate coord = new Coordinate();
    List<Sector> sec = new ArrayList<Sector>();
    List<Player> p = new ArrayList<Player>();
    Sector playingVoid;
    Sector playingType;
    Sector playingAll;
    Sector hatchVoid;
    Sector hatchLocked;
    Sector hatchAll;
    Sector hatchTest;
    Sector dangerousSector;
    SpecialSector special = new Dangerous();
    Sector TestHatch = new Hatch();
    Player p1 = new Player("TestPlayer1");
    Player p2 = new Player("TestPlayer2");
    Player p3 = new Player("TestPlayer3");

    @Before
    public void init() {
        playingVoid = new PlayingSector();
        playingType = new PlayingSector(StartedSectorType.ALIENSTARTPOINT);
        playingAll = new PlayingSector(StartedSectorType.HUMANSTARTPOINT,
                coord, sec, p);
        hatchVoid = new Hatch();
        hatchLocked = new Hatch(4, true);
        hatchAll = new Hatch(2, false, coord, sec, p);
        dangerousSector = new Dangerous();
    }

    @Test
    public void removeTest() {
        List<Player> playerListControl = new ArrayList<Player>();
        playerListControl.add(p1);
        playerListControl.add(p2);
        TestHatch.setPlayer(p1);
        TestHatch.setPlayer(p2);
        TestHatch.setPlayer(p3);
        TestHatch.removePlayers(playerListControl);
        List<Player> p = TestHatch.getPlayers();
        assertEquals(1, p.size());
    }

    @Test
    public void removeFailTest() {
        List<Player> playerListControl = new ArrayList<Player>();
        playerListControl.add(p1);
        playerListControl.add(p2);
        TestHatch.setPlayer(p2);
        TestHatch.setPlayer(p3);
        assertFalse(TestHatch.removePlayers(playerListControl));
    }

    @Test
    public void sectorVarTest() {
        Coordinate c = new Coordinate();
        List<Sector> s = new ArrayList<Sector>();
        List<Player> p = new ArrayList<Player>();
        assertTrue(dangerousSector.getCoordinates().equals(c));
        assertTrue(dangerousSector.getNearSector().equals(s));
        assertTrue(dangerousSector.getPlayers().equals(p));
        Player pl = new Player(dangerousSector, "Test", CharType.ALIEN);
        p.add(pl);
        dangerousSector.setPlayer(pl);
        for (Player player : p)
            assertTrue(dangerousSector.getPlayers().contains(player));
        c = new Coordinate(6, 5);
        dangerousSector = new Dangerous(c, s, p);
        Coordinate x = dangerousSector.getCoordinates();
        assertEquals(c, x);

    }

    @Test
    public void number() {
        int x = ((Hatch) hatchAll).getNumber();
        assertEquals(2, x);
    }

    @Test
    public void lockedTest() {
        assertTrue(((Hatch) hatchLocked).isLocked());
        assertTrue(!((Hatch) hatchAll).isLocked());
        ((Hatch) hatchAll).lock();
        ((Hatch) hatchLocked).lock();
        assertTrue(((Hatch) hatchAll).isLocked());
        assertTrue(((Hatch) hatchLocked).isLocked());
    }

    @Test
    public void typeTest() {
        PlayingSector p = (PlayingSector) playingVoid;
        assertEquals(p.getType(), StartedSectorType.SAFE);
        p = (PlayingSector) playingType;
        assertEquals(p.getType(), StartedSectorType.ALIENSTARTPOINT);
        p = (PlayingSector) playingAll;
        assertEquals(p.getType(), StartedSectorType.HUMANSTARTPOINT);

    }

}
