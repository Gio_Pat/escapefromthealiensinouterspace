package it.polimi.ingsw.cg7.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class GameModelTest {

    Player p1, p2, p3;
    long date;
    Date d;
    Map map;
    List<Player> players;
    Message m1, m2;
    Game g;

    @Before
    public void init() {
        p1 = new Player("player1");
        p2 = new Player("player2");
        p3 = new Player("player3");
        date = System.currentTimeMillis();
        d = new Date(date);
        map = new Map();
        g = new Game(p1, map);
        g.addPlayer(p2);
        g.start(new Date());
    }

    @Test
    public void pauseTest() {
        assertFalse(g.isPaused());
        g.changePaused();
        assertTrue(g.isPaused());
    }

    @Test
    public void roundTurnTest() {
        Player pTest = g.getCurrentTurn().getCurrentPlayer();
        assertEquals(g.numberRound(), 0);
        g.changeTurn();
        assertEquals(g.numberRound(), 0);
        g.changeTurn();
        assertEquals(g.numberRound(), 1);
        assertEquals(g.getCurrentTurn().getCurrentPlayer().getUsername(),
                pTest.getUsername());
    }

    @Test
    @Ignore
    public void messageTest() {
        g.addMessage(m1);
        g.addMessage(m2);
        assertEquals(g.getLastMessage(), m2);
    }

    @Test
    public void startingGameTest() {
        g.addPlayer(p2);
        g.start(new Date());
        assertFalse(g.getItemDeck() == null);

    }

}
