package it.polimi.ingsw.cg7.model;

import static org.junit.Assert.*;
import it.polimi.ingsw.cg7.model.Coordinate;

import org.junit.Before;
import org.junit.Test;

public class CoordinateTest {
    
    Coordinate c;
    
    @Before
    public void init()
    {
        c = new Coordinate();
    }
    
    @Test
    public void setgetTest()
    {
        c.setxLett(3);
        c.setY(2);
        assertEquals(3,c.getxLett());
        assertEquals(2,c.getY());
    }
}
