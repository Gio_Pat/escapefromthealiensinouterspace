package it.polimi.ingsw.cg7.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import it.polimi.ingsw.cg7.Client.model.JoinRequest;
import it.polimi.ingsw.cg7.Client.model.ViewMap;
import it.polimi.ingsw.cg7.model.Game;
import it.polimi.ingsw.cg7.model.Map;
import it.polimi.ingsw.cg7.model.Player;

import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

public class LobbyManagerTest {
    Map galilei = new Map("Galvani");
    Map prova = new Map("Fermi");
    Game g1, g2, g3;
    Player playerTest1 = new Player("Giovanni");
    Player playerTest2 = new Player("Paolo");
    Player playerTest3 = new Player("Debora");
    UUID gioUUID = UUID.randomUUID();
    UUID paoloUUID = UUID.randomUUID();
    UUID debUUID = UUID.randomUUID();
    // I expect Giovanni and Paolo in the same lobby, Debora in an other lobby
    JoinRequest gioJoinRequest = new JoinRequest(playerTest1, gioUUID, new ViewMap("Fermi"));
    JoinRequest paoloJoinRequest = new JoinRequest(playerTest2, paoloUUID,
            new ViewMap("Fermi"));
    JoinRequest debJoinRequest = new JoinRequest(playerTest3, debUUID, new ViewMap("Galvani"));

    @Before
    public void initTest() {
        LobbyManager.joinLobby(gioJoinRequest);
        LobbyManager.joinLobby(paoloJoinRequest);
        LobbyManager.joinLobby(debJoinRequest);
        g1 = LobbyManager.getPlayerGame(playerTest1);
        g2 = LobbyManager.getPlayerGame(playerTest2);
        g3 = LobbyManager.getPlayerGame(playerTest3);
    }

    @Test
    public void checkRequestsResults() {
        assertEquals(g1, g2);
        assertFalse(g3.equals(g2));
        LobbyManager.joinLobby(gioJoinRequest);
        assertEquals(g1, g2);
        assertEquals(g1.getPlayers().size(), 2);
        assertEquals(g3.getPlayers().size(), 1);
    }
}
