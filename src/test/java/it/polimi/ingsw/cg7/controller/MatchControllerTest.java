package it.polimi.ingsw.cg7.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg7.model.Card;
import it.polimi.ingsw.cg7.model.CharType;
import it.polimi.ingsw.cg7.model.Defense;
import it.polimi.ingsw.cg7.model.Game;
import it.polimi.ingsw.cg7.model.Map;
import it.polimi.ingsw.cg7.model.Player;
import it.polimi.ingsw.cg7.model.PlayingSector;
import it.polimi.ingsw.cg7.model.PlayingState;
import it.polimi.ingsw.cg7.model.Sector;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class MatchControllerTest {

    Player p1, p2, p3, p4;
    PlayerStateHandler psh;
    MatchController mc;
    Card c = new Defense();
    Game g;
    List<Player> players, notPlayers;
    Map map;
    Sector sector;

    @Before
    public void init() {
        map = new Map();
        sector = new PlayingSector();
        p1 = new Player("p1");
        p2 = new Player("p2");
        p3 = new Player("p3");
        p4 = new Player("p4");
        players = new ArrayList<Player>();
        notPlayers = new ArrayList<Player>();
        players.add(p1);
        players.add(p2);
        players.add(p3);
        players.add(p4);
        p2.getDeck().insert(c);
        g = new Game(players, new Date(), map);
        psh = new PlayerStateHandler(g) {
            @Override
            public void finishGameControl() {
                if ((alienWinner()) || (g.numberRound() > 39)
                        || (humanWinner()) || (onlyHumansRemain())) {
                    if (g.numberRound() > 39)
                        for (Player p : g.getPlayers())
                            if (p.getState() == PlayingState.PLAYING)
                                p.setState(PlayingState.LOSER);
                    g.finishGame();
                }
            }
        };
        p1.setPosition(sector);
        p2.setPosition(sector);
        p3.setPosition(sector);
        p4.setPosition(sector);
        p1.setType(CharType.ALIEN);
        p2.setType(CharType.HUMAN);
        p3.setType(CharType.ALIEN);
        p4.setType(CharType.HUMAN);
        mc = new MatchController(psh);
    }

    @Test
    public void matchAndPlayerStateTest() {
        List<Player> notPlayingPlayer = new ArrayList<Player>();
        notPlayingPlayer.add(p3);
        notPlayingPlayer.add(p4);
        mc.kill(p1);
        for (Player p : notPlayingPlayer) {
            assertTrue(psh.getNotPlayingPlayers().contains(p));
        }
        assertTrue(p2.getDeck().getCards().isEmpty());
        psh.updateState();
        assertFalse(g.isFinished());
        mc.kill(p1);
        assertEquals(p1.getKillNumber(), 3);
        assertEquals(p2.getKillNumber(), 0);
        psh.updateState();
        assertTrue(g.isFinished());
    }
}