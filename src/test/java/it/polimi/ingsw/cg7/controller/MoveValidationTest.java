package it.polimi.ingsw.cg7.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg7.Client.model.NetResponseMessage;
import it.polimi.ingsw.cg7.Client.model.NetTypeRequest;
import it.polimi.ingsw.cg7.Client.model.ViewMap;
import it.polimi.ingsw.cg7.Client.model.flagResponseType;
import it.polimi.ingsw.cg7.model.CharType;
import it.polimi.ingsw.cg7.model.Coordinate;
import it.polimi.ingsw.cg7.model.Game;
import it.polimi.ingsw.cg7.model.Map;
import it.polimi.ingsw.cg7.model.Player;
import it.polimi.ingsw.cg7.model.PlayingState;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class MoveValidationTest {

    MoveValidation move;
    Player p1, p2;
    PlayerStateHandler psh;
    Game game;
    ViewMap map;
    List<Player> players;

    @Before
    public void init() {
        p2 = new Player("Giovanni");
        p1 = new Player("Debora");
        game = new Game(p2, new Map("Galilei"));
        game.addPlayer(p1);
        game.start(new Date());
        psh = new PlayerStateHandler(game) {
            @Override
            public void finishGameControl() {
                if ((alienWinner()) || (game.numberRound() > 39)
                        || (humanWinner()) || (onlyHumansRemain())) {
                    if (game.numberRound() > 39)
                        for (Player p : game.getPlayers())
                            if (p.getState() == PlayingState.PLAYING)
                                p.setState(PlayingState.LOSER);
                    game.finishGame();
                }
            }
        };
        p1.setType(CharType.ALIEN);
        p2.setType(CharType.HUMAN);
        p1.setPosition(game.getMap().getAlienSector());
        p2.setPosition(game.getMap().getHumanSector());
    }

    @Test
    public void firstAttackTest() {
        move = new MoveValidation(p1, game);
        assertTrue(move.attackRequest().getDescription()
                .equals("Movement not executed yet"));
    }

    @Test
    public void firstMoveTest() {
        move = new MoveValidation(p1, game);
        NetResponseMessage s = move.movementRequest(new Coordinate(13, 6));
        assertTrue(s.getDescription().equals("Movement executed"));
    }

    @Test
    public void twoMovementTest() {
        move = new MoveValidation(p1, game);
        move.movementRequest(new Coordinate(12, 5));
        NetResponseMessage s = move.movementRequest(new Coordinate(14, 8));
        assertTrue(s.getDescription().equals("Movement already executed"));
    }

    @Test
    public void moveAttackTest() {
        move = new MoveValidation(p1, game);
        move.movementRequest(new Coordinate(12, 5));
        move = new MoveValidation(p1, game);
        assertTrue(move.attackRequest().getDescription()
                .equals("Attack executed"));
    }

    @Test
    public void moveAndTwoAttackTest() {
        move = new MoveValidation(p1, game);
        move.movementRequest(new Coordinate(12, 5));
        move.attackRequest();
        assertTrue(move.attackRequest().getDescription()
                .equals("Attack already executed"));
    }

    @Test
    public void passTurn() {
        move = new MoveValidation(p1, game);
        move.movementRequest(new Coordinate(12, 5));
        move = new MoveValidation(p1, game) {
            @Override
            public NetResponseMessage finishTurn() {
                NetResponseMessage msg = new NetResponseMessage(
                        NetTypeRequest.FINISHTURN);
                if (p1.getDeck().getCards().size() == 4) {
                    msg.finishTurnRequest("You must discard an item card",
                            false);
                    return msg;
                }
                if (!game.getCurrentTurn().isFirstMovement()) {
                    msg.finishTurnRequest("Movement not executed yet", false);
                    return msg;
                }
                if (game.getCurrentTurn().getFlag() == flagResponseType.FALSERUMOR) {
                    msg.finishTurnRequest("You have to say one sector", false);
                    return msg;
                }
                game.changeTurn();
                msg.finishTurnRequest(
                        "Turn change, you have finished your turn", true);
                return msg;
            }

        };
        move.finishTurn();
        assertFalse(p1.equals(game.getCurrentTurn().getCurrentPlayer()));
    }

    @Test
    public void useItem() {
        p1.addBuff();
        move = new MoveValidation(p1, game) {
            @Override
            public NetResponseMessage movementRequest(Coordinate coord) {
                NetResponseMessage msg = new NetResponseMessage(
                        NetTypeRequest.MOVEMENT);
                if (game.getCurrentTurn().isFirstMovement()) {
                    msg.movementResponse("Movement already executed", false,
                            flagResponseType.FALSE);
                    return msg;
                }
                if (p1.getDeck().getCards().size() == 4) {
                    msg.movementResponse("You must discard an item card",
                            false, flagResponseType.FALSE);
                    return msg;
                }
                if (game.getCurrentTurn().getFlag() == flagResponseType.FALSERUMOR) {
                    msg.movementResponse("You have to say one sector", false,
                            flagResponseType.FALSERUMOR);
                    return msg;
                }

                movement = new MovementAction(playerStateHandler);
                game.getCurrentTurn().changeFlag(
                        movement.actionValidation(p1, coord));
                if (game.getCurrentTurn().getFlag() != flagResponseType.FALSE) {
                    game.getCurrentTurn().setFirstMovement();
                    this.playerStateHandler.updateState();
                    msg.movementResponse("Movement executed", true, game
                            .getCurrentTurn().getFlag());
                    if (game.getCurrentTurn().getFlag() == flagResponseType.TRUE) {
                        return msg;
                    }
                    return msg;
                }
                msg.movementResponse("You can't perform the movement", false,
                        flagResponseType.FALSE);
                return msg;
            }
        };
        NetResponseMessage s = move.movementRequest(new Coordinate(14, 6));
        assertTrue(s.getDescription().equals("Movement executed"));
    }

    @Test
    public void buffAttack() {
        p1.setPosition(game.getMap().getSector(new Coordinate(11, 5)));
        p2.setPosition(game.getMap().getSector(new Coordinate(12, 5)));
        move = new MoveValidation(p1, game) {
            @Override
            public NetResponseMessage attackRequest() {
                this.playerStateHandler = psh;
                NetResponseMessage msg = new NetResponseMessage(
                        NetTypeRequest.ATTACK);
                if (p1.getDeck().getCards().size() == 4)
                    msg.attackRequest("You must discard an item card", false);
                if (!game.getCurrentTurn().isFirstMovement())
                    msg.attackRequest("Movement not executed yet", false);
                if (game.getCurrentTurn().isAttack())
                    msg.attackRequest("Attack already executed", false);
                if (game.getCurrentTurn().getFlag() == flagResponseType.FALSERUMOR)
                    msg.attackRequest("You have to say one sector", false);
                move = new AttackAction();
                if (move.actionValidation(p1, playerStateHandler)) {
                    game.getCurrentTurn().setAttack();
                    this.playerStateHandler.updateState();
                    msg.attackRequest("Attack executed", true);
                    return msg;
                }
                msg.attackRequest("You can't perform the attack", false);
                return msg;
            }
        };
        move.movementRequest(new Coordinate(12, 5));
        move.attackRequest();
        psh.updateState();
        assertEquals(p1.getKillNumber(), 1);
        assertEquals(p2.getState(), PlayingState.LOSER);
        assertEquals(p1.getState(), PlayingState.WINNER);
        assertTrue(game.isFinished());
    }

    @Test
    public void useItemTest() {
        move = new MoveValidation(p1, game);
        move.movementRequest(new Coordinate(12, 5));
        // move.finishTurn();
        move = new MoveValidation(p2, game);

    }
}
