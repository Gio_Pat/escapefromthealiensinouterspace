package it.polimi.ingsw.cg7.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg7.model.Coordinate;
import it.polimi.ingsw.cg7.model.Deck;
import it.polimi.ingsw.cg7.model.Game;
import it.polimi.ingsw.cg7.model.Hatch;
import it.polimi.ingsw.cg7.model.Map;
import it.polimi.ingsw.cg7.model.Player;
import it.polimi.ingsw.cg7.model.Sector;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class DeckControllerTest {

    Map map;
    Player player, playerTest;
    Game game;
    DeckController deckController;
    DeckController deckControllerClient;
    PlayerStateHandler playerStateHandler;
    List<Player> players, notPlayers;
    Sector sector;
    Coordinate coord;

    @Before
    public void init() {
        map = new Map();
        coord = new Coordinate(1, 2);
        sector = new Hatch();
        sector.setCoord(coord);
        player = new Player("player 1");
        playerTest = new Player("player Test");
        players = new ArrayList<Player>();
        players.add(player);
        players.add(playerTest);
        game = new Game(player, map);
        playerStateHandler = new PlayerStateHandler(game);
        game.start(new Date());
        player.setPosition(sector);
        notPlayers = new ArrayList<Player>();
        deckController = new DeckController(game, playerStateHandler);
        deckControllerClient = new DeckController(game, playerStateHandler);

    }

    @Test
    public void deckEmptyTest() {
        assertFalse(deckController.isEscapeDeckEmpty());
        assertEquals(deckController.refreshDecks(), 0);
        Deck itemDeck = game.getItemDeck();
        for (int x = 0; x < 12; x++)
            itemDeck.draw();
        assertTrue(deckController.isItemDeckEmpty());
        assertEquals(deckController.refreshDecks(), 1);

    }
}
