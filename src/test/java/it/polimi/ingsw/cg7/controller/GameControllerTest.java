package it.polimi.ingsw.cg7.controller;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg7.model.Map;
import it.polimi.ingsw.cg7.model.Player;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class GameControllerTest {

    Player player, p2, p3;
    List<Player> players;
    Map map;
    GameController gameController;
    PlayerStateHandler playerStateHandler;

    @Before
    public void init() {

        player = new Player("player 1");
        p2 = new Player("player 2");
        p3 = new Player("player 3");
        map = new Map();
        players = new ArrayList<Player>();
        players.add(player);
        players.add(p2);
        players.add(p3);
        gameController = new GameController(player, map);

    }

    @Test
    public void gameControllerTest() {
        assertFalse(gameController.getGame().isStarted());
        assertFalse(gameController.getGame().isPaused());
        gameController.startGame();
        assertTrue(gameController.getGame().isStarted());

    }
}