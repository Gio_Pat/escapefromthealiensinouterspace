package it.polimi.ingsw.cg7.controller;

import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg7.Client.model.JoinRequest;
import it.polimi.ingsw.cg7.model.Game;
import it.polimi.ingsw.cg7.model.Map;
import it.polimi.ingsw.cg7.model.Player;

import java.util.UUID;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class TimerTest {

    Map galilei = new Map();
    Game g1;
    Player playerTest1;
    Player playerTest2;
    UUID gioUUID = UUID.randomUUID();
    UUID paoloUUID = UUID.randomUUID();
    JoinRequest gioJoinRequest;
    JoinRequest paoloJoinRequest;

    @Before
    @Ignore
    public void initTest() {
        playerTest1 = new Player("Giovanni");
        playerTest2 = new Player("Paolo");
        gioJoinRequest = new JoinRequest(playerTest1, gioUUID, galilei);
        paoloJoinRequest = new JoinRequest(playerTest2, paoloUUID, galilei);
    }

    @Test
    @Ignore
    public void startingTest() {
        LobbyManager.joinLobby(gioJoinRequest);
        LobbyManager.joinLobby(paoloJoinRequest);
        try {
            Thread.sleep(13000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        g1 = LobbyManager.getPlayerGame(playerTest1);
        assertTrue(g1.isStarted());
    }

}
