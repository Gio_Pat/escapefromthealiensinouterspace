package it.polimi.ingsw.cg7.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg7.model.CharType;
import it.polimi.ingsw.cg7.model.Game;
import it.polimi.ingsw.cg7.model.Map;
import it.polimi.ingsw.cg7.model.Player;
import it.polimi.ingsw.cg7.model.PlayingState;
import it.polimi.ingsw.cg7.model.Sector;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class ActionTest {

    Player playerTest, p1, p2, p3;
    Sector sectorTest;
    Game game;
    PlayerStateHandler playerStateHandler;
    Map map;
    MovementAction move;
    AttackItemAction attackBuff;
    AdrenalineItemAction adrenalineBuff;
    TeleportItemAction teleport;
    AttackAction attackAction;
    List<Player> aliens, humans;

    @Before
    public void init() {
        map = new Map();
        playerTest = new Player("playerTest");
        p1 = new Player("p1");
        p2 = new Player("p2");
        p3 = new Player("p3");
        game = new Game(playerTest, map);
        playerStateHandler = new PlayerStateHandler(game);
        game.addPlayer(p1);
        game.addPlayer(p2);
        game.addPlayer(p3);
        game.start(new Date());
        aliens = new ArrayList<Player>();
        humans = new ArrayList<Player>();
        for (Player p : game.getPlayers())
            if (p.getType() == CharType.ALIEN)
                aliens.add(p);
            else
                humans.add(p);
    }

    // Test INUTILISSIMO per ora.. dovrebbe dirti che non si è mosso ed è
    // rimasto nella sua posizione perchè la posizione
    // richiesta per muoversi è la stessa dove sta ora. Ma non ho modo di
    // controllare che NON HA fatto il movimento.
    @Test
    public void movementTest() {
        Sector playerSector = playerTest.getPosition();
        playerTest.setType(CharType.HUMAN);
        move = new MovementAction(playerStateHandler);
        move.actionValidation(playerTest, playerSector.getCoordinates());
        assertEquals(playerSector, playerTest.getPosition());
    }

    // prendo un settore a fianco a dove sta l'umano e ci va.
    @Test
    public void movement2Test() {
        Sector playerSector = playerTest.getPosition();
        playerTest.setType(CharType.HUMAN);
        List<Sector> nearSector = playerSector.getNearSector();
        sectorTest = nearSector.get(0);
        move = new MovementAction(playerStateHandler);
        move.actionValidation(playerTest, sectorTest.getCoordinates());
        assertFalse(playerSector.equals(playerTest.getPosition()));
    }

    @Test
    public void movement3Test() {
        Sector playerSector = playerTest.getPosition();
        playerTest.setType(CharType.HUMAN);
        List<Sector> nearSector = playerSector.getNearSector();
        List<Sector> sectors = nearSector.get(0).getNearSector();
        nearSector.add(playerSector);
        for (Sector s : nearSector)
            if (sectors.contains(s))
                sectors.remove(s);
        move = new MovementAction(playerStateHandler);
        move.actionValidation(playerTest, sectors.get(0).getCoordinates());
        assertTrue(playerSector.equals(playerTest.getPosition()));
    }

    @Test
    public void movement4Test() {
        Sector playerSector = playerTest.getPosition();
        playerTest.setType(CharType.ALIEN);
        List<Sector> nearSector = playerSector.getNearSector();
        List<Sector> sectors = nearSector.get(0).getNearSector();
        nearSector.add(playerSector);
        for (Sector s : nearSector)
            if (sectors.contains(s))
                sectors.remove(s);
        move = new MovementAction(playerStateHandler);
        move.actionValidation(playerTest, sectors.get(0).getCoordinates());
        assertEquals(sectors.get(0).getCoordinates().getxLett(), (playerTest
                .getPosition().getCoordinates().getxLett()));
        assertEquals(sectors.get(0).getCoordinates().getY(), (playerTest
                .getPosition().getCoordinates().getY()));
    }

    @Test
    public void movement5Test() {
        playerTest = humans.get(0);
        Sector playerSector = playerTest.getPosition();
        adrenalineBuff = new AdrenalineItemAction();
        adrenalineBuff.actionValidation(playerTest, playerStateHandler);
        List<Sector> nearSector = playerSector.getNearSector();
        List<Sector> sectors = nearSector.get(0).getNearSector();
        nearSector.add(playerSector);
        for (Sector s : nearSector)
            if (sectors.contains(s))
                sectors.remove(s);
        move = new MovementAction(playerStateHandler);
        move.actionValidation(playerTest, sectors.get(0).getCoordinates());
        assertTrue(sectors.get(0).equals(playerTest.getPosition()));
        teleport = new TeleportItemAction();
        teleport.actionValidation(playerTest, playerStateHandler);
        assertTrue(playerSector.equals(playerTest.getPosition()));

    }

    @Test
    public void AttackTest() {

        attackAction = new AttackAction();
        attackAction.actionValidation(p1, playerStateHandler);
        assertEquals(0, p1.getKillNumber());
    }

    @Test
    public void Attack2Test() {

        Sector sector;
        sectorTest = aliens.get(0).getPosition();
        sector = sectorTest.getNearSector().get(0);
        move = new MovementAction(playerStateHandler);
        move.actionValidation(aliens.get(0), sector.getCoordinates());
        move.actionValidation(aliens.get(1), sector.getCoordinates());
        attackAction = new AttackAction();
        attackAction.actionValidation(aliens.get(0), playerStateHandler);
        assertEquals(1, aliens.get(0).getKillNumber());
        assertEquals(aliens.get(1).getState(), PlayingState.LOSER);
    }

    @Test
    public void Attack3Test() {

        Sector sector;
        sectorTest = humans.get(0).getPosition();
        sector = sectorTest.getNearSector().get(0);
        move = new MovementAction(playerStateHandler);
        move.actionValidation(humans.get(0), sector.getCoordinates());
        move.actionValidation(humans.get(1), sector.getCoordinates());
        attackAction = new AttackAction();
        attackAction.actionValidation(humans.get(0), playerStateHandler);
        assertEquals(0, humans.get(0).getKillNumber());
        assertEquals(humans.get(1).getState(), PlayingState.PLAYING);
    }

    @Test
    public void Attack4Test() {

        Sector sector;
        attackBuff = new AttackItemAction();
        sectorTest = humans.get(0).getPosition();
        sector = sectorTest.getNearSector().get(0);
        move = new MovementAction(playerStateHandler);
        move.actionValidation(humans.get(0), sector.getCoordinates());
        move.actionValidation(humans.get(1), sector.getCoordinates());
        attackBuff.actionValidation(humans.get(0), playerStateHandler);
        attackAction = new AttackAction();
        attackAction.actionValidation(humans.get(0), playerStateHandler);
        assertEquals(humans.get(1).getState(), PlayingState.LOSER);
    }

    @Test
    public void AttackBuffTest() {

        attackBuff = new AttackItemAction();
        attackBuff.actionValidation(humans.get(0), playerStateHandler);
        attackAction = new AttackAction();
        attackAction.actionValidation(humans.get(0), playerStateHandler);
        assertEquals(0, humans.get(0).getKillNumber());
        assertEquals(humans.get(1).getState(), PlayingState.PLAYING);
    }

}
