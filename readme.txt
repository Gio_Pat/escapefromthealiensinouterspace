Readme v1.0 
  __________________________________
 /                                  \
 |  Patruno Giovanni & Rebai Debora |
 \__________________________________/


███████╗███████╗████████╗ █████╗ ██╗ ██████╗ ███████╗
██╔════╝██╔════╝╚══██╔══╝██╔══██╗██║██╔═══██╗██╔════╝
█████╗  █████╗     ██║   ███████║██║██║   ██║███████╗
██╔══╝  ██╔══╝     ██║   ██╔══██║██║██║   ██║╚════██║
███████╗██║        ██║   ██║  ██║██║╚██████╔╝███████║

Group cg_7

---------------------------
1 Starting a match
---------------------------
	1.1 To start a match you must run first the Host.java in package: it.polimi.ingsw.cg7.communication.network
	1.2 When the log advice you that the RMI and Socket Listener are ready you can run Guest.java in: it.polimi.ingsw.cg7.communication.guest
	1.3 When the client will start you will have to choose between CLI or GUI.
	1.4 The program will save locally a token (UUID object) for handling the reconnection, named: *username*myTokenEftAioS.tpe

------------------
2 Playing a match
------------------
	2.a Remember that you have 90s to play a move; after expiring this timer you'll have kicked out from the match!!
	2.b Playing with CLI 
			2.b.1 Remember that CLI is not case sensitive
			2.b.2 Playing with CLI is simple, you only have to read instructions!
			2.b.3 In every tourn remember to read all infos displayed (E.g. your position, your cards, etc...)
			2.b.4 You'll be able to insert x coordinate just by typing the letter of the sector; if you want you can insert the full coordinate address in the first X coordinate request: like M09
	2.c Playing with GUI
			2.c.1 You will start entering all required informations, in map selection you can see the name of the map by a tooltip;
			      after the last view (name request) you'll have to wait a match to start (10s timer expired or 8 player joined the same map), until this moment you'll see a loading window to find the match.
			2.c.2 Your play-view contains all infos required to play; In the Left-side you have the chat; in bottom the cards icon (with a tool-tip to remember the name of the card) and a blue botton to stop the music
			      Rightside you have a panel that shows you all player in the game with a generic icon (the green one) that you can change by clicking on this icon. During the match
		              it will help you to remember who is an alien, who is a human.
			      at the bottom of this panel you can see the current playing player; by passing pointer on one of the icon-player, the text at the bottom will update with infos of that specific player
			2.c.3 Sectors in light-blue are the sectors where your player can be moved.
			2.c.4 Clicking one time on a sector will underline that sector in red.
			2.c.5 Double-clicking a sector will try to perform a movement on that specific sector.
			2.c.6 all infos (Notifications) about game (Game finished, Drawing or attacking selection, Rumors, kills ecc...) will appear in several other windows that you'll be able to close just by clicking bottons. 
            2.c.7 closing the game just by pressing ALT-F4 Or by clicking the X in left-upper (Will be appear a confirm message)
 
----------
3 ADVICES
----------
    3.a Try also the cli game!